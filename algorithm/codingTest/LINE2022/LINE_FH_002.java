package codingTest.LINE2022;

import java.util.*;

public class LINE_FH_002 {

    public static void main(String[] args) {
        //최대 값 구하기.
        //파라메터 1
        String[] sentences = {"line in line", "LINE", "in lion"};
        int n = 5;

        //파라메터 2
//        String[] sentences = {"ABcD", "bdbc", "a", "Line neWs"};
//        int n = 7;

        /*
        문자열을 key, 문자열에 필요한 키를 리스트로 넣어서 해보면 어떨까?
        A ~ Z = 65 ~ 90 / a ~ z = 97 ~ 122
        문자열 보단 계산이 좀 더 빠른 아스키 값으로 하자.
         */
        List<Character> letterList;
        Map<String, Integer> sentenceScore = new HashMap<String, Integer>();
        Map<String, Boolean> upperCaseFlagMap = new HashMap<String, Boolean>();
        Map<String, List<Character>> sentenceAnalysis = new HashMap<String, List<Character>>();
        Map<String, List<Character>> sortedAnalysis = new LinkedHashMap<String, List<Character>>();

        // 문자열 탐색
        for(int i = 0; i < sentences.length; i++) {
            String[] element = sentences[i].split(" ");
            letterList = new ArrayList<Character>();
            upperCaseFlagMap.put(sentences[i], false);

            // 문자열 내 단어 탐색
            for(int j = 0; j < element.length; j++) {
                String word = element[j];
                //단어 탐색. '1'은 shift 키 여부.
                for(int k = 0; k < word.length(); k++) {
                    char ch = word.charAt(k);
                    if(!letterList.contains(ch)) {
                        letterList.add(ch);
                    }
                    if(ch >= 65 && ch <= 90) {
                        if(!upperCaseFlagMap.containsKey(sentences[i])) {
                            upperCaseFlagMap.put(sentences[i], true);
                        }
                    }
                }
            }

            sentenceAnalysis.put(sentences[i], letterList);
            sentenceScore.put(sentences[i], sentences[i].length());
        }

        /*
        생각을 해보자. 문자열이 필요한 키가 단 1개라도 없으면 점수는 없다.
        문자열의 길이가 긴 것이 점수가 젤 높다.
        문제는 히든 케이스를 고려해서 생각해야하는데, 적은 문자만으로 긴 문자열이 구성될 수도 있다는 거다.
        이게 가장 효율적으로 점수가 높은 문자열이다.
         */
//        List<Map.Entry<String, List<Character>>> entries = new ArrayList<>(sentenceAnalysis.entrySet());
//        Collections.sort(entries, (o1, o2) -> {
//            if(o1.getValue().size() < o2.getValue().size()) {
//                return -1;
//            } else if(o1.getValue().size() == o2.getValue().size()) {
//                return 0;
//            } else {
//                return 1;
//            }
//        });

        List<Map.Entry<String, Integer>> entries = new ArrayList<>(sentenceScore.entrySet());
        Collections.sort(entries, (o1, o2) -> {
            if(o1.getValue() < o2.getValue()) {
                return -1;
            } else if(o1.getValue() == o2.getValue()) {
                if(upperCaseFlagMap.get(o1.getKey()) == upperCaseFlagMap.get(o2.getKey())) {
                    return 0;
                } else {
                    if(upperCaseFlagMap.get(o1.getKey()) == true) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            } else {
                return 1;
            }
        });

        for(Map.Entry<String, Integer> entry : entries) {
            System.out.println("======================");
            System.out.println(entry.getValue());
            System.out.println(upperCaseFlagMap.get(entry.getKey()));
        }

        for(int i = 0; i < sentences.length; i++) {

        }
    }

}
