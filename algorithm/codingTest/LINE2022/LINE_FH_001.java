package codingTest.LINE2022;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LINE_FH_001 {

    public static void main(String[] args) {
        //최대 값 구하기.
        //파라메터 1
        String[] sentences = {"line in line", "LINE", "in lion"};
        int n = 5;

        //파라메터 2
//        String[] sentences = {"ABcD", "bdbc", "a", "Line neWs"};
//        int n = 7;

        Map<Character, Integer> charCount;
        Map<String, Integer> sentenceScore = new HashMap<String, Integer>();
        Map<String, Map<Character, Integer>> sentenceAnalysis = new HashMap<String, Map<Character, Integer>>();

        //sentences 탐색
        for(int i = 0; i < sentences.length; i++) {
            String[] element = sentences[i].split(" ");
            charCount = new HashMap<Character, Integer>();
            //문자열 탐색
            for(int j = 0; j < element.length; j++) {
                String word = element[j];
                //단어 탐색. '1'은 shift 키 여부
                for(int k = 0; k < word.length(); k++) {
                    char ch = word.charAt(k);
                    if(!charCount.containsKey(ch)) {
                        if(ch >= 65 && ch <= 90) {
                            charCount.put('1', 1);
                        }
                        charCount.put(ch, 1);
                    }
                }
            }
            sentenceAnalysis.put(sentences[i], charCount);
            sentenceScore.put(sentences[i], sentences[i].length());
        }

        // greedy로 해보자.
        List<Character> selectKeyCount = new ArrayList<>();
        for(int i = 0; i < sentences.length; i++) {

        }
    }

}
