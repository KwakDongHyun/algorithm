package codingTest.SKTICT2022;

public class SKTICT2022_002 {

    public static void main(String[] args) {
        // 시계방향 홀수
//        boolean clockwise = true;
//        int n = 5;

        // 시계방향 짝수
//        boolean clockwise = true;
//        int n = 6;

        // 반시계방향 홀수
//        boolean clockwise = false;
//        int n = 5;

        // 반시계방향 짝수
        boolean clockwise = false;
        int n = 6;

        /* 알고리즘을 설명할 때 배열이 사용되면 인덱싱 관련해서는 아래와 같이 설명하도록 하겠다.
         * 0   -> 1
         * n-1 -> n
         */
        int[][] answer = new int[n][n];
        int value = 1;
        if(clockwise) {
            /* 시계 방향일 때는 번호가 기본적으로 기입되는 방식과 흐름이 아래와 같다.
             * 소용돌이 최외곽의 경우에 아래와 같이 작동한다.
             * 상 // (1, 1) -> (1, n-1)
             * 우 // (1, n) -> (n-1, n)
             * 하 // (n, n) -> (n, 2)
             * 좌 // (n, 1) -> (2, 1)
             *
             * 여기서 외곽에서 소용돌이 내부로 갈수록 기입되는 칸의 수가 양끝에서 1개씩 줄어들어 총 -2만큼 줄어든다.
             * 즉, 상을 예로 들어서 설명하면 2번째 번호의 흐름은
             * 상 // (2, 2) -> (2, n-2)
             * 나머지들도 마찬가지로 동일한 방식으로 진행이 된다.
             * 그렇기 때문에 소용돌이 layer마다 배열을 순차적으로 같은 흐름대로 번호를 기입해야 하므로
             * 2중 for문을 반드시 써야한다.
             */
            if(n%2 == 1) { // n이 홀수
                /* 홀수일 때는 n/2까지는 위의 알고리즘대로 돌아가다가 마지막에 중앙 1개의 칸만 기입하면 끝난다.
                 * 짝수일 때는 (n/2 - 1)까지 알고리즘대로 돌아가다가 마지막에 중앙 4개의 칸만 기입하면 끝난다.
                 */
                for(int i = 1; i <= (n/2 + 1); i++) {

                    for(int j = i; j <= (n-i); j++) {
                        answer[i-1][j-1] = value;
                        answer[j-1][n-i] = value;
                        answer[n-i][n-j] = value;
                        answer[n-j][i-1] = value;
                        value++;
                    }

                    // 홀수일 때는 소용돌이 중심은 번호가 기입 안되서 추가해줌.
                    if(i == (n/2 + 1)) {
                        answer[i-1][i-1] = value;
                    }

                    System.out.println("=========================");
                    System.out.println("i : " + i + "번째");
                    System.out.print("[");
                    for(int k = 0; k < n; k++) {
                        System.out.print("[");
                        for(int l = 0; l < n; l++) {
                            System.out.print(answer[k][l]);
                            if(l != n-1) {
                                System.out.print(",");
                            }
                        }
                        System.out.print("]");
                        if(k != n-1) {
                            System.out.print(",");
                        }
                    }
                    System.out.println("]");
                }
            } else { // n이 짝수
                for(int i = 1; i <= n/2; i++) {

                    for(int j = i; j <= (n-i); j++) {
                        answer[i-1][j-1] = value;
                        answer[j-1][n-i] = value;
                        answer[n-i][n-j] = value;
                        answer[n-j][i-1] = value;
                        value++;
                    }

                    System.out.println("=========================");
                    System.out.println("i : " + i + "번째");
                    System.out.print("[");
                    for(int k = 0; k < n; k++) {
                        System.out.print("[");
                        for(int l = 0; l < n; l++) {
                            System.out.print(answer[k][l]);
                            if(l != n-1) {
                                System.out.print(",");
                            }
                        }
                        System.out.print("]");
                        if(k != n-1) {
                            System.out.print(",");
                        }
                    }
                    System.out.println("]");
                }
            }
        } else {
            /* 반시계 방향일 때는 시계방향과 반대 방향으로 데이터를 기입하면 된다.
             * 단, start point와 end point가 약간 다르니 주의하자.
             * 소용돌이 최외곽의 경우에 아래와 같이 작동한다.
             * 상 // (1, n) -> (1, 2)
             * 좌 // (1, 1) -> (n-1, 1)
             * 하 // (n, 1) -> (n, n-1)
             * 우 // (n, n) -> (2, n)
             *
             * 여기서 외곽에서 소용돌이 내부로 갈수록 기입되는 칸의 수가 양끝에서 1개씩 줄어들어 총 -2만큼 줄어든다.
             * 즉, 상을 예로 들어서 설명하면 2번째 번호의 흐름은
             * 상 // (2, n-1) -> (2, 3)
             * 나머지들도 마찬가지로 동일한 방식으로 진행이 된다.
             * 그렇기 때문에 소용돌이 layer마다 배열을 순차적으로 같은 흐름대로 번호를 기입해야 하므로
             * 2중 for문을 반드시 써야한다.
             */
            if(n%2 == 1) { // n이 홀수
                /* 홀수일 때는 n/2까지는 위의 알고리즘대로 돌아가다가 마지막에 중앙 1개의 칸만 기입하면 끝난다.
                 * 짝수일 때는 (n/2 - 1)까지 알고리즘대로 돌아가다가 마지막에 중앙 4개의 칸만 기입하면 끝난다.
                 */
                for(int i = 1; i <= (n/2 + 1); i++) {

                    for(int j = i; j <= (n-i); j++) {
                        answer[i-1][n-j] = value;
                        answer[j-1][i-1] = value;
                        answer[n-i][j-1] = value;
                        answer[n-j][n-i] = value;
                        value++;
                    }

                    // 홀수일 때는 소용돌이 중심은 번호가 기입 안되서 추가해줌.
                    if(i == (n/2 + 1)) {
                        answer[i-1][i-1] = value;
                    }

                    System.out.println("=========================");
                    System.out.println("i : " + i + "번째");
                    System.out.print("[");
                    for(int k = 0; k < n; k++) {
                        System.out.print("[");
                        for(int l = 0; l < n; l++) {
                            System.out.print(answer[k][l]);
                            if(l != n-1) {
                                System.out.print(",");
                            }
                        }
                        System.out.print("]");
                        if(k != n-1) {
                            System.out.print(",");
                        }
                    }
                    System.out.println("]");
                }
            } else { // n이 짝수
                for(int i = 1; i <= n/2; i++) {

                    for(int j = i; j <= (n-i); j++) {
                        answer[i-1][n-j] = value;
                        answer[j-1][i-1] = value;
                        answer[n-i][j-1] = value;
                        answer[n-j][n-i] = value;
                        value++;
                    }

                    System.out.println("=========================");
                    System.out.println("i : " + i + "번째");
                    System.out.print("[");
                    for(int k = 0; k < n; k++) {
                        System.out.print("[");
                        for(int l = 0; l < n; l++) {
                            System.out.print(answer[k][l]);
                            if(l != n-1) {
                                System.out.print(",");
                            }
                        }
                        System.out.print("]");
                        if(k != n-1) {
                            System.out.print(",");
                        }
                    }
                    System.out.println("]");
                }
            }
        }

        System.out.println("=========================");
        System.out.println("최종 결과");
        System.out.print("[");
        for(int i = 0; i < n; i++) {
            System.out.print("[");
            for(int j = 0; j < n; j++) {
                System.out.print(answer[i][j]);
                if(j != n-1) {
                    System.out.print(",");
                }
            }
            System.out.print("]");
            if(i != n-1) {
                System.out.print(",");
            }
        }
        System.out.print("]");

    }

}
