package codingTest.SKTICT2022;

public class SKTICT2022_003 {

    public static void main(String[] args) {

//        int width = 2;
//        int height = 2;
//        int[][] diagonals = {{1,1}, {2,2}};

        int width = 51;
        int height = 37;
        int[][] diagonals = {{17,19}};

        double answer = 0;
        double sum = 0;
        int[] diagonal;
        int coordinateX;
        int coordinateY;

        /*
         * 어차피 경로 선택은 브루트 포스에서도 봤듯이, DP 영역 문제에 가까우니 문제될 것은 없을듯?
         * i C j 형식으로 구성됨.
         */
        double[][] combinationArr = new double[width + height + 1][width + height + 1];
        for(int i = 1; i <= (width + height); i++) {
            for(int j = 0; j <= i; j++) {
                if(j == 0 || j == i) {
                    combinationArr[i][j] = 1;
                } else {
                    combinationArr[i][j] = combinationArr[i-1][j-1] + combinationArr[i-1][j];
                }
            }
        }
        System.out.println("combinationArr ==================");
        for(int i = 1; i <= (width + height); i++) {
            System.out.print(i + " = [ ");
            for(int j = 0; j <= i; j++) {
                if(combinationArr[i][j] != 0) {
                    System.out.print(combinationArr[i][j] + ", ");
                }
            }
            System.out.println(" ]");
        }

//        System.out.println("length : " + diagonals.length);
        for(int i = 0; i < diagonals.length; i++) {
            diagonal = diagonals[i];
            coordinateX = diagonal[0];
            coordinateY = diagonal[1];

            // (2,2)에서 1,1
            int bottomRow = coordinateX;
            int bottomCol = coordinateY - 1;
            int upperRow = width - coordinateX + 1;
            int upperCol = height - coordinateY;

            System.out.println("bottomRow : " + bottomRow);
            System.out.println("bottomCol : " + bottomCol);
            System.out.println("upperRow : " + upperRow);
            System.out.println("upperCol : " + upperCol);

            /* 최대한 제거하는 식으로 연산 수를 줄여봐야겠다.
             * - 갱신 -
             * 순열이나 팩토리얼로 풀게 아니라, 수학 공식을 보니까 조합이다 이거. 조합은 재귀로 풀면 되니까 이걸로 해보자.
             * 이게 왜 조합이냐면, n!/r! * (n-r)! = nCr이 되기 때문이다... 수학이 겁나 들어가네;
             *
             * - 추가 -
             * 조합을 재귀함수로 하려니까 변수 제한을 넘어서서 안된다; 이래서 내가 재귀를 싫어함.. 그냥 DP로 조합식 만드는게 빠르겄다..
             */
            double bottomCase;
            double upperCase;

//            System.out.println("bottomCase =====");
            if(bottomRow < bottomCol) {
                bottomCase = combinationArr[(bottomRow + bottomCol)][bottomRow];
            } else {
                bottomCase = combinationArr[(bottomRow + bottomCol)][bottomCol];
            }

//            System.out.println("upperCase =====");
            if(upperRow < upperCol) {
                upperCase = combinationArr[(upperRow + upperCol)][upperRow];
            } else {
                upperCase = combinationArr[(upperRow + upperCol)][upperCol];
            }

            System.out.println("running : " + i);
            System.out.println("bottomCase : " + bottomCase);
            System.out.println("upperCase : " + upperCase);

            double result = 2 * (bottomCase * upperCase);
//            result = upperCase / 10000019;
            System.out.println("result : " + result);

            answer += result;
//            sum += result % 10000019;
//            System.out.println("diagonal : " + diagonal);
        }

        System.out.println("answer : " + (answer % 10000019));
//        System.out.println("answer2 : " + sum);

    }

//    public static long combination(int n, int r) {
//        if(n == r || r == 0) {
//            return 1;
//        } else {
//            System.out.println(n + " C " + r + " = " + (n-1) + " C " + r + " + " + (n-1) + " C " + (r-1));
//            return combination(n-1, r) + combination(n-1, r-1);
//        }
//    }

}
