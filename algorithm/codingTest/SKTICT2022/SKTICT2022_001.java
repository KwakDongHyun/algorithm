package codingTest.SKTICT2022;

public class SKTICT2022_001 {

    public static void main(String[] args) {
//        int money = 4578;
        int money = 1999;
//        int[] costs = {1, 4, 99, 35, 50, 1000};
        int[] costs = {2, 11, 20, 100, 200, 600};
//        int[] coinPrice = {1, 5, 10, 50, 100, 500};
        int[] coinPrice = {1, 5, 10, 50, 100, 500};

        int[] avg = new int[costs.length];
        for(int i = 0; i < costs.length; i++) {
            switch(i) {
                case 0:
                    avg[i] = costs[i] * 500;
                    break;
                case 1:
                    avg[i] = costs[i] * 100;
                    break;
                case 2:
                    avg[i] = costs[i] * 50;
                    break;
                case 3:
                    avg[i] = costs[i] * 10;
                    break;
                case 4:
                    avg[i] = costs[i] * 5;
                    break;
                case 5:
                    avg[i] = costs[i];
                    break;
            }
        }

        //버블 소팅으로 500원대 기준으로 생산단가 비교해서 정렬해야함
        for(int i = 1; i < costs.length; i++) {
            for(int j = 0; j < costs.length - i; j++) {
                if(avg[j] > avg[j+1]) {
                    int temp = avg[j];
                    avg[j] = avg[j+1];
                    avg[j+1] = temp;

                    int save = coinPrice[j];
                    coinPrice[j] = coinPrice[j+1];
                    coinPrice[j+1] = save;

                    int coin = costs[j];
                    costs[j] = costs[j+1];
                    costs[j+1] = coin;
                } else if(avg[j] == avg[j+1]) {
                    if(coinPrice[j] < coinPrice[j+1]) {
                        int save = coinPrice[j];
                        coinPrice[j] = coinPrice[j+1];
                        coinPrice[j+1] = save;

                        int coin = costs[j];
                        costs[j] = costs[j+1];
                        costs[j+1] = coin;
                    }
                }
            }
        }

        for(int i = 0; i < costs.length; i++) {
            System.out.println("i : " + i);
            System.out.println("costs : " + avg[i]);
            System.out.println("costPrice : " + coinPrice[i]);

        }

        System.out.println("===========================");

        int answer = 0;
        int remian = money;
        int total = 0;
        int[] coinUse = new int[costs.length];
        for(int i = 0; i < costs.length; i++) {
            coinUse[i] = remian / coinPrice[i];
            remian = remian % coinPrice[i];
//            remian = money - (coinUse[i] * coinPrice[i]);
            System.out.println("i : " + i);
            System.out.println("coinPrice : " + coinPrice[i]);
            System.out.println("coinUse : " + coinUse[i]);
            System.out.println("costs : " + costs[i]);
            System.out.println("remian : " + remian);
            if(remian == 0) {
                break;
            }
        }

        for(int i = 0; i < costs.length; i++) {
            total += coinUse[i] * costs[i];
        }
        System.out.println("total : " + total);
    }

    public static void change(int[] a, int i, int j) {

    }

}
