package backjoon.network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class MaxFlow1014 {

    private static int[] availableSeats;
    private static int count;
    private static int[][] testData;
    private static int N, M;
    private static boolean[] matched;
    private static int[] matchInfo;

    private static int[] moveRow = {-1, 0, 1, -1, 0, 1};
    private static int[] moveCol = {-1, -1, -1, 1, 1, 1};
    private static int[] numberRule = {-1, -1, -1, 0, 0, 0};

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력의 첫 줄에는 테스트케이스의 개수 C가 주어진다. 각각의 테스트 케이스는 아래와 같이 두 부분으로 이루어진다.
         * 첫 번째 부분에서는 교실의 세로길이 N과 가로길이 M이 한 줄에 주어진다. (1 ≤ M ≤ 10, 1 ≤ N ≤ 10)
         * 두 번째 부분에서는 정확하게 N줄이 주어진다.
         * 그리고 각 줄은 M개의 문자로 이루어져있다. 모든 문자는 ‘.’(앉을 수 있는 자리) 또는 ‘x’(앉을 수 없는 자리, 소문자)로 구성된다.
         *
         * 2. output 조건
         * 각각의 테스트 케이스에 대해 그 교실에서 시험을 볼 수 있는 최대 학생의 수를 출력한다.
         *
         * 3. 주의사항
         * 이론 없이 풀이만 집중해서 풀려고 하려면 절대로 풀 수 없다는 것을 느꼈다.
         * 최대 가용 좌석에만 초점을 맞춰서 brute force로 풀려고 해도 대체 어느 경우에 최대 가용 좌석을 확보할 수 있는 것인지
         * 조건을 알 수가 없기 때문이다.
         * 그렇기 때문에 완전 풀이에 접근조차 못하는 문제의 경우, 차라리 다른 사람 풀이를 통해 사용된 이론을 빠르게 접하고
         * 그 이른올 공부한 다음, 어떤 방식으로 문제에 적용하여 풀 것인지를 훈련하는게 알고리즘 실력 향상에 더 도움이 된다.
         * --------------------------------------------------------------------------------
         * 이 문제의 풀이법은 크게 max flow를 적용한 풀이와 bitmasking을 적용한 dp가 있다.
         * dp가 얼마나 강력하고 효과적인 것인지를 새삼 느끼게 해준 문제이기도 하다.
         * 그러나 개념적으로 이 문제를 가장 설명하기 쉽고, 정석이라고 사람들이 인정한 풀이는 네트워크 플로우를 바탕으로 하는
         * max flow 풀이이다.
         *
         * 따라서 본 문제의 풀이는 max flow대로 풀 것이며, 네트워크 플로우에 관해서 전반적인 지식을 알고 있는 것이 좋다.
         * 이 문제의 풀이는 결국 문제를 이분 그래프 형태로 변환해서 쾨니그의 정리를 적용한 풀이를 구현할 것이기 때문이다.
         *
         * 4. 해석
         * 문제를 보자마자 가장 먼저 떠오르는 생각은 일렬로 선택하는 것이 가장 많은 좌석 수를 선택할 수 있다는 것이다.
         * 모든 좌석 선택이 가능한 가장 일반적인 경우로 놓고 봤을 때, 대각선 방향 때문에 좌우로 좌석 선택을 하게 되면
         * 1자리를 무조건 손해보기 때문이다.
         * 일렬로 선택한다는 방침으로 밀고 나가게 되면, 홀수 열의 좌석을 모두 선택하는 것이 가장 좋다는 결론으로 가게 된다.
         *
         * 그런데 이건 너무 안일한 생각인데다 이 이론이 과연 모든 상황에서 항상 최선의 결과인 것인지를 입증할 방법이 없다.
         * 따라서 완전히 다른 측면에서 이 문제를 생각해볼 필요가 있다. 가장 많은 좌석을 선택하기 위해서는 어떤 조건이 필요할까라는.
         *
         *
         * Example
         * 1.
         * 4
         * 2 3
         * ...
         * ...
         * 2 3
         * x.x
         * xxx
         * 2 3
         * x.x
         * x.x
         * 10 10
         * ....x.....
         * ..........
         * ..........
         * ..x.......
         * ..........
         * x...x.x...
         * .........x
         * ...x......
         * ........x.
         * .x...x....
         * ----------------------
         * answer
         * 4
         * 1
         * 2
         * 46
         *
         * 2.
         * 1
         * 4 5
         * xxxxx
         * xx.xx
         * xx.xx
         * x.x.x
         * ----------------------
         * answer
         * 3
         *
         */

        List<int[][]> inputData = inputData();
        List<Integer> results = solveProblem(inputData);

        for(int i = 0; i < inputData.size(); i++) {
            System.out.println(availableSeats[i] - results.get(i));
        }

    }

    public static List<int[][]> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            List<int[][]> inputData = new ArrayList<int[][]>(testCase);
            availableSeats = new int[testCase];

            for(int i = 0; i < testCase; i++) {
                StringTokenizer st = new StringTokenizer(br.readLine());
                int N = Integer.parseInt(st.nextToken());
                int M = Integer.parseInt(st.nextToken());
                int brokenSeat = 0;

                int[][] data = new int[N][M];
                for(int j = 0; j < N; j++) {
                    String str = br.readLine();

                    for(int k = 0; k < M; k++) {
                        if(str.charAt(k) == 'x') {
                            data[j][k] = -2;
                            brokenSeat++;
                        } else {
                            data[j][k] = -1;
                        }
                    }
                }

                inputData.add(data);
                availableSeats[i] = N*M - brokenSeat;
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<Integer> solveProblem(List<int[][]> inputData) {
        List<Integer> results = new LinkedList<Integer>();

        /*
         * bipartite matching 알고리즘을 호출하기 전 전처리와 호출 부분
         */
        for(int[][] data : inputData) {
            testData = data;
            N = data.length;
            M = data[0].length;
            int evenColCnt = M/2;

            // 짝수열의 자리가 오른쪽 정점 집단이다.
            matched = new boolean[N * evenColCnt];
            matchInfo = new int[N * evenColCnt];
            Arrays.fill(matchInfo, -1);
            count = 0;

            // 2차원 배열을 1차원 자료로 만드는 전처리
            /*for(int i = 0; i < M; i++) {
                for(int j = 0; j < N; j++) {

                }
            }*/

            /*
             * 일단은 모든 자리를 순차적으로 번호를 매겨야만 한다.
             * 파손여부를 따지면서 번호를 매기면 알고리즘을 진행할 수 없으니까.
             * 번호는 홀수열, 짝수열끼리 각각 따로 매긴다. 순서는 위에서 아래로, 좌에서 우로 한다.
             * 즉 0  3  이렇게 매긴다.
             *   1  4
             *   2  5
             *
             * 좌측 vertex 집단을 순회하면서 matching하기 때문에 여기선 홀수열에만 접근할 것이다.
             * (i*N + j)가 좌측 vertex 집단내에서의 넘버링이 된다.
             * i*2는 2차원 배열에서 col의 좌표값이 된다.
             */
            for(int i = 0; i*2 < M; i++) {
                for(int j = 0; j < N; j++) {
                    // 파손된 자리는 matching 불가
//                    System.out.println("table check : " + data[j][i*2]);
                    if(data[j][i*2] == -2) {
                        continue;
                    }

                    Arrays.fill(matched, false);
                    if(findMatching(i, j)) {
                        count++;
//                        System.out.println("result count : " + count);
                    }
                }
            }

//            System.out.println("result count : " + count);
            results.add(count);
        }

        return results;
    }

    private static boolean findMatching(int colOrder, int row) {
        /*
         * 연결된 짝수열 그룹의 vertex을 돌면서 연결한다.
         * 짝수열 넘버링은 위에 홀수열 넘버링과 원리가 같다.
         *
         * (i*N + j)가 vertex 집단내에서의 넘버링이 된다.
         * i*2는 2차원 배열에서 col의 좌표값이 된다.
         * 원래 col 값에서 계산할 때는 i로 만들어야하기 때문에 divide by 2를 하면 된다. (나누기 2)
         */
        int col = colOrder * 2;
        int num = row + colOrder * N;

        for(int i = 0; i < 6; i++) {
            int adjCol = col + moveCol[i];
            int adjRow = row + moveRow[i];
            int adjNum = adjRow + (colOrder + numberRule[i]) * N;

            if(adjCol < 0 || adjCol >= M || adjRow < 0 || adjRow >= N) {
                continue;
            }

            if(testData[adjRow][adjCol] == -2) {
                continue;
            }

            if(matched[adjNum]) {
                continue;
            }
            matched[adjNum] = true;

            if(matchInfo[adjNum] == -1 || findMatching(matchInfo[adjNum] / N, matchInfo[adjNum] % N)) {
                matchInfo[adjNum] = num;
                return true;
            }

        }

        return false;
    }

}
