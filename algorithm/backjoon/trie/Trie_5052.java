package backjoon.trie;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Trie_5052 {

    static class Node5052 {

        Map<Character, Node5052> children = new HashMap<Character, Node5052>();
        boolean endOfWords;

    }

    static class Trie5052 {

        Node5052 root;

        Trie5052() {
            this.root = new Node5052();
        }

        boolean insert(String str) {
            Node5052 node = this.root;
            char[] chars = str.toCharArray();

            for(int i = 0; i < chars.length; i++) {
                node = node.children.computeIfAbsent(chars[i], key -> new Node5052());

                if(node.endOfWords) {
                    return false;
                }
            }

            node.endOfWords = true;
            return true;
        }

    }

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 테스트 케이스의 개수 t가 주어진다. (1 ≤ t ≤ 50)
         * 각 테스트 케이스의 첫째 줄에는 전화번호의 수 n이 주어진다. (1 ≤ n ≤ 10000)
         * 다음 n개의 줄에는 목록에 포함되어 있는 전화번호가 하나씩 주어진다.
         * 전화번호의 길이는 길어야 10자리이며, 목록에 있는 두 전화번호가 같은 경우는 없다.
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해서, 일관성 있는 목록인 경우에는 YES, 아닌 경우에는 NO를 출력한다.
         * 전화번호 목록이 일관성을 유지하려면, 한 번호가 다른 번호의 접두어인 경우가 없어야 한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 이 문제를 풀기 위해서는 Trie라는 트리 계열의 자료구조를 알아야 한다.
         * Trie를 알고 직접 구현할 수 있다면, 풀이법은 쉬운 편이다.
         *
         *
         * Example
         * 1.
         * 2
         * 3
         * 911
         * 97625999
         * 91125426
         * 5
         * 113
         * 12340
         * 123440
         * 12345
         * 98346
         * -------
         * answer
         * NO
         * YES
         *
         */

        List<List<String>> inputData = inputData();
        List<String> results = solveProblem(inputData);
        for(String result : results) {
            System.out.println(result);
        }

    }

    public static List<List<String>> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            List<List<String>> inputData = new ArrayList<List<String>>(testCase);

            for(int i = 0; i < testCase; i++) {
                int phoneNumberListCnt = Integer.parseInt(br.readLine());
                List<String> list = new ArrayList<String>(phoneNumberListCnt);

                for(int j = 0; j < phoneNumberListCnt; j++) {
                    list.add(br.readLine());
                }
                inputData.add(list);
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<String> solveProblem(List<List<String>> inputData) {
        int testCase = inputData.size();
        List<String> results = new ArrayList<String>(testCase);

        for(int i = 0; i < testCase; i++) {
            List<String> list = inputData.get(i);
            Collections.sort(list);
            Trie5052 trie = new Trie5052();
            boolean isConsistent = true;

            for(String phoneNum : list) {
                if(!trie.insert(phoneNum)) {
                    isConsistent = false;
                    break;
                }
            }

            if(isConsistent) {
                results.add("YES");
            } else {
                results.add("NO");
            }
        }

        return results;
    }

}
