package backjoon.floyd_warshall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.StringTokenizer;

public class Floyd_Warshall1389 {

    private static int vertices;
    private static int[][] graphInfo;
    private static int INF = 100;
    private static int[] minimumKevinBaconNumbers;
    private static int min = 10_000;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        for(int i = 1; i <= vertices; i++) {
            if(min == minimumKevinBaconNumbers[i]) {
                System.out.println(i);
                break;
            }
        }

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            vertices = Integer.parseInt(st.nextToken());
            int edge = Integer.parseInt(st.nextToken());

            graphInfo = new int[vertices + 1][vertices + 1];
            minimumKevinBaconNumbers = new int[vertices + 1];
            for(int i = 1; i <= vertices; i++) {
                for(int j = 1; j <= vertices; j++) {
                    graphInfo[i][j] = INF;

                    if(i == j) {
                        graphInfo[i][j] = 0;
                    }
                }
            }

            int source, destination;
            for(int i = 0; i < edge; i++) {
                st = new StringTokenizer(br.readLine());
                source = Integer.parseInt(st.nextToken());
                destination = Integer.parseInt(st.nextToken());

                graphInfo[source][destination] = 1;
                graphInfo[destination][source] = 1;
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        for(int k = 1; k <= vertices; k++) {
            for(int a = 1; a <= vertices; a++) {
                for(int b = 1; b <= vertices; b++) {
                    graphInfo[a][b] = Math.min(graphInfo[a][b], graphInfo[a][k] + graphInfo[k][b]);
                }
            }
        }

        int sum;
        for(int i = 1; i <= vertices; i++) {
            sum = 0;
            for(int j = 1; j <= vertices; j++) {
                sum += graphInfo[i][j];
            }

            if(min > sum)
                min = sum;
            minimumKevinBaconNumbers[i] = sum;
        }
    }

}
