package backjoon.floyd_warshall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.StringTokenizer;

public class Floyd_Warshall11403 {

    private static int vertices;
    private static int[][] graphInfo;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        for(int i = 1; i <= vertices; i++) {
            for(int j = 1; j <= vertices; j++) {
                System.out.print(graphInfo[i][j] + " ");
            }

            System.out.println();
        }

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            vertices = Integer.parseInt(br.readLine());
            graphInfo = new int[vertices + 1][vertices + 1];

            StringTokenizer st;
            for(int i = 1; i <= vertices; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 1; j <= vertices; j++) {
                    graphInfo[i][j] = Integer.parseInt(st.nextToken());
                }
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        for(int k = 1; k <= vertices; k++) {
            for(int i = 1; i <= vertices; i++) {
                for(int j = 1; j <= vertices; j++) {
//                    graphInfo[i][j] = Math.max(graphInfo[i][j], graphInfo[i][k] * graphInfo[k][j]);
                    graphInfo[i][j] = (graphInfo[i][j] + graphInfo[i][k] * graphInfo[k][j]) > 0 ? 1 : 0;
                }
            }
        }
    }

}
