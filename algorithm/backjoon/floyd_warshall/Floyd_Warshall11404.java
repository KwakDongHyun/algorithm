package backjoon.floyd_warshall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.StringTokenizer;

public class Floyd_Warshall11404 {

    private static int vertices;
    private static int edge;
    private static int[][] graphInfo;
//    private static int[][] costInfo;
    private static final int INF = 10_000_000;

    public static void main(String[] args) {
        preprocessData();
        solveProblem();

        for(int a = 1; a <= vertices; a++) {
            for (int b = 1; b <= vertices; b++) {
                System.out.print(graphInfo[a][b] + " ");
            }
            System.out.println();
        }
    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            vertices = Integer.parseInt(br.readLine());
            edge = Integer.parseInt(br.readLine());

            graphInfo = new int[vertices + 1][vertices + 1];
//            costInfo = new int[vertices + 1][vertices + 1];

            for(int i = 1; i <= vertices; i++) {
                for(int j = 1; j <= vertices; j++) {
                    graphInfo[i][j] = INF;

                    if(i == j) {
                        graphInfo[i][j] = 0;
                    }
                }
            }

            StringTokenizer st;
            int source, destination, cost;
            for(int i = 0; i < edge; i++) {
                st = new StringTokenizer(br.readLine());
                source = Integer.parseInt(st.nextToken());
                destination = Integer.parseInt(st.nextToken());
                cost = Integer.parseInt(st.nextToken());

                graphInfo[source][destination] = Math.min(cost, graphInfo[source][destination]);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        /*
         * 거쳐가는 노드 순회
         */
        for(int k = 1; k <= vertices; k++) {
            for(int a = 1; a <= vertices; a++) {
                for(int b = 1; b <= vertices; b++) {
                    graphInfo[a][b] = Math.min(graphInfo[a][b], graphInfo[a][k] + graphInfo[k][b]);
                }
            }

            System.out.println("k : " + k);
            for(int i = 1; i <= vertices; i++) {
                for(int j = 1; j <= vertices; j++) {
                    System.out.print(graphInfo[i][j] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }

        /*
         * INF의 값을 0으로 바꿔줘야만 한다.
         */
        for(int a = 1; a <= vertices; a++) {
            for (int b = 1; b <= vertices; b++) {
                if(graphInfo[a][b] == INF) {
                    graphInfo[a][b] = 0;
                }
            }
        }
    }

}
