package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp11052 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 구매하려고 하는 카드의 개수 N이 주어진다. (1 ≤ N ≤ 1,000)
         * 둘째 줄에는 Pi가 P1부터 PN까지 순서대로 주어진다. (1 ≤ Pi ≤ 10,000)
         * 카드팩의 종류는 카드 1개가 포함된 카드팩, 카드 2개가 포함된 카드팩, ... 카드 N개가 포함된 카드팩과 같이 총 N가지가 존재한다.
         * 즉, Pi는 i개의 카드가 들어있는 카드팩이다.
         *
         * 2. output 조건
         * 카드 N개를 갖기 위해 지불해야 하는 금액의 최대값을 출력한다.
         *
         * 3. 해석
         * dp[i]을 최적화시키고, 이걸 토대로 다음 dp[i+1]을 구하려고 했는데.. 너무 복잡하다.
         * 하지만 이 방법을 사용하면 카드 수가 바뀌어도 항상 최적의 결과를 얻을 수가 있다고 생각한다.
         * ----------------------------
         * 생각한 게 맞다.
         * dp[4] = P3 + P1, dp[5] = P3 + P2 라고 가정해보자.
         * P9 > dp[4] + dp[5] 가 아니라면, dp[9] = dp[4] + dp[5] = (2 * P3) + P2 + P1 이 될 것이다.
         * 하위 dp에서 부분 최적화가 잘 이루어졌으면, 상위 dp를 형성할 때 Pi로 교체되지 않고서야 하위 최적화의 합으로 구성될 것이다.
         * n^2으로 할 수 밖에 없는 이유는 어느 시점에서 Pi로 교체되었는지 알 수가 없기 때문이다.
         * 그래도 inner recursive statement의 횟수를 줄이기 위해서 i/2를 적용해서 최소한의 loop만 순회하기로 했다.
         *
         * 4. 해결 방법
         * 두 가지 방법을 생각했었다.
         * 첫 번째 방법은 위에 설명한 대로 하위에서 카드 구매시 최대 금액을 구한 다음에 이를 조합하여 상위의 값을 찾는 것이었다.
         * 두 번째 방법은 새로운 배열을 하나 생성해서 Pi/i 의 값을 double로 기록하고, 정렬하여 가장 우수한 카드팩부터 사용하는 방식이다.
         * ----------------------------
         * 두 번째 방법은 단점이 있다.. Prime Number끼리 나눌 경우 나누어 지지 않을 뿐만 아니라, 소수점 몇 번째 이하까지 같을 경우를 어떻게 비교할 것이냐는 거다.
         * 그래서 프로그래밍을 할 때, divide를 통한 우위 비교는 되도록이면 안하는 게 좋다.
         * divide 자체가 완벽한 연산을 기대하기가 힘들다.
         * 첫 번째 방법을 통해서 해결하도록 노력해보자.
         *
         */
        int[] inputData = inputData();
        int maxCost = getMaxCost(inputData);
        System.out.println(maxCost);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int cardNumber = Integer.parseInt(br.readLine());
            StringTokenizer stringTokenizer = new StringTokenizer(br.readLine());
            if(cardNumber != stringTokenizer.countTokens()) {
                throw new Exception("입력한 카드와 카드팩 수가 일치하지 않습니다");
            }

            int[] inputData = new int[cardNumber + 1];
            for(int i = 1; i <= cardNumber; i++) {
                inputData[i] = Integer.parseInt(stringTokenizer.nextToken());
            }
            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            int[] inputData = new int[1];
            inputData[0] = 0;
            return inputData;
        }
    }

    public static int getMaxCost(int[] inputData) {
        int cardNumber = inputData.length-1;
        int[] dp = new int[cardNumber + 1];

        /*
         * 일단 한 번 해보자.
         * 아래에서 최적화가 이루어지면, 상위에서 값이 바뀌더라도 최적화값 x n만큼 되는 것이라 계속해서 최적화가 유지된다고 생각한다.
         * 설사 상위에서 Pi가 훨씬 커서 바뀌어지더라도, 바뀐 값을 기준으로 결국 dp를 형성하기 때문에..
         * N에 가까워지도록 수가 커지더라도 최적화의 성질은 독립적으로 계속해서 유지된다.
         *
         * example
         * 1. 4 / 1 5 6 7  - 10
         * 2. 5 / 10 9 8 7 6  - 50
         * 3. 10 / 1 1 2 3 5 8 13 21 34 55  - 55
         * 4. 10 / 5 10 11 12 13 30 35 40 45 47  - 50
         * 5. 4 / 5 2 8 10  - 20
         * 6. 4 / 3 5 15 16  - 18
         *
         */
        for(int i = 1; i <= cardNumber; i++) {
            dp[i] = inputData[i];
            for(int j = 1; j <= i/2; j++) {
                dp[i] = Math.max(dp[i], dp[j] + dp[i-j]);
            }
//            System.out.println("dp[" + i + "] : " + dp[i]);
        }

        return dp[cardNumber];
    }

}
