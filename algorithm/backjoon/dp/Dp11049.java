package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp11049 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 행렬의 개수 N(1 ≤ N ≤ 500)이 주어진다.
         * 둘째 줄부터 N개 줄에는 행렬의 크기 r과 c가 주어진다. (1 ≤ r, c ≤ 500) (A(r,c) 형태로 주어진다.)
         * 항상 순서대로 곱셈을 할 수 있는 크기만 입력으로 주어진다.
         *
         * 2. output 조건
         * 첫째 줄에 입력으로 주어진 행렬을 곱하는데 필요한 곱셈 연산의 최솟값을 출력한다.
         * 정답은 231-1 보다 작거나 같은 자연수이다. 또한, 최악의 순서로 연산해도 연산 횟수가 231-1보다 작거나 같다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 바로 이전에 풀었던 11066번 DP문제와 매우 유사하다.
         * 같은 형태의 자료구조를 사용해서 비슷한 풀이로 풀어나가면 될 것 같다.
         * 한편, 마지막 matrix 곱셉의 연산 수를 알기 위해서는 matrix 곱셈 후의 matrix의 row와 col의 개수에 대한 정보를 알고 있어야한다.
         *
         *
         * Example
         * 1.
         * 3
         * 5 3
         * 3 2
         * 2 6
         * -------
         * answer
         * 90
         *
         * 2.
         * 5
         * 1 10
         * 10 1
         * 1 10
         * 10 1
         * 1 10
         * -------
         * answer
         * 31
         *
         * 3.
         * 4
         * 5 3
         * 3 2
         * 2 6
         * 6 3
         * -------
         * answer
         * 96
         *
         */

        int[][] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int matrixCnt = Integer.parseInt(br.readLine());
            int[][] inputData = new int[matrixCnt][2];

            StringTokenizer st;
            for(int i = 0; i < matrixCnt; i++) {
                st = new StringTokenizer(br.readLine());
                inputData[i][0] = Integer.parseInt(st.nextToken());
                inputData[i][1] = Integer.parseInt(st.nextToken());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[][] inputData) {
        int matrixCnt = inputData.length;

        /*
         * dp[i][j] = i번째 matrix부터 j번째 matrix까지 곱할 때 필요한 곱셈 연산의 최소 수
         * 점화식은 아래와 같다.
         * dp[i][j] = minimum({from k = i to k = j-1} dp[i][k] + dp[k+1][j]);
         */
        int[][] dp = new int[matrixCnt][matrixCnt];
        int[] matrixSizeInfo = new int[3];
        int temp;
        for(int j = 1; j < matrixCnt; j++) {
            for(int i = 0; i + j < matrixCnt; i++) {
                for(int k = i; k < i + j; k++) {
                    if(k == i) {
                        dp[i][i+j] = dp[i][k] + dp[k+1][i+j];
                        matrixSizeInfo[0] = inputData[i][0];
                        matrixSizeInfo[1] = inputData[k+1][0];
                        matrixSizeInfo[2] = inputData[i+j][1];

                        dp[i][i+j] += matrixSizeInfo[0] * matrixSizeInfo[1] * matrixSizeInfo[2];
                    }

                    /*
                     * 인덱스에 대한 데이터가 추가로 저장할 필요가 있으므로 Math.min을 쓰지 않는다.
                     * 한편, 행렬 특성상 같은 곱셉 연산의 수라고 할지라도 결과로 형성된 matrix 크기가 다를 수 있다.
                     * 예제 2처럼 연산 수는 같더라도 생성된 행렬 크기가 1x1이면 추후에 연산 수를 많이 줄여주기 때문이다.
                     * 그렇기에 생성될 matrix 크기까지 고려해서 알고리즘을 설계해야 한다.
                     */
                    temp = dp[i][k] + dp[k+1][i+j] + (inputData[i][0] * inputData[k+1][0] * inputData[i+j][1]);
                    if(dp[i][i+j] > temp) {
                        dp[i][i+j] = temp;
                    }

//                    System.out.println("i : " + i + ", j : " + j + ", k : " + k);
//                    System.out.println("dp[" + i + "][" + (i+j) + "] : " + dp[i][i+j]);
                }

//                dp[i][i+j] += matrixSizeInfo[0] * matrixSizeInfo[1] * matrixSizeInfo[2];
//                System.out.println("final dp[" + i + "][" + (i+j) + "] : " + dp[i][i+j]);
//                System.out.println();
            }

//            System.out.println();
        }

        return dp[0][matrixCnt - 1];
    }

}
