package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp2748 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * n 입력 (1 ≤ n ≤ 90)
         *
         * 2. output 조건
         * n번째 피보나치 수를 출력한다.
         *
         * 3. 주의사항
         * 피보나치 수열이기 때문에 무조건 overflow가 발생한다.
         * 피보나치를 어떻게 표현할지 고민해야 한다.
         *
         */
        int inputData = inputData();
        long fibonacciSequence = getFibonacciSeries(inputData);
        System.out.println(fibonacciSequence);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static long getFibonacciSeries(int inputData) {
        long[] dp = new long[inputData + 1];
        for(int i = 0; i <= inputData; i++) {
            if(i == 0) {
                dp[i] = 0;
            } else if(i == 1) {
                dp[i] = 1;
            } else {
                dp[i] = dp[i-1] + dp[i-2];
            }
        }
        return dp[inputData];
    }

}
