package backjoon.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class Dp1006_important {

    public static void main(String[] args) throws IOException {

        /*
         * 1. input 조건
         * 첫째 줄에 테스트 케이스의 개수 T가 주어진다. 각 테스트 케이스는 다음과 같이 구성되어있다.
         *
         * 첫째 줄에는 (구역의 개수)/2 값 N과 특수 소대원의 수 W가 주어진다. (1 ≤ N ≤ 10000, 1 ≤ W ≤ 10000).
         * 둘째 줄에는 1~N번째 구역에 배치된 적의 수가 주어지고, 셋째 줄에는 N+1 ~ 2N번째 구역에 배치된 적의 수가 공백으로 구분되어 주어진다.
         * (1 ≤ 각 구역에 배치된 최대 적의 수 ≤ 10000)
         * 단, 한 구역에서 특수 소대원의 수보다 많은 적이 배치된 구역은 존재하지 않는다. (따라서, 각 구역에 배치된 최대 적의 수 ≤ W)
         *
         * 2. output 조건
         * 각 테스트케이스에 대해서 한 줄에 하나씩 원타곤의 모든 구역을 커버하기 위해 침투 시켜야 할 특수 소대의 최소 개수를 출력하시오.
         * Rule은 아래와 같다.
         *
         *  1) 한 특수소대는 침투한 구역 외에, 인접한 한 구역 더 침투할 수 있다.
         *     (같은 경계를 공유하고 있으면 인접 하다고 한다. 위 그림에서 1구역은 2, 8, 9 구역과 서로 인접한 상태다.)
         *     즉, 한 특수소대는 한 개 혹은 두 개의 구역을 커버할 수 있다.
         *  2) 특수소대끼리는 아군인지 적인지 구분을 못 하기 때문에, 각 구역은 하나의 소대로만 커버해야 한다.
         *  3) 한 특수소대가 커버하는 구역의 적들의 합은 특수소대원 수 W 보다 작거나 같아야 한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 모든 경우의 수에 대해서 탐색을 해야한다고 생각한다.
         * Brute Force 계열의 문제이고, 효과적으로 풀기 위해 BackTracking을 사용한다.
         * 이런 경우와 조합의 문제의 경우 부분적으로 경우를 바꿔가며 검토해야하기 때문이다.
         * -------------------------------------------------------------
         * BackTracking으로 구현해본 결과 원하는 답도 잘 안 나오고, 변칙적인 경우에 대처를 못해서 구현이 매우 어려웠다.
         * 무엇보다 결국 모든 알고리즘은 결과를 도출하기 위해서 풀이를 단계별로 만들어서 반복해야하는데
         * 단계를 어디서 어디까지로 정의하고 어떻게 만들지가 모호했다.
         * 결국 힌트를 보고서 접근하기로 했고, 의외로 DP로 해결이 된다는 것에 적잖이 충격을 받았다.
         * DP라는 힌트만을 가지고서 문제의 풀이를 작게 나눈 다음, 단계별로 다시 분석해보고 구현한다.
         * -------------------------------------------------------------
         * 지금 단계에서는 혼자서 도저히 풀 수가 없어서 다른 사람의 풀이를 보면서 공부한다.
         * 나름대로 이해한 내용은 종이에 4장에 걸쳐서 정리했으니 해당 문제는 내가 모아놓은 풀이를 다시 참고하자.
         * 이번 문제를 통해서 알게 된 것은 3가지였다.
         *
         *  1. 수학적으로 문제를 바라보고 풀이를 생각할 것
         *     생각할 것이 많고 복잡하면서 변화무쌍한 문제라도 침착하게 문제를 작게 나눠서 바라봐야한다.
         *     작게 나눈 것을 어떻게 변수로 지정하고, 변수에 따른 결과 값이 어떻게 도출되는지를 수학적으로 풀어야만 한다. (대수학의 관점으로)
         *     그리고 문제의 지엽적인 부분을 너무 고려한 나머지 풀이에 접근하기 더 어려울 수가 있다.
         *     그럴 때는 조건을 무시하고 풀이를 만들고 나서 하나씩 조건을 붙여나가는 식으로 해도 된다.
         *     마치 성을 쌓아나가듯이 하나씩 밑에서부터 풀이를 만들어나가는 연습을 해야한다.
         *     그러다보면 이 과정이 머리에서 매우 빠르게 일어나면서 보자마자 문제 풀이가 생각나는 경지까지 가게될 터이다.
         *
         *  2. 성급하게 생각하지 말고 침착하게 놓친 부분이 없는지 확인할 것
         *     너무 특정 부분만 집중하여 생각하게 되면 분명 놓치는 부분이 있게 된다. 작게 나누어서 문제를 생각하다 보면 특히나 더 그럴 수 있다.
         *     그렇기 때문에 일반적인 풀이라면 당연히 고려하거나 포함되어야할 부분이 전체 풀이에서 빠져있지는 않은지 꼼꼼히 생각해봐야 한다.
         *
         *  3. 구현할 때 데이터의 특성과 컴퓨터만의 특징을 잘 고려해서 구현한다.
         *     이번 문제처럼 데이터가 특정한 형태를 띄고 있을 수 있기 때문에 풀이를 정의하고 나서 반드시 특성을 고려해서 구현해야한다.
         *     또한 컴퓨터는 순차적인 탐색 밖에 못하기 때문에 고차원의 복잡한 문제일수록 컴퓨터가 이해하기 쉬운 형태로 풀어서 구현해야한다.
         *     가령 지수승으로 걸리는 문제를 log나 선형 수준으로 낮출 수 있도록 프로그래밍을 한다던지 등등..
         *
         * Example
         * 1.
         * 1
         * 8 100
         * 70 60 55 43 57 60 44 50
         * 58 40 47 90 45 52 80 40
         * -------
         * answer
         * 11
         *
         * 2.
         *
         * --------
         * answer
         *
         * 3.
         *
         * --------
         * answer
         *
         *
         */

        Map<String, Object>  inputData = inputData();
        List<Integer> results = solveProblem(inputData);
        for(int result : results) {
            System.out.println(result);
        }

    }

    public static Map<String, Object>  inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            Map<String, Object> inputData = new HashMap<String, Object>();
            int[] halfOfAreas = new int[testCase];
            int[] members = new int[testCase];
            List<int[][]> areas = new ArrayList<int[][]>(testCase);

            StringTokenizer st;
            int halfOfArea, member;
            for(int i = 0; i < testCase; i++) {
                st = new StringTokenizer(br.readLine());
                halfOfArea = Integer.parseInt(st.nextToken());
                member = Integer.parseInt(st.nextToken());

                int[][] area = new int[2][halfOfArea];
                halfOfAreas[i] = halfOfArea;
                members[i] = member;

                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < halfOfArea; j++) {
                    area[0][j] = Integer.parseInt(st.nextToken());
                }

                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < halfOfArea; j++) {
                    area[1][j] = Integer.parseInt(st.nextToken());
                }

                areas.add(area);
            }

            inputData.put("halfOfAreas", halfOfAreas);
            inputData.put("members", members);
            inputData.put("areas", areas);

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<Integer> solveProblem(Map<String, Object>  inputData) {
        int[] halfOfAreas = (int[]) inputData.get("halfOfAreas");
        int[] members = (int[]) inputData.get("members");
        List<int[][]> areas = (List<int[][]>) inputData.get("areas");

        int testCase = halfOfAreas.length;

        int N, W;
        int[][] e;
        int a[], b[], c[];
        int result;

        List<Integer> results = new ArrayList<Integer>(testCase);

        for(int i = 0; i < testCase; i++) {
            e = areas.get(i);
            N = halfOfAreas[i];
            W = members[i];

            a = new int[N];
            b = new int[N];
            c = new int[N + 1];

            a[0] = 1;
            b[0] = 1;
            c[0] = 0;

            result = 20_001;

            /*
             * 어떤 경우가 가장 최소인지 모르기 때문에 모든 케이스에 대해서 알고리즘을 수행해보고 최소값을 정해야한다.
             */
            solve(0, N, W, e, a, b, c);
            result = Math.min(result, c[N]);

            /*
             * 1. case 4
             * 위아래 모두 각각 수평으로 시작지점과 끝지점을 동시에 점령한 경우
             *
             * 2. case 2
             * 윗 행만 시작지점과 끝지점을 동시에 점령한 경우
             *
             * 3. case 3
             * 아래 행만 시작지점과 끝지점을 동시에 점령한 경우
             */
            if(N > 1) {
                if(e[0][0] + e[0][N - 1] <= W && e[1][0] + e[1][N - 1] <= W) {
                    a[1] = 1;
                    b[1] = 1;
                    c[1] = 0;

                    solve(1, N, W, e, a, b, c);
                    result = Math.min(result, c[N - 1] + 2);
                }

                if(e[0][0] + e[0][N - 1] <= W) {
                    a[1] = 2;
                    b[1] = e[1][0] + e[1][1] > W ? 2 : 1;
                    c[1] = 1;

                    solve(1, N, W, e, a, b, c);
                    result = Math.min(result, b[N - 1] + 1);
                }

                if(e[1][0] + e[1][N - 1] <= W) {
                    a[1] = e[0][0] + e[0][1] > W ? 2 : 1;
                    b[1] = 2;
                    c[1] = 1;

                    solve(1, N, W, e, a, b, c);
                    result = Math.min(result, a[N - 1] + 1);
                }
            }

            results.add(result);
        } // for-end

        return results;
    }

    /**
     * 메인 로직 함수. 세팅한 초기화 값을 필두로 각 케이스별로 최소 팀의 수를 구한다.
     * @param index
     * @param N
     * @param W
     * @param e
     * @param a
     * @param b
     * @param c
     */
    private static void solve(int index, int N, int W, int[][] e, int[] a, int[] b, int[] c) {

        for(int i = index; i < N; i++) {
            /*
             * 3. c[i+1] 구하기
             * 3-1. 일반적인 케이스
             */
            c[i + 1] = Math.min(a[i] + 1, b[i] + 1);

            // 3-2. 수직으로 끝에 추가된 두 지역 커버
            if(e[0][i] + e[1][i] <= W) {
                c[i + 1] = Math.min(c[i + 1], c[i] + 1);
            }

            /*
             * 3-3. 두 팀이 각각 수평으로 네 지역 커버
             * 이 케이스를 측정하기 위해서는 최소 2개 이상의 행이 필요하다
             */
            if(i > 0 && e[0][i-1] + e[0][i] <= W && e[1][i-1] + e[1][i] <= W) {
                c[i + 1] = Math.min(c[i + 1], c[i-1] + 2);
            }

            /*
             * 아래 2가지 항에 대해서 구한다.
             * 1. a[i + 1]
             * 2. b[i + 1]
             *
             * c 수열의 경우 N까지 확인해봐야 하지만 a와 b는 N-1까지만 확인하면 된다.
             * 이를 위해서 조건을 걸어서 인덱스를 보정한다.
             */
            if(i < N - 1) {
                /*
                 * 1-1. 일반적인 케이스
                 * 2-1. 일반적인 케이스
                 */
                a[i + 1] = c[i + 1] + 1;
                b[i + 1] = c[i + 1] + 1;

                // 1-2. 한 팀이 두 지역을 커버
                if(e[0][i] + e[0][i + 1] <= W) {
                    a[i + 1] = Math.min(a[i + 1], b[i] + 1);
                }

                // 2-2. 한 팀이 두 지역을 커버
                if(e[1][i] + e[1][i + 1] <= W) {
                    b[i + 1] = Math.min(b[i + 1], a[i] + 1);
                }

            }

        }

    }

}
