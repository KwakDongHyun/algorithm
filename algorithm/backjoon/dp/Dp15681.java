package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class Dp15681 {

    public static void main(String[] args) {

        String inputData = inputData();
        List<String> results = solveProblem(inputData);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<String> solveProblem(String inputData) {
        return null;
    }

}
