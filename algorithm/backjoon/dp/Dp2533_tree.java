package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

public class Dp2533_tree {

    private static List<Integer>[] graph;
    private static boolean[] visit;
    private static int[][] dp;

    public static void main(String[] args) {

        inputData();
        solveProblem();
        System.out.println(Math.min(dp[1][0], dp[1][1]));

    }

    public static void inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int nodeCnt = Integer.parseInt(br.readLine());
            graph = new List[nodeCnt + 1];

            for(int i = 1; i <= nodeCnt; i++) {
                graph[i] = new LinkedList<Integer>();
            }

            for(int i = 0; i < nodeCnt - 1; i++) {
                StringTokenizer st = new StringTokenizer(br.readLine());
                int source = Integer.parseInt(st.nextToken());
                int destination = Integer.parseInt(st.nextToken());

                graph[source].add(destination);
                graph[destination].add(source);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        int nodeCount = graph.length;
        visit = new boolean[nodeCount + 1];
        dp = new int[nodeCount + 1][2];

        exploreByDepthFirstSearch(1);
    }

    private static void exploreByDepthFirstSearch(int node) {
        visit[node] = true;
        dp[node][0] = 0;
        dp[node][1] = 1;

        for(int child : graph[node]) {
            if(!visit[child]) {
                exploreByDepthFirstSearch(child);
                dp[node][0] += dp[child][1];
                dp[node][1] += Math.min(dp[child][0], dp[child][1]);
            }
        }
    }

}
