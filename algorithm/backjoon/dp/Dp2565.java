package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

public class Dp2565 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에는 두 전봇대 사이의 전깃줄의 개수가 주어진다. 전깃줄의 개수는 100 이하의 자연수이다.
         * 둘째 줄부터 한 줄에 하나씩 전깃줄이 A전봇대와 연결되는 위치의 번호와 B전봇대와 연결되는 위치의 번호가 차례로 주어진다.
         * 위치의 번호는 500 이하의 자연수이고, 같은 위치에 두 개 이상의 전깃줄이 연결될 수 없다.
         *
         * 2. output 조건
         * 첫째 줄에 남아있는 모든 전깃줄이 서로 교차하지 않게 하기 위해 없애야 하는 전깃줄의 최소 개수를 출력한다.
         *
         * 3. 해석
         * 교차 전깃줄을 제거하는 방향으로 문제를 해결하려면 작업이 크게 3가지가 필요하다.
         * 1. 전깃줄을 추가할 때마다 교차 리스트를 만들어야한다.
         * 2. 교차하는 전깃줄이 가장 많은 전깃줄부터 제거한다.
         * 3. 한 전깃줄을 제거하면 관련된 전깃줄 노드를 일일이 찾아가서 노드를 전부 제거해줘야 한다.
         * 더군다나 노드 정보는 어떻게 구성할 것인지도 문제이고, 2,3번 과정도 쉽지가 않다.
         * ---------------------------------------------------------
         * 그래서 오히려 반대로 생각을 하는게 더 편하다.
         * 겹치지 않도록 최대한 많은 전깃줄을 배치하는 것이 메모이제이션 사용하기도 유용하다.
         * 제거의 경우, 일정 구간마다 부분 최적화가 다르기 때문에 DP를 사용할 수가 없다.
         * 설치의 경우에는 새로운 전깃줄을 1개 추가해도 기존의 부분 최적화에 더하기만 하면 되므로 DP에 유리하다.
         * 그렇기 때문에 설치하는 방식으로 문제 가닥을 잡고서 구현할 방법을 고민해보는 것이 문제 해결 방법이다.
         *
         * Example
         * 1.
         * 8
         * 1 8
         * 3 9
         * 2 2
         * 4 1
         * 6 4
         * 10 10
         * 9 7
         * 7 6
         * -------
         * answer
         * 3
         *
         */
        int[][] inputData = inputData();
        System.out.println(solveProblem(inputData));

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int edgeCnt = Integer.parseInt(br.readLine());
            int[][] inputData = new int[edgeCnt][2]; // 0번째 A전봇대 위치, 1번째 B전봇대 위치
            StringTokenizer st;

            for(int i = 0; i < edgeCnt; i++) {
                st = new StringTokenizer(br.readLine());
                inputData[i][0] = Integer.parseInt(st.nextToken());
                inputData[i][1] = Integer.parseInt(st.nextToken());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[][] inputData) {
        // A전봇대 위치를 기준으로 정렬해야 부분 최적화를 하기 편하다.
        Arrays.sort(inputData, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return o1[0] - o2[0];
            }
        });

        int edgeCnt = inputData.length;
        int[] dp = new int[edgeCnt];
        for(int i = 0; i < edgeCnt; i++) {
            /*
             * 무조건 자기 자신을 설치하는 경우, 전깃줄 설치 개수는 1은 보장함.
             * LIS문제와 풀이 방법이 같다. 11053번 참고.
             * 생각해보면 매번 새로운 전깃줄 연결할 때마다 겹치지 않게 설치하려면 이전 모든 케이스별로 확인해봐야 한다.
             * 가장 많이 설치하는 경우는 그때그때 다르기 때문이다.
             */
            dp[i] = 1;
            for(int j = 0; j < i; j++) {
                if(inputData[j][1] < inputData[i][1] && dp[i] < dp[j] + 1) {
                    dp[i] = dp[j] + 1;
                }
            }
        }

        // 전깃줄 최대로 설치를 했다면, 제거한 전깃줄 또한 최소일 것이다. 이건 차를 구하면 바로 나온다.
        return edgeCnt - Arrays.stream(dp).max().getAsInt();

    }

}
