package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Dp9095 {

    public static void main(String[] args) {

        List<Integer> inputDataList = inputDataList();
        List<Integer> countList = new ArrayList<Integer>();

        for(int i = 0; i < inputDataList.size(); i++) {
            countList.add(countCombination(inputDataList.get(i)));
            System.out.println(countList.get(i));
        }

    }

    public static int countCombination(int parameter) {
        int[] dp = new int[parameter+1];

        for(int i = 1; i < parameter+1; i++) {
            if(i == 1) {
                dp[i] = 1;
            } else if(i == 2) {
                dp[i] = 2;
            } else if(i == 3) {
                dp[i] = 4;
            } else {
                dp[i] = dp[i-1] + dp[i-2] + dp[i-3];
            }
        }

        return dp[parameter];
    }

    public static List<Integer> inputDataList() {
        List<Integer> inputDataList = new ArrayList<Integer>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCaseCnt = Integer.parseInt(br.readLine());

            for(int i = 0; i < testCaseCnt; i++) {
                inputDataList.add(Integer.parseInt(br.readLine()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputDataList;
    }

}
