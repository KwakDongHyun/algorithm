package backjoon.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Dp1904 {

    public static void main(String[] args) throws IOException {

        /*
         * 1. input 조건
         * 첫 번째 줄에 자연수 N이 주어진다. (1 ≤ N ≤ 1,000,000)
         *
         * 2. output 조건
         * 첫 번째 줄에 지원이가 만들 수 있는 길이가 N인 모든 binary seriesv개수를 15746으로 나눈 나머지를 출력한다.
         *
         * 3. 해석
         * dp[n] = dp[n-1](1만 붙이는 경우) + dp[n-2](00을 붙이는 경우) (n ≥ 3)
         * 피보나치 수열과 동일한 공식이다.
         *
         */
        int inputData = inputData();
        int numberOfBinarySeriesOfSpecifiedLength = countBinarySeriesOfSpecifiedLength(inputData);
        System.out.println(numberOfBinarySeriesOfSpecifiedLength);
    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static int countBinarySeriesOfSpecifiedLength(int inputData) {
        int[] dp = new int[inputData + 1];
        for(int i = 1; i <= inputData; i++) {
            if(i == 1) {
                dp[1] = 1;
            } else if(i == 2) {
                dp[2] = 2;
            } else {
                dp[i] = (dp[i-1] + dp[i-2]) % 15746;
            }
        }
        return dp[inputData];
    }

}
