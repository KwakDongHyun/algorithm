package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Dp11066 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력은 T개의 테스트 데이터로 이루어져 있는데, T는 입력의 맨 첫 줄에 주어진다.
         * 각 테스트 데이터는 두 개의 행으로 주어지는데, 첫 행에는 소설을 구성하는 장의 수를 나타내는 양의 정수 K (3 ≤ K ≤ 500)가 주어진다.
         * 두 번째 행에는 1장부터 K장까지 수록한 파일의 크기를 나타내는 양의 정수 K개가 주어진다.
         * 파일의 크기는 10,000을 초과하지 않는다.
         *
         * 2. output 조건
         * 각 테스트 데이터마다 정확히 한 행에 출력하는데, 모든 장을 합치는데 필요한 최소비용을 출력한다.
         * 두 개의 파일을 합칠 때 필요한 비용(시간 등)이 두 파일 크기의 합이라고 가정할 때,
         * 최종적인 한 개의 파일을 완성하는데 필요한 총 합의 최소 비용을 계산하시오.
         *
         * 3. 주의사항
         * 정말 많은 의미를 느끼고 생각을 가지게 해준 문제였다. 한 눈에 봐도 딱 알맞게 떨어지게 보이는 부분이 이 문제에서는 없다.
         * 사실 대부분의 문제들은 별도의 공식도 없고, 눈에 띌만큼 이미 정형화된 문제를 알고리즘에서 제시하는 것은 거의 없기 때문에 처음에 접근할 때는 brute force 형태로 접근하는 것이 많다.
         * 그렇기 때문에 경험이 부족하거나 암산이 느려서 시간이 다소 걸리더라도 처음부터 모든 경우에 대해서 하나씩 접근해가면서 풀 수 밖에 없다.
         * 그리고 거기서 공통적으로 묶을 수 있는 부분을 묶고 정리해나가는 식으로 한다.
         * 이런 작업을 반복하다보면 공통된 부분은 하나의 공식으로 묶이게 되고, 최종적으로는 점화식이라는 형태가 정립된다는 것을 알 수 있었다.
         * 이런 문제를 많이 접해보고 풀어서 사고력을 기를 수 있도록 하자.
         *
         * 가끔 고효율을 추구하다 보면 너무 단편적이고 단일 차원으로 문제를 해석하게 된다.
         * 물론 반대로 너무 고차원적으로 생각해서 문제를 여러 문제의 측면에서 동시에 복합적으로 보려고 하다보니까 손을 못대는 경우도 있지만..
         * 다방면에서 바라보되 자세히 접근할 때는 단일 차원 측면에서 빠르게 분석하고 풀이를 완성해나가는 방법을 체화시키자.
         *
         * 4. 해석
         * 초기에는 페이지가 추가될 때마다 점진적으로 어떤식으로 이전에 구했던 페이지 합하는 비용을 메모이제이션해서 사용하는지 알기 어려웠다.
         * 이렇게 생각하게 된 원인은 특별하게 어떤 공식이라는게 반드시 있고, 거기에 맞춰서 메모이제이션을 사용하겠구나라고 생각해서 그렇다.
         * 그래서 i부터 j까지 페이지를 더할 때마다 풀이가 어떻게 되는지를 손으로 풀이하고, 거기서 공통점을 찾는 방식으로 진행했다.
         * 그 결과 점화식을 쓸 수 있을 만큼 기계적으로 기존의 dp값을 사용해서 새로운 값을 구한다는 것을 알았다.
         * 얼핏 복잡해보여도 결국 작게 단위를 나눠서 바라보면 이전에 구한 값을 조합한다는 것을 알 수 있었다.
         *
         * ex) dp[i][i+4] 5페이지 간격의 경우
         * 1-3. dp[i][i+2] + dp[i+3][i+4] + sumAll
         * 페이지 순에 맞춰서 a, b, c, d, e라는 소설 비용이 있다고 가정하자.
         * 아래의 예시 1처럼 양 사이드 페이지를 (a,b)를 묶고, (d,e)를 묶고 나서 c를 (a,b)에 묶은다고 하면,
         * 이건 (a,b,c)를 합치는 과정 중 하나라는 것을 알 수 있다.
         *
         * Example
         * 1.
         * 2
         * 4
         * 40 30 30 50
         * 15
         * 1 21 3 4 5 35 5 4 3 5 98 21 14 17 32
         * -------
         * answer
         * 300
         * 864
         *
         */

        List<int[]> testCases = inputData();
        for(int[] inputData : testCases) {
            System.out.println(solveProblem(inputData));
        }

    }

    public static List<int[]> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCaseCnt = Integer.parseInt(br.readLine());
            List<int[]> testCases = new ArrayList<int[]>();

            int pages;
            int[] testData;
            StringTokenizer st;
            for(int i = 0; i < testCaseCnt; i++) {
                pages = Integer.parseInt(br.readLine());
                testData = new int[pages];

                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < pages; j++) {
                    testData[j] = Integer.parseInt(st.nextToken());
                }
                testCases.add(testData);
            }

            return testCases;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[] inputData) {
        int pages = inputData.length;

        int[] sums = new int[pages + 1]; // 순차적으로 페이지를 더할 때의 파일 용량 합. ex) index 2 = (1 page value + 2 page value)
        int sum = 0;
        for(int i = 0; i <= pages; i++) {
            if(i == 0) {
                sums[i] = 0;
                continue;
            }

            sum += inputData[i - 1];
            sums[i] = sum;
        }

        /*
         * dp[i][j] = i 페이지부터 j 페이지까지 합칠 때 필요한 비용
         * 점화식은 아래와 같다.
         * dp[i][j] = minimum({from k = i to k = j-1} dp[i][k] + dp[k+1][j]) + sumRange(from i page to j page);
         */
        int[][] dp = new int[pages][pages];
        for(int j = 1; j < pages; j++) {
            for(int i = 0; i + j < pages; i++) {
                for(int k = i; k < i + j; k++) {
                    if(k == i) {
                        dp[i][i+j] = dp[i][k] + dp[k+1][i+j];
                    }

                    dp[i][i+j] = Math.min(dp[i][i+j], dp[i][k] + dp[k+1][i+j]);
//                    System.out.println("i : " + i + ", j : " + j + ", k : " + k);
//                    System.out.println("dp[" + i + "][" + (i+j) + "] : " + dp[i][i+j]);
                }

                dp[i][i+j] += sums[i+j+1] - sums[i];
//                System.out.println("final dp[" + i + "][" + (i+j) + "] : " + dp[i][i+j]);
            }

//            System.out.println();
        }

        return dp[0][pages - 1];
    }

}
