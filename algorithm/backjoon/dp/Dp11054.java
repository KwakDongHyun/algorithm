package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Dp11054 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 수열 A의 크기 N이 주어지고, 둘째 줄에는 수열 A를 이루고 있는 Ai가 주어진다.
         * (1 ≤ N ≤ 1,000, 1 ≤ Ai ≤ 1,000)
         *
         * 2. output 조건
         * 첫째 줄에 수열 A의 부분 수열 중에서 가장 긴 바이토닉 수열의 길이를 출력한다.
         * 수열 S가 어떤 수 Sk를 기준으로 S1 < S2 < ... Sk-1 < Sk > Sk+1 > ... SN-1 > SN을 만족한다면, 그 수열을 바이토닉 수열이라고 한다.
         *
         * 3. 해석
         * LIS 계열의 문제이다. 여기서는 LDS 성질까지 포함되어 있다. (* 11053 참고)
         * LIS와 LDS 각각을 구해서 저장한 다음, 두 성질을 합해서 해답을 구해야만 한다.
         * 커졌다가 작아지는 성질이므로 LIS는 1보다 크고 LDS는 1인 인덱스를 먼저 찾는다.
         * 그리고 찾은 인덱스로부터 LDS가 가장 큰 값을 구하여 (LIS + LDS - 1) 식을 통해서 부분 바이토닉 수열의 최장 길이를 구한다.
         * ------------------------------------------------------------
         * 이걸 시각만 좀 다르게 보면 바로 풀리는데.. 나는 너무 곧이곧대로 봤기 때문에 제 시간에 풀지 못했었다.
         * 바이토닉 수열 자체를 DP로 만들지는 못한다. 수열이 새로 추가될 때마다 매번 Sk가 달라질 수 있기 때문이다.
         * LIS, LDS를 구할 때도 같은 근본적인 문제가 있기 때문에 수열이 추가될 때마다 사실상 매번 모든 수열을 완전 탐색을 했어야 했다.
         * 그렇기 때문에 입력된 값까지 LIS, LDS를 모두 구하고 나서 바이토닉 수열을 찾으려고 했다면 문제를 빠르게 해결할 수 있었을거라 생각한다.
         * 한편, 위에서 언급한 시각은 바이토닉 수열에서 Sk를 향해서 모두 IS(증가하는 수열)이므로 배열의 끝에서 LIS를 찾는 관점을 말한거다.
         *
         *Todo
         * DP에서 핵심인 것은 메모이제이션과 어떤식으로 최적화를 해서 다음 상위 문제를 해결해나갈 것인지를 아는 것이라 생각한다.
         * 그리고 무엇보다.. 어느 알고리즘이던지 문제의 본질을 잘 이해하고, 특성을 활용해야 한다고 생각한다.
         *
         * Example
         * 10
         * 1 5 2 1 4 3 4 5 2 1
         * Answer = 7
         *
         */
        int[] inputData = inputData();
        int longestLengthOfBitonicSeries = getLongestLengthOfBitonicSeries(inputData);
        System.out.println(longestLengthOfBitonicSeries);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int seriesLength = Integer.parseInt(br.readLine());
            int[] inputData = new int[seriesLength + 1];
            StringTokenizer st = new StringTokenizer(br.readLine());

            if(seriesLength != st.countTokens()) {
                throw new Exception("입력한 수열 길이와 수열의 개수가 서로 일치하지 않습니다.");
            }

            for(int i = 1; i <= seriesLength; i++) {
                inputData[i] = Integer.parseInt(st.nextToken());
            }
            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getLongestLengthOfBitonicSeries(int[] inputData) {
        int seriesLength = inputData.length - 1;
        int[][] dp = new int[seriesLength + 1][3];

        // 왼쪽에서 오른쪽으로 LIS 구하기
        for(int i = 1; i <= seriesLength; i++) {
            // LIS 길이
            dp[i][0] = 1;
            for(int j = 1; j < i; j++) {
                if(inputData[i] > inputData[j] && dp[i][0] < dp[j][0] + 1) {
                    dp[i][0] = dp[j][0] + 1;
                }
            }
        }

        // LDS이지만, 거꾸로 오른쪽에서 보면 이것 또한 LIS다.
        // 양쪽 LIS를 구하고 더해야 바이토닉 수열을 구하기 쉽다.
        for(int i = seriesLength; i >= 1; i--) {
            // LDS 길이
            dp[i][1] = 1;
            for(int j = seriesLength; j > i; j--) {
                if(inputData[i] > inputData[j] && dp[i][1] < dp[j][1] + 1) {
                    dp[i][1] = dp[j][1] + 1;
                }
            }
        }

        for(int i = 1; i <= seriesLength; i++) {
            dp[i][2] = dp[i][0] + dp[i][1] - 1; // 바이토닉수열
//            System.out.println("dp[" + i + "][0] : " + dp[i][0] + ", dp[" + i + "][1] : " + dp[i][1] + ", dp[" + i + "][2] : " + dp[i][2]);
        }

        return Arrays.stream(dp)
                .mapToInt(data -> data[2])
                .max()
                .getAsInt();
    }

}
