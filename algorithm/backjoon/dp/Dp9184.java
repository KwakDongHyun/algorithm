package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class Dp9184 {

    private static int[][][] dp = new int[21][21][21];

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력은 세 정수 a, b, c로 이루어져 있으며, 한 줄에 하나씩 주어진다. (-50 ≤ a, b, c ≤ 50)
         * 입력의 마지막은 -1 -1 -1로 나타내며, 세 정수가 모두 -1인 경우는 입력의 마지막을 제외하면 없다.
         *
         * 2. output 조건
         * 입력으로 주어진 각각의 a, b, c에 대해서, w(a, b, c)를 출력한다.
         *
         * 3. 해석
         * 메모이제이션을 추가해서 같은 재귀 작업을 진행하지 않도록 하는 것이 핵심이다.
         *
         * Example
         * 1.
         * 1 1 1
         * 2 2 2
         * 10 4 6
         * 50 50 50
         * -1 7 18
         * -1 -1 -1
         * -------
         * answer
         * w(1, 1, 1) = 2
         * w(2, 2, 2) = 4
         * w(10, 4, 6) = 523
         * w(50, 50, 50) = 1048576
         * w(-1, 7, 18) = 1
         *
         */
        List<int[]> inputData = inputData();
        StringBuilder sb = new StringBuilder();
        for(int[] data : inputData) {
            sb.append("w(");
            sb.append(data[0]);
            sb.append(", ");
            sb.append(data[1]);
            sb.append(", ");
            sb.append(data[2]);
            sb.append(") = ");
            sb.append(solveProblem(data[0], data[1], data[2]));
            sb.append("\n");
        }

        System.out.println(sb);

    }

    public static List<int[]> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            boolean complete = false;
            List<int[]> inputData = new LinkedList<int[]>();
            StringTokenizer st;
            int a, b, c;

            while(!complete) {
                st = new StringTokenizer(br.readLine());
                a = Integer.parseInt(st.nextToken());
                b = Integer.parseInt(st.nextToken());
                c = Integer.parseInt(st.nextToken());

                if(a == -1 && b == -1 && c == -1) {
                    break;
                }

                int[] data = {a, b, c};
                inputData.add(data);
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int a, int b, int c) {

        /*
           if a <= 0 or b <= 0 or c <= 0, then w(a, b, c) returns:
               1

           if a > 20 or b > 20 or c > 20, then w(a, b, c) returns:
               w(20, 20, 20)

           커트라인 로직
           ------------------------------------

           if a < b and b < c, then w(a, b, c) returns:
               w(a, b, c-1) + w(a, b-1, c-1) - w(a, b-1, c)

           otherwise it returns:
               w(a-1, b, c) + w(a-1, b-1, c) + w(a-1, b, c-1) - w(a-1, b-1, c-1)
         */
        if(a <= 0 || b <= 0 || c <= 0) {
            return 1;
        }

        if(a > 20 || b > 20 || c > 20) {
            return solveProblem(20, 20, 20);
        }

        if(dp[a][b][c] != 0) {
            return dp[a][b][c];
        }

        if(a < b && b < c) {
            dp[a][b][c] = solveProblem(a, b, c-1) + solveProblem(a, b-1, c-1) - solveProblem(a, b-1, c);
            return dp[a][b][c];
        }

        dp[a][b][c] = solveProblem(a-1, b, c) + solveProblem(a-1, b-1, c) + solveProblem(a-1, b, c-1) - solveProblem(a-1, b-1, c-1);
        return dp[a][b][c];

    }

}
