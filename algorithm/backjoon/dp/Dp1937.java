package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Dp1937 {

    private static int[][] inputData;
    private static int[][] dp;
    private static int mapSize;

    private static int[] moveX = {0, 1, 0, -1};
    private static int[] moveY = {-1, 0, 1, 0};

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 대나무 숲의 크기 n(1 ≤ n ≤ 500)이 주어진다. 그리고 둘째 줄부터 n+1번째 줄까지 대나무 숲의 정보가 주어진다.
         * 대나무 숲의 정보는 공백을 사이로 두고 각 지역의 대나무의 양이 정수 값으로 주어진다.
         * 대나무의 양은 1,000,000보다 작거나 같은 자연수이다.
         *
         * 2. output 조건
         * 첫째 줄에는 판다가 이동할 수 있는 칸의 수의 최댓값을 출력한다.
         *
         * 3. 주의사항
         * 기존에 풀었던 경로탐색 + DP 문제와 비슷하며서 다른 점이 있다.
         * 전에는 경로의 수를 찾는 것이었기 때문에 이전에 탐색했는지 유무만 저장해두면 재방문시 +1만 해서 반환하면 됐었다.
         * 단순히 경로가 있는지 없는지 확인만 하면 되기 때문이다.
         * 그러나 이번에는 다르다. 경로 유무에 더해서 해당 경로에는 몇 개의 지점이 있는지 알아야 한다.
         * 다른 DP 문제들처럼 이전에 단순히 길이를 묻듯이 경로의 수만 물었다면, 이번에는 그 경로를 구성하는 항목에 대해서 알고 있어야 하는 것이다.
         *
         * 4. 해석
         * 풀고 나서 논리적으로 분석 후에 알게 된 사실이지만 전에 풀었던 경로의 수와 사실 같은 구성이라고 보면 된다.
         * 목표지점에 대한 정보가 없고, 약간의 처리가 추가된 부분과 탐색 완료후 return 지점을 함수의 맨 마지막에 배치한 것 빼고는.
         *
         * 경로의 수 vs 경로에 있는 지점의 수를 구하는 것에는 2가지 차이점이 있다.
         *  1. 도중에 멈춰야할 목표 지점을 아는가 모르는가
         *  2. 단순히 해당 경로를 방문했다는 것을 return vs 목표지점까지 거쳐가는 지점의 개수를 return
         *
         * 처음에는 가장 작은 수부터 순차적으로 +1씩 기록하는 방식을 했다.
         * 이렇게 하면 새롭게 더 작은 값이 등장할 시 다시 한 번 재탐색을 하면서 값을 갱신해줘야 했다.
         * 최악의 경우 계속해서 더 작은 지점이 나타나는, 역순 방식으로 데이터가 있을 경우 사실상 메모이제이션이 없는 것과 다를바 없는
         * 수행 시간이 걸린다는 것이었다.
         *
         * 그렇기 때문에 반대로 목표 지점을 1로 지정해두고, 재귀에서 빠져나올때 마다 +1씩 누적해서 저장하도록 설계했다.
         * 이렇게 설계하면 좋은 점은 더 작은 시작점이 등장해도 주변에 저장된 값에 +1만 해주면 된다.
         * 더 작은 지점이라는 말은 새로운 시작점일 뿐이고, 나머지 거쳐가는 지점은 기존 경로와 동일하다.
         * 즉, 메모이제이션을 활용할 수 있다는 것이고, worst case로 데이터가 역순으로 등장해도 O(1)만큼의 수행 시간만 걸린다.
         *
         * 물론 이렇게 할 경우, 목표지점에 도달해서 함수의 마지막 구문까지 도착한 것인지 아니면 정상적으로 경로탐색을 다 끝내고 나서
         * 마지막 구문까지 도착한 것인기 구분해줘야만 한다.
         * 또한 같은 중간 지점을 경유하는 2개 이상의 경로가 존재할 수 있기 때문에, 이럴 때는 가장 큰 값이 저장되도록 처리해줘야만 한다.
         * 어차피 우리가 알고 싶은건 경로 내의 최다 지점의 개수이기 때문이다.
         *
         * Example
         * 1.
         * 4
         * 14 9 12 10
         * 1 11 5 4
         * 7 15 2 13
         * 6 3 16 8
         * -------
         * answer
         * 4
         *
         */

        inputData();
        int result = solveProblem();
        System.out.println(result);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            mapSize = Integer.parseInt(br.readLine());
            inputData = new int[mapSize][mapSize];
            dp = new int[mapSize][mapSize];

            StringTokenizer st;
            for(int i = 0; i < mapSize; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < mapSize; j++) {
                    inputData[i][j] = Integer.parseInt(st.nextToken());
//                    dp[i][j] = -1;
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem() {
        for(int i = 0; i < mapSize; i++) {
            for(int j = 0; j < mapSize; j++) {
                exploreByDepthFirstSearch(i, j);
            }
        }

        return Arrays.stream(dp).flatMapToInt(arrays -> Arrays.stream(arrays))
                .max()
                .getAsInt();

        // 또는 아래와 같이 사용
//        return Arrays.stream(dp).flatMapToInt(Arrays::stream)
//                .max()
//                .getAsInt();
    }

    private static int exploreByDepthFirstSearch(int y, int x) {

        if(dp[y][x] != 0) {
            /*
             * 경로 재탐색을 할 필요가 없는 상황에서 이미 이전에 탐색을 해봤다면 넘어가야한다.
             * 이미 최장 길이만큼 탐색하고 난 결과가 저장되어 있고, 도중에 바뀌지는 않기 때문이다.
             */
            return dp[y][x];
        } else {
            int nextX, nextY;
            for(int i = 0; i < 4; i++) {
                nextX = x + moveX[i];
                nextY = y + moveY[i];

                if(nextX < 0 || nextX >= mapSize || nextY < 0 || nextY >= mapSize) {
                    continue;
                }
                if(inputData[nextY][nextX] <= inputData[y][x]) {
                    continue;
                }

                int exploreResult = exploreByDepthFirstSearch(nextY, nextX);
                if(exploreResult + 1 > dp[y][x]) {
                    dp[y][x] = exploreResult + 1;
                }

//            System.out.println("다음 탐색 지점 dp[" + nextY + "][" + nextX + "] : " + dp[nextY][nextX]);
//            System.out.println();

            }
        }

        /*
         * 여기까지 왔다는 것은 더 이상 갈 곳이 없다는 것이다.
         * 즉, 각각의 지점에서 탐색했을 때 방문할 수 있는 마지막 지점이라는 말이다.
         * 마지막 지점에서 더는 방문할 곳이 없으므로 1을 return한다.
         * 단, 정상적으로 경로 탐색을 다 하고난 이후에도 도착하므로 그때와 구분지어서 처리해야한다.
         */
        if(dp[y][x] == 0) {
            dp[y][x] = 1;
        }

        return dp[y][x];

    }

}
