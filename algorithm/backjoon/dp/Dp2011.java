package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class Dp2011 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 5000자리 이하의 암호가 주어진다. 암호는 숫자로 이루어져 있다.
         * (1 ≤ 암호문 length ≤ 100)
         *
         * 2. output 조건
         * 나올 수 있는 해석의 가짓수를 구하시오. 정답이 매우 클 수 있으므로, 1000000으로 나눈 나머지를 출력한다.
         * 암호가 잘못되어 암호를 해석할 수 없는 경우에는 0을 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 처음에 문제를 풀 때 단계적으로 생각하려고 했다.
         * 뒤에 암호문자가 1개씩 붙을 때마다 기존에 계산한 암호문 형식과 어떤 상관관계가 있는지를 파악하는데 집중했다.
         * ---------------------------------------------
         * 두 자리수를 만들 때 1<= <=26 만 생각하다 보니까 놓친 부분이 있었다.
         * 앞 자리 수가 0일 경우에도 제외시켜줘야 했던 것이다...
         * 이런 세세한 부분을 놓치지 않으려면.. 시간이 걸리더라도 역시 다양한 케이스의 데이터에 대해서 빠르게
         * 검토하는 수 밖에 없는 것 같다.
         *
         * 이런 문자를 다루는 문제를 계속해서 연습해야만 한다.
         *
         * Example
         * 1.
         * 25114
         * ----------------------
         * answer
         * 6
         *
         * 2.
         * 1111111111
         * ----------------------
         * answer
         * 89
         *
         */

        String inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String inputData) {
        int length = inputData.length();
        char[] chars = inputData.toCharArray();
        int[] dp = new int[length + 1];
        dp[0] = 1;
        int denominator = 1000_000;

        int curNum, prevNum, value;
        for(int i = 1; i <= length; i++) {
            curNum = inputData.charAt(i-1) - '0';
            /*
             * 0은 대응되는 값이 10, 20 두 개 말고는 없다.
             * 따라서 0은 건너뛰어야 한다.
             */
            if(curNum > 0 && curNum < 10) {
                dp[i] = dp[i-1];
                dp[i] %= denominator;
            }

            if(i == 1) { // 첫 숫자는 이후에 연산을 건너뛴다.
                continue;
            }

            prevNum = inputData.charAt(i-2) - '0';
            value = prevNum * 10 + curNum;

            if(value > 9 && value < 27) {
                dp[i] += dp[i-2];
                dp[i] %= denominator;
            }
        }

        return dp[length];
    }

}
