package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp17070 {

    private static int[][] inputData;
    private static int[][] dp;
    private static int mapSize;

    private static int[] moveX = {1, 0, 1};
    private static int[] moveY = {0, 1, 1};

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 집의 크기 N(3 ≤ N ≤ 16)이 주어진다. 둘째 줄부터 N개의 줄에는 집의 상태가 주어진다.
         * 빈 칸은 0, 벽은 1로 주어진다. (1, 1)과 (1, 2)는 항상 빈 칸이다.
         * 파이프를 밀 수 있는 방향은 총 3가지가 있으며, →, ↘, ↓ 방향이다. 파이프는 밀면서 회전시킬 수 있다.
         * 회전은 45도만 회전시킬 수 있으며, 미는 방향은 오른쪽, 아래, 또는 오른쪽 아래 대각선 방향이어야 한다.
         *
         * 2. output 조건
         * 첫째 줄에 파이프의 한쪽 끝을 (N, N)으로 이동시키는 방법의 수를 출력한다. 이동시킬 수 없는 경우에는 0을 출력한다.
         * 방법의 수는 항상 1,000,000보다 작거나 같다.
         *
         * 3. 주의사항
         * 회전 및 이동은 첨부된 그림을 보는게 낫다.. 설명하기가 좀 그렇다.
         * 가로는 시계방향 45도 회전. 세로는 반시계방향 45도 회전, 대각선은 어느 방향이던 45도 회전이 가능하다.
         *
         * 4. 해석
         * DP 1520과 매우 비슷한 문제라고 생각한다.
         * 경우의 수는 모든 경우에 대해서 일일히 찾아보는 방법 밖에 없고, 재귀를 반드시 사용할 수 밖에 없는 문제인 것 같다.
         * 연산에 필요한 시간을 줄여주는 메모이제이션을 사용하기에도 좋다.
         * 특히 경로 탐색 경우는 어차피 가지 못하는 길 혹은 특정 지점에서 탐색을 완료한  곳은 재차 탐색할 필요가 없기도 해서 편리하다.
         * ----------------------------------------------
         * dp를 적용하려고 했는데 거지같이도 안되네.. 젠장..
         * 이게 점으로 이동하는게 아니라 직선의 형태라서 그렇다.
         * dp로 풀려면 기존 문제처럼 점이 아닌 직선의 형태로 메모이제이션을 해야하는데, 그게 쉽지가 않다.
         * 주어진 제한 시간 안에 풀어야 하기 때문에 사실 그렇게까지 고 퀄리티로 문제를 바로 짜기는 어렵다.
         * 반복 문제로 아주 숙달이 된 상황이라면 모를까.. 냉정하게 말해서 거의 불가능에 가깝다고 생각한다.
         *
         * 한편, 다른 사람 풀이를 봤는데 역시나 파이프 놓는 타입에 따라서 처리를 하는 것으로 되어있다...
         * 이런 하드 코딩식 풀이를 좋아하지 않아서 시도하지 않았던 문제였는데.. 문제가 썩 좋은 것 같지는 않다.
         * 3차원 배열로 풀겠다는 건데.. 좀만 고차원으로 문제가 간다면 이런식으로 풀 수가 있으려나..
         *
         * Example
         * 1.
         * 4
         * 0 0 0 0
         * 0 0 0 0
         * 0 0 0 0
         * 0 0 0 0
         * -------
         * answer
         * 3
         *
         * 2.
         * 5
         * 0 0 1 0 0
         * 0 0 0 0 0
         * 0 0 0 0 0
         * 0 0 0 0 0
         * 0 0 0 0 0
         * --------
         * answer
         * 0
         *
         * 3.
         * 6
         * 0 0 0 0 0 0
         * 0 1 0 0 0 0
         * 0 0 0 0 0 0
         * 0 0 0 0 0 0
         * 0 0 0 0 0 0
         * 0 0 0 0 0 0
         * --------
         * answer
         * 13
         *
         */
        inputData();
        solveProblem(0, 0, 0, 1);
        System.out.println(dp[mapSize-1][mapSize-1]);

    }

    public static void inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            mapSize = Integer.parseInt(br.readLine());
            inputData = new int[mapSize][mapSize];
            dp = new int[mapSize][mapSize];

            StringTokenizer st;
            for(int i = 0; i < mapSize; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < mapSize; j++) {
                    inputData[i][j] = Integer.parseInt(st.nextToken());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem(int r1, int c1, int r2, int c2) {
        if(r2 == mapSize-1 && c2 == mapSize-1) {
            dp[r2][c2] += 1;
            return;
        }

        int pipePositionY = r2 - r1;
        int pipePositionX = c2 - c1;
        int nextX, nextY;
        if((pipePositionX ^ pipePositionY) == 0) {
            // 대각선 위치
//            System.out.println("대각선 위치 : (" + r1 + ", " + c1 + "), (" + r2 + ", " + c2 + ")");
            for(int i = 0; i < 3; i++) {
                nextX = c2 + moveX[i];
                nextY = r2 + moveY[i];

                if(nextX < 0 || nextX >= mapSize || nextY < 0 || nextY >= mapSize) {
                    continue;
                }

                if(i == 2) {
                    if(inputData[nextY][nextX] == 1 || inputData[nextY][nextX - 1] == 1 || inputData[nextY - 1][nextX] == 1) {
                        continue;
                    }
                } else {
                    if(inputData[nextY][nextX] != 0) {
                        continue;
                    }
                }

//                    System.out.println("다음 탐색 파이프 : (" + (r1 + 1) + ", " + (c1 + 1) + "), (" + nextY + ", " + nextX + ")");
                solveProblem(r1 + 1, c1 + 1, nextY, nextX);
            }
        } else {
            if(pipePositionX == 1) {
                // 가로 위치
//                System.out.println("가로 위치 : (" + r1 + ", " + c1 + "), (" + r2 + ", " + c2 + ")");
                for(int i = 0; i < 3; i += 2) {
                    nextX = c2 + moveX[i];
                    nextY = r2 + moveY[i];
//                        System.out.println("nextY : " + nextY + ", nextX : " + nextX);

                    if(nextX < 0 || nextX >= mapSize || nextY < 0 || nextY >= mapSize) {
                        continue;
                    }

                    if(i == 2) {
                        if(inputData[nextY][nextX] == 1 || inputData[nextY][nextX - 1] == 1 || inputData[nextY - 1][nextX] == 1) {
                            continue;
                        }
                    } else {
                        if(inputData[nextY][nextX] != 0) {
                            continue;
                        }
                    }

//                        System.out.println("다음 탐색 파이프 : (" + r1 + ", " + (c1 + 1) + "), (" + nextY + ", " + nextX + ")");
                    solveProblem(r1, c1 + 1, nextY, nextX);
                }
            } else {
                // 세로 위치
//                System.out.println("세로 위치 : (" + r1 + ", " + c1 + "), (" + r2 + ", " + c2 + ")");
                for(int i = 1; i < 3; i++) {
                    nextX = c2 + moveX[i];
                    nextY = r2 + moveY[i];

                    if(nextX < 0 || nextX >= mapSize || nextY < 0 || nextY >= mapSize) {
                        continue;
                    }

                    if(i == 2) {
                        if(inputData[nextY][nextX] == 1 || inputData[nextY][nextX - 1] == 1 || inputData[nextY - 1][nextX] == 1) {
                            continue;
                        }
                    } else {
                        if(inputData[nextY][nextX] != 0) {
                            continue;
                        }
                    }

//                        System.out.println("다음 탐색 파이프 : (" + (r1 + 1) + ", " + c1 + "), (" + nextY + ", " + nextX + ")");
                    solveProblem(r1 + 1, c1, nextY, nextX);
                }
            }
        }

    }

}
