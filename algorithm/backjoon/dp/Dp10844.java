package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Dp10844 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 N이 주어진다. (1 ≤ n ≤ 100)
         *
         * 2. output 조건
         * 45656이란 수를 보자.
         * 이 수는 인접한 모든 자리의 차이가 1이다. 이런 수를 계단 수라고 한다.
         * N이 주어질 때, 길이가 N인 계단 수가 총 몇 개 있는지 구한 후에 1,000,000,000으로 나눈 나머지를 출력한다.
         *
         * 3. 주의사항
         * n x 2 문제처럼 나머지가 들어갔다는 것은 overflow가 발생할 수 있는 수가 있다는 거다.
         *
         * 4. 해석
         * 숫자가 0으로 시작하면 안되므로 dp[1] = 9다.
         * 그래프로 펼쳐서 생각을 해보면, 마치 파스칼의 삼각형 또는 트리구조처럼 값이 형성이 된다.
         * First Level을 제외하고 Second Level부터는 0 ~ 9 까지 node가 가로로 늘어져있다고 생각해봐라. (마치 인공지능 신경망처럼)
         * 그러면 문제가 훨씬 풀기 쉬워진다.
         *
         * 예를 들어 끝자리 수가 2인 길이가 4인 계단수의 경우의 수는 아래의 경우를 포함한다.
         * (xx12) 경우의 수 + (xx32) 경우의 수.
         *
         */
        int inputData = inputData();
        long stepNumberOfCases = getStepNumberOfCases(inputData);
        System.out.println(stepNumberOfCases);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static long getStepNumberOfCases(int inputData) {
        long[] dp = new long[inputData + 1]; // 길이가 index만큼 일 때의 계단 수의 경우의 수
        long[] stepNumberOfCurrentLevel = new long[10];
        long[] previousStepNumArr = null;

        for(int i = 1; i <= inputData; i++) {
            if(i == 1) {
                for(int j = 1; j < 10; j++) {
                    stepNumberOfCurrentLevel[j] = 1;
                }
            } else {
                stepNumberOfCurrentLevel[0] = previousStepNumArr[1];
                stepNumberOfCurrentLevel[9] = previousStepNumArr[8];
                for(int j = 1; j < 9; j++) {
                    stepNumberOfCurrentLevel[j] = previousStepNumArr[j-1] + previousStepNumArr[j+1];
                }
            }

            for(int j = 0; j < 10; j++) {
                stepNumberOfCurrentLevel[j] = stepNumberOfCurrentLevel[j] % 1_000_000_000;
            }
            previousStepNumArr = stepNumberOfCurrentLevel.clone();
            dp[i] = Arrays.stream(stepNumberOfCurrentLevel)
                    .sum() % 1_000_000_000;

            System.out.println("dp[" + i + "] : " + dp[i]);
            for(int j = 0; j < 10; j++) {
                if(j != 9) {
                    System.out.print(stepNumberOfCurrentLevel[j] + ", ");
                } else {
                    System.out.println(stepNumberOfCurrentLevel[j]);
                }
            }
        }
        return dp[inputData];
    }

}
