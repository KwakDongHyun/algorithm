package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp1463 {

    public static void main(String[] args) {
        String input = inputData();
//        System.out.println("Integer : " + input);

        int data = Integer.parseInt(input);
        int dp[] = new int[data+1];
        dp[0] = 0;
        dp[1] = 0;

        /*
         * min(d[i-1], d[i/2], d[i/3])을 구하면 된다.
         * 나머지나 나눗셈 연산을 할 때는 if-else 문법을 절대로 사용하면 안된다.
         * 2와 3의 공배수에 대해서 연산을 한다고 가정하면, 먼저 선언한 연산만 진행하고 뒤의 연산은 진행하지 않게 된다.
         */
        for(int i = 2; i < data+1; i++) {
            dp[i] = dp[i-1] + 1;
            if(i % 2 == 0) {
                dp[i] = Math.min(dp[i], dp[i/2] + 1);
            }

            if(i % 3 == 0) {
                dp[i] = Math.min(dp[i], dp[i/3] + 1);
            }

//            System.out.println("dp[" + i + "] : " + dp[i]);
        }

//        System.out.println("dp[" + data + "] : " + dp[data]);
        System.out.println(dp[data]);

    }

    public static String inputData() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String inputData = "";

        try {
//            System.out.print("Enter the data : ");
            inputData = br.readLine();
            br.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return inputData;
    }

}
