package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Dp11055 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 수열 A의 크기 N (1 ≤ N ≤ 1,000)이 주어진다.
         * 둘째 줄에는 수열 A를 이루고 있는 Ai가 주어진다. (1 ≤ Ai ≤ 1,000)
         *
         * 2. output 조건
         * 첫째 줄에 수열 A의 합이 가장 큰 증가하는 부분 수열의 합을 출력한다.
         *
         * 3. 해석
         * 수열 자체는 구하기 여전히 어렵다. 하지만 LIS의 Sum은 구하기 쉽다.
         * 11053 문제에서 길이가 아니라 합을 기준으로 구분하는 로직으로 대체하면 된다.
         * 그리고 나서 LIS sum 중에서 가장 큰 값을 return하면 된다.
         *
         * Example
         * 1.
         * 10
         * 1 100 2 50 60 3 5 6 7 8
         * -------
         * answer
         * 113
         *
         * 2.
         * 7
         * 1 20 21 4 7 100 20033
         */
        int[] inputData = inputData();
        int maxLISSum = getMaxLISSum(inputData);
        System.out.println(maxLISSum);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int seriesSize = Integer.parseInt(br.readLine());
            int[] inputData = new int[seriesSize + 1];
            StringTokenizer st = new StringTokenizer(br.readLine());
            for(int i = 1; i <= seriesSize; i++) {
                inputData[i] = Integer.parseInt(st.nextToken());
            }
            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getMaxLISSum(int[] inputData) {
        int seriesSize = inputData.length - 1;
        int[] dp = new int[seriesSize + 1];
        for(int i = 1; i <= seriesSize; i++) {
            dp[i] = inputData[i];
            for(int j = 1; j < i; j++) {
                if(inputData[i] > inputData[j] && dp[i] < dp[j] + inputData[i]) {
                    dp[i] = dp[j] + inputData[i];
                }
            }
            System.out.println(dp[i]);
        }
        return Arrays.stream(dp)
                .max()
                .getAsInt();
    }

}
