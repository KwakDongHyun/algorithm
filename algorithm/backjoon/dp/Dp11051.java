package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp11051 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 N과 K가 주어진다. (1 ≤ N ≤ 1,000), (1 ≤ K ≤ N)
         *
         * 2. output 조건
         * (N Choose K)를 10,007로 나눈 나머지를 출력한다.
         *
         * 3. 해석
         * Combination 계열 문제. 이항정리라고는 하지만 결국 조합 공식을 사용해야 하므로 dp식으로 풀어야함.
         * 흔히 파스칼의 삼각형을 떠올리면 된다. nCr = n-1Cr-1 + n-1Cr;
         *
         * Example
         * 1.
         * 5 2
         * -------
         * answer
         * 10
         *
         */
        int[] inputData = inputData();
        int combination = getCombination(inputData);
        System.out.println(combination);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int[] inputData = {Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken())};
            return inputData;
        } catch (Exception e) {
            return null;
        }
    }

    public static int getCombination(int[] inputData) {
        int n = inputData[0];
        int r = inputData[1];
        int[][] dp = new int[n + 1][r + 1];

        for(int i = 1; i <= n; i++) {
            for(int j = 0; j <= i && j <= r; j++) {
                if(j == 0 || i == j) {
                    dp[i][j] = 1;
                } else {
                    dp[i][j] = (dp[i-1][j-1] + dp[i-1][j]) % 10_007;
                }
            }
        }

        return dp[n][r];
    }

}
