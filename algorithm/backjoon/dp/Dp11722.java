package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Dp11722 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 수열 A의 크기 N (1 ≤ N ≤ 1,000)이 주어진다.
         * 둘째 줄에는 수열 A를 이루고 있는 Ai가 주어진다. (1 ≤ Ai ≤ 1,000)
         *
         * 2. output 조건
         * 첫째 줄에 수열 A의 가장 긴 감소하는 부분 수열의 길이를 출력한다.
         *
         * 3. 해석
         * LIS와 똑같은 방식이지만, 수를 체크하는 곳만 작게 체크하면 된다.
         * 기본적인 로직은 똑같다.
         *
         * Example
         * 1.
         * 6
         * 10 30 10 20 20 10
         * -------
         * answer
         * 3
         *
         */
        int[] inputData = inputData();
        int longestDecreasingSequenceSize = getLongestDecreasingSequenceSize(inputData);
        System.out.println(longestDecreasingSequenceSize);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int seriesLength = Integer.parseInt(br.readLine());
            int[] inputData = new int[seriesLength + 1];
            StringTokenizer st = new StringTokenizer(br.readLine());

            for(int i = 1; i <= seriesLength; i++) {
                inputData[i] = Integer.parseInt(st.nextToken());
            }
            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getLongestDecreasingSequenceSize(int[] inputData) {
        int seriesLength = inputData.length - 1;
        int[] dp = new int[seriesLength + 1];
        for(int i = 1; i <= seriesLength; i++) {
            dp[i] = 1;
            for(int j = 1; j < i; j++) {
                if(inputData[i] < inputData[j] && dp[i] < dp[j] + 1) {
                    dp[i] = dp[j] + 1;
                }
            }
        }

        return Arrays.stream(dp)
                .max()
                .getAsInt();
    }

}
