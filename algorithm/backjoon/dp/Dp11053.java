package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Dp11053 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 수열 A의 크기 N (1 ≤ N ≤ 1,000)이 주어진다.
         * 둘째 줄에는 수열 A를 이루고 있는 Ai가 주어진다. (1 ≤ Ai ≤ 1,000)
         *
         * 2. output 조건
         * 수열 A의 가장 긴 증가하는 부분 수열의 길이를 출력한다.
         *
         * 3. 해석
         * 해석학의 기본 개념으로 생각했을 때, 수열은 작은 수열들의 집합으로 구성된다는 것을 알 수 있다.
         * LIS도 수열이기 때문에, 작은 수열들의 집합으로 나눌 수 있다.
         * 그렇다면 입력받은 수열을 탐색하면서 Increasing Sequence를 모두 찾아내는 것부터 시작하면 되지 않겠는가?
         * 그런 다음 Increasing Sequence를 n^2으로 순회하면서 최대한 길게 이어붙이면 그게 LIS라고 생각한다.
         * LIS를 만드는 과정이 이 문제에서 가장 창의적인 부분이고 핵심이라고 생각한다. (성능, 속도 모두)
         *
         * 이 방법으로 하면 실패한다.
         * LIS 수열 자체를 만드는 생각으로 문제를 해결하려고 하면 안된다.
         * LIS를 만드는 것과 길이만 구하는 건 전혀 다른 문제이고, LIS를 찾는게 훨씬 어렵다.
         * -------------------------------------------------------
         * 이런 단순한 문제는 단순하게 생각하면 풀기 쉽다.
         * 수열 자체를 자꾸 의식하다 보니까 정작 본 문제를 풀지 못한거라고 생각한다.
         * 다시 한 번 강조하지만 LIS 자체를 구하는 것은 훨씬 어려운 문제다.
         *
         */
        int[] inputDataList = inputDataList();
        /*Todo.
         * n^2 방법. 이 방법으로 하면 메모리 사용량과 조회 시간이 너무 오래 걸린다.
         */
        int longestIncreasingSequenceSize = getLongestIncreasingSequenceSize_old(inputDataList);
        System.out.println(longestIncreasingSequenceSize);
        /*
        int longestIncreasingSequenceSize = getLongestIncreasingSequenceSize1(inputDataList);
        System.out.println("LIS : " + longestIncreasingSequenceSize);
        System.out.println(longestIncreasingSequenceSize);
        */

    }

    public static int[] inputDataList() {
        int[] inputDataList = null;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int sequenceSize = Integer.parseInt(br.readLine());
            inputDataList = new int[sequenceSize + 1];

            String[] sequence = br.readLine().split(" ");
            sequenceSize = sequence.length;
            for(int i = 0; i < sequenceSize; i++) {
                inputDataList[i + 1] = Integer.parseInt(sequence[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputDataList;
    }

    /*Todo.
     * n^2 방법. 이 방법으로 하면 메모리 사용량과 조회 시간이 너무 오래 걸린다.
     * 그리고 주의해야할 것은 길이만 구하는 문제라서... 메모리랑 수행 시간에 제약이 좀 빡세다.
     * 길이만 구하는 거라면 솔직히 굳이 maxValue를 구할 필요는 없다.
     */
    public static int getLongestIncreasingSequenceSize_old(int[] inputDataList) {
        int sequenceSize = inputDataList.length - 1;
        int[] dp = new int[inputDataList.length];

        for(int i = 1; i <= sequenceSize; i++) {
            // LIS 구성을 못할 경우, default로 1을 넣기 위함.
            dp[i] = 1;
            for(int j = 1; j <= i-1; j++) {
                /*
                 * 단순하게 생각하자. 현재 수열의 값이 이전 수열보다 큰 경우를 먼저 찾는다.
                 * 그리고 그런 경우 중에서 IS가 가장 긴 경우를 찾기만 하면 LIS의 길이를 구할 수 있지 않을까?
                 * 어차피 같거나 작을 경우에는 1로 is 길이가 들어갈테니 연산에 문제도 없을거다.
                 */
                if(inputDataList[i] > inputDataList[j] && dp[j]+1 > dp[i]) {
                    dp[i] = dp[j] + 1;
                }
            }
        }

        return Arrays.stream(dp)
                .max()
                .getAsInt();
    }

    /*
     *Todo
     * nlog(n)으로 만들기 위해서 다른 방법을 찾음.
     * 수열의 성질 2가지만 충족시키기만 하면 "길이"만 구하는건 문제가 없다.
     * 즉 수열의 항목이 변경되는 것은 신경 안써도 된다는 장점을 생각하면서 풀면.. 답이 보인다.
     * 이건 추후에 난이도가 오를 때 다른 문제에서 접하게 될 거다.
     *
     * 이분탐색을 활용하면 쉽게 이해할 수 있을거다.
     * 주의할 건, LIS 자체를 구하는 것과 LIS의 길이를 구하는 것은 전혀 다른 문제다.
     * 수열을 구하는게 훨씬 어렵고 복잡하다.
     */

}
