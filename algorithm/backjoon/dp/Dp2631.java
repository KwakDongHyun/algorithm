package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class Dp2631 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에는 아이들의 수 N이 주어진다.
         * 둘째 줄부터는 1부터 N까지의 숫자가 한 줄에 하나씩 주어진다. (2 ≤ N ≤ 200)
         *
         * 2. output 조건ㅣ
         * 첫째 줄에는 번호 순서대로 줄을 세우는데 옮겨지는 아이들의 최소 수를 출력한다.
         *
         * 3. 주의사항
         * 위치 이동을 할 때는 한 숫자를 어느 숫자의 뒤(1차원 상에서는 수의 오른쪽)로 이동하는 형식이다.
         *
         * 4. 해석
         * 풀이법은 그럴싸하게 만들었지만, 시간 내에 풀이법을 구현하지 못했다.
         * ---------------------------------------
         * LIS(Longest Increasing Sequence) 원리를 응용하면 쉽게 풀 수 있다.
         * 결국 LIS를 구한 다음에 거기에 속하지 않은 번호들만 LIS에서 자신이 있어야할 위치로 옮기기만 하면 된다.
         * 아.. 이런식으로 LIS를 응용할 수도 있구나...
         *
         * LIS란 원소가 n개인 배열의 일부 원소를 골라내서 만든 부분 수열 중,
         * 각 원소가 이전 원소보다 크다는 조건을 만족하고, 그 길이가 최대인 부분 수열을 최장 증가 부분 수열이라고 한다.
         * LIS의 경우 DP와 이분탐색 2가지 방법으로 구할 수 있지만 이분탐색의 성능이 매우 좋다.
         * 수열이기 때문에 뒤에 더 작은 숫자가 나오면 이분탐색으로 적절할 위치를 찾아서 새롭게 넣거나 기존 숫자보다 작으면
         * 그 위치에 덮어씌우는 형식으로 완성해나간다.
         *
         * Example
         * 1.
         * 7
         * 3
         * 7
         * 5
         * 2
         * 6
         * 1
         * 4
         * ----------------------
         * answer
         * 4
         *
         */

        int[] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int childrenCount = Integer.parseInt(br.readLine());
            int[] inputData = new int[childrenCount];

            for(int i = 0; i < childrenCount; i++) {
                inputData[i] = Integer.parseInt(br.readLine());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[] inputData) {
        int childrenCount = inputData.length;

        int lengthOfLIS = getLengthOfLIS(inputData);
//        System.out.println("childrenCount : " + childrenCount + ", lengthOfLIS : " + lengthOfLIS);
        return childrenCount - lengthOfLIS;
    }

    private static int getLengthOfLIS(int[] arr) {
        int length = arr.length;
        int[] LIS = new int[length];

        LIS[0] = arr[0];
        int j = 0;
        int i = 1;

        while(i < length) {
            if(LIS[j] < arr[i]) {
                LIS[j + 1] = arr[i];
                j += 1;
            } else {
                int index = binarySearch(LIS, 0, j, arr[i]);
                LIS[index] = arr[i];
            }

            i += 1;
        }

        return j + 1;
    }

    private static int binarySearch(int[] arr, int left, int right, int target) {
        int mid;

        while(left < right) {
            mid = (left + right) / 2;

            if(arr[mid] < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }

        return right;
    }

}
