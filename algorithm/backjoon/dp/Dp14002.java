package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Stack;
import java.util.StringTokenizer;

public class Dp14002 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 수열 A의 크기 N (1 ≤ N ≤ 1,000)이 주어진다.
         * 둘째 줄에는 수열 A를 이루고 있는 Ai가 주어진다. (1 ≤ Ai ≤ 1,000)
         *
         * 2. output 조건
         * 첫째 줄에 수열 A의 가장 긴 증가하는 부분 수열의 길이를 출력한다.
         * 둘째 줄에는 가장 긴 증가하는 부분 수열을 출력한다. 그러한 수열이 여러가지인 경우 아무거나 출력한다.
         *
         * 3. 주의사항
         * 며칠을 고민하면서 나름대로 좋은 솔루션을 오래 고민해봤지만 별로 좋은 풀이는 없었다.
         * 한 두 개씩 엣지 케이스에 대해서 처리하기가 까다로웠기 때문이다..
         *
         * 4. 해석
         * 이전에 풀었던 문제들처럼 풀지 말고, 더 효율적인 방법으로 푸는 것을 생각해보자.
         * -------------------------------------------
         * 왜 기존의 LIS 풀이의 연장선상에 있는 문제인지 알 수 있게 해주는 문제였다.
         * 결국 입력된 값을 따라가면서 dp를 구하게 될텐데, dp마다의 lis값은 다르게 보면 그 수가 lis 내에서 어디에 위치하는지
         * 알려주는 인덱스와 같다고도 생각할 수 있다.
         * 더 좋은 수열이 나올 수가 있기 때문에 lis를 구할 때는 해당 수열의 항으로 끝나는 가장 긴 수열 LIS를 구하는 형식으로 푼다.
         * 그렇기 때문에 길이만 역으로 따라가도 중간에 수열의 법칙이 깨질 걱정없이, 실제 수열 정보를 잘 얻어올 수 있는 것이다.
         * 역으로 추적할 때는 stack를 활용하기가 가장 좋았다.
         *
         * Example
         * 1.
         * 6
         * 10 20 10 30 20 50
         * -------
         * answer
         * 4
         * 10 20 30 50
         *
         * 2.
         * 11
         * 10 20 40 50 100 70 30 40 50 60 90
         * -------
         * answer
         * 7
         * 10 20 30 40 50 60 90
         *
         * 3.
         * 8
         * 10 70 80 100 20 30 80 200
         * -------
         * answer
         * 5
         * 10 20 30 80 200
         */
        int[] inputData = inputData();
        int[] result = solveProblem(inputData);

        StringBuilder sb = new StringBuilder();
        sb.append(result.length);
        sb.append("\n");
        for(int i : result) {
            sb.append(i);
            sb.append(" ");
        }
        System.out.println(sb.toString());

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int length = Integer.parseInt(br.readLine());
            int[] inputData = new int[length];

            StringTokenizer st = new StringTokenizer(br.readLine());
            for(int i = 0; i < length; i++) {
                inputData[i] = Integer.parseInt(st.nextToken());
            }
            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 기존과 다르게 이진 탐색을 활용해서 푼다.
     * @param inputData
     * @return
     */
    private static int[] solveProblem(int[] inputData) {
        int length = inputData.length;
        int[] sequence = new int[length];
        int[] dp = new int[length];
        int seqlen = 0, insertionIdx = 0;
        // 최초로 데이터가 들어갈 때는 무조건 들어가야하므로 초기화를 시켜줌.
        sequence[seqlen++] = inputData[0];
        dp[0] = seqlen;

        for(int i = 1; i < length; i++) {
            System.out.println("phase(index) " + i);
            System.out.println("current term : " + inputData[i]);
            System.out.println("sequence");
            for(int element : sequence) {
                System.out.print(element + " ");
            }
            System.out.println("");

            System.out.println("dp");
            for(int element : dp) {
                System.out.print(element + " ");
            }
            System.out.println("");
            System.out.println("");

            if(inputData[i] > sequence[seqlen - 1]) {
                sequence[seqlen++] = inputData[i];
                dp[i] = seqlen;

            } else {
                insertionIdx = binarySearch(sequence, seqlen, inputData[i]);
                sequence[insertionIdx] = inputData[i];
                dp[i] = insertionIdx + 1;
            }
        }

        /*
         * stack으로 LIS 마지막 항부터 추적한다.
         * 거꾸로 탐색하다보면 자연스레 LIS를 찾을 수 있다.
         */
        int lisLen = Arrays.stream(dp).max().getAsInt();
        int[] result = new int[lisLen];
        Stack<Integer> stack = new Stack<Integer>();
        for(int i = dp.length - 1; i > -1; i--) {
            if(dp[i] == lisLen) {
                stack.push(inputData[i]);
                lisLen--;
            }
        }

        while(!stack.isEmpty()) {
            result[lisLen++] = stack.pop();
        }

        return result;
    }

    /**
     * 이진 탐색을 통해 value가 수열 내에서 위치할 곳을 찾아서 값을 넣는다.
     * 그리고 value가 위치한 index를 반환한다.
     * @param sequence
     * @param seqlen
     * @param value
     * @return
     */
    private static int binarySearch(int[] sequence, int seqlen, int value) {
        int start = 0, end = seqlen - 1;
        int mid = 0;

        System.out.println("binarySearch target : " + value);
        while(start <= end) {
            mid = (start + end) / 2;
            System.out.println("start : " + start + ", end : " + end + ", mid : " + mid);

            if(value > sequence[mid]) {
                start = mid + 1;
            } else if(value < sequence[mid]) {
                end = mid - 1;
            } else {
                // 일치하는 값을 찾았으면 종료
                System.out.println("agreement value");
                System.out.println("");
                return mid;
            }

        }

        /*
         * 이진 탐색을 끝까지 완료했을 경우이다.
         * 대부분의 경우에는 탐색을 끝까지 완료했을텐데, 순차적 탐색과 같은 결과를 도출해야하므로 한 번 검증을 해야한다.
         * 넣는 값은 직전의 수열 항목의 값보다 큰 지점이어야 하기 때문이다.
         * 쉽게 말해서 일치하는 값을 찾지 못했더라도 결국에는 가장 근사한 값까지 도달했을 것이다.
         * 결국 value는 여전히 최종적으로 탐색을 했던 수열 항목의 값보다 크거나 작은 값이라고 생각할 수 있다.
         *
         * 1. value가 더 크다면 현재 mid 위치 말고 +1인 다음 위치에 저장해야한다.
         *   이전 수열을 최대한 유지해야 길어지기 때문이다.
         * 2. value가 더 작다면 현재 mid 위치에 저장해야한다.
         *   직전 수열 항목의 값보다 큰 것은 명확하므로 현재 위치를 고수한다.
         *
         * 문제는 이렇게 인덱스를 조절하다보면 반드시 범위를 넘어서는 경우가 있다.
         * 1은 만약 mid가 배열 끝을 가리킨다면, Array out of bound Exception이 발생하므로 예외적으로 현재 위치를 고수한다.
         */
        if(value > sequence[mid] && (mid + 1) <= (seqlen - 1)) {
            mid = mid + 1;
        }

        System.out.println("approximate idx : " + mid);
        System.out.println("");
        return mid;
    }

}
