package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Dp9465 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 테스트 케이스의 개수 T가 주어진다.
         * 각 테스트 케이스의 첫째 줄에는 2xn 스티커의 열 값인 n이 주어진다. (1 ≤ n ≤ 100,000)
         * 다음 두 줄에는 n개의 정수가 주어지며, 각 정수는 그 위치에 해당하는 스티커의 점수이다. (0 ≤ value ≤ 100)
         * 공백을 기준으로 스티커 점수를 구별한다.
         *
         * 2. output 조건
         * 각 테스트 케이스 마다, 2xn개의 스티커 중에서 두 변을 공유하지 않는 스티커 점수의 최댓값을 출력한다.
         * (스티커를 선택하면, 이웃한 주변의 스티커는 선택할 수가 없다는 규칙이 있다.)
         *
         * 3. 해석
         * 연속한 것을 선택할 수 없다는 문제의 종류가 그렇듯이 이 문제도 해결법이 비슷하다.
         * 이 문제를 1차원 배열만으로 해결하기는 어렵다. 따라서 2차원 배열을 사용하여 풀어야 한다.
         * n번째 열의 스티커 input[0][n] or input[1][n]를 선택하게 된다면, n-1번째에서는 각각 dp[1][n-1], dp[0][n-1]까지의 부분 최적화를 고려해야한다.
         * 여기까지는 쉽게 생각할 수가 있다. 추가적으로 고려해야할 것이 한 가지 더 있다.
         * 이전 열의 스티커를 선택하지 않을 수도 있다는 것이다.
         * -----------------------------------------------------------
         * 현재 시점을 기준으로 해석하면, 새로운 스티커 1줄이 추가되었을 때 이를 선택하는게 큰 이득인 경우가 있을 수 있다.
         * 그러나 내가 선택하고자 하는 새로운 스티커의 인접 스티커를 선택한 경우, 원하는 큰 이득의 스티커를 선택할 수 없다.
         * 그렇기 때문에 과거 -1만큼의 스티커 선택을 다시 하고서 현재 열의 스티커의 가치 선택을 과감히 포기하는 경우가 발생한다.
         * -----------------------------------------------------------
         * 위와 같은 이유로 1차원 배열만으로는 해결하기 어려웠었다.
         * 2차원 배열을 사용해서 첫번째 줄과 두번째 줄을 각각 달리 계산해줘야 한다.
         *
         * Example
         * 2
         * 5
         * 50 10 100 20 40
         * 30 50 70 10 60
         * 7
         * 10 30 10 50 100 20 40
         * 20 40 30 50 60 20 80
         *
         */
        List<int[][]> inputDataList = inputDataList();
        int[] maxScoreList = getMaxScoreList(inputDataList);
        for(int result : maxScoreList) {
            System.out.println(result);
        }

    }

    public static List<int[][]> inputDataList() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            List<int[][]> inputDataList = new ArrayList<int[][]>();
            StringTokenizer st1, st2;

            for(int i = 0; i < testCase; i++) {
                int column = Integer.parseInt(br.readLine());
                st1 = new StringTokenizer(br.readLine());
                st2 = new StringTokenizer(br.readLine());
                int[][] inputData = new int[2][column];

                for(int j = 0; j < column; j++) {
                    inputData[0][j] = Integer.parseInt(st1.nextToken());
                    inputData[1][j] = Integer.parseInt(st2.nextToken());
                }
                inputDataList.add(inputData);
            }

            return inputDataList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int[] getMaxScoreList(List<int[][]> inputDataList) {
        int testCase = inputDataList.size();
        int[] maxScoreList = new int[testCase];
        int[][] inputData;
        int column;
        int[][] dp;

        for(int index = 0; index < testCase; index++) {
            inputData = inputDataList.get(index);
            column = inputData[0].length;
            dp = new int[2][column];

//            System.out.println("case " + index);
            for(int i = 0; i < column; i++) {
                if(i == 0) {
                    dp[0][i] = inputData[0][i];
                    dp[1][i] = inputData[1][i];
                } else if(i == 1) {
                    dp[0][i] = dp[1][i-1] + inputData[0][i];
                    dp[1][i] = dp[0][i-1] + inputData[1][i];
                } else {
                    dp[0][i] = Math.max(dp[0][i-2], dp[1][i-2]) + inputData[0][i];
                    dp[0][i] = Math.max(dp[0][i], dp[1][i-1] + inputData[0][i]);

                    dp[1][i] = Math.max(dp[0][i-2], dp[1][i-2]) + inputData[1][i];
                    dp[1][i] = Math.max(dp[1][i], dp[0][i-1] + inputData[1][i]);
                }
//                System.out.println("dp[0][" + i + "] : " + dp[0][i]);
//                System.out.println("dp[1][" + i + "] : " + dp[1][i]);
            }

            maxScoreList[index] = Math.max(dp[0][column-1], dp[1][column-1]);
        }

        return maxScoreList;
    }

}
