package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp17626 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력은 자연수 n을 포함하는 한 줄로 구성된다. 여기서, 1 ≤ n ≤ 50,000이다.
         *
         * 2. output 조건
         * 합이 n과 같게 되는 제곱수들의 최소 개수를 한 줄에 출력한다.
         * 라그랑주는 1770년에 모든 자연수는 넷 혹은 그 이하의 제곱수의 합으로 표현할 수 있다고 증명하였다.
         * 어떤 자연수는 복수의 방법으로 표현된다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 11339
         * -------
         * answer
         * 3
         *
         * 2.
         * 34567
         * --------
         * answer
         * 4
         *
         */

        int inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch(Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    private static int solveProblem(int inputData) {
        int[] dp = new int[inputData + 1];
        dp[1] = 1;
        for(int i = 2; i <= inputData; i++) {
            for(int j = 1; j*j <= i; j++) {
                if(j == 1) {
                    dp[i] = dp[i - (j*j)] + dp[j*j];
                }

                /*
                 * 제곱 수는 0이 저장될 것이므로 0은 +1로 저장한다.
                 */
                if(i == j*j) {
                    dp[i] = 1;
                } else {
                    dp[i] = Math.min(dp[i], dp[i - (j*j)] + dp[j*j]);
                }
            }

//            System.out.println("dp[" + i + "] : " + dp[i]);
        }

        return dp[inputData];
    }

}
