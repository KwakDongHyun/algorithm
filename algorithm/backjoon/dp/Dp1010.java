package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Dp1010 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력의 첫 줄에는 테스트 케이스의 개수 T가 주어진다.
         * 그 다음 줄부터 각각의 테스트케이스에 대해 강의 서쪽과 동쪽에 있는 사이트의 개수 정수 N, M (0 < N ≤ M < 30)이 주어진다.
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해 주어진 조건하에 다리를 지을 수 있는 경우의 수를 출력한다.
         *
         * 3. 해석
         * 언뜻 보면 복잡해보이지만, 결국 같은 쌍에 대해서 순서를 허용하지 않겠다는 말이다.
         * 이건 조합(Combination)의 전형적인 특징이다.
         * 그리고 아래는 조합에서 유용한 공식 중 하나이다.
         * nCr = n-1Cr-1 + n-1Cr
         *
         */
        int[][] inputDataList = inputDataList();
        int maxParametricNum = Arrays.stream(inputDataList)
                .mapToInt(data -> data[1])
                .max()
                .getAsInt(); // 테스트케이스 중에서 가장 큰 모수 구하기. (어차피 구할거면 최대치까지 돌려서 구해야하기 때문)
        int[] combinationResultArr = getCombinationResultArr(inputDataList, maxParametricNum);

        for(int result : combinationResultArr) {
            System.out.println(result);
        }

    }

    public static int[][] inputDataList() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            int[][] inputDataList = new int[testCase][2];
            StringTokenizer stringTokenizer;

            for(int i = 0; i < testCase; i++) {
                stringTokenizer = new StringTokenizer(br.readLine());
                inputDataList[i][0] = Integer.parseInt(stringTokenizer.nextToken()); // west site 수 (N)
                inputDataList[i][1] = Integer.parseInt(stringTokenizer.nextToken()); // east site 수 (M)
            }
            return inputDataList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int[] getCombinationResultArr(int[][] inputDataList, int maxParametricNum) {
        int testCase = inputDataList.length;
        int[] resultArr = new int[testCase];
        int[][] dp = new int[maxParametricNum + 1][maxParametricNum + 1]; // Combination 2Dimension-Array
        for(int i = 1; i <= maxParametricNum; i++) {
            for(int j = 0; j <= i; j++) {
                if(j == 0 || j == i) {
                    dp[i][j] = 1;
//                    System.out.print(dp[i][j] + " ");
                } else {
                    dp[i][j] = dp[i-1][j] + dp[i-1][j-1];
//                    System.out.print(dp[i][j] + " ");
                }
            }
//            System.out.println();
        }

        for(int i = 0; i < testCase; i++) {
            int westSite = inputDataList[i][0];
            int eastSite = inputDataList[i][1];
            resultArr[i] = dp[eastSite][westSite];
        }

        return resultArr;
    }

}
