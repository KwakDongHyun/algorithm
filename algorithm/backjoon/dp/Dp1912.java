package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Dp1912 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 정수 n(1 ≤ n ≤ 100,000)이 주어지고, 둘째 줄에는 n개의 정수로 이루어진 수열이 주어진다.
         * 수는 (-1,000 ≤ A ≤ 1,000) 정수이다.
         *
         * 2. output 조건
         * 연속된 몇 개의 수를 선택해서 구할 수 있는 합 중 가장 큰 합.
         *
         */
        int[] inputDataList = inputDataList();
        int maxContinuousSum = getMaxCountinuousSum(inputDataList);
        System.out.println(maxContinuousSum);

    }

    public static int[] inputDataList() {
        int[] inputDataList = null;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int sequenceSize = Integer.parseInt(br.readLine());
            inputDataList = new int[sequenceSize];

            String[] sequence = br.readLine().split(" ");
            sequenceSize = sequence.length;
            for(int i = 0; i < sequenceSize; i++) {
                inputDataList[i] = Integer.parseInt(sequence[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputDataList;
    }

    public static int getMaxCountinuousSum(int[] inputDataList) {
        int sequenceSize = inputDataList.length;
        int[] dp = new int[inputDataList.length];

        for(int i = 0; i < sequenceSize; i++) {
            if(i == 0) {
                dp[i] = inputDataList[i];
                continue;
            }

            dp[i] = Math.max(inputDataList[i], dp[i-1] + inputDataList[i]);
        }

        return Arrays.stream(dp)
                .max()
                .getAsInt();
    }

}
