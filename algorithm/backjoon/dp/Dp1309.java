package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp1309 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 우리의 크기 N이 주어진다. (1 ≤ N ≤ 100,000)
         *
         * 2. output 조건
         * 사자를 배치하는 경우의 수를 9901로 나눈 나머지를 출력하여라.
         *
         * 3. 주의사항
         * 점화식을 세울 때는 되도록 수학적으로 생각해서 풀도록 해라.
         * 즉, 문제의 속성이 기하학적이던지 대수적이던지 매 번 무식하게 경우의 수를 그려보고 나서 점화식을 찾지 말라는 말이다.
         * 대수적인 사고로 풀어야 앞으로 더 어려운 문제가 주어졌을 때 돌파할 수 있을 것이다.
         * 대수학적 사고가 수학과의 핵심인 이유는 다 그래서 그렇다.
         * 공학자이지만 지금의 한계를 뛰어넘고 싶다면 단순히 유형을 외우고 수학 공식만 찾을게 아니라 문제를 수학적인 관점에서 보고
         * 대수적으로 생각해라.
         *
         * 4. 해석
         * 1부터 N까지 배열이 1개씩 늘어날 때마다 경우의 수는 어떻게 계산이 될지 단계적으로 생각해보자.
         * 기본적으로 경우의 수는 결국 순열의 관점에서 접근하고 생각하면 풀 수 있다.
         * 새로운 데이터가 추가되더라도 경우의 수는 기존까지 계산해서 찾은 경우의 수에 더해서 새롭게 찾은 경우의 수를 더하는 것으로 계산을 할 것이다.
         * 즉 기존에 최적화된 데이터를 사용할 수 있다는 것이고, 메모이제이션을 사용함으로써 Dynamic Programming으로 풀 수 있다는 말이다.
         *
         * 그렇다면 N-1번째까지 찾은 경우의 수에 더해서 새롭게 찾은 경우의 수를 더하기만 하면 문제는 풀린다.
         * 새롭게 찾은 경우의 수는 새로 추가한 배열의 데이터에 따라서 크게 3가지 기준으로 classify(분류)를 할 수 있다.
         * 배열의 한 row에서 가능한 경우의 수는 (o,o), (o,x), (x,o) 이다. (x는 데이터가 있는 경우, o는 데이터가 없는 경우)
         * 각 경우의 수에 맞춰서 어떤 데이터가 발생하는지를 생각해야 된다.
         *
         * (1) o,o 의 경우
         * 아예 선택을 안한 경우에는 조건에 위배되는 것이 없으므로 그냥 맘편히 dp[n-1]를 쓰면 된다.
         *
         * (2) x,o 의 경우
         * N-1번째 데이터를 바로 따지지 말고 우선은 {dp[n-1] * (위의 3가지 경우의 수)}로 계산할 경우 다음과 같이 된다
         * ...  ...   ...
         * o o | x o | o x
         * x o | x o | x o
         *
         * n-1번째 데이터도 발생 가능한 경우의 수는 동일하므로 위와 같이 구성될 것이다.
         *
         * (3) o,x 의 경우
         * {dp[n-1] * (위의 3가지 경우의 수)}로 계산할 경우 다음과 같이 된다
         * ...  ...   ...
         * o o | x o | o x
         * o x | o x | o x
         *
         * (4) 데이터 정제
         * 조건에 위배되는 것을 빼면 되는데 그럴 경우 아래의 경우의 수만 취하면 된다.
         * ...  ...      ...
         * o o | o x     o o | x o
         * x o | x o  +  o x | o x
         *
         * 근데 N번째 데이터를 무시하고 N-1번째 데이터 관점에서 본다면
         * (2)와 (3)의 데이터를 더할 경우 {2 * (o,o) + (o,x) + (x,o)}가 된다는 것을 알 수 있다.
         * 대수적으로 본다면 결국 (n-1번째 데이터의 3가지 경우의 수) + (n-1번째 데이터가 없을 경우)가 된다.
         * 아까 위에서 dp[n]을 { (o,o) + (o,x) + (x,o) }로 보자고 했던게 기억이 나는가?
         * 이 대수적 관점에서 보면 {(2)와 (3)의 경우를 합할 경우} = dp[n-1] + dp[n-2]가 된다.
         *
         * (5) 합산
         * 그러면 dp[n]은 아래와 같이 나타낼 수 있다
         * (1) + (2) + (3) = dp[n] = dp[n-1] + (dp[n-1] + dp[n-2])
         *                         = 2 * dp[n-1] + dp[n-2]
         * 점화식은 이렇게 찾을 수가 있다.
         * 다시 한 번 말하지만 무식하게 푸는게 아니라 대수적으로 생각하고 문제를 풀자.
         * 대수적으로 생각한다는 것은 선형대수 공부할 때도 느꼈지만, {일반적인 수의 개념(자연수, 정수 등), 함수, 어떤 속성의 데이터 등}의 개념으로 보라는 말이다.
         * 대수는 무엇이든지 될 수 있다. 수의 법칙의 관점에서 보는 것이지, 항목이나 구성에 대해서 보는게 아니라는 말이다.
         *
         * Example
         * 1.
         * 4
         * -------
         * answer
         * 41
         *
         */
        int inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch(Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    private static int solveProblem(int inputData) {
        int[] dp = new int[inputData];
        for(int i = 0; i <inputData; i++) {
            switch (i) {
                case 0 :
                    dp[i] = 3;
                    break;
                case 1 :
                    dp[i] = 7;
                    break;
                default :
                    dp[i] = (2 * dp[i-1] + dp[i-2]) % 9901;
                    break;
            }
        }
        return dp[inputData - 1];
    }

}
