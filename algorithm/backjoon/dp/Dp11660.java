package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Dp11660 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 표의 크기 N과 합을 구해야 하는 횟수 M이 주어진다. (1 ≤ N ≤ 1024, 1 ≤ M ≤ 100,000)
         * 둘째 줄부터 N개의 줄에는 표에 채워져 있는 수가 1행부터 차례대로 주어진다.
         * 다음 M개의 줄에는 네 개의 정수 x1, y1, x2, y2 가 주어지며, (x1, y1)부터 (x2, y2)의 합을 구해 출력해야 한다.
         * 표에 채워져 있는 수는 1,000보다 작거나 같은 자연수이다. (x1 ≤ x2, y1 ≤ y2)
         *
         * 2. output 조건
         * 총 M줄에 걸쳐 (x1, y1)부터 (x2, y2)까지 합을 구해 출력한다.
         *
         * 3. 해석
         * 최악의 경우, 1024 x 1024 Grid에서 10만번 동안 full coordinate 합을 구하면 2^20 * 10^5 라는 조회 회수가 나온다.
         * 약 10^11 라는 수치가 나오는데.. 답도 없어진다.
         * 수행 시간을 줄이려면 반드시 유동적으로 답을 만드는 방법이 필요한대..
         * 메모이제이션을 어떤식으로 구성할 지 그리고 어떻게 효율적으로 계산을 할건지를 정해야하지 않나 싶다.
         * 무식하게 full scan을 해서 답을 구하는 거였으면 애초에 알고리즘이 아니었겠지만..
         * 아니 근데 아무리 생각해봐도 효율적인 방법을 모르겠다;
         * 애당초 그런게 존재하는게 맞나? -- 찾았다. 찾은 방법은 아래에 추가로 설명.
         * ------------------------------------------------------------------------
         *Todo
         * 행을 기준으로 방법을 설명하겠다. 원한다면 열을 기준으로 이 방법을 진행해도 된다. (원리는 같기 때문)
         * 행마다 1열부터 N열까지 누적한 값을 더해서 각각의 cell에 저장한다.
         * 1 2 3 4 가 한 행의 데이터라면 -> 1 3 6 10 이런식으로 데이터를 저장한다는 말이다.
         * 그리고서 구간의 합을 구하면 되는데 이 때 각 행마다 연산을 구하기가 매우 쉬워진다.
         * 1. y1이 0일 경우 x1 ~ x2까지 누적값을 단순히 더하기만 하면 된다. dp[x1][y2]을 계속 더하면 된다.
         * 2. y1이 0이 아니라면 x1 ~ x2까지 (dp[x1][y2] - dp[x1][y1-1])을 더하면 된다.
         *
         * Example
         * 1.
         * 4 3
         * 1 2 3 4
         * 2 3 4 5
         * 3 4 5 6
         * 4 5 6 7
         * 2 2 3 4
         * 3 4 3 4
         * 1 1 4 4
         * -------
         * answer
         * 27
         * 6
         * 64
         *
         * 2.
         * 2 4
         * 1 2
         * 3 4
         * 1 1 1 1
         * 1 2 1 2
         * 2 1 2 1
         * 2 2 2 2
         * -------
         * answer
         * 1
         * 2
         * 3
         * 4
         *
         */
        List<int[][]> input2DimensionData = input2DimensionData();
        int[] sumOfGridCoordinateList = getSumOfGridCoordinate(input2DimensionData);
        for(int result : sumOfGridCoordinateList) {
            System.out.println(result);
        }

    }

    public static List<int[][]> input2DimensionData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int gridSize = Integer.parseInt(st.nextToken());
            int testCase = Integer.parseInt(st.nextToken());

            int[][] gridData = new int[gridSize][gridSize];
            int[][] testData = new int[testCase][4];
            int countToken;
            for(int i = 0; i < gridSize; i++) {
                st = new StringTokenizer(br.readLine());
                countToken = st.countTokens();
                for(int j = 0; j < countToken; j++) {
                    gridData[i][j] = Integer.parseInt(st.nextToken());
                }
            }

            for(int i = 0; i < testCase; i++) {
                st = new StringTokenizer(br.readLine());
                countToken = st.countTokens();
                for(int j = 0; j < countToken; j++) {
                    testData[i][j] = Integer.parseInt(st.nextToken());
                }
            }

            List<int[][]> input2DimensionData = new ArrayList<int[][]>();
            input2DimensionData.add(gridData);
            input2DimensionData.add(testData);
            return input2DimensionData;
        } catch (Exception e) {
            return null;
        }
    }

    public static int[] getSumOfGridCoordinate(List<int[][]> input2DimensionData) {
        int[][] gridData = input2DimensionData.get(0);
        int[][] testData = input2DimensionData.get(1);
        int gridSize = gridData.length;
        int testCase = testData.length;

        // 행을 기준으로 오름차순 열로 가면서 누적값을 저장.
        int[][] accumulationArr = new int[gridSize][gridSize];
        int accumulation;
        for(int i = 0; i < gridSize; i++) {
            accumulation = 0;
            for(int j = 0; j < gridSize; j++) {
                accumulation += gridData[i][j];
                accumulationArr[i][j] = accumulation;
            }
        }

        // 누적 배열 검사
        /*for(int i = 0; i < gridSize; i++) {
            for(int j = 0; j < gridSize; j++) {
                System.out.print(accumulationArr[i][j] + " ");
            }
            System.out.println();
        }*/

        int[] result = new int[testCase];
        int sourceX, sourceY, destinationX, destinationY;
        for(int i = 0; i < testCase; i++) {
            sourceX = testData[i][0] - 1;
            sourceY = testData[i][1] - 1;
            destinationX = testData[i][2] - 1;
            destinationY = testData[i][3] - 1;

            if(sourceX == destinationX && sourceY == destinationY) {
                result[i] = gridData[sourceX][sourceY];
            } else {
                switch (sourceY) {
                    case 0:
                        for(int x = sourceX; x <= destinationX; x++) {
                            result[i] += accumulationArr[x][destinationY];
//                            System.out.println("result[" + i + "] : " + result[i]);
                        }
                        break;
                    default:
                        for(int x = sourceX; x <= destinationX; x++) {
                            result[i] += accumulationArr[x][destinationY] - accumulationArr[x][sourceY - 1];
//                            System.out.println("result[" + i + "] : " + result[i]);
                        }
                        break;
                }
            }
        }

        return result;
    }

}
