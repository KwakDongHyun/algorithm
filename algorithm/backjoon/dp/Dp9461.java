package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Dp9461 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 테스트 케이스의 개수 T가 주어진다.
         * 각 테스트 케이스는 한 줄로 이루어져 있고, N이 주어진다. (1 ≤ N ≤ 100)
         * 파도반 수열 P(N)은 나선에 있는 정삼각형의 변의 길이이다. P(1)부터 P(10)까지 첫 10개 숫자는 1, 1, 1, 2, 2, 3, 4, 5, 7, 9이다.
         *
         * 2. output 조건
         * 각 테스트 케이스마다 P(N)을 출력한다.
         *
         */
        int[] inputDataList = inputDataList();
        long[] padovanSequence = getPadovanSequence(inputDataList);
        for(int index : inputDataList) {
//            System.out.println(index + " : " + padovanSequence[index]);
            System.out.println(padovanSequence[index]);
        }

    }

    public static int[] inputDataList() {
        int[] inputDataList = null;

        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            inputDataList = new int[testCase];

            for(int i = 0; i < testCase; i++) {
                inputDataList[i] = Integer.parseInt(br.readLine());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputDataList;
    }

    public static long[] getPadovanSequence(int[] inputDataList) {
        int maxSequence = Arrays.stream(inputDataList)
                .max()
                .getAsInt();

        long[] dp = new long[maxSequence + 1];

        for(int i = 1; i <= maxSequence; i++) {
            dp[i] = i > 0 && i < 4 ? 1 : (dp[i-2] + dp[i-3]);
        }

        return dp;
    }

}
