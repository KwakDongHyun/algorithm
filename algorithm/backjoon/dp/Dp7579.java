package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp7579 {

    private static int demendMemory;

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫 줄에는 정수 N과 M이 공백문자로 구분되어 주어지며, 둘째 줄과 셋째 줄에는 각각 N개의 정수가 공백문자로 구분되어 주어진다.
         * 둘째 줄의 N개의 정수는 현재 활성화 되어 있는 앱 A1, ..., AN이 사용 중인 메모리의 바이트 수인 m1, ..., mN을 의미하며,
         * 셋째 줄의 정수는 각 앱을 비활성화 했을 경우의 비용 c1, ..., cN을 의미한다
         *
         * 단, 1 ≤ N ≤ 100, 1 ≤ M ≤ 10,000,000이며, 1 ≤ m1, ..., mN ≤ 10,000,000을 만족한다.
         * 또한, 0 ≤ c1, ..., cN ≤ 100이고, M ≤ m1 + m2 + ... + mN이다.
         *
         * 2. output 조건
         * 필요한 메모리 M 바이트를 확보하기 위한 앱 비활성화의 최소의 비용을 계산하여 한 줄에 출력해야 한다.
         *
         * 3. 주의사항
         * n x 2 문제처럼 나머지가 들어갔다는 것은 overflow가 발생할 수 있는 수가 있다는 거다.
         *
         * 4. 해석
         * dp를 어디에 초점을 맞춰야할지 잡는게 매우 힘들었다.
         * 문제에서 각 프로세스가 추가될 때마다(각각의 케이스별로) 최소 비용은 독립적인 성질을 유지한다(부분최적화가 된다는 말)는 것은 빠르게 알아냈다.
         * 그러나 메모리 기준을 먼저 맞추고 나서 비용을 최소화하려니 필요한 수행시간과 메모리량이 많았다.
         *
         * 나는 cost를 dp에 저장하려고 했고, cost는 case가 추가될 수록 변동이 일어날 수 있는 값(매번 타당성을 따져야하는 변수)이었다.
         * 매번 조건에 부합하는지 확인해서 값을 갱신시켜야하는 불안정한 변수라서 dp의 값으로 쓰이기엔 부적절하다는 것을 깨달았다.
         * 다른 사람 풀이를 결국 봤는데, memory를 오히려 dp의 값으로 한 것이 놀라웠다.
         * cost는 memory에 종속적이지만 memory는 cost와 상관없는 독립적인 개념이라는 것에 착안을 둔 것이었다.
         * 게다가 memory 기준에 부합하는 것을 찾는 방법은 단지 dp값 탐색을 통해 제일 빨리 memory 요구사항을 충족시키는 index인 cost를
         * 찾기만 하면 되기 때문에 훨씬 효율적이다.
         *
         * cost와 메모리의 관계를 역의 관계로 본 것이다.
         * memory에 대한 최소 cost를, cost에 대한 최대 메모리로 생각한다는 건 관점만 다르게 볼 뿐이고 이치는 같기 때문이다.
         * 그리고 메모리를 index로 잡는것보다는 cost를 index로 잡는게 메모리 소모도 적고 탐색할 것도 적기 때문에 훨씬 낫다.
         *
         * 이렇게 문제를 다른 관점에서 해석하고 풀이에 접근한다는 것이 놀라웠다...
         * 추후에 다시 한 번 풀어보자. 좋은 문제라고 생각한다.
         *
         * Example
         * 1.
         * 5 60
         * 30 10 20 35 40
         * 3 0 3 5 4
         * -------
         * answer
         * 6
         *
         */

        int[][] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int processCnt = Integer.parseInt(st.nextToken());
            demendMemory = Integer.parseInt(st.nextToken());

            int[][] inputData = new int[processCnt][2];
            inputData[0][0] = demendMemory;

            st = new StringTokenizer(br.readLine());
            for(int i = 0; i < processCnt; i++) {
                inputData[i][0] = Integer.parseInt(st.nextToken());
            }

            st = new StringTokenizer(br.readLine());
            for(int i = 0; i < processCnt; i++) {
                inputData[i][1] = Integer.parseInt(st.nextToken());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[][] inputData) {
        int processCnt = inputData.length;

        int[][] dp = new int[processCnt][10001];
        int memory, cost, minCost = Integer.MAX_VALUE;

        for(int i = 0; i < processCnt; i++) {
            memory = inputData[i][0];
            cost = inputData[i][1];

            // j는 cost를 index로 한 것을 탐색하기 위한 변수이다.
            for(int j = 0; j <= 10000; j++) {
                if(i == 0) {
                    // 맨 처음에는 그냥 넣어야한다. 프로세스가 단 1개이기 때문이다. 일종의 예외처리.
                    if(j >= cost) {
                        dp[i][j] = memory;
                    }

                } else {
                    if(j >= cost) {
                        dp[i][j] = Math.max(dp[i - 1][j - cost] + memory, dp[i - 1][j]);
                    } else {
                        dp[i][j] = dp[i - 1][j];
                    }
                }

                // 나중에 불필요하게 탐색해서 2번 일할 필요 없이 로직 마지막에 체크를 해준다.
                if(dp[i][j] >= demendMemory) {
                    minCost = Math.min(minCost, j);
                }

            }
        }

        return minCost;
    }

}
