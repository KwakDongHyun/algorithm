package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class Dp15486 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 N (1 ≤ N ≤ 1,500,000)이 주어진다.
         * 둘째 줄부터 N개의 줄에 Ti와 Pi가 공백으로 구분되어서 주어지며, 1일부터 N일까지 순서대로 주어진다.
         * (1 ≤ Ti ≤ 50, 1 ≤ Pi ≤ 1,000)
         *
         * 2. output 조건
         * 첫째 줄에 백준이가 얻을 수 있는 최대 이익을 출력한다.
         *
         * 3. 주의사항
         * 이전에 풀었던 최대 이익을 얻기 위한 상담 일정을 잡는 문제의 업그레이드 버전이다.
         *
         * 4. 해석
         * 좀 더 단순하게 생각해봐야 한다.
         * 결국에는 상담을 진행하는 동안에는 다른 상담을 진행하지 못하기 때문에, 상담이 끝나는 시점에 이익을
         * 계산해서 해당 일의 최대 이익 값으로 저장하는게 더 나을 수도 있다.
         * 이전에 최대 값에 더해서 새로운 최대 값을 갱신하기 위해서 max를 들고 있어야하는데,
         * 종료시점 뿐만 아니라 선택한 시점부터 pi를 더해서 dp에 넣어버리면, max값이 변질되어서 올바른 값을 알 수가 없게 된다.
         *
         * Example
         * 1.
         * 7
         * 3 10
         * 5 20
         * 1 10
         * 1 20
         * 2 15
         * 4 40
         * 2 200
         * ----------------------
         * answer
         * 45
         *
         * 2.
         * 10
         * 1 1
         * 1 2
         * 1 3
         * 1 4
         * 1 5
         * 1 6
         * 1 7
         * 1 8
         * 1 9
         * 1 10
         * ----------------------
         * answer
         * 55
         *
         * 3.
         * 10
         * 5 50
         * 4 40
         * 3 30
         * 2 20
         * 1 10
         * 1 10
         * 2 20
         * 3 30
         * 4 40
         * 5 50
         * ----------------------
         * answer
         * 90
         *
         */

        int[][] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int day = Integer.parseInt(br.readLine());
            int[][] inputData = new int[day + 2][2]; // 10이면 0 ~ 11

            for(int i = 1; i <= day; i++) {
                StringTokenizer st = new StringTokenizer(br.readLine());
                inputData[i][0] = Integer.parseInt(st.nextToken());
                inputData[i][1] = Integer.parseInt(st.nextToken());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[][] inputData) {
        int length = inputData.length;
        int[] dp = new int[length];

        int max = -1;
        for(int i = 1; i <= length - 2; i++) {
            int ti = inputData[i][0];
            int pi = inputData[i][1];
            int endDay = i + ti;

            if(max < dp[i]) {
                max = dp[i];
            }

//            System.out.println("ti : " + ti + ", pi : " + pi + ", endDay : " + endDay);

            if(endDay < length) {
                dp[endDay] = Math.max(dp[endDay], max + pi);
            }

            /*
             * 저장된 것 확인.
             */
            /*System.out.println("day : " + i);
            for(int j = 1; j < length; j++) {
                System.out.print(j + " ");
            }

            System.out.println();
            for(int j = 1; j < length; j++) {
                System.out.print(dp[j] + " ");
            }
            System.out.println();
            System.out.println();*/
        }

        return Arrays.stream(dp).max().getAsInt();
    }

}
