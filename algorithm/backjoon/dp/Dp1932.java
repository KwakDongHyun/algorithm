package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Dp1932 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 삼각형의 크기 n(1 ≤ n ≤ 500)이 주어지고
         * 둘째 줄부터 n+1번째 줄까지 정수 삼각형이 주어진다.
         * A (0 ≤ A ≤ 9999) 범위는 0 이상 9999 이하이다.
         *
         * 2. output 조건
         * 첫째 줄에 합이 최대가 되는 경로에 있는 수의 합을 출력한다.
         *
         */
        int[][] inputDatas = input2DimensionData();
        int maxSum = maxRoutineSum(inputDatas);
        System.out.println(maxSum);

    }

    public static int[][] input2DimensionData() {
        int[][] input2DimensionData = null;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int triangleLength = Integer.parseInt(br.readLine());
//            System.out.println("triangleLength : " + triangleLength);
            input2DimensionData = new int[triangleLength + 1][triangleLength + 1];

            for(int i = 1; i <= triangleLength; i++) {
                String[] rowDatas = br.readLine().split(" ");
                for (int index = 0; index < rowDatas.length; index++) {
                    input2DimensionData[i][index + 1] = Integer.parseInt(rowDatas[index]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return input2DimensionData;
    }

    public static int maxRoutineSum(int[][] inputDatas) {
        int triangleLength = inputDatas.length - 1;
        int[] dp = new int[triangleLength + 1];
        int[] lastValues = null;

        for(int i = 1; i <= triangleLength; i++) {
            if(i == 1) {
                dp[i] = inputDatas[i][i];
                continue;
            }

            // 삼각형 변은 순수하게 그냥 더하면 된다.
            lastValues = dp.clone();
            dp[1] = lastValues[1] + inputDatas[i][1];
            dp[i] = lastValues[i-1] + inputDatas[i][i];

            // i >= 3부터 돈다.
            for(int j = 2; j <= i-1; j++) {
                dp[j] = Math.max(lastValues[j], lastValues[j-1]) + inputDatas[i][j];
            }
        }

        return Arrays.stream(dp)
                .max()
                .getAsInt();
    }

}
