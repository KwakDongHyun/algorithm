package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Stack;

public class Dp9252 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄과 둘째 줄에 두 문자열이 주어진다. 문자열은 알파벳 대문자로만 이루어져 있으며, 최대 1000글자로 이루어져 있다.
         *
         * 2. output 조건
         * 첫째 줄에 입력으로 주어진 두 문자열의 LCS의 길이를, 둘째 줄에 LCS를 출력한다.
         * LCS가 여러 가지인 경우에는 아무거나 출력하고, LCS의 길이가 0인 경우에는 둘째 줄을 출력하지 않는다.
         *
         * 3. 주의사항
         * LIS의 향상된 문제와 비슷한 의미의 문제다.
         * 기존 해결책에서 시작해보는게 좋을 것 같다.
         *
         * 4. 해석
         * LCS 9251번 문제를 참고하면 길이를 구하는 방법은 알 수 있다.
         * 문자열을 dp를 생성하면서 만들어가는 방식을 택했는데 worst case에서 메모리 초과가 발생한다.
         * ----------------------------------------
         * 문자열을 구하려면 룰을 따라서 LCS를 완성해갔으므로 그 역순으로 탐색을 하면 구할 수 있게 된다.
         * -------------------------------------------
         * 이번 문제도 그렇고 LIS 문제도 풀면서 깨닫게 된 것은 dp를 통해서 구한 길이가 LIS와 LCS를 이루는 인덱스도 겸한다는 것을
         * 알 수 있었다.
         * 수학적으로 생각하면 결국에는 숫자가 변하는 지점이 새롭게 데이터가 추가되는 지점이기 때문에, 변화하는 곳만 캐치할 수 있다면
         * 데이터를 구성할 수 있는건 일도 아니라는 것을 알 수 있었다.
         * 이 말은 다르게 해석하면, 고유 값만 알 수 있다면 그에 따른 메타데이터를 구하는 것은 일도 아니라는 말이다.
         * 독립적인 성질을 유지할 수 있고, 이에 따른 고유 값을 얻어내는 것이 수학이던지 프로그램이던지 모두에게 있어서 핵심이라는
         * 것을 알 수 있었다.
         *
         * Example
         * 1.
         * ACAYKP
         * CAPCAK
         * -------
         * answer
         * 4
         * ACAK
         *
         * 2.
         * ACDEB
         * BCDEA
         * -------
         * answer
         * 3
         * CDE
         *
         */

        String[] inputData = inputData();
        String result = solveProblem(inputData);

        if(result != null && result.length() > 0) {
            System.out.println(result.length());
            System.out.println(result);
        } else {
            System.out.println(0);
        }

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] inputData = new String[2];
            inputData[0] = br.readLine();
            inputData[1] = br.readLine();
            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String solveProblem(String[] inputData) {
        char[] targetStr = inputData[0].toCharArray();
        char[] searchStr = inputData[1].toCharArray();
        int targetStrLen = targetStr.length;
        int searchStrLen = searchStr.length;

        /*
         * compact하게 배열을 쓰지 말고 더미 데이터를 한줄씩 추가하기 위해서 1부터 돌린다.
         * 이렇게 하는 이유는 dp (i-1, j) vs (i, j-1) 에서 더 큰 값을 가져올 때, 최외곽의 경우(한쪽은 단일 문자만으로 비교)
         * IndexOutOfBound Exception이 발생한다. 물론 이렇게 안하고 최외곽을 먼저 데이터를 채우고 나서
         * 나머지 데이터를 채우는 방법이 있지만 소스가 지저분해지고 복잡해보인다...
         * 그러므로 dp에서는 1부터 데이터를 채워나가는 형식으로 진행한다.
         */
        int[][] dp = new int[targetStrLen + 1][searchStrLen + 1];

        /*
         * 문자열을 dp따라서 생성하면 메모리 초과 오류가 발생한다.
         */
        for(int i = 1; i <= targetStrLen; i++) {
            for(int j = 1; j <= searchStrLen; j++) {
                if(targetStr[i-1] == searchStr[j-1]) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                }
            }
        }

        /*
         * stack를 활용해서 rule을 따라서 역순으로 탐색
         */
        int i = targetStrLen;
        int j = searchStrLen;
        Stack<Character> stack = new Stack<Character>();
        StringBuilder sb = new StringBuilder();

        while(i > 0 && j > 0) {
            if(i == 0 || j == 0) {
                break;
            }

            if(dp[i][j] == dp[i-1][j]) {
                i--;
            } else if(dp[i][j] == dp[i][j-1]) {
                j--;
            } else {
                stack.push(targetStr[i-1]);
                i--;
                j--;
            }
        }
        while(!stack.isEmpty()) {
            sb.append(stack.pop());
        }
        String stackStr = sb.toString();

        return stackStr;

    }

}
