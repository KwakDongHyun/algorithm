package backjoon.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Dp12865_knapsack {

    public static void main(String[] args) throws IOException {

        /*
         * 1. input 조건
         * 첫 줄에 물품의 수 N(1 ≤ N ≤ 100)과 버틸 수 있는 가방의 무게 K(1 ≤ K ≤ 100,000)가 주어진다.
         * 두 번째 줄부터 N개의 줄에 걸쳐서 각각의 물건의 무게 W(1 ≤ W ≤ 100,000)와 해당 물건의 가치 V(0 ≤ V ≤ 1,000)가 주어진다.
         *
         * 2. output 조건
         * 가방의 수용 가능한 무게 범위 내에서 얻을 수 있는 최대 가치를 구한다.
         *
         * 3. 해석
         * 그 유명한 knapsack 알고리즘이다. 이런 종류의 문제는 특징이 있다.
         * 한도내에서 최대 가치를 얻고 싶다면, cost가 적게 나가면서 profit이 최대한 큰 물건을 담으면 된다.
         * 그렇기 때문에 가방(여기서는 데이터를 저장하는 자료구조)에 있는 데이터는 cost의 오름차순으로 정렬되어 있어야 한다.
         * 이 방법은 선택한 아이템의 정보까지 얻고 싶을 때 사용하는 방법이라고 생각한다.
         * 아래 해결 방법을 참고하라.
         *
         * 4. 해결 방법
         * 단순히 가치만을 얻고 싶다면 물건을 넣을 때마다 최적의 가치를 얻는게 맞는 것 같다.
         * 먼저 dp를 무게별 최대 가치를 저장하는 배열로 정의한다.
         * 1개의 물건을 넣을 때마다 K부터 시작해서 (K-물건의 무게)가 0일 때까지 dp를 순회하면서 최대 가치를 얻는게 맞는 것 같다.
         * 아이템의 무게부터 조회를 하지 않는 이유는, (아이템의 무게 * 2)의 경우에 자기 자신을 참조하는 현상이 발생하기 때문이다.
         * 예를 들어 (3, 6)의 아이템의 경우 dp[6]을 정의할 때 (dp[3] + 6)을 비교 대상으로 삼게 된다.
         * 3부터 K까지 조회를 하는 방식으로 알고리즘을 설계할 경우, 자기 자신을 참조하는 오류를 범하게 된다.
         * 그렇기 때문에 조회를 K부터 0까지 거꾸로 하는 것이다.
         *
         */
        int[][] input2DimensionData = input2DimensionData();
        int knapsackMaxValue = getKnapsackMaxValue(input2DimensionData);
        System.out.println(knapsackMaxValue);

    }

    public static int[][] input2DimensionData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer stringTokenizer = new StringTokenizer(br.readLine());
            int itemCount = Integer.parseInt(stringTokenizer.nextToken());
            int knapsackWeight = Integer.parseInt(stringTokenizer.nextToken());

            int[][] input2DimensionData = new int[itemCount + 1][2]; // 0번째 index는 아이템 개수와 가방 허용 무게를 저장.
            input2DimensionData[0][0] = itemCount;
            input2DimensionData[0][1] = knapsackWeight;

            for(int i = 1; i <= itemCount; i++) {
                stringTokenizer = new StringTokenizer(br.readLine());
                input2DimensionData[i][0] = Integer.parseInt(stringTokenizer.nextToken()); // 아이템 무게
                input2DimensionData[i][1] = Integer.parseInt(stringTokenizer.nextToken()); // 아이템 가치
            }
            return input2DimensionData;
        } catch (Exception e) {
            e.printStackTrace();
            int[][] input2DimensionData = new int[1][1];
            input2DimensionData[0][0] = 0;
            input2DimensionData[0][1] = 0;
            return input2DimensionData;
        }
    }

    public static int getKnapsackMaxValue(int[][] input2DimensionData) {
        int itemCount = input2DimensionData[0][0]; // 생성한 전체 아이템 개수
        int knapsackMaxWeight = input2DimensionData[0][1]; // 가방의 허용 가능 무게

        int[] dp = new int[knapsackMaxWeight + 1]; // 무게 별로 최대 가치를 저장하는 배열.

        for(int i = 1; i <= itemCount; i++) {
            int weight = input2DimensionData[i][0];
            int value = input2DimensionData[i][1];
//            System.out.println("weight : " + weight + ", value : " + value);

            for(int j = knapsackMaxWeight; j > 0; j--) {
                if(j - weight < 0) {
                    break;
                }
                dp[j] = Math.max(dp[j - weight] + value, dp[j]);
//                System.out.println("dp[" + j + "] : " + dp[j]);
            }
        }

        return Arrays.stream(dp)
                .max()
                .getAsInt();
    }

}
