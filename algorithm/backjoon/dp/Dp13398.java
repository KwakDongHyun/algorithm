package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class Dp13398 {

    public static void main(String[] args) {

        int[] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int length = Integer.parseInt(br.readLine());
            int[] inputData = new int[length];

            StringTokenizer st = new StringTokenizer(br.readLine());
            for(int i = 0; i < length; i++) {
                inputData[i] = Integer.parseInt(st.nextToken());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[] inputData) {
        int length = inputData.length;
        int[] sum = new int[length];
        sum[0] = inputData[0];

        int[] dp = new int[length];
        dp[0] = inputData[0];

        int max = inputData[0];

        for(int i = 1; i < length; i++) {
            sum[i] = Math.max(sum[i-1] + inputData[i], inputData[i]);
            /*
             * 자기 자신을 제거한 경우와 그렇지 않은 경우를 생각해야함.
             * 그냥 더한 값과 하나라도 제거한 값 중 당연히 제거한 값이 더 무조건 클 것이다.
             */
            dp[i] = Math.max(sum[i-1], dp[i-1] + inputData[i]);
            // 모든 수열이 음수인 경우와 같은 edge case의 경우에는 항상 dp가 정답이지 않다.
            max = Math.max(max, Math.max(sum[i], dp[i]));
        }

        return max;
    }

}
