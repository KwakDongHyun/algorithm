package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp2294 {

    private static int kind;
    private static int targetValue;
    private static int dp[];

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 n, k가 주어진다. (1 ≤ n ≤ 100, 1 ≤ k ≤ 10,000)
         * 다음 n개의 줄에는 각각의 동전의 가치가 주어진다.
         * 동전의 가치는 100,000보다 작거나 같은 자연수이다. (1 ≤ v ≤ 100,000)
         * 가치가 같은 동전이 여러 번 주어질 수도 있다.
         *
         * 2. output 조건
         * 가치의 합이 k원이 되도록 하고 싶다. 사용한 동전의 최소 개수를 구하시오. (각각의 동전은 몇 개라도 사용할 수 있다.)
         * 사용한 동전의 구성이 같은데, 순서만 다른 것은 같은 경우이다.
         * 첫째 줄에 사용한 동전의 최소 개수를 출력한다. 불가능한 경우에는 -1을 출력한다.
         *
         * 3. 해석
         * 예외 상황이 뭐가 있을까?
         *
         *
         * Example
         * 3 15
         * 1
         * 5
         * 12
         * -----------------
         * answer
         * 3
         *
         */
        int[] inputData = inputData();
        solveProblem(inputData);
        System.out.println(dp[targetValue]);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            kind = Integer.parseInt(st.nextToken());
            targetValue = Integer.parseInt(st.nextToken());
            int[] inputData = new int[kind];
            dp = new int[targetValue + 1];

            int value;
            for(int i = 0; i < kind; i++) {
                value = Integer.parseInt(br.readLine());
                inputData[i] = value;

                // 초기 값 미리 세팅
                if(value <= targetValue && dp[value] == 0) {
                    dp[value] = 1;
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void solveProblem(int[] inputData) {
        int nextValue;

        for(int i = 1; i < targetValue; i++) {
            if(dp[i] == 0) {
                continue;
            }

            for(int j = 0; j < kind; j++) {
                nextValue = i + inputData[j];

                if(nextValue <= targetValue) {
                    if(dp[nextValue] == 0) {
                        dp[nextValue] = dp[i] + 1;
                    } else {
                        dp[nextValue] = Math.min(dp[nextValue], dp[i] + 1);
                    }
                }
            }
        }

        if(dp[targetValue] == 0) {
            dp[targetValue] = -1;
        }
    }

}
