package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Dp2579 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력의 첫째 줄에 계단의 개수가 주어진다.
         * 둘째 줄부터 한 줄에 하나씩 제일 아래에 놓인 계단부터 순서대로 각 계단에 쓰여 있는 점수가 주어진다.
         * 계단의 개수는 300이하의 자연수이고, 계단에 쓰여 있는 점수는 10,000이하의 자연수이다.
         *
         * 2. output 조건
         * 얻을 수 있는 총 점수의 최댓값을 출력한다.
         *
         * 3. 특징
         *Todo
         * --------------------------------------------------------------------------------------------
         * 1. 계단은 한 번에 한 계단씩 또는 두 계단씩 오를 수 있다. 즉, 한 계단을 밟으면서 이어서 다음 계단이나, 다음 다음 계단으로 오를 수 있다.
         * 2. 연속된 세 개의 계단을 모두 밟아서는 안 된다. 단, 시작점은 계단에 포함되지 않는다.
         * 3. 마지막 도착 계단은 반드시 밟아야 한다.
         * --------------------------------------------------------------------------------------------
         *
         * 단순히 생각하면 dp[n]은 아래와 같다.
         * dp[n] = max(dp[n-1], dp[n-2]) + stair[n].
         * 그러나 이 방법에는 문제가 있다.
         *
         * dp[n-2]는 Rule을 위반하지 않고 안전하지만, dp[n-1]의 경우 그렇지 않다.
         * dp[n-1]이 dp[n-2] + stair[n-1]이라고 하고, (dp[n-1] > dp[n-2]) 라고 가정해보자. 
         * 위의 식대로 하면 dp[n] = dp[n-1] + stair[n]이 된다.
         * 그런데 이 경우에 Rule 2를 위반하게 된다. 식을 열거하면 아래와 같이 된다.
         * dp[n] = dp[n-1] + stair[n]  =>  dp[n] = (dp[n-2] + stair[n-1]) + stair[n]
         * dp[n]은 항상 n번째 계단을 밟게 되므로 위의 식대로라면 stair[n-2], stair[n-1], stair[n]을 연속해서 밟으므로 Rule 2를 위반하게 된다.
         * 
         * 그러므로 도착하는 경우의 수는 예를 들어 아래와 같이 생각해야 한다.
         * 1. 0 -> 1 -> 3 -> 4 (1칸, 2칸, 1칸)  | n-3 -> n-1 -> n
         * 2. 0 -> 2 -> 4      (2칸, 2칸)       | n-2 -> n
         * 이걸 토대로 DP화 해서 문제를 보면 된다.
         */
        List<Integer> inputDataList = inputDataList();
        int listSize = inputDataList.size();
        int[] dp = new int[listSize + 1];
        dp[0] = 0;

        for(int i = 1; i <= listSize; i++) {
            if(i == 1) {
                dp[i] = inputDataList.get(i-1);
            } else if(i == 2) {
                dp[i] = inputDataList.get(i-1) + inputDataList.get(i-2);
            } else {
                dp[i] = Math.max(dp[i-2], dp[i-3] + inputDataList.get(i-2)) + inputDataList.get(i-1);
            }
//            System.out.println("dp[" + i + "]: " + dp[i]);
        }

        System.out.println(dp[listSize]);
    }

    public static List<Integer> inputDataList() {
        List<Integer> inputDataList = new ArrayList<Integer>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int stairCount = Integer.parseInt(br.readLine());
//            System.out.println("stairCount : " + stairCount);

            for(int i = 0; i < stairCount; i++) {
                inputDataList.add(Integer.parseInt(br.readLine()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputDataList;
    }

}
