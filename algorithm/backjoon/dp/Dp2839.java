package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp2839 {

    public static void main(String[] args) {
        String weight = inputData();
//        System.out.println("weight : " + weight);

        int size = Integer.parseInt(weight);
        int sugarArr[] = {3, 5}; // 3kg and 5kg
        int dp[] = new int[size+1];

        for (int i = 1; i < size+1; i++) {
            dp[i] = -1;
            if(i == 3 || i == 5) {
                dp[i] = 1;
            }
        }

        //6부터 dp 구하기
        for (int i = 6; i < size+1; i++) {
            dp[i] = minimum(dp[i-3], dp[i-5]);
//            System.out.println("dp[" + i + "] : " + dp[i]);
        }

//        System.out.println("minimum count : " + dp[size]);
        System.out.println(dp[size]);

    }

    public static String inputData() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String inputData = "";

        try {
//            System.out.print("Enter the data : ");
            inputData = br.readLine();
            br.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return inputData;
    }

    // 음수는 방법이 없는 것이기 때문에 음수는 제외
    public static int minimum(int minusThree, int minusFive) {
        if(minusThree <= 0) {
            if(minusFive <= 0) {
                return -1;
            } else {
                return minusFive+1;
            }
        } else {
            if(minusFive < 0) {
                return minusThree+1;
            }
        }

        if(minusThree >= minusFive) {
            return minusFive+1;
        } else {
            return minusThree+1;
        }
    }

}
