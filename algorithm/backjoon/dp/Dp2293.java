package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp2293 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 n, k가 주어진다. (1 ≤ n ≤ 100, 1 ≤ k ≤ 10,000)
         * 다음 n개의 줄에는 각각의 동전의 가치가 주어진다.
         * 동전의 가치는 100,000보다 작거나 같은 자연수이다. (1 ≤ v ≤ 100,000)
         *
         * 2. output 조건
         * 가치의 합이 k원이 되도록 하고 싶다. 그 경우의 수를 구하시오. (각각의 동전은 몇 개라도 사용할 수 있다.)
         * 사용한 동전의 구성이 같은데, 순서만 다른 것은 같은 경우이다. 경우의 수는 2^31보다 작다.
         *
         * 3. 해석
         * Combination 부류의 문제이다. 이런 종류의 문제는 Greedy 알고리즘으로 접근할 수도 있다고 생각하지만 풀릴지는 모르겠다.
         * 목표치 크기 만큼의 배열을 잡아두고서 dp를 찾아나가야 한다.
         * 조회를 할 때는 코인을 기준으로 돌리는게 맞다고 생각한다.
         * 예시 문제를 예를 들어서 설명하면 아래와 같다.
         * dp[10] = dp[10-1] + dp[10-2] + dp[10-5] 점화식이 성립한다.
         *
         * 이렇게 될 수 있는 이유는 각 코인을 1개만 선택하는 것이기 때문에 결국 -coinValue 만큼의 모든 경우의 수를 더할 수 있기 때문이다.
         * 부분 최적화도 성립하기 때문에 오류나 예외가 발생할 것이 없다.
         *
         * Example
         * 3 10
         * 1
         * 2
         * 5
         * Answer = 10
         *
         */
        int[] inputData = inputData();
        int numberOfCases = getNumberOfCases(inputData);
        System.out.println(numberOfCases);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer stringTokenizer = new StringTokenizer(br.readLine());
            int kindOfCoin = Integer.parseInt(stringTokenizer.nextToken());
            int targetValue = Integer.parseInt(stringTokenizer.nextToken());

            int[] inputData = new int[kindOfCoin + 1];
            inputData[0] = targetValue;
            for(int i = 1; i <= kindOfCoin; i++) {
                inputData[i] = Integer.parseInt(br.readLine());
            }
            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getNumberOfCases(int[] inputData) {
        int targetValue = inputData[0];
        int kindOfCoin = inputData.length - 1;
        int coinValue;
        int[] dp = new int[targetValue + 1];
        dp[0] = 1; // 코인 1개만 선택했을 때의 경우의 수를 추가해줘야 하는데, 이 초기 세팅을 따로 안해주기 위해서 설정함.

        for(int i = 1; i <= kindOfCoin; i++) {
            coinValue = inputData[i];
            for(int value = coinValue; value <= targetValue; value++) {
                dp[value] += dp[value - coinValue];
            }
        }

        return dp[targetValue];
    }

}
