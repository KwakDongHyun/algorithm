package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.StringTokenizer;

public class Dp5557 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 숫자의 개수 N이 주어진다. (3 ≤ N ≤ 100)
         * 둘째 줄에는 0 이상 9 이하의 정수 N개가 공백으로 구분해 주어진다.
         *
         * 2. output 조건
         * 첫째 줄에 상근이가 만들 수 있는 올바른 등식의 개수를 출력한다. 이 값은 2^63 - 1 이하이다.
         *
         * 3. 주의사항
         * 마지막 두 숫자 사이에는 '='을 넣고, 나머지 숫자 사이에는 '+' 또는 '-'를 넣어 등식을 만든다.
         * 왼쪽부터 계산할 때, 중간에 나오는 수가 모두 0 이상 20 이하이어야 한다.
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 11
         * 8 3 2 4 8 7 2 4 0 8 8
         * ----------------------
         * answer
         * 10
         *
         * 2.
         * 40
         * 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 1 1
         * ----------------------
         * answer
         * 7069052760
         *
         */

        int[] inputData = inputData();
        long result = solveProblem(inputData);
        System.out.println(result);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberCount = Integer.parseInt(br.readLine());
            int[] inputData = new int[numberCount];

            StringTokenizer st = new StringTokenizer(br.readLine());
            for(int i = 0; i < numberCount; i++) {
                inputData[i] = Integer.parseInt(st.nextToken());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static long solveProblem(int[] inputData) {
        int numberCount = inputData.length;
        int lastNumber = inputData[numberCount - 1];
        /*
         * 왼쪽부터 계산할 때, 중간에 나오는 수는 모두 (0 ≤ intermediate calculation result ≤ 20) 이다.
         */
        long[][] dp = new long[numberCount][21];
        int startNumber = inputData[0];
        dp[0][startNumber] = 1;
        /*System.out.println("startNumber : " + startNumber);
        System.out.println();*/

        int resultOfAddition;
        int resultOfSubtraction;
        for(int i = 1; i < numberCount - 1; i++) {
            int number = inputData[i];

            for(int j = 0; j < 21; j++) {
                if(dp[i-1][j] > 0) {
                    resultOfAddition = j + number;
                    resultOfSubtraction = j - number;

                    if((resultOfAddition) > -1 && (resultOfAddition) < 21) {
                        dp[i][resultOfAddition] += dp[i-1][j];
                    }

                    if((resultOfSubtraction) > -1 && (resultOfSubtraction) < 21) {
                        dp[i][resultOfSubtraction] += dp[i-1][j];
                    }
                }
            }

            /*System.out.println("i : " + i + ", number : " + number);
            for(int j = 0; j < 21; j++) {
                System.out.print(j + " : " + dp[i][j] + ", ");
            }
            System.out.println();
            System.out.println();*/

        }

//        System.out.println("lastNumber : " + lastNumber + ", result : " + dp[numberCount - 2][lastNumber]);
        return dp[numberCount - 2][lastNumber];
    }

}
