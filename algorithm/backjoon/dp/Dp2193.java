package backjoon.dp;


import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp2193 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 N이 주어진다. (1 ≤ N ≤ 90)
         *
         * 2. output 조건
         * N자리 이친수의 개수를 출력한다.
         *
         * 3. 해석
         * 이진수 중 특별한 성질을 갖는 '이친수'에 대해서 정의하면 아래와 같다.
         *   1. 이친수는 0으로 시작하지 않는다.
         *   2. 이친수에서는 1이 두 번 연속으로 나타나지 않는다. 즉, 11을 부분 문자열로 갖지 않는다.
         *
         */
        int specificBinaryNumLength = inputData();
        long countCaseOfSpecificBinaryNum = getCaseOfSpecificBinaryNum(specificBinaryNumLength);
        System.out.println(countCaseOfSpecificBinaryNum);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            return testCase;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static long getCaseOfSpecificBinaryNum(int specificBinaryNumLength) {
        long[][] dp = new long[specificBinaryNumLength + 1][2];
        for(int i = 1; i <= specificBinaryNumLength; i++) {
            if(i == 1) {
                dp[1][0] = 0;
                dp[1][1] = 1;
            } else {
                dp[i][0] = dp[i-1][0] + dp[i-1][1];
                dp[i][1] = dp[i-1][0];
            }
            System.out.println(dp[i][0] + " " + dp[i][1]);
        }
        return dp[specificBinaryNumLength][0] + dp[specificBinaryNumLength][1];
    }

}
