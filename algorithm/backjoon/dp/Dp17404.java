package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class Dp17404 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 집의 수 N(2 ≤ N ≤ 1,000)이 주어진다.
         * 둘째 줄부터 N개의 줄에는 각 집을 빨강, 초록, 파랑으로 칠하는 비용이 1번 집부터 한 줄에 하나씩 주어진다.
         * 집을 칠하는 비용은 1,000보다 작거나 같은 자연수이다.
         *
         * 2. output 조건
         * 첫째 줄에 모든 집을 칠하는 비용의 최솟값을 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 한 번에 완벽한 최적화를 노리려고 하다보니까 DP만의 강점을 제대로 사용하지 못한 것 같다는 생각이 든다.
         * 단 번에 최적화를 노리는 것은 아주 실력이 좋거나 문제를 제대로 이해했을 때나 가능한 얘기같다.
         * 나눠서 작게.. 독립적인 사건으로 풀 수 있도록 만들어서 푸는 연습을 하자.
         *
         * 나의 가장 큰 단점은 너무 많은 것을 고려해서 처음부터 완벽하게 진행하려고 하는 마음이 문제인 것 같다.
         * 이걸 고쳐야지만 지금보다 몇 단계 위로 올라갈 수 있을 것 같다.
         *
         * Example
         * 1.
         * 3
         * 26 40 83
         * 49 60 57
         * 13 89 99
         * ----------------------
         * answer
         * 110
         *
         * 2.
         * 3
         * 1 100 100
         * 100 1 100
         * 100 100 1
         * ----------------------
         * answer
         * 3
         *
         * 3.
         * 3
         * 1 100 100
         * 100 100 100
         * 1 100 100
         * ----------------------
         * answer
         * 201
         *
         */

        int[][] inputData = inputData();
        int minValue = solveProblem(inputData);
        System.out.println(minValue);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int houseCount = Integer.parseInt(br.readLine());
            int[][] inputData = new int[houseCount][3];

            for(int i = 0; i < houseCount; i++) {
                String[] data = br.readLine().split(" ");

                for(int j = 0; j < 3; j++) {
                    inputData[i][j] = Integer.parseInt(data[j]);
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[][] inputData) {
        int houseCount = inputData.length;
        int[][] dp = new int[houseCount][3];
        dp[0][0] = inputData[0][0];
        dp[0][1] = inputData[0][1];
        dp[0][2] = inputData[0][2];

        int INF = 1000_1001;
        int answer = INF;

        /*
         * 한 번에 하게 되면 dp의 값을 채우는 과정에서 어떤 색깔에서 시작했는지를 정확히 알기가 어렵다.
         * 설사 그것을 기록하면서 진행한다고 해도, 차선책으로 좋은 경우의 수를 모두 고려하지 않고서
         * 진행하게 되기 때문에, 제한적인 경우에 대해서만 최소값을 고려하게 된다..
         * 즉, 전체 경우의 수에 대해서 고려를 하지 않고서 최소값을 구하게 된다는 말이다.
         *
         * 따라서 아예 시작 색깔을 정하고서 dp를 채우는 방식으로 하나의 색에 대한 최적의 결과를 얻어낸다.
         */
        for(int startColor = 0; startColor < 3; startColor++) {
            for(int i = 0; i < 3; i++) {
                dp[0][i] = INF;

                if(i == startColor) {
                    dp[0][i] = inputData[0][i];
                }
            }

            for(int i = 1; i < houseCount; i++) {
                dp[i][0] = Math.min(dp[i-1][1], dp[i-1][2]) + inputData[i][0];
                dp[i][1] = Math.min(dp[i-1][0], dp[i-1][2]) + inputData[i][1];
                dp[i][2] = Math.min(dp[i-1][0], dp[i-1][1]) + inputData[i][2];
            }

            // 현 시작 색깔을 기준으로 최소값을 구한다.
            for(int i = 0; i < 3; i++) {
                if(startColor != i) {
                    answer = Math.min(answer, dp[houseCount - 1][i]);
                }
            }
        }

        return answer;
    }

}
