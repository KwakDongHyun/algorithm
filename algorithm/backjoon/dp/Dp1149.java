package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp1149 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 집의 수 N(2 ≤ N ≤ 1,000)이 주어진다.
         * 둘째 줄부터 N개의 줄에는 각 집을 빨강, 초록, 파랑으로 칠하는 비용이 1번 집부터 한 줄에 하나씩 주어진다.
         * 집을 칠하는 비용은 1,000보다 작거나 같은 자연수이다.
         *
         * 2. output 조건
         * 모든 집을 칠하는 비용의 최솟값을 출력한다.
         *
         * 3. 특징
         * red 기준으로 아래와 같다.
         * k번째 red를 칠할 때 최소비용 = min(k-1번째 G 최소비용, k-1번째 B 최소비용) + (k번째 R 비용)
         * 나머지 색에 대해서도 기본적으로 같다.
         */
        int[][] inputRGBArray = input2DimensionData();
        int[][] dp = new int[inputRGBArray.length][3];

        dp[1][0] = inputRGBArray[1][0];
        dp[1][1] = inputRGBArray[1][1];
        dp[1][2] = inputRGBArray[1][2];

        for (int i = 2; i < inputRGBArray.length; i++) {
            dp[i][0] = Math.min(dp[i - 1][1], dp[i - 1][2]) + inputRGBArray[i][0];
            dp[i][1] = Math.min(dp[i - 1][0], dp[i - 1][2]) + inputRGBArray[i][1];
            dp[i][2] = Math.min(dp[i - 1][0], dp[i - 1][1]) + inputRGBArray[i][2];
//            System.out.println("dp[" + i + "][0] : " + dp[i][0]);
//            System.out.println("dp[" + i + "][1] : " + dp[i][1]);
//            System.out.println("dp[" + i + "][2] : " + dp[i][2]);
        }

        int miniumCost = Math.min(dp[inputRGBArray.length - 1][0], dp[inputRGBArray.length - 1][1]);
        miniumCost = Math.min(miniumCost, dp[inputRGBArray.length - 1][2]);
        System.out.println(miniumCost);

    }

    public static int[][] input2DimensionData() {
        int[][] input2DimensionArray = null;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int houseCount = Integer.parseInt(br.readLine());
//            System.out.println("houseCount : " + houseCount);
            input2DimensionArray = new int[houseCount + 1][3];

            for (int i = 1; i <= houseCount; i++) {
                String[] line = br.readLine().split(" ");
                input2DimensionArray[i][0] = Integer.parseInt(line[0]);
                input2DimensionArray[i][1] = Integer.parseInt(line[1]);
                input2DimensionArray[i][2] = Integer.parseInt(line[2]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return input2DimensionArray;
    }

}
