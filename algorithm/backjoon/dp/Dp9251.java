package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp9251 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄과 둘째 줄에 두 문자열이 주어진다.
         * 문자열은 알파벳 대문자로만 이루어져 있으며, 최대 1000글자로 이루어져 있다.
         *
         * 2. output 조건
         * 첫째 줄에 입력으로 주어진 두 문자열의 LCS의 길이를 출력한다.
         * LCS(Longest Common Subsequence, 최장 공통 부분 수열)
         *
         * 3. 해석
         * 11053 문제와는 결이 다른 문제이다.
         * 비슷한 문제라고 처음에 잘못 생각해서 오히려 해결하기가 더 힘들었다.
         * 기존 해결법을 참고해서 접근하는 방법은 좋지만, 문제의 본질을 제대로 파악하지 못할 경우에 소모되는 비용이 너무 컸다..
         *
         * 4. 해결 방법
         *Todo
         * 본론으로 돌아오자.
         * 처음부터 한 번에 해결하려고 하기 보다는 문제를 단계 별로 하나씩 접근해야지 비로소 해결 방법에 가까워진다.
         * 1. 문자열 비교
         * 두 문자열을 우선은 서로 하나씩 비교를 해서 일치하지 않으면 0, 일치하면 1로 저장하자.
         * 그러면 2차원 배열에 일치하는 문자열이 한 눈에 알아보기가 쉬워진다.
         * 2차원 배열의 구성을 보면서 문제 해결의 힌트가 보여야 한다.
         * ---------------------------------------------------------------------------
         * 2. 문자열이 일치할 경우
         * 문자열 A와 B가 있다고 가정하고 설명한다.
         * A의 i번째 문자와 B의 j번째 문자가 서로 일치한다고 가정하면, dp[i][j] = dp[i-1][j-1] + 1이 된다는 것을 알 수 있다.
         * 왜냐면 각각 i번째 문자와 j번째 문자가 일치하기 전에, i-1번째 문자까지의 문자열과 j-1번째 문자까지의 문자열의 LCS의 값이 있을 것이고
         * 거기에 1만큼 길이가 추가되는 것이기 때문이다.
         * ---------------------------------------------------------------------------
         * 3. 문자열이 일치하지 않을 경우
         * 문자열이 일치하지 않을 경우는 문자열이 일치할 때의 경우의 연장 선상으로 생각하는 것이 보다 이해하기가 쉽다.
         * dp[i][j](LCS)에서 문자열 A의 i번째 문자와 문자열 B의 j번째 문자가 일치했을 경우에서 이어서 설명하겠다.
         * A 혹은 B에 임의의 문자 X가 뒤에 1개 추가됐다고 가정해보자.
         * (---AX) 문자열과 (--A) 문자열의 LCS은 X가 추가되기 이전과 같지 않은가?
         * 즉, dp[i+1][j] = dp[i][j] 라는 것을 알 수 있다.
         * 이는 반대로 dp[i][j+1] = dp[i][j]인 경우에도 성립하는 것을 알 수 있다.
         * ---------------------------------------------------------------------------
         * 기하학적으로 본다면 문자열이 일치하는 위치인 (i, j)를 기준으로 오른쪽과 아래를 향해서 각각의 인덱스에 위치하는 값들은 모두 같다는 것이다.
         * 그렇게 진행하다 보면 또다른 지점에서 일치하는 문자열이 새로이 등장할 것이고, 두 값이 상이할 경우 더 큰 값만을 넣으면 된다.
         * LCS 문자열 자체를 구하는게 아니라 단지 길이 만을 구하는 것이라서 이런 풀이와 접근이 가능하다고 생각한다.
         *
         * 참고로 이렇게 될 경우, 두 문자열 중에 누가 더 짧은지 비교할 필요도 없어지게 된다.
         *
         */
        String[] inputData = inputData();
        int lengthOfLongestCommonSubsequence = getLengthOfLongestCommonSubsequence(inputData);
        System.out.println(lengthOfLongestCommonSubsequence);

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] inputData = new String[2];
            inputData[0] = br.readLine();
            inputData[1] = br.readLine();
            return inputData;
        } catch (Exception e) {
            String[] inputData = new String[2];
            inputData[0] = "A";
            inputData[1] = "A";
            return inputData;
        }
    }

    public static int getLengthOfLongestCommonSubsequence(String[] inputData) {
        char[] searchString = inputData[0].toCharArray(); // 탐색 문자열
        char[] targetString = inputData[1].toCharArray(); // 타겟 문자열
        int searchLength = searchString.length;
        int targetLength = targetString.length;

        // LCS 길이 값과 일치하는 문자의 index를 저장. 공식을 위해서 0번째 index 추가.
        // i or j가 0일 때는 자동으로 0으로 세팅해주니 별도로 해줄 필요는 없다.
        /*
         * example
         * ACAYKP
         * CAPCAK
         */
        int[][] dp = new int[searchLength + 1][targetLength + 1];
        for(int i = 1; i <= searchLength; i++) {
            for(int j = 1; j <= targetLength; j++) {
                if(searchString[i-1] == targetString[j-1]) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                }
            }
        }

        return dp[searchLength][targetLength];
    }

}
