package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp1890 {

    private static int[][] inputData;
    private static long[][] dp;
    private static int mapSize;

    private static int[] moveX = {1, 0};
    private static int[] moveY = {0, 1};

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 게임 판의 크기 N (4 ≤ N ≤ 100)이 주어진다.
         * 그 다음 N개 줄에는 각 칸에 적혀져 있는 수가 N개씩 주어진다.
         * 칸에 적혀있는 수는 (0 ≤ N ≤ 9)인 정수이며, 가장 오른쪽 아래 칸에는 항상 0이 주어진다.
         *
         * 2. output 조건
         * 가장 왼쪽 위 칸에서 가장 오른쪽 아래 칸으로 문제의 규칙에 맞게 갈 수 있는 경로의 개수를 출력한다.
         * 경로의 개수는 (2^63 - 1)보다 작거나 같다.
         * 각 칸에 적혀있는 수는 현재 칸에서 갈 수 있는 거리를 의미한다. 반드시 오른쪽이나 아래쪽으로만 이동해야 한다.
         * 한 번 점프를 할 때, 방향을 바꾸면 안 된다.
         * 즉, 한 칸에서 오른쪽으로 점프를 하거나, 아래로 점프를 하는 두 경우만 존재한다.
         *
         * 3. 주의사항
         * 경우의 수가 많기 때문에 메모이제이션 필수다. DP를 무조건 써야한다는 것이다.
         * 또한 경우의 수이면서 경로 탐색이기 때문에 완전 탐색을 생각하고 DFS를 사용해야한다.
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 4
         * 2 3 3 1
         * 1 2 1 3
         * 1 2 3 1
         * 3 1 1 0
         * -------
         * answer
         * 3
         *
         */
        inputData();
        long result = solveProblem(0, 0);
        System.out.println(result);

    }

    public static void inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            mapSize = Integer.parseInt(br.readLine());
            inputData = new int[mapSize][mapSize];
            dp = new long[mapSize][mapSize];

            StringTokenizer st;
            for(int i = 0; i < mapSize; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < mapSize; j++) {
                    inputData[i][j] = Integer.parseInt(st.nextToken());
                    dp[i][j] = -1;
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static long solveProblem(int row, int col) {
        if(row == mapSize - 1 && col == mapSize - 1) {
            return 1;
        }

        if(dp[row][col] != -1) {
            return dp[row][col];
        } else {
            dp[row][col] = 0;
            int nextX, nextY;
            int jumpPower = inputData[row][col];

            for(int i = 0; i < 2; i++) {
                nextX = col + jumpPower * moveX[i];
                nextY = row + jumpPower * moveY[i];

                if(nextX < 0 || nextX >= mapSize || nextY < 0 || nextY >= mapSize) {
                    continue;
                }
//                if(inputData[nextY][nextX] == 0) {
//                    continue;
//                }

                dp[row][col] += solveProblem(nextY, nextX);
            }
        }

        return dp[row][col];
    }

}
