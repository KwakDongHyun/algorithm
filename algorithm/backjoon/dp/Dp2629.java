package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class Dp2629 {

    private static int[] weights;
    private static int[] beads;

    public static void main(String[] args) {
        inputData();
        String[] results = solveProblem();
        for(String result : results) {
            System.out.print(result + " ");
        }
    }

    public static void inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int weightCount = Integer.parseInt(br.readLine());
            weights = new int[weightCount];
            StringTokenizer st = new StringTokenizer(br.readLine());

            for(int i = 0; i < weightCount; i++) {
                weights[i] = Integer.parseInt(st.nextToken());
            }

            int beadCount = Integer.parseInt(br.readLine());
            beads = new int[beadCount];
            st = new StringTokenizer(br.readLine());

            for(int i = 0; i < beadCount; i++) {
                beads[i] = Integer.parseInt(st.nextToken());
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static String[] solveProblem() {
        int weightCount = weights.length;
        int beadCount = beads.length;

        boolean[][] dp = new boolean[weightCount][15001];
        dp[0][weights[0]] = true;

        for(int i = 1; i < weightCount; i++) {
            int weight = weights[i];
            dp[i][weight] = true;

            for(int j = 1; j < 15001; j++) {
                if(dp[i-1][j]) {
                    dp[i][j] = true;
                    if(j + weight < 15001) {
                        dp[i][j + weight] = true;
                    }
                    dp[i][Math.abs(j - weight)] = true;
                }
            }
        }

        String[] outputs = new String[beadCount];
        for(int i = 0; i < beadCount; i++) {
            int bead = beads[i];
            if(bead < 15001 && dp[weightCount - 1][bead]) {
                outputs[i] = "Y";
            } else {
                outputs[i] = "N";
            }
        }

        return outputs;
    }

}
