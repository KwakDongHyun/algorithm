package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Dp1003 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 0 <= N <= 40
         *
         * 2. output 조건
         * case별 0과 1의 호출 개수.
         */
        List<Integer> inputDataList = inputDataList();
        List<int[][]> resultList = new ArrayList<int[][]>();

        for(int i = 0; i < inputDataList.size(); i++) {
            resultList.add(countCallFibo(inputDataList.get(i)));
            int[][] result = resultList.get(i);
            System.out.println(result[inputDataList.get(i)][0] + " " + result[inputDataList.get(i)][1]);
        }

    }

    public static List<Integer> inputDataList() {
        List<Integer> inputDataList = new ArrayList<Integer>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCaseCnt = Integer.parseInt(br.readLine());

            for(int i = 0; i < testCaseCnt; i++) {
                inputDataList.add(Integer.parseInt(br.readLine()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputDataList;
    }

    /**
     * 0과 1인 경우도 생각해야한다.
     * @param sequence
     * @return
     */
    public static int[][] countCallFibo(int sequence) {
        int[][] dp = new int[sequence + 1][2];
        for(int i = 0; i <= sequence; i++) {
            if(i == 0) {
                dp[i][0] = 1;
                dp[i][1] = 0;
            } else if(i == 1) {
                dp[i][0] = 0;
                dp[i][1] = 1;
            } else {
                dp[i][0] = dp[i-1][0] + dp[i-2][0];
                dp[i][1] = dp[i-1][1] + dp[i-2][1];
            }
        }

        return dp;
    }

}
