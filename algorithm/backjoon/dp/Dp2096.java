package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp2096 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 N(1 ≤ N ≤ 100,000)이 주어진다. 다음 N개의 줄에는 숫자가 세 개씩 주어진다.
         * 숫자는 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 중의 하나가 된다.
         *
         * 2. output 조건
         * 얻을 수 있는 최대 점수와 최소 점수를 띄어서 출력한다.
         *
         * 3. 주의사항
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 3
         * 1 2 3
         * 4 5 6
         * 4 9 0
         * -------
         * answer
         * 18 6
         *
         */

        int[][] inputData = inputData();
        int[] result = solveProblem(inputData);
        System.out.print(result[0] + " " + result[1]);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int rowSize = Integer.parseInt(br.readLine());
            int[][] inputData = new int[rowSize][3];

            StringTokenizer st;
            for(int i = 0; i < rowSize; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < 3; j++) {
                    inputData[i][j] = Integer.parseInt(st.nextToken());
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int[] solveProblem(int[][] inputData) {
        int rowSize = inputData.length;
        int[][][] dp = new int[rowSize][3][2];
        int[] result = new int[2];

        dp[0][0][0] = inputData[0][0];
        dp[0][1][0] = inputData[0][1];
        dp[0][2][0] = inputData[0][2];

        dp[0][0][1] = inputData[0][0];
        dp[0][1][1] = inputData[0][1];
        dp[0][2][1] = inputData[0][2];

        for(int i = 1; i < rowSize; i++) {
            for(int j = 0; j < 3; j++) {
                switch (j) {
                    case 0 :
                        dp[i][j][0] = Math.max(dp[i-1][j][0], dp[i-1][j+1][0]) + inputData[i][j];

                        dp[i][j][1] = Math.min(dp[i-1][j][1], dp[i-1][j+1][1]) + inputData[i][j];
                        break;
                    case 1 :
                        dp[i][j][0] = Math.max(dp[i-1][j][0], dp[i-1][j-1][0]);
                        dp[i][j][0] = Math.max(dp[i][j][0], dp[i-1][j+1][0]) + inputData[i][j];

                        dp[i][j][1] = Math.min(dp[i-1][j][1], dp[i-1][j-1][1]);
                        dp[i][j][1] = Math.min(dp[i][j][1], dp[i-1][j+1][1]) + inputData[i][j];
                        break;
                    case 2 :
                        dp[i][j][0] = Math.max(dp[i-1][j][0], dp[i-1][j-1][0]) + inputData[i][j];

                        dp[i][j][1] = Math.min(dp[i-1][j][1], dp[i-1][j-1][1]) + inputData[i][j];
                        break;
                }

            }
        }

        int max = 0, min = 900_001;
        for(int i = 0; i < 3; i++) {
            if(max < dp[rowSize - 1][i][0]) {
                max = dp[rowSize - 1][i][0];
            }

            if(min > dp[rowSize - 1][i][1]) {
                min = dp[rowSize - 1][i][1];
            }
        }

        result[0] = max;
        result[1] = min;

        return result;
    }

}
