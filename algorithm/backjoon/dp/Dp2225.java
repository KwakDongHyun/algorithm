package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp2225 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 두 정수 N(1 ≤ N ≤ 200), K(1 ≤ K ≤ 200)가 주어진다.
         * 다음 n개의 줄에는 각각의 동전의 가치가 주어진다.
         * 동전의 가치는 100,000보다 작거나 같은 자연수이다. (1 ≤ v ≤ 100,000)
         * 가치가 같은 동전이 여러 번 주어질 수도 있다.
         *
         * 2. output 조건
         * 0부터 N까지의 정수 K개를 더해서 그 합이 N이 되는 경우의 수를 구하시오.
         * 덧셈의 순서가 바뀐 경우는 다른 경우로 센다(1+2와 2+1은 서로 다른 경우).
         * 또한 한 개의 수를 여러 번 쓸 수도 있다.
         *
         * 3. 해석
         * 경우의 수 중에서 이렇게 배열로 중첩해서 푸는 문제가 있었는데.. 기억이 안나네.
         * 아무튼 기존에 풀어봤었던 유형이다.
         * 이런 문제는 규칙을 빠르게 찾을 수록 빠르게 풀 수 있다. 손으로 하나씩 써가면서..
         *
         *
         * Example
         * 1.
         * 20 2
         * -----------------
         * answer
         * 21
         *
         */
        int[] inputData = inputData();
        System.out.println(solveProblem(inputData));

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int[] inputData = new int[2];
            inputData[0] = Integer.parseInt(st.nextToken());
            inputData[1] = Integer.parseInt(st.nextToken());

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[] inputData) {
        int targetValue = inputData[0];
        int totalCount = inputData[1];
        int[] dp = new int[targetValue + 1];
        int[] countArr = new int[totalCount + 1];
        int count = 0;

        for(int c = 1; c <= totalCount; c++) {
            for(int i = targetValue; i > -1; i--) {
                if(i == targetValue) {
                    dp[i] = 1;
                } else {
                    if(c > 1) {
                        dp[i] = (dp[i+1] + dp[i]) % 1_000_000_000;
                    }
                }

                countArr[c] = (countArr[c] + dp[i]) % 1_000_000_000;
            }
        }

        return countArr[totalCount];
    }

}
