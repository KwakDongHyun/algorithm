package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp1699 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 자연수 N이 주어진다. (1 ≤ N ≤ 100,000)
         *
         * 2. output 조건
         * 주어진 자연수를 제곱수의 합으로 나타낼 때에 그 제곱수 항의 최소 개수를 출력한다.
         * 예를 들어 11 = 3^2 + 1^2 + 1^2(3개 항)이다.
         * 이런 표현방법은 여러 가지가 될 수 있는데, 11의 경우 11 = 2^2 + 2^2 + 1^2 + 1^2 + 1^2 (5개 항)도 가능하다.
         *
         * 3. 해석
         * dp로 하려면 결국 dp[n]은 min(dp[i] + dp[n-i])의 값이 들어간다. (1 ≤ i ≤ n/2)
         * ---------------------------
         * 위 방법대로 하면 time out이 발생하고 불필요한 연산이 들어간다.
         * dp는 가장 큰 제곱수를 빼고 나머지 수만큼의 항으로 이루어져 있다.
         * 예를 들면 11은 가장 큰 제곱수가 9이고, 나머지 2만큼만 채워주면 된다.
         * 그리고 초기 수들은 1로만 구성되어 있기 때문에, 이 성질만 만족시키면 dynamic하게 프로그램을 돌릴 수가 있다.
         *
         * 4. 주의사항
         * 2중 for문을 통한 완전 탐색의 경우 worst case가 10^10이다.
         * 너무 단순하게 생각하면 안된다.. 문제를 분석해서 부분 최적화를 항상 잘하자.
         *
         * Example
         * 1.
         * 7
         * -------
         * answer
         * 4
         *
         */
        int inputData = inputData();
        int squareNumberMinTerm = getSquareNumberMinTerm(inputData);
        System.out.println(squareNumberMinTerm);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static int getSquareNumberMinTerm(int inputData) {
        int[] dp = new int[inputData + 1];

        for(int i = 1; i <= inputData; i++) {
            // default는 1의 제곱으로만 표현할 수 있을 것이다.
            dp[i] = i;
            for(int j = 2; j*j <= i; j++) {
                // 제곱의 경우 무조건 1이 저장되는 구조도 충족시킨다.
                dp[i] = Math.min(dp[i], dp[i-j*j] + 1);
            }
//            System.out.println("dp[" + i + "] : " + dp[i]);
        }
        return dp[inputData];
    }

}
