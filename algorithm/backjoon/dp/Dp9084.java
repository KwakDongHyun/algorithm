package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Dp9084 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력의 첫 줄에는 테스트 케이스의 개수 T(1 ≤ T ≤ 10)가 주어진다.
         * 각 테스트 케이스의 첫 번째 줄에는 동전의 가지 수 N(1 ≤ N ≤ 20)이 주어지고,
         * 두 번째 줄에는 N가지 동전의 각 금액이 오름차순으로 정렬되어 주어진다.
         * 각 금액은 정수로서 1원부터 10000원까지 있을 수 있으며 공백으로 구분된다.
         * 세 번째 줄에는 주어진 N가지 동전으로 만들어야 할 금액 M(1 ≤ M ≤ 10000)이 주어진다.
         * 편의를 위해 방법의 수는 2^31 - 1 보다 작고, 같은 동전이 여러 번 주어지는 경우는 없다.
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해 입력으로 주어지는 N가지 동전으로 금액 M을 만드는 모든 방법의 수를 한 줄에 하나씩 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 3
         * 2
         * 1 2
         * 1000
         * 3
         * 1 5 10
         * 100
         * 2
         * 5 7
         * 22
         * ----------------------
         * answer
         * 501
         * 121
         * 1
         *
         */

        List<int[]> inputData = inputData();
        for(int i = 0; i < inputData.size(); i++) {
            System.out.println(solveProblem(inputData.get(i)));
        }

    }

    public static List<int[]> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            List<int[]> inputData = new ArrayList<int[]>();

            for(int i = 0; i < testCase; i++) {
                int coinNumber = Integer.parseInt(br.readLine());
                int[] coins = new int[coinNumber + 1];

                StringTokenizer st = new StringTokenizer(br.readLine());
                for(int j = 1; j <= coinNumber; j++) {
                    coins[j] = Integer.parseInt(st.nextToken());
                }

                int targetAmount = Integer.parseInt(br.readLine());
                coins[0] = targetAmount;
                inputData.add(coins);
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[] inputData) {
        int length = inputData.length;
        int targetAmount = inputData[0];
        int[] dp = new int[targetAmount + 1];
        dp[0] = 1;

        for(int i = 1; i < length; i++) {
            int coin = inputData[i];
            int amount;

            for(int j = 0; j <= targetAmount; j++) {
                amount = j + coin;
                if(amount <= targetAmount) {
                    dp[amount] += dp[j];
                }
            }
        }

        return dp[targetAmount];
    }

}
