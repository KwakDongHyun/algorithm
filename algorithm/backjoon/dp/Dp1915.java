package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Dp1915 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 n, m(1 ≤ n, m ≤ 1,000)이 주어진다.
         * 다음 n개의 줄에는 m개의 숫자로 배열이 주어진다.
         * n x m 크기의 배열이다.
         *
         * 2. output 조건
         * 1로 된 가장 큰 정사각형의 넓이를 출력한다.
         *
         * 3. 주의사항
         * 기하학적인 문제를 컴퓨터의 관점에서 푸는 것이라 난이도가 어렵다.
         * 즉, 수학적으로는 정사각형을 증명하는 방법이야 많겠지만 증명 모두가 컴퓨터적인 사고에 맞지 않을 수도 있기 때문에
         * 컴퓨터적인 사고로 쉽게 이해할 수 있는 방법을 찾는 것이 핵심이다.
         * 다차원적인 정교하고 심오한 문제일수록 이런 현상이 두드러진다는 것을 알 수 있다.
         * 다음 단계로 나아가기 위해서는 이런 시각과 사고에 대해서 훈련을 해야한다.
         *
         * 4. 해석
         * 양파 껍질처럼 정사각형이 점진적으로 늘어난다고 생각하는 것까지 접근했다.
         * 어차피 컴퓨터는 사람처럼 한 눈에 모든 걸 보고 탐색할 수 없기 때문에 반드시 순차적으로 탐색할 수 밖에 없다.
         * 4x4 정사각형을 아래와 같이 생각해보자.
         * 원래라면 4x4를 그리고 연필로 겹겹이 만드는 시야를 그리갰지만.. 주석에서 그리 표현을 못해 이렇게 표현한다.
         *
         * 1   1 1   1 1 1   1 1 1 1
         *     1 1   1 1 1   1 1 1 1
         *           1 1 1   1 1 1 1
         *                   1 1 1 1
         *
         * 정사각형이 되기 위해서는 원론적으로 아래 원칙이 지켜져야 한다.
         *   1. 정사각형의 내부는 모두 1로 채워져야한다.
         * 그래서 난 처음에는 가로로 수를 누적하면서 특정 공식을 통한 연산 처리로 답을 구하고자 했다.
         * 근데 가로 길이만 파악이 되고 세로 길이를 알 수가 없기 때문에, 내가 만든 공식은 정사각형이라는 것을 증명하기가 까다로웠다.
         * 그렇다고 세로 길이를 알고자 하여 세로 방향 연산 과정을 추가하면 연산 수가 늘면서 처리가 복잡해진다.
         * 그럼 다른 방법으로 문제에 접근을 해야할 것 같았다.
         *
         * 한 정사각형은 더 작은 정사각형들로 표현이 가능하다는 것을 주목했다.
         * 더 작은 정사각형이 존재하는지 증명이 되면, 더 큰 정사각형도 증명이 쉽지 않을까? 라는게 접근 포인트였다.
         * 즉, dp[i][j]는 (i,j) 포인트를 오른쪽 하단의 꼭짓점이라고 정의했을 때의 어떤 정사각형인지 확인할 수 있는 어떤 정보를 기입하면 될 것 같았다.
         * 이후 여기부터는 막혀서 약간의 힌트를 얻었다.
         *
         * dp에는 최대로 구성할 수 있는 정사각형 길이를 기입하면 된다.
         * 가로 길이 누적 때도 직감적으로 느꼈지만 한 예로 2x2가 정사각형이 되려면 위, 아래, 대각선이 정사각형이면 된다.
         * 즉, 정사각형 내부를 구성하는 각각의 1x1 정사각형이 정사각형인지 증명이 되면 된다.
         * 대각선 방향은 이전 단계의 정사각형인 1x1 정사각형을 의미한다.
         *
         * 3x3은 2x2의 정사각형들로 이루어져 있고, 2x2가 정사각형들로 전부 이루어져 있는지 확인만 하면 되었다.
         * 0이 하나라도 있으면 (i,j) 포인트가 1의 값을 가질 때, 오로지 1x1 정사각형 밖에 만들 수가 없었다. (본인이 0이면 당연히 0)
         * 근데 왜 위, 아래, 대각선만 확인하면 되느냐?
         * 3x3을 예시로 설명하면 아래와 같다.
         *
         * 이 정사각형이 내부가 전부 1로 이루어져 있는지 알고 싶으면 더 작은 2x2 정사각형들이 존재하는지만 확인하면 된다.
         *
         * 1 1 1   1 1 | 1      1 | 1 1     1 1   1
         * 1 1 1   1 1 | 1      1 | 1 1     - - - -
         * 1 1 1   - - - -      - - - -     1 1 | 1
         *         1 1   1      1   1 1     1 1 | 1
         *
         * 내부가 바로 이전 크기인 n-1 x n-1 정사각형인지 확인할 수만 있으면 된다.
         * 예를 들어서 4x4라면 3x3 정사각형이 3군데 존재한다는걸 증명하면 되고.
         * 만약 셋 중에서 하나라도 2x2가 아닌 1x1 정사각형 밖에 안된다면 (i,j) 포인트에서는 2x2가 최대로 만들 수 있는 정사각형인 것이다.
         * 더 작은 정사각형이 있다는 것은 상위 정사각형 입장에서는 어딘가에 0이 존재해서 만들 수 없다는 것을 의미하기 때문이다.
         * dp에는 최대로 구성할 수 있는 정사각형의 길이가 무엇인지 저장하기 때문에, 추후에 오른쪽 하단으로 데이터를 탐색해나갈 수록
         * 더 상위의 정사각형이 존재하는지 알기도 쉽다.
         *
         * 이런 풀이가 가능한 것도 데이터 특성으로 인해서 길이가 1씩 늘어난다는 rule이 있기 때문인 것도 있다.
         * 게다가 직사각형이나 다각형일 경우 이렇게 쉽게 풀이가 되지는 않겠지.. 적분이나 다른 고등 수학 개념이 들어가 있어야 풀 수 있을 것이다.
         *
         * Example
         * 1.
         * 4 4
         * 0100
         * 0111
         * 1110
         * 0010
         * -------
         * answer
         * 4
         *
         */

        int[][] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result * result);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int n = Integer.parseInt(st.nextToken());
            int m = Integer.parseInt(st.nextToken());

            int[][] inputData = new int[n + 1][m + 1];
            char[] chars;
            for(int i = 1; i <= n; i++) {
                chars = br.readLine().toCharArray();
                for(int j = 1; j <= m; j++) {
                    // ASCII 0 ~ 9 = 80 ~ 89
                    inputData[i][j] = chars[j-1] - '0';
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[][] inputData) {
        int height = inputData.length;
        int width = inputData[0].length;

        /*
         * 위의 풀이법을 쉽게 적용하기 위해서 입력받을 때 배열의 각 차원을 +1만큼 늘려서 IndexOutOfBound exception을 방지한다.
         */
        int[][] dp = new int[height][width];
        for(int i = 1; i < height; i++) {
            for(int j = 1; j < width; j++) {
                if(inputData[i][j] == 0) {
                    dp[i][j] = 0;
                } else {
                    dp[i][j] = min(dp[i-1][j-1], dp[i-1][j], dp[i][j-1]) + inputData[i][j];
                }

//                System.out.println("dp[" + i + "][" + j + "] : " + dp[i][j]);
            }
        }

        return Arrays.stream(dp).flatMapToInt(arrays -> Arrays.stream(arrays))
                .max()
                .getAsInt();
    }

    private static int min(int a, int b, int c) {
        int result = Math.min(a, b);
        return Math.min(result, c);
    }

}
