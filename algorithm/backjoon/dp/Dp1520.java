package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp1520 {

    private static int row, col;
    private static int[][] inputData;
    private static int[][] dp; // 메모이제이션뿐만 아니라 visit 여부를 기록하는 역할도 한다.
    // 좌 상 우 하
    private static int[] moveX = {-1, 0, 1, 0};
    private static int[] moveY = {0, -1, 0, 1};

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에는 지도의 세로의 크기 M과 가로의 크기 N이 빈칸을 사이에 두고 주어진다.  (1 ≤ M, N ≤ 500)
         * 이어 다음 M개 줄에 걸쳐 한 줄에 N개씩 위에서부터 차례로 각 지점의 높이가 빈 칸을 사이에 두고 주어진다.
         * 각 지점의 높이는 10000이하의 자연수이다. (1 ≤ value ≤ 10,000)
         *
         * 2. output 조건
         * 현재 제일 왼쪽 위 칸에서 제일 오른쪽 아래 칸 지점으로 가려고 한다. 항상 높이가 더 낮은 지점으로만 이동하여 목표 지점까지 가고자 한다.
         * 첫째 줄에 이동 가능한 경로의 수 H를 출력한다. 모든 입력에 대하여 H는 10억 이하의 음이 아닌 정수이다.
         *
         * 3. 해석
         * 문제 자체만 봤을 때는 DFS로 완전 탐색을 하는 것 말고는 생각나는 방법이 전혀 없었다.
         * 그런데 이걸 DP로 풀 수 있다는데.. 이게 가능할까?
         *
         * 4. 풀이 과정
         * DFS로 하면 특정 case에서 역시 Time out이 발생한다.
         * 심지어 내가 처음에 푼 방법은 DFS의 일종인 backtracking이었다..
         * 그렇다고 DFS를 쓰지 않을 수는 없기 때문에 생각해볼만한 것은 메모이제이션을 일부 도입하는 것이다.
         * 이미 전에 경로 탐색을 하면서 지나가본적 있는 경우, 같은 탐색을 할 필요 없이 저장된 배열의 값을 반환해서 빠르게 푸는 것이다.
         * --------------------------------------------------------------------------------------
         * 이렇게만 해도 time out이 발생한다.
         * 극한까지 최적화를 해주지 않았기 때문이다.
         * 어차피 가봤자 길이 없는 곳인데도 탐색을 하면서 재차 다시 검사하는 구간이 존재하기 때문이다.
         * 즉, 한 번 가봤으면 거기에 길이 있는지 없는지를 저장해두고 저장한 결과를 토대로 빠르게 답을 도출하라는 의미이다.
         *
         * 솔직히 이미 직감적으로 어떻게 풀지는 알았던 상황이었는데.. 논리적으로 충분히 설명을 하지 못했고, 유도를 못했다..
         * 아직도 내가 DP에 대해서 제대로 감을 못 잡아서 풀지 못한 것 같다.
         * 처음부터 완벽하게 잘 풀려는 생각보다는 점진적으로 문제를 업그레이드 해가면서 푸는 연습을 우선 해보자.
         *
         *
         * Example
         * 1.
         * 4 5
         * 50 45 37 32 30
         * 35 50 40 20 25
         * 30 30 25 17 28
         * 27 24 22 15 10
         * -------
         * answer
         * 3
         *
         */
        inputData();
        int result = getNumberOfRoute(0, 0);
        System.out.println(result);

    }

    public static void inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            row = Integer.parseInt(st.nextToken());
            col = Integer.parseInt(st.nextToken());
            inputData = new int[row][col];
            dp = new int[row][col];

            for(int i = 0; i < row; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < col; j++) {
                    inputData[i][j] = Integer.parseInt(st.nextToken());
                    dp[i][j] = -1;
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * dfs로 순회. 여기에 메모이제이션 개념을 더해서 속도를 개선한다.
     * @param x
     * @param y
     * @return
     */
    public static int getNumberOfRoute(int x, int y) {
//        System.out.println("row : " + row + " col : " + col);
        if(x == col - 1 && y == row - 1) {
            return 1;
        }

        /*
         * 방문한 적 있으면 메모이제이션을 활용해서 빠르게 풀기.
         * 일부 구간이 겹치면, 그 구간은 굳이 처음부터 다시 탐색할 필요는 없으므로 분기점에서 +1만큼 해준다는 개념으로 진행하면 된다.
         * 탐색은 단 한 번만 하도록 한 번 더 최적화를 해준다.
         */
        if(dp[y][x] != -1) {
            return dp[y][x];
        } else {
            dp[y][x] = 0; // 이렇게 해주면 길이 없는 곳을 재차 탐색하는 과정을 없앨 수 있다.
            for(int i = 0; i < 4; i++) {
                int nextX = x + moveX[i];
                int nextY = y + moveY[i];

                if(nextX < 0 || nextX >= col || nextY < 0 || nextY >= row) {
                    continue;
                }

                if(inputData[y][x] > inputData[nextY][nextX]) {
                    dp[y][x] += getNumberOfRoute(nextX, nextY);
                }
            }
        }

        // 탐색 과정에서 얻은 경로의 수를 시작점까지 끌고와야 하므로 이렇게 해야한다.
        // 가만히 냅두면 분기점에만 정보를 저장한 채로 다시 회귀한다.
        return dp[y][x];
    }

}
