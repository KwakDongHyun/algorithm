package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Dp2775 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄 test case 수, 이후 각 case 별로 2줄씩 입력 받음.
         * 첫째 줄은 층 수 k, 둘째 줄은 호 n
         * 1 <= k, n <= 14
         *
         * 2. output 조건
         * case별 k층 n호에 사는 거주민 수.
         */
        List<int[]> inputDataList = inputDataList();
        List<Integer> resultList = new ArrayList<Integer>();
        for(int i = 0; i < inputDataList.size(); i++) {
            resultList.add(countResident(inputDataList.get(i)));
            System.out.println(resultList.get(i));
        }

    }

    public static List<int[]> inputDataList() {
        List<int[]> inputDataList = new ArrayList<int[]>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCaseCnt = Integer.parseInt(br.readLine());
//            System.out.println("testCaseCnt : " + testCaseCnt);

            for(int i = 0; i < testCaseCnt; i++) {
                int[] data = new int[2];
                data[0] = Integer.parseInt(br.readLine());
                data[1] = Integer.parseInt(br.readLine());
                inputDataList.add(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputDataList;
    }

    /**
     * Household Count(a, b) = sum (a-1, k) from k=1 to b.
     * = {sum (a, b-1) from k=1 to b-1} + Household Count(a-1, b)
     * 이거 파스칼의 삼각형의 정리와 유사하다. 잘 이해가 안되면 그림을 그려봐.
     *
     * @param householdInfo
     * @return
     */
    public static int countResident(int[] householdInfo) {
        int layer = householdInfo[0];
        int ho = householdInfo[1];
        int[][] dp = new int[layer + 1][ho + 1];

        for(int i = 0; i <= layer; i++) {
            if(i == 0) {
                for(int j = 1; j <= ho; j++) {
                    dp[i][j] = j;
                }
            } else {
                //
                for(int j = 1; j <= ho; j++) {
                    if(j == 1) {
                        dp[i][j] = dp[i-1][j];
                    } else {
                        dp[i][j] = dp[i][j-1] + dp[i-1][j];
                    }
                }
            }
        }

        return dp[layer][ho];
    }

}
