package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Dp2156 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 포도주 잔의 개수 n이 주어진다. (1 ≤ n ≤ 10,000)
         * 둘째 줄부터 n+1번째 줄까지 포도주 잔에 들어있는 포도주의 양이 순서대로 주어진다. (0 ≤ A ≤ 1,000)
         *
         * 2. output 조건
         * 최대로 마실 수 있는 포도주의 양을 출력한다.
         *
         * 3. Rule
         * 연속으로 놓여 있는 3잔을 모두 마실 수는 없다.
         *
         * 4. 특징
         * DP 2579번 문제와 거의 같다. 그러나 차이점이 하나가 있고, 그로 인해서 확인해야할 값이 하나 더 생긴다.
         * 차이점은 목표지점의 값(data[n] 값)을 반드시 포함해야 하느냐 아니냐의 차이일 뿐이다.
         * 포함하지 않아도 되기 때문에 dp[n-1]도 비교 대상에 넣어야 정확한 답이 도출된다.
         * 이 의미는 무엇이냐면, 값을 1개 더 추가했는데 이를 포함해서 총합을 구해봤더니 오히려 총합이 작아져서 기존의 총합을 유지하는게 더 나은 경우이다.
         *
         */
        int[] inputDataList = inputDataList();
        int maxSumSequence = getMaxSumSequence(inputDataList);
        System.out.println(maxSumSequence);
    }

    public static int[] inputDataList() {
        int[] inputDataList = null;

        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int sequenceSize = Integer.parseInt(br.readLine());
            inputDataList = new int[sequenceSize + 1];

            for(int i = 1; i <= sequenceSize; i++) {
                inputDataList[i] = Integer.parseInt(br.readLine());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputDataList;
    }

    public static int getMaxSumSequence(int[] inputDataList) {
        int listSize = inputDataList.length - 1;
        int[] dp = new int[listSize + 1];

        for(int i = 1; i <= listSize; i++) {
            if(i == 1) {
                dp[i] = inputDataList[i];
            } else if(i == 2) {
                dp[i] = inputDataList[i] + inputDataList[i-1];
            } else {
                dp[i] = Math.max(dp[i-2], dp[i-3] + inputDataList[i-1]) + inputDataList[i];
                /*
                 * 그리고 하나 더 비교해야하는 대상. 바로 dp[i-1]이다.
                 * (dp[i-2] + inputDataList[i]), (dp[i-3] + inputDataList[i-1] + inputDataList[i]), dp[i-1]
                 * 위 3가지 중에서 max 값을 구해야 한다.
                 */
                dp[i] = Math.max(dp[i], dp[i-1]);
            }
//            System.out.println("dp[" + i + "] : " + dp[i]);
        }

        return Arrays.stream(dp)
                .max()
                .getAsInt();
    }

}
