package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp2133 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 N(1 ≤ N ≤ 30)이 주어진다.
         *
         * 2. output 조건
         * 3×N 크기의 벽을 2×1, 1×2 크기의 타일로 채우는 경우의 수를 구해보자.
         * 첫째 줄에 경우의 수를 출력한다.
         *
         * 3. 해석
         * 기존에 풀었던 타일 채우기 문제가 변형된 버전이다.
         *
         * 4. 풀이 과정
         * 점화식 세우기가 매우 힘들었다..
         * 타일의 모양에 집중하지 말고, 기본 형태에 문자를 부여하고 다시 분석해보면 결국 수열이라는 것을 알 수 있다.
         * N=2 일 때는 3가지 타입(a, b, c)이 존재하고, N이 4 이상일 때 추가적으로 2가지 타입 (e, f)을 사용할 수 있다.
         *
         * 예를 들자면 아래와 같다.
         * N = 6 -> 3^3 + 3^1 * 2^1
         * N = 8 -> 3^4 + 3^2 * 2^1 + 2^2
         * 3는 a,b,c 타입을 상징하고, 2는 e,f 타입을 상징한다고 생각하고 바라보면 딱 들어맞다.
         * 이런 류의 문제는 수열이라고 인식하고, 수열의 입장에서 관찰하면서 점화식을 빠르게 찾는 것이 핵심이다.
         * 이번에 이런 부류의 문제에 대해서 깨달았으니 앞으로 비슷한 문제가 나올 때는 잘 풀어보자.
         * --------------------------
         * 아.. 수열마다 예외가 새롭게 계속 추가된다는 것을 알아채지 못했다..
         * 점진적으로 잘 알아갔지만, 결국 예외 상황을 찾지 못했던 게 패착의 요인 같다..
         * 예외가 있는지 항상 잘 생각해보자.
         * 제대로 된 점화식은
         * f(n) = f(n-2) * dp[2](3이다) + {k is from 1 to (n-4)/2 || f(2k) * 2}
         *
         * Example
         * 1.
         * 2
         * -------
         * answer
         * 3
         *
         */
        int number = inputData();
        System.out.println(solveProblem(number));

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch(Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static int solveProblem(int number) {
        int[] dp = new int[number + 1];
        // 기본 타입 세팅을 위한 사전 작업.
        int temp = 0;
        for(int i = 1; i <= number; i++) {
            // 기본 타입 세팅.
            if(i == 2) {
                dp[i] = 3;
            }

            // 2의 배수일 때만 타일의 개수를 세어야 한다.
            if(i > 2 && (i % 2) == 0) {
                dp[i] = dp[i-2] * dp[2] + 2;
//                System.out.println("f[" + i + "] * 3 : " + dp[i]);
                temp += dp[i-4] * 2; // i가 4일 때는 0, i가 6일 때는 f(2) * 2...
//                System.out.println("sigma sum : " + temp);
                dp[i] += temp;
            }
        }

        return dp[number];
    }

}