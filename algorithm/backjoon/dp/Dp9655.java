package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp9655 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 N이 주어진다. (1 ≤ N ≤ 1000)
         *
         * 2. output 조건
         * 상근이가 게임을 이기면 SK를, 창영이가 게임을 이기면 CY을 출력한다.
         * 탁자 위에 돌 N개가 있다. 상근이와 창영이는 턴을 번갈아가면서 돌을 가져가며, 돌은 1개 또는 3개 가져갈 수 있다.
         * 마지막 돌을 가져가는 사람이 게임을 이기게 된다. 게임은 상근이가 먼저 시작한다.
         *
         * 3. 해석
         * N에 따라서 이기는 사람이 정해져 있다라는 건가?
         * dp[n] = dp[n-4] 같은 승리자이다.
         * n-1과 n-3은 반대되는 사람이고. n-2는 n까지 2밖에 차이가 안난다면 같은 승리자이다.
         *
         * Example
         * 1.
         * 5
         * -------
         * answer
         * SK
         *
         */
        int inputData = inputData();
        System.out.println(solveProblem(inputData));

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch(Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static String solveProblem(int inputData) {
        String result;
        int index = 1;
        // SK가 답임. 즉 같은 답이 나올 수 밖에 주기를 계산해서 점핑 중이다.
        while((index + 4) <= inputData) {
            index += 4;
        }

        switch (inputData - index) {
            case 1:
                result = "CY";
                break;
            case 2:
                result = "SK";
                break;
            case 3:
                result = "CY";
                break;
            default:
                result = "SK";
                break;
        }

        return result;
    }

}
