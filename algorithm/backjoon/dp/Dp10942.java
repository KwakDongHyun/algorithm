package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dp10942 {

    private static int seqLen;
    private static int[] seq;
    private static int[][] question;
    private static boolean[][] dp;

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 수열의 크기 N (1 ≤ N ≤ 2,000)이 주어진다.
         * 둘째 줄에는 홍준이가 칠판에 적은 수 N개가 순서대로 주어진다. 칠판에 적은 수는 100,000보다 작거나 같은 자연수이다.
         * 셋째 줄에는 홍준이가 한 질문의 개수 M (1 ≤ M ≤ 1,000,000)이 주어진다.
         * 넷째 줄부터 M개의 줄에는 홍준이가 명우에게 한 질문 S와 E가 한 줄에 하나씩 주어진다.
         *
         * 2. output 조건
         * 총 M개의 줄에 걸쳐 홍준이의 질문에 대한 명우의 답을 입력으로 주어진 순서에 따라서 출력한다.
         * 팰린드롬인 경우에는 1, 아닌 경우에는 0을 출력한다.
         * 팰린드롬(palindrome)은 거꾸로 읽어도 제대로 읽는 것과 같은 문장이나 낱말, 숫자, 문자열(sequence of characters) 등이다.
         * 국어로는 회문(回文)이라고 한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 처음에 짝수인 경우에는 팰린드롬이 없을 것이라고 생각한게 크다.
         * 홀수인 경우에만 몰두해서 알고리즘을 짰기 때문에 원래 가야할 방향에서 더 멀어졌다고 본다.
         * 팰린드롬의 성질을 분석만 제대로 했으면 이런 삽질은 안했을텐데..
         * --------------
         * 한편, 푸는데 이렇게 오래 걸린 이유는 더 나은 방법을 찾고 고민하다가 오래걸렸다.
         * 아직은 더 나은 방법이 나올 수가 있는지 직감으로 알 수 있을만큼 실력이 쌓이지 않았다.. ㅠㅠ
         * 너무 한 번에 좋은 알고리즘을 만들려고 하기 보다는 단순한 것부터 만들어보고 거기서 개선해나가는 방식으로
         * 공부를 해나가는게 좋을 것 같다.
         * 직감과 고차원 알고리즘을 작성하는 사고력은 하루 아침에 만들어지지 않는다.
         *
         *
         * Example
         * 1.
         * 7
         * 1 2 1 3 1 2 1
         * 4
         * 1 3
         * 2 5
         * 3 3
         * 5 7
         * -------
         * answer
         * 1
         * 0
         * 1
         * 1
         *
         */

        inputData();
        solveProblem();
//        int[] results = solveProblem();
//        for(int result : results) {
//            System.out.println(result);
//        }

    }

    public static void inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            seqLen = Integer.parseInt(br.readLine());
            seq = new int[seqLen + 1];
            dp = new boolean[seqLen + 1][seqLen + 1]; // 초기에 보류해뒀던 방법대로 진행. 인덱스별로 true false 저장형식.

            int i ;
            StringTokenizer st = new StringTokenizer(br.readLine());
            for(i = 1; i <= seqLen; i++) {
                seq[i] = Integer.parseInt(st.nextToken());
            }

            int questLen = Integer.parseInt(br.readLine());
            question = new int[questLen][2];
            for(i = 0; i < questLen; i++) {
                st = new StringTokenizer(br.readLine());
                question[i][0] = Integer.parseInt(st.nextToken());
                question[i][1] = Integer.parseInt(st.nextToken());
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        /*
         * 홀수 짝수 수열은 각각 처리방법이 약간 다르다.
         * 홀수는 가운데 값을 중심으로 대칭관계이고 짝수는 중심값이 없이 대칭관계이어야 한다.
         * 가장 작은 값인 1과 2를 먼저 만들고 dp로 하나씩 수열의 범위를 늘려가는게 좋다.
         * 팰린드롬이 있을 때, 팰린드롬은 자신 내부에 있는 더 작은 팰린드롬을 가지는 형태이기 때문이다.
         * 배추나 양배추 같은 겹겹이 식물처럼.
         */
        for(int i = 1; i <= seqLen; i++) {
            dp[i][i] = true;
        }

        for(int i = 1; i + 1 <= seqLen; i++) {
            if(seq[i] == seq[i + 1]) {
                dp[i][i + 1] = true;
            }
        }

        for(int j = 2; j < seqLen; j++) {
             for(int i = 1; i + j <= seqLen; i++) {
                 if(dp[i + 1][i + j - 1] && seq[i] == seq[i + j]) {
                     dp[i][i + j] = true;
                 }
             }
        }

//        int[] result = new int[question.length];
//        for(int i = 0; i < question.length; i++) {
//            result[i] = dp[question[i][0]][question[i][1]] ? 1 : 0;
//        }

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < question.length; i++) {
            sb.append(dp[question[i][0]][question[i][1]] ? 1 : 0);
            sb.append("\n");
        }
        System.out.println(sb);
    }

}
