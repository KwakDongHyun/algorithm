package backjoon.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Dp14501 {

    public static void main(String[] args) throws IOException {

        /*
         * 1. input 조건
         * 첫째 줄에 N (1 ≤ N ≤ 15)이 주어진다.
         * 둘째 줄부터 N개의 줄에 Ti와 Pi가 공백으로 구분되어서 주어지며, 1일부터 N일까지 순서대로 주어진다.
         * (1 ≤ Ti ≤ 5, 1 ≤ Pi ≤ 1,000)
         *
         * 2. output 조건
         * N+1일차에 얻을 수 있는 최대 이익을 출력한다.
         * 그렇기 때문에 N+1일에는 모든 스케줄이 마무리가 되어야 한다.
         *
         * 3. 주의사항
         * N+1일차에 마무리가 되는 스케줄이면 선택가능하다.
         *
         */
        int[][] input2DimensionData = input2DimensionData();
        int maxProfit = getMaxProfit(input2DimensionData);
        System.out.println(maxProfit);

    }

    public static int[][] input2DimensionData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int day = Integer.parseInt(br.readLine());
            int[][] input2DimensionData = new int[day + 1][2];
            StringTokenizer stringTokenizer;

            for(int i = 1; i <= day; i++) {
                stringTokenizer = new StringTokenizer(br.readLine());
                input2DimensionData[i][0] = Integer.parseInt(stringTokenizer.nextToken()); // Ti(상담에 걸리는 일 수)
                input2DimensionData[i][1] = Integer.parseInt(stringTokenizer.nextToken()); // Pi(얻는 이익)
            }
            return input2DimensionData;
        } catch (Exception e) {
            e.printStackTrace();
            int[][] input2DimensionData = new int[1][1];
            input2DimensionData[0][0] = 1;
            input2DimensionData[0][1] = 1;
            return input2DimensionData;
        }
    }

    public static int getMaxProfit(int[][] input2DimensionData) {
        int maxDay = input2DimensionData.length - 1;
        int destinationDay = maxDay + 1;
        int[] dp = new int[destinationDay + 1];
        int day, profit;
        int currentMaxProfit = 0;

        for(int i = 1; i <= maxDay; i++) {
            day = input2DimensionData[i][0];
            profit = input2DimensionData[i][1];

            // dp 특성상 완전 탐색(n^2)으로 조회할 수 밖에 없음. 이전까지 이익 중에 큰 것을 골라야 하기 때문에.
            // 이거의 개선된 버전은 이분탐색(Binary Search)가 있을 것 같다.
            for(int j = 1; j <= i; j++) {
                currentMaxProfit = Math.max(currentMaxProfit, dp[j]);
            }
            if(i + day <= destinationDay) {
                dp[i + day] = Math.max(currentMaxProfit + profit, dp[i + day]);
                System.out.println("dp[" + (i + day) + "] : " + dp[i + day]);
            }
        }

        return Arrays.stream(dp)
                .max()
                .getAsInt();
    }

}
