package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Dp11048 {

    public static void main(String[] args) {
        /*
         * 1. input 조건
         * 첫째 줄에 미로의 크기 N, M이 주어진다. (1 ≤ N, M ≤ 1,000)
         * 둘째 줄부터 N개 줄에는 총 M개의 숫자가 주어지며, r번째 줄의 c번째 수는 (r, c)에 놓여져 있는 사탕의 개수이다.
         * 사탕의 개수는 0보다 크거나 같고, 100보다 작거나 같다. (0 ≤ value ≤ 100)
         *
         * 2. output 조건
         * 첫째 줄에 준규가 (N, M)으로 이동할 때, 가져올 수 있는 사탕 개수의 최댓값을 출력한다.
         * (1, 1)에 있고, (N, M)으로 이동하려고 한다. 준규가 (r, c)에 있으면, (r+1, c), (r, c+1), (r+1, c+1)로 이동할 수 있다.
         *
         * 3. 주의사항
         * 주의사항은 딱히 없다.
         *
         * 4. 해석
         * M = 0일 때는 dp[n][m] = dp[n-1][m] + data[n][m]
         * N = 0일 때는 dp[n][m] = dp[n][m-1] + data[n][m]
         * M,N > 0일 때는 dp[n][m] = max(dp[n-1][m], dp[n][m-1], dp[n-1][m-1]) + data[n][m]
         *
         * Example
         * 1.
         * 3 4
         * 1 2 3 4
         * 0 0 0 5
         * 9 8 7 6
         * -------
         * answer
         * 31
         *
         * 2.
         * 4 3
         * 1 2 3
         * 6 5 4
         * 7 8 9
         * 12 11 10
         * --------
         * answer
         * 47
         *
         */
        int[][] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int n = Integer.parseInt(st.nextToken());
            int m = Integer.parseInt(st.nextToken());
            int[][] inputData = new int[n][m];

            for(int i = 0; i < n; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < m; j++) {
                    inputData[i][j] = Integer.parseInt(st.nextToken());
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[][] inputData) {
        int n = inputData.length;
        int m = inputData[0].length;
        int result = 0;

        int[][] dp = new int[n][m];
        dp[0][0] = inputData[0][0];
        for(int i = 1; i < n; i++) {
            dp[i][0] = dp[i-1][0] + inputData[i][0];
        }

        for(int j = 1; j < m; j++) {
            dp[0][j] = dp[0][j-1] + inputData[0][j];
        }

        for(int i = 1; i < n; i++) {
            for(int j = 1; j < m; j++) {
                dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                dp[i][j] = Math.max(dp[i][j], dp[i-1][j-1]);
                dp[i][j] += inputData[i][j];
            }
        }

        return Arrays.stream(dp)
                .flatMapToInt(Arrays::stream)
                .max()
                .getAsInt();
    }

}
