package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Dp12852 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 정수 X에 사용할 수 있는 연산은 다음과 같이 세 가지 이다.
         * X가 3으로 나누어 떨어지면, 3으로 나눈다.
         * X가 2로 나누어 떨어지면, 2로 나눈다.
         * 1을 뺀다.
         *
         * 첫째 줄에 자연수 N이 주어진다. (1 ≤ n ≤ 10^6)
         *
         * 2. output 조건
         * 첫째 줄에 연산을 하는 횟수의 최솟값을 출력한다.
         * 둘째 줄에는 N을 1로 만드는 방법에 포함되어 있는 수를 공백으로 구분해서 순서대로 출력한다.
         * 정답이 여러 가지인 경우에는 아무거나 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 2
         * -------
         * answer
         * 1
         * 2 1
         *
         * 2.
         * 10
         * --------
         * answer
         * 3
         * 10 9 3 1
         *
         */

        int inputData = inputData();
        solveProblem(inputData);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch(Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    private static void solveProblem(int inputData) {
        int[] dp = new int[inputData + 1];

        for(int i = 2; i <= inputData; i++) {
            dp[i] = dp[i-1] + 1;

            if(i % 2 == 0) {
                dp[i] = Math.min(dp[i], dp[i/2] + 1);
            }

            if(i % 3 == 0) {
                dp[i] = Math.min(dp[i], dp[i/3] + 1);
            }

//            System.out.print("dp[" + i + "] : " + dp[i] + " ");
        }
//        System.out.println();

        int temp = inputData;
        int calculationCnt = dp[inputData];
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(temp);

        while(temp > 1) {
//            System.out.println("temp : " + temp + ", calculationCnt : " + calculationCnt);

            if(temp % 3 == 0) {
                if(calculationCnt - 1 == dp[temp/3]) {
                    temp = temp / 3;
                    queue.add(temp);
                    calculationCnt--;
                    continue;
                }
            }

            if(temp % 2 == 0) {
                if(calculationCnt - 1 == dp[temp/2]) {
                    temp = temp / 2;
                    queue.add(temp);
                    calculationCnt--;
                    continue;
                }
            }

            temp--;
            queue.add(temp);
            calculationCnt--;

        }

        StringBuilder sb = new StringBuilder();
        sb.append(queue.size() - 1);
        sb.append("\n");
        while(!queue.isEmpty()) {
            sb.append(queue.poll());
            sb.append(" ");
        }
        System.out.println(sb);

    }

}
