package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp11057 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 N (1 ≤ N ≤ 1,000)이 주어진다.
         *
         * 2. output 조건
         * 첫째 줄에 길이가 N인 오르막 수의 개수를 10,007로 나눈 나머지를 출력한다.
         * 오르막 수는 수의 자리가 오름차순을 이루는 수를 말한다. 이때, 인접한 수가 같아도 오름차순으로 친다.
         * 수는 0으로 시작할 수 있다.
         *
         * 3. 해석
         * 공식으로 바로 해결하려고 하면 잘 안풀린다.
         * dp를 만들어갈 때 공식이 있기 때문에, 직접 손으로 작성해가면서 어떻게 완성이 되어가는지 확인해보는게 가장 빠르고 정확하다.
         *
         * Example
         * 1
         * answer - 10
         * 2
         * answer - 55
         * 3
         * answer - 220
         *
         */
        int inputData = inputData();
        long numberOfAscendantNumber = getAscendantNumber(inputData);
        System.out.println(numberOfAscendantNumber);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch (Exception e) {
            return 1;
        }
    }

    public static long getAscendantNumber(int inputData) {
        long[][] dp = new long[inputData + 1][11];
        /*
         * 기본 구성은 이렇게 되는게 맞지만, 뺄셈과 나머지 연산은 동시에 사용하면 문제가 생긴다.
         * 다른 방식으로 이 구성을 만들어보자.
         * 10 9 8 7 6 ... 이렇게 아니라 거꾸로 1, 2, 3 ... 10 이렇게 누적식으로 넣으면 나머지 연산 적용도 쉬울거다.
         * 덧셈을 통해서 할 수 있을 것 같다.
         */
        /*for(int i = 1; i <= inputData; i++) {
            for(int j = 1; j <= 10; j++) {
                if(i == 1) {
                    dp[i][j] = 1;
                } else {
                    if(j == 1) {
                        dp[i][j] = dp[i-1][0] % 10_007;
                    } else {
                        dp[i][j] = dp[i][j-1] - dp[i-1][j-1];
                    }
                }
                // dp[i]에 저장. (길이가 i인 오르막 수의 개수의 합.)
                dp[i][0] += dp[i][j];
                System.out.print("dp[" + i + "][" + j + "] : " + dp[i][j] + ", ");
            }
            dp[i][0] = dp[i][0] % 10_007;
            System.out.println();
            System.out.println("dp[" + i + "][0] : " + dp[i][0]);
        }*/
        int sumOfAscendantNumber = 0;
        for(int i = 1; i <= inputData; i++) {
            for(int j = 1; j <= 10; j++) {
                if(i == 1) {
                    dp[i][j] = 1;
                } else {
                    // dp[i]에 저장. (길이가 i인 오르막 수의 개수의 합.)
                    dp[i][0] += dp[i-1][j];
                    dp[i][j] = dp[i][0] % 10_007;
                }
                sumOfAscendantNumber += dp[i][j];
                System.out.print("dp[" + i + "][" + j + "] : " + dp[i][j] + ", ");
            }
            dp[i][0] = sumOfAscendantNumber % 10_007;
            sumOfAscendantNumber = 0;
            System.out.println();
            System.out.println("dp[" + i + "][0] : " + dp[i][0]);
        }

        return dp[inputData][0];
    }

}
