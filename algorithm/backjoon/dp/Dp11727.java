package backjoon.dp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dp11727 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 2xn에서 n의 크기 (가로길이)
         * (1 ≤ n ≤ 1,000)
         *
         * 2. output 조건
         * 2×n 크기의 직사각형을 채우는 방법의 수를 10,007로 나눈 나머지를 출력한다.
         *
         * 3. 주의사항
         * 피보나치 수열이기 때문에 무조건 overflow가 발생할 수 밖에 없다.
         * 그렇기 때문에 수식을 생각해서 다르게 접근해야한다. 괜히 나머지 연산을 넣은게 아니다.
         *
         */
        int inputData = inputData();
        int sequence = getSequence(inputData);
        System.out.println(sequence);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static int getSequence(int inputData) {
        int[] dp = new int[inputData + 1];
        for(int i = 1; i <= inputData; i++) {
            if(i == 1) {
                dp[i] = 1;
            } else if(i == 2) {
                dp[i] = 3;
            } else {
                dp[i] = ((dp[i-1] % 10007) + (2 * dp[i-2] % 10007)) % 10007;
            }
        }
        return dp[inputData];
    }

}
