package backjoon.kmp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Kmp1305 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 광고판의 크기 L이 주어지고, 둘째 줄에 현재 광고판에 보이는 문자열이 주어진다. (1 ≤ L ≤ 1,000,000)
         *
         * 2. output 조건
         * 첫째 줄에 가능한 광고의 길이중 가장 짧은 것의 길이를 출력한다.
         * 예를 들어, 광고 업자 백은진이 광고하고 싶은 내용이 aaba 이고, 전광판의 크기가 6이라면 맨 처음에 보이는 내용은 aabaaa 이다.
         * 시간이 1초가 지날 때마다, 문자는 한 칸씩 옆으로 이동한다. 따라서 처음에 aabaaa가 보였으면 그 다음에는 abaaab가 보인다.
         * 그 다음에는 baaaba가 보인다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * KMP 알고리즘을 사용하면 풀 수 있다.
         * 전광판에 보이는 문자열에서 부분 접두사 일치하는걸 찾고, 찾은 위치까지의 길이를 구하는 문제라고 생각한다.
         * 따로 검색어가 존재하는 것도 아니라서 엄밀히 말하자면 PI배열을 구하는 과정과 같다고 생각한다.
         *
         * Example
         * 1.
         * 5
         * aaaaa
         * -------
         * answer
         * 1
         *
         * 2.
         * 6
         * aabaaa
         * --------
         * answer
         * 4
         *
         */

        String inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int length = Integer.parseInt(br.readLine());
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String inputData) {
        int length = inputData.length();
        int[] pi = new int[length];
        char[] charsOfPattern = inputData.toCharArray();
        int patternIdx = 0;

        for(int i = 1; i < length; i++) {
            while(patternIdx > 0 && charsOfPattern[patternIdx] != charsOfPattern[i]) {
                patternIdx = pi[patternIdx - 1];
            }

            if(charsOfPattern[patternIdx] == charsOfPattern[i]) {
                pi[i] = ++patternIdx;
            }
        }

        return length - pi[length - 1];
    }

}
