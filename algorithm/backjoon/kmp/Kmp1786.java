package backjoon.kmp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

public class Kmp1786 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 문자열 T가, 둘째 줄에 문자열 P가 주어진다.
         * P는 검색어인 문자열 Pattern 이고, T는 검색을 실행할 데이터 풀인 문자열 Text이다.
         * T와 P의 길이 n, m은 (1 ≤ n, m ≤ 1,000,000)이고, 알파벳 대소문자와 공백으로만 이루어져 있다.
         * 일반적으로, n ≥ m이라고 가정해도 무리가 없다. n < m이면 어차피 P는 T중간에 나타날 수 없기 때문이다.
         *
         * 2. output 조건
         * 첫째 줄에 T 중간에 P가 몇 번 나타나는지를 나타내는 음이 아닌 정수를 출력한다.
         * 둘째 줄에는 P가 나타나는 곳의 첫 번째 인덱스 위치를 차례대로 공백으로 구분해 출력한다.
         * 예컨대, T의 i～i+m-1번 문자와 P의 1～m번 문자가 차례로 일치한다면, i를 출력하는 식이다.
         *
         * 3. 주의사항
         * 문제 지문에서 풀이 방법에 대한 힌트를 직접적으로 제시하고 있으므로 참고해라.
         * 문자열 검색(사실상 문자열 비교. 비교 없이는 검색도 못한다)의 성능을 극한으로 끌어올리는 방법이 적혀있다.
         * O(nm)에서 O(n)으로 시간 복잡도를 개선할 수 있다.
         * 해석의 구문만으로는 상당히 이해하기 어렵기 때문에 https://bowbowbow.tistory.com/6를 참고하라.
         *
         * 4. 해석
         * KMP 알고리즘을 사용해서 풀 수 있는 문제이다.
         * 이 알고리즘을 구현하기 위해서 prefix와 suffix, pi배열이라는 개념 2가지를 알아야 한다.
         * prefix와 suffix는 알고 있으니 넘어가고 pi배열에 대해서 기록한다.
         *
         *  1. pi배열이란
         *  어떤 주어진 문자열의 길이를 n이라고 하고 i를 (0 ≤ i ≤ n-1)이라 가정하자.
         *  pi[i]는 주어진 문자열 n에서 i까지의 부분 문자열 중에서 prefix == suffix가 될 수 있는 가장 긴 부분 문자열의 길이를 의미한다.
         *  여기서 prefix와 suffix는 i까지의 부분 문자열 내에서 찾는다.
         *  또한, 전체 문자열은 제외한다. 즉, prefix가 0 ~ i까지인 부분 문자열 전체인 경우는 제외한다.
         *  (집합론의 개념에 의거하여 전체 문자열 또한 부분 집합 중 1개로 여겨지나, 전체 문자열은 항상 prefix와 suffix가 같으므로 제외한다.)
         *  예를 들어 ABAABAB라는 n이 7인 문자열에서 i가 5인 pi[i]는 ABAABA라는 부분 문자열에서 ABA가 최장 길이이므로 pi[i] = 3 이다.
         *
         * 문자열 검색 도중 불일치한 지점에 따라서 전체 자료의 어느 지점에 pattern 문자열을 대입하고 나서, pattern 문자열의 어느 지점부터
         * 대입해서 재검색을 시도할지가 달라지기 때문이다. 즉, pi[i]의 인덱스 i가 바뀐다는 말과 동일하다.
         * 위 예를 들어 설명하면 ABAABA까지만 맞고 마지막 i = 6에서 불일치한 경우인지, ABAA까지만 맞고 i = 4에서 불일치한 경우인지 등..
         * 각 경우에 따라서 pi[i]가 달라지고, 재검색시 대입할 pattern의 위치가 달라진다.
         * pi배열이 kmp 알고리즘의 핵심이다.
         *
         * 한편, pi배열을 선형적인 수행으로 만들다가 P[pi[i-1]} != P[i] (다음 패턴 문자와 pi배열 뒤에 새롭게 올 문자의 비교)인 경우
         * 어떻게 해결해야할지 갈피를 못잡았다.
         * pi[i]를 구할 때 i-1번째의 dp를 사용하지 못할 경우, 처음부터 prefix와 suffix가 같은 부분 문자열을 구해야하기 때문이다.
         * 참고 자료를 검색한 결과, pi배열도 kmp 알고리즘을 구하는 방법과 동일한 원리를 적용해서 구하면 된다.
         * 무슨 말이냐 하면, kmp 알고리즘에서 pi배열을 사용하여 패턴 문자열에서 반복되는 구간에 초점을 맞춰 대입하여 재검색을 하듯이,
         * prefix와 suffix가 같은 구간도 새롭게 맞춘다는 말이다.
         *
         * 문제는 이걸 i번째부터 1까지 역순대로 문자끼리 비교하는 O(m^3)인 방법을 쓰지 않는 방법을 찾는게 힘들었다.
         * 이에 대한 해결은 pi배열의 인덱스를 하나씩 줄이면서 선형적인 방법으로 prefix와 일치하는 suffix를 찾는 것이다.
         * 그림으로 이해하면 이해하기 쉽다.
         * 말로 이해하기 쉽게 정리하면, pattern 문자열 두 개를 1행 2행에 두고 일치하는 것을 찾는다고 가정할 때
         * i번째에서 문자가 서로 달라서 이전 pi배열 값에 +1을 못할 경우 2행의 띄를 오른쪽으로 한칸 당기는 느낌이라는 것이다.
         * 즉, 0번째 문자와 일치하는 n-k+1부터 n까지 밀면서 일치하는 것을 찾는 것이다. 역순이 아닌 정방향으로 찾는다는 말이다.
         * 이렇게 하면 pi를 구할 수 있다.
         *
         *
         * Example
         * 1.
         * ABC ABCDAB ABCDABCDABDE
         * ABCDABD
         * -------
         * answer
         * 1
         * 16
         *
         */

        String[] inputData = inputData();
        List<Integer> results = solveProblem(inputData);

        StringBuilder sb = new StringBuilder();
        sb.append(results.size());
        sb.append("\n");
        for(int idx : results) {
            sb.append(idx + 1);
            sb.append(" ");
        }

        System.out.println(sb.toString());

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] inputData = new String[2];
            inputData[0] = br.readLine();
            inputData[1] = br.readLine();
            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<Integer> solveProblem(String[] inputData) {

        /*
         * 2가지가 알고리즘을 구현의 핵심이다.
         *  1. pattern 문자열 전처리를 통해서 pi배열 정보 얻기
         *  2. 1번을 토대로 문자열 검색 진행
         */
        String text = inputData[0];
        String pattern = inputData[1];
        List<Integer> results = new LinkedList<Integer>();

        int tLen = text.length();
        int pLen = pattern.length();

        int[] pi = new int[pLen];
        char[] patternChars = pattern.toCharArray();
        int patternIdx = 0;

        /*
         * pi배열 구하기
         * 위 해석내용 참고.
         * 어떤 관점으로는 DP의 메모이제이션과 같은 풀이법이기도 하다.
         * 전체 문자열은 제외이므로 길이가 1인 경우 0임.
         */
        for(int i = 1; i < pLen; i++) {
            while(patternIdx > 0 && patternChars[i] != patternChars[patternIdx]) {
                /*
                 * 다음에 비교할 부분 문자열의 인덱스
                 * 단순한 -1이 아닌 pi의 값이 포인트다. 패턴은 변화가능하기 때문.
                 */
                patternIdx = pi[patternIdx - 1];
            }

            if(patternChars[i] == patternChars[patternIdx]) {
                pi[i] = ++patternIdx;
            }
        }

//        // 검증
//        for(int i = 0; i < pi.length; i++) {
//            System.out.println("pi[" + i + "] : " + pi[i]);
//        }

        /*
         * pi배열을 사용하여 문자열 검색하기
         */
        char[] textChars = text.toCharArray();
        patternIdx = 0;

        for(int i = 0; i < tLen; i++) {
            while(patternIdx > 0 && textChars[i] != patternChars[patternIdx]) {
                patternIdx = pi[patternIdx - 1]; // 나머지 부분 문자열에서 pi배열을 찾아나간다.
            }

            if(textChars[i] == patternChars[patternIdx]) {
                /*
                 * 검색어와 일치하는 문자열을 찾았을 경우, pi배열을 사용하여 다시 부분적으로 일치하는 문자열 부분부터 검색을 이어나간다.
                 * 반복 패턴부분부터 검색어가 다시 존재할 수도 있기 때문에
                 */
                if(patternIdx == pLen - 1) {
                    results.add(i - pLen + 1);
                    patternIdx = pi[patternIdx];
                } else {
                    patternIdx++;
                }
            }
        }

        return results;
    }

}
