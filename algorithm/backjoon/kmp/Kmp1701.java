package backjoon.kmp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Kmp1701 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 문자열이 주어진다. 문자열의 길이는 최대 5,000이고, 문자열은 모두 소문자로만 이루어져 있다.
         *
         * 2. output 조건
         * 입력에서 주어진 문자열의 두 번 이상 나오는 부분 문자열 중에서 가장 긴 길이를 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 특정 부분 문자열이 여러 곳에 존재한다는 말은 결국 문자열을 자기자신과 비교한다고 생각하면 일정 부분에서 겹치는 곳이 존재한다는
         * 것과 같은 말이라고 해석할 수 있다.
         * 그렇기 때문에 KMP 알고리즘을 이용하면 쉽게 풀 수 있을 것이라고 생각한다.
         *
         * 문제는 어떤 형태로 어느 지점에서 반복해서 나타나는지는 모른다는 것이다. 이것을 알아야 하는 것이 이 문제의 키포인트다.
         * 이 문제를 해결하기 위해서는 KMP 알고리즘에서 pattern(검색어)에 대한 pi 배열을 만드는 것을 응용하는 것이 좋을 것 같다.
         * pi 배열은 index 0부터 i까지의 부분 문자열의 border의 최고 길이 정보를 저장한다.
         * 이를 활용하면 index 0부터 시작하는 부분 문자열에 대해서는 pi 배열의 value를 통해서 2번 이상 존재하는 것을 보장받을 수 있고,
         * 길이 뿐만 아니라 문자열에 대해서도 구할 수 있다.
         *
         * 여기서 나아가 다른 index 지점부터 시작하는 부분 문자열에 관한 pi 배열의 값을 구할 수 있다면 모든 부분 문자열에 대해서
         * 2번 이상 존재하는 부분 문자열에 대한 길이와 문자열 자체에 대해서도 정보를 얻을 수 있게 된다.
         * 이를 숙지하고 알고리즘을 구현하면 해결할 수 있다.
         *
         * Example
         * 1.
         * abcdabcabb
         * -------
         * answer
         * 3
         *
         * 2.
         * abcabcabc
         * --------
         * answer
         * 6
         *
         */

        String inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String inputData) {
        int strLen = inputData.length();

        int value = 0;
        int borderLen = 0;

        for(int i = 0; i < strLen - 1; i++) {
            int[] pi = getPiArr(inputData.substring(i));
            borderLen = Arrays.stream(pi).max().getAsInt();

            if(borderLen > value) {
                value = borderLen;
            }
        }

        return value;
    }

    private static int[] getPiArr(String str) {
        int strLen = str.length();
        char[] charOfStr = str.toCharArray();

        int[] pi = new int[strLen];
        int patternIdx = 0;
        for(int i = 1; i < strLen; i++) {
            while(patternIdx > 0 && charOfStr[patternIdx] != charOfStr[i]) {
                patternIdx = pi[patternIdx - 1];
            }

            if(charOfStr[patternIdx] == charOfStr[i]) {
                pi[i] = ++patternIdx;
            }
        }

        return pi;
    }

}
