package backjoon.tree;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Tree14725 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫 번째 줄은 로봇 개미가 각 층을 따라 내려오면서 알게 된 먹이의 정보 개수 N (1 ≤ N ≤ 1000)개가 주어진다.
         * 두 번째 줄부터 N+1 번째 줄까지, 각 줄의 시작은 로봇 개미 한마리가 보내준 먹이 정보 개수 K (1 ≤ K ≤ 15)가 주어진다.
         * 다음 K개의 입력은 로봇 개미가 왼쪽부터 순서대로 각 층마다 지나온 방에 있는 먹이 정보이며 먹이 이름 길이 t는 1 ≤ t ≤ 15를 만족한다. 먹이 정보는 알파벳 대문자로만 이루어져 있다.
         *
         * 2. output 조건
         * 개미굴의 시각화된 구조를 출력하여라.
         * 개미굴의 각 층을 "--" 로 구분하며, 같은 층에 여러개의 방이 있을 때에는 사전 순서가 앞서는 먹이 정보가 먼저 나온다.
         * 최상위 굴을 포함하여 하나의 굴에서 개미굴이 여러개로 나뉠 때 먹이 종류별로 최대 한 번만 나올 수 있다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 3
         * 2 B A
         * 4 A B C D
         * 2 A C
         * -------
         * answer
         * A
         * --B
         * ----C
         * ------D
         * --C
         * B
         * --A
         *
         * 2.
         * 4
         * 2 KIWI BANANA
         * 2 KIWI APPLE
         * 2 APPLE APPLE
         * 3 APPLE BANANA KIWI
         * --------
         * answer
         * APPLE
         * --APPLE
         * --BANANA
         * ----KIWI
         * KIWI
         * --APPLE
         * --BANANA
         *
         */

        List<List<String>> inputData = inputData();
        solveProblem(inputData);

    }

    public static List<List<String>> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int informationCnt = Integer.parseInt(br.readLine());
            List<List<String>> inputData = new ArrayList<List<String>>(informationCnt);

            for(int i = 0; i < informationCnt; i++) {
                List<String> data = new LinkedList<String>();
                String[] rooms = br.readLine().split(" ");

                for(int j = 1; j < rooms.length; j++) {
                    data.add(rooms[j]);
                }
                inputData.add(data);
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void solveProblem(List<List<String>> inputData) {


    }

}
