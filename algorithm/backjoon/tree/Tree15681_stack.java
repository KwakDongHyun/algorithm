package backjoon.tree;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;

public class Tree15681_stack {

    private static int vertices;
    private static int root;
    private static int queryCount;
    private static int[] queries;
    private static List<List<Integer>> graphInfo;

    public static void main(String[] args) {

        preprocessData();
        List<Integer> results = solveProblem();
        for(int result : results) {
            System.out.println(result);
        }

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st;
            st = new StringTokenizer(br.readLine());
            vertices = Integer.parseInt(st.nextToken());
            root = Integer.parseInt(st.nextToken());
            queryCount = Integer.parseInt(st.nextToken());

            graphInfo = new ArrayList<List<Integer>>();
            for(int i = 0; i <= vertices; i++) {
                graphInfo.add(new LinkedList());
            }

            queries = new int[queryCount];
            for(int i = 0; i < vertices - 1; i++) {
                st = new StringTokenizer(br.readLine());
                int source = Integer.parseInt(st.nextToken());
                int destination = Integer.parseInt(st.nextToken());

                graphInfo.get(source).add(destination);
                graphInfo.get(destination).add(source);
            }

            for(int i = 0; i < queryCount; i++) {
                queries[i] = Integer.parseInt(br.readLine());
            }

            Map<String, Object> inputData = new HashMap<String, Object>();
            inputData.put("graphInfo", graphInfo);
            inputData.put("queries", queries);
            inputData.put("root", root);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static List<Integer> solveProblem() {
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(root);
        boolean[] visit = new boolean[graphInfo.size()];
        int[] parentInfo = new int[graphInfo.size()];

        /*
         * initialize
         */
        List[] pathInfo = new List[graphInfo.size()];
        int[] subtreeInfo = new int[graphInfo.size()];

        for(int i = 1; i < graphInfo.size(); i++) {
            subtreeInfo[i] = 1;
            pathInfo[i] = new LinkedList<Integer>();
        }

        while(!stack.empty()) {
            int vertex = stack.pop();
            visit[vertex] = true;
            List<Integer> list = graphInfo.get(vertex);

            for(int connectedVertex : list) {
                if(!visit[connectedVertex]) {
                    List<Integer> path = pathInfo[vertex];
                    for(int point : path) {
                        pathInfo[connectedVertex].add(point);
                    }

                    pathInfo[connectedVertex].add(vertex);
                    stack.push(connectedVertex);
                }
            }

            /*
             * 현재 노드는 방문 후에는 다시는 정보에 접근할 이유가 없어지므로, 자식 노드에게
             * 경로 정보를 전달한 후에는 메모리를 비워주는게 좋다.
             */
            List<Integer> path = pathInfo[vertex];
            for(int point : path) {
                subtreeInfo[point]++;
            }
            pathInfo[vertex] = null;
        }

        /*for(int i = 1; i < graphInfo.size(); i++) {
            List<Integer> path = pathInfo[i];
            for(int point : path) {
                subtreeInfo[point]++;
            }
        }*/

        /*for(int i = 1; i < graphInfo.size(); i++) {
            System.out.print(subtreeInfo[i] + " ");
        }
        System.out.println();*/

        List<Integer> results = new LinkedList<Integer>();
        for(int query : queries) {
            results.add(subtreeInfo[query]);
        }
        return results;

    }

}
