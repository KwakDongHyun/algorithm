package backjoon.tree;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;

public class Tree15681 {

    private static int vertices;
    private static int root;
    private static int queryCount;
    private static int[] queries;
    private static List<List<Integer>> graphInfo;
    private static boolean[] visit;
    private static int[] dp;

    public static void main(String[] args) {

        preprocessData();
        List<Integer> results = solveProblem();
        for(int result : results) {
            System.out.println(result);
        }

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st;
            st = new StringTokenizer(br.readLine());
            vertices = Integer.parseInt(st.nextToken());
            root = Integer.parseInt(st.nextToken());
            queryCount = Integer.parseInt(st.nextToken());

            graphInfo = new ArrayList<List<Integer>>();
            for(int i = 0; i <= vertices; i++) {
                graphInfo.add(new LinkedList());
            }

            queries = new int[queryCount];
            for(int i = 0; i < vertices - 1; i++) {
                st = new StringTokenizer(br.readLine());
                int source = Integer.parseInt(st.nextToken());
                int destination = Integer.parseInt(st.nextToken());

                graphInfo.get(source).add(destination);
                graphInfo.get(destination).add(source);
            }

            for(int i = 0; i < queryCount; i++) {
                queries[i] = Integer.parseInt(br.readLine());
            }

            Map<String, Object> inputData = new HashMap<String, Object>();
            inputData.put("graphInfo", graphInfo);
            inputData.put("queries", queries);
            inputData.put("root", root);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static List<Integer> solveProblem() {
        visit = new boolean[graphInfo.size()];
        dp = new int[graphInfo.size()];

        depthFirstSearch(root);

        List<Integer> results = new LinkedList<Integer>();
        for(int query : queries) {
            results.add(dp[query]);
        }
        return results;

    }

    private static int depthFirstSearch(int vertex) {
        visit[vertex] = true;
        int subtreeCount = 1;

        List<Integer> adjacentVertices = graphInfo.get(vertex);
        for(int adjacentVertex : adjacentVertices) {
            if(!visit[adjacentVertex]) {
                subtreeCount += depthFirstSearch(adjacentVertex);
            }
        }

        dp[vertex] = subtreeCount;
        return dp[vertex];
    }

}
