package backjoon.minimum_spanning_tree;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class Minimum_Spanning_Tree1197 {

    /*
     6 9
1 5 1
1 3 4
1 4 3
1 2 9
2 4 4
2 5 5
3 6 6
4 5 2
4 6 8
     */

    private static int vertices;
    private static int edge;
    private static List<Node1197> graphInfo;
    private static int[] parent;
    private static long result;
    private static int mstSize = 0;
//    private static List<Node1197> mst = new LinkedList<>();

    static class Node1197 implements Comparable<Node1197> {

        private int source;
        private int destination;
        private int weight;

        Node1197(int source, int destination, int weight) {
            this.source = source;
            this.destination = destination;
            this.weight = weight;
        }

        public int compareTo(Node1197 node) {
            return this.weight - node.weight;
        }
    }

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        System.out.println(result);

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            vertices = Integer.parseInt(st.nextToken());
            edge = Integer.parseInt(st.nextToken());

            graphInfo = new ArrayList<>();

            int source, destination, weight;
            for(int i = 0; i < edge; i++) {
                st = new StringTokenizer(br.readLine());
                source = Integer.parseInt(st.nextToken());
                destination = Integer.parseInt(st.nextToken());
                weight = Integer.parseInt(st.nextToken());

                graphInfo.add(new Node1197(source, destination, weight));
            }

            /*
             * union find setting
             */
            Collections.sort(graphInfo);
            parent = new int[vertices + 1];
            for(int i = 1; i <= vertices; i++) {
                parent[i] = i;
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Kruskal Algorithm
     */
    private static void solveProblem() {
        for(Node1197 node : graphInfo) {
            int source = node.source;
            int destination = node.destination;

            /*
             * 사이클이 발생할 경우 다음 edge 탐색
             */
            if(findRoot(source) == findRoot(destination))
                continue;

            mstSize++;
//            mst.add(node);
            result += node.weight;
            unionRoot(source, destination);

            if(mstSize == vertices - 1)
                return;
        }
    }

    private static int findRoot(int x) {
        if(parent[x] == x)
            return x;
        return parent[x] = findRoot(parent[x]);
    }

    private static void unionRoot(int x, int y) {
        x = findRoot(x);
        y = findRoot(y);

        if(x != y)
            parent[y] = x;
    }

}
