package backjoon.regExp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExp2941_important {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 최대 100글자의 단어가 주어진다. 알파벳 소문자와 '-', '='로만 이루어져 있다.
         * 단어는 크로아티아 알파벳으로 이루어져 있다. 문제 설명의 표에 나와있는 알파벳은 변경된 형태로 입력된다.
         *
         * 크로아티아 알파벳	변경
         *        č	         c=
         *        ć	         c-
         *        dž	     dz=
         *        đ	         d-
         *        lj	     lj
         *        nj	     nj
         *        š	         s=
         *        ž	         z=
         *
         * 2. output 조건
         * 입력으로 주어진 단어가 몇 개의 크로아티아 알파벳으로 이루어져 있는지 출력한다.
         * dž는 무조건 하나의 알파벳으로 쓰이고, d와 ž가 분리된 것으로 보지 않는다. lj와 nj도 마찬가지이다.
         * 위 목록에 없는 알파벳은 한 글자씩 센다.
         *
         * 3. 주의사항
         * 연습겸 java regular expression을 사용하면서 grouping을 해줄 때 주의해야할 점이 많다는 것을 알았다.
         * []나 ()는 오직 하나의 패턴 그룹에 대해서만 정의하는 것이다.
         * 이메일이나 전화번호의 예시를 보면 이해할 수 있다.
         * 여러 패턴의 조합으로 이루어진 하나의 패턴을 조합하는 형태이고, 그것들을 이어주는 고정 문자가 존재한다.
         * -----------------------------------------------------------------------------------------
         * 이 문제의 경우 아래 두 가지 상황에 따라서 주의해야할 점이 2개가 있다.
         *  1. 일치하는 패턴을 찾을 때
         * 일치하는 패턴을 찾을 때는, 오직 하나의 그룹 패턴만이 필요한 상황이기 때문에 내가 했던 실수처럼 최외곽에 []나 ()가 여러 개 존재해서는 안된다.
         * 주어진 크로아티아 패턴에 대한 그룹이기 때문에 () 한 쌍만 정의하고, 그 내부에서 |을 이용하여 여러 패턴 조건을 추가한다.
         *
         *  2. 일치하지 않는 패턴을 찾을 때
         * 1번의 내용을 그대로 가져와도 되지만 |을 기준으로 각각의 group으로 나눠서 정의하는 것이 더 좋다고 생각한다.
         * 그리고 not을 써야하기 때문에 []안에 ^을 넣고 나서 패턴을 넣는다. <--- 이건 아래 핵심로직의 주석을 참고하라. 사용하지 않는다.
         *
         * 또한 패턴 중에 두 패턴 사이에 부분 집합 관계가 있는 경우, 상위 집합을 먼저 검사할 수 있도록 앞에 배치한다.
         * 따로 조건을 줘서 처리하려고 했지만 너무 어렵고 번거로웠다. (공부를 더 하다보면 될지도?)
         * ex) dz= 먼저 확인 후에 z= 확인.
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * ljes=njak
         * -------
         * answer
         * 6
         *
         * 2.
         * ddz=z=
         * --------
         * answer
         * 3
         *
         * 3.
         * dz=ak
         * --------
         * answer
         * 3
         *
         */

        String inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String inputData) {
        /*
         * java regular expression(정규식 표현)을 사용하는 연습을 하는 차원에서 정규식을 사용한다.
         * 참고한 자료에서 모르는 부분이 있어서 이건 기록으로 남겨둔다.
         * ?= 는 전방 탐색 기호로 (?=.*[a-z]) 소문자가 1개 이상 들어갔는지 확인하는 부분
         * ?는 없거나 있거나 !는 아닌 문자를 의미한다.
         * [^]와 같다고 생각했는데 막상 실제로 적용해보면 원하는 답이 안나오는걸로 봐서.. 심층적으로 이해를 잘 못하고 있나보다..
         * ----------------------------------------------------
         * 1시간 이상 고민해봤는데 내린 결론은 아래와 같다.
         * 1. 매치하지 않는 것을 고르기 위해 따로 패턴을 만들 필요가 없고, 할 수도 없다고 생각한다.
         * 그 이유는 일치하는 것 다음 인덱스부터 검색을 해야하는데, lj의 경우 j를 다른 문자로 인식해버리는 문제가 생긴다.
         * 일치하지 않으면 다음 인덱스로 lj의 다음이 아닌 l의 다음 인덱스를 가리키기 때문인 것으로 생각한다.
         * 거기다 ^(?!) 방식은 제외해야할 패턴이 연속된 문자열에 있는 순간에 전체적으로 문자열을 탐색하지 않는? 경향이 있는 것 같다..
         *
         * 그래서 차라리 모든 패턴을 다 검토하고 나서 추가로 알파벳 소문자와 -=를 일치하는지 확인하는 패턴을 추가하는게 맞는 것 같다.
         */
        String matchedRegexp = "(dz=|lj|nj|[csz]{1}[=]{1}|[cd]{1}[-]{1}|[a-z-=])";
//        String unmatchedRegexp = "^(?!dz=|lj|nj|[csz]{1}[=]{1}|[cd]{1}[-]{1}){1}$";

        Pattern matchedPattern = Pattern.compile(matchedRegexp);
//        Pattern unmatchedPattern = Pattern.compile(unmatchedRegexp);

        Matcher matcher = matchedPattern.matcher(inputData);
//        Matcher unmatcher = unmatchedPattern.matcher(inputData);

        int matchCnt = 0;
        while(matcher.find()) {
//            String groupStr = matcher.group();
//            System.out.println("groupStr : " + groupStr);
            matchCnt++;
        }

//        int unmatchCnt = 0;
//        while(unmatcher.find()) {
//            String otherStr = unmatcher.group();
//            System.out.println("otherStr : " + otherStr);
//            unmatchCnt++;
//        }

        return matchCnt;
    }

}
