package backjoon.regExp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RegExp2671 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 0과 1로 구성된 스트링이 1개 들어있다. 이때 각 스트링의 길이는 문자열 길이는 (1 ≤ N ≤ 150)의 범위를 갖는다.
         *
         * 2. output 조건
         * 입력에 들어있는 스트링을 읽고, 이것이 잠수함의 엔진소리를 나타내는 스트링인지 아니면 그냥 물속의 잡음인지를 판정한다.
         * 잠수함의 엔진 소리에 해당하는 스트링이면 "SUBMARINE"을 출력하고, 그렇지 않으면 "NOISE"를 출력한다.
         * 식별하고자 하는 문자열 패턴은 다음과 같다. (100~1~|01)~
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 1013번 문제와 똑같다. 주어진 패턴을 거의 그대로 사용하면 풀어진다.
         *
         * Example
         * 1.
         * 10010111
         * -------
         * answer
         * NOISE
         *
         * 2.
         * 100000000001101
         * --------
         * answer
         * SUBMARINE
         *
         */

        String inputData = inputData();
        String result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String solveProblem(String inputData) {
        String matchedRexExp = "(100+1+|01)+";
        if(inputData.matches(matchedRexExp)) {
            return "SUBMARINE";
        }

        return "NOISE";
    }

}
