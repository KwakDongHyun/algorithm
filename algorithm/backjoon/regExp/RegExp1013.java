package backjoon.regExp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RegExp1013 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력의 첫 줄에는 테스트 케이스의 개수 T가 주어진다.
         * 그 다음 줄부터 각각의 테스트 케이스에 대해 전파를 표현하는, { 0, 1 }만으로 이루어진 문자열이 공백 없이 주어진다.
         * 문자열 길이는 (1 ≤ N ≤ 200)의 범위를 갖는다.
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해 주어진 전파가 문제에서 제시한 패턴이면 “YES”를 그렇지 않은 경우는 “NO”를 출력한다.
         * 출력 문자열은 모두 대문자로 구성되어 있다.
         * 제시한 패턴은 (100+1+ | 01)+ 이며, +는 최소 1개 이상의 해당 문자열 pattern으로 이루어져 있다는 의미이다.
         *
         * 3. 주의사항
         * 정규식 표현으로 풀면 쉽게 풀 수 있다고 생각한다.
         * regular expression을 직접 만드는 경우가 있을까? 있다고 한다면 어떻게 할 수 있는지가 궁금하다
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 3
         * 10010111
         * 011000100110001
         * 0110001011001
         * -------
         * answer
         * NO
         * NO
         * YES
         *
         */

        String[] inputData = inputData();
        String[] results = solveProblem(inputData);
        for(String result : results) {
            System.out.println(result);
        }

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            String[] inputData = new String[testCase];

            for(int i = 0; i < testCase; i++) {
                inputData[i] = br.readLine();
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String[] solveProblem(String[] inputData) {
        int testCase = inputData.length;
        String[] results = new String[testCase];

        String matchedRegExp = "(100+1+|01)+";

        for(int i = 0; i < testCase; i++) {
            if(inputData[i].matches(matchedRegExp)) {
                results[i] = "YES";
            } else {
                results[i] = "NO";
            }
        }

        return results;
    }

}
