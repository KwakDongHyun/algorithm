package backjoon.dijkstra;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Dijkstra13549 {

    private static int[] coordinate = new int[2];
    private static int[] visit = new int[100_001];

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        System.out.println(visit[coordinate[1]]);

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            coordinate[0] = Integer.parseInt(st.nextToken());
            coordinate[1] = Integer.parseInt(st.nextToken());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * 왜 이 문제가 dijkstra의 개념을 도입할 수 있는지 알 수 있다.
     * 최소 가중치를 먼저 선택한다는 개념을 이 문제에 적용시켜보면, 시간 소요를 하지 않는 이동방식인
     * x2배 이동 방법을 우선적으로 먼저 경로 탐색을 하게 만드는 것이다.
     * 때문에 최단경로 알고리즘을 적용할 수 있는 것이다.
     */
    private static void solveProblem() {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(coordinate[0]);
        for(int i = 0; i <= 100_000; i++) {
            visit[i] = -1;
        }
        visit[coordinate[0]] = 0;

        while(!queue.isEmpty()) {
            int point = queue.poll();
            if(point == coordinate[1]) {
                break;
            }

            for(int i = 0; i < 3; i++) {
                int nextPoint;
                switch (i) {
                    case 0:
                        nextPoint = 2 * point;
                        break;
                    case 1:
                        nextPoint = point - 1;
                        break;
                    default:
                        nextPoint = point + 1;
                        break;
                }

                if(nextPoint < 0 || nextPoint > 100_000 || visit[nextPoint] > -1) {
                    continue;
                }

                if(i == 0) {
                    visit[nextPoint] = visit[point];
                } else {
                    visit[nextPoint] = visit[point] + 1;
                }
                queue.add(nextPoint);
            }
        }
    }

}
