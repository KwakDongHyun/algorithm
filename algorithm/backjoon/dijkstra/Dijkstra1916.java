package backjoon.dijkstra;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.StringTokenizer;

public class Dijkstra1916 {

    private static int startVertex;
    private static int destinationVertex;

    static class Node1916 implements Comparable<Node1916> {

        int vertex;
        int weight;

        Node1916(int vertex, int weight) {
            this.vertex = vertex;
            this.weight = weight;
        }

        @Override
        public int compareTo(Node1916 o) {
            return this.weight - o.weight;
        }
    }

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 도시의 개수 N(1 ≤ N ≤ 1,000)이 주어지고 둘째 줄에는 버스의 개수 M(1 ≤ M ≤ 100,000)이 주어진다.
         * 그리고 셋째 줄부터 M+2줄까지 다음과 같은 버스의 정보가 주어진다.
         * 먼저 처음에는 그 버스의 출발 도시의 번호가 주어진다. 그리고 그 다음에는 도착지의 도시 번호가 주어지고 또 그 버스 비용이 주어진다.
         * 버스 비용은 0보다 크거나 같고, 100,000보다 작은 정수이다. (0 ≤ V < 100,000)
         * 그리고 M+3째 줄에는 우리가 구하고자 하는 구간 출발점의 도시번호와 도착점의 도시번호가 주어진다.
         * 출발점에서 도착점을 갈 수 있는 경우만 입력으로 주어진다.
         *
         * 2. output 조건
         * 첫째 줄에 출발 도시에서 도착 도시까지 가는데 드는 최소 비용을 출력한다.
         *
         * 3. 주의사항
         * 이전에 풀었던 1753번 문제와 동일하다.
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 5
         * 8
         * 1 2 2
         * 1 3 3
         * 1 4 1
         * 1 5 10
         * 2 4 2
         * 3 4 1
         * 3 5 1
         * 4 5 3
         * 1 5
         * -------
         * answer
         * 4
         *
         */

        List<List<Node1916>> graphData = graphData();
        int result = solveProblem(graphData);
        System.out.println(result);

    }

    public static List<List<Node1916>> graphData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int vertexCnt = Integer.parseInt(br.readLine());
            int edgeCnt = Integer.parseInt(br.readLine());

            List<List<Node1916>> graphData = new ArrayList<List<Node1916>>(vertexCnt + 1);
            for(int i = 0; i <= vertexCnt; i++) {
                graphData.add(new LinkedList<Node1916>());
            }

            StringTokenizer st;
            int src, dest, weight;
            for(int i = 1; i <= edgeCnt; i++) {
                st = new StringTokenizer(br.readLine());
                src = Integer.parseInt(st.nextToken());
                dest = Integer.parseInt(st.nextToken());
                weight = Integer.parseInt(st.nextToken());

                // List<Node1916>
                graphData.get(src).add(new Node1916(dest, weight));
            }

            st = new StringTokenizer(br.readLine());
            startVertex = Integer.parseInt(st.nextToken());
            destinationVertex = Integer.parseInt(st.nextToken());

            return graphData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(List<List<Node1916>> graphData) {
        int vertexCnt = graphData.size() - 1;

        // 최단거리 테이블 초기화
        int[] shortestTable = new int[vertexCnt + 1];
        for(int i = 1; i <= vertexCnt; i++) {
            shortestTable[i] = 100_000_000;
        }

        // 시간 초과를 막기 위해서 방문여부 테이블 추가
        boolean[] visit = new boolean[vertexCnt + 1];

        /*
         * 우선순위 큐를 이용해서 O(V+E)만큼의 시간만 걸리도록 설계한다.
         * 시작 지점을 큐에 넣으면서 거리는 0으로 설정한다. (자기 자신에 대해서는 이동거리가 0이니까)
         */
        Queue<Node1916> queue = new PriorityQueue<Node1916>();
        queue.add(new Node1916(startVertex, 0));
        shortestTable[startVertex] = 0;

        Node1916 queueNode;
        int cost, vertex;
        List<Node1916> linkedList;
        while(!queue.isEmpty()) {
            queueNode = queue.poll();
            vertex = queueNode.vertex;
            cost = queueNode.weight;

            // 이미 방문했으면 로직 수행을 하지 않음
            if(visit[vertex]) {
                continue;
            }
            visit[vertex] = true;

            linkedList = graphData.get(vertex);
            for(Node1916 node : linkedList) {

                /*
                 * 현재 방문 중인 노드까지의 최단거리 비용과 주변 노드까지 이동시 소모되는 비용의 합을 이전에 저장된 최단거리 값과 비교
                 * 최단거리가 갱신된다면 해당 노드를 방문한다.
                 * 새로 발견한 최단경로 값을 넣더라도 우선순위 큐 특성상, 최소값이 먼저 나오게 되어있다.
                 * 그렇기 때문에 더 큰 값은 탐색 순위가 밀리게 되고, 조건문에 부합하지 않기 때문에 실질적인 로직은 수행하지 않게 된다.
                 *
                 * 자바에서 우선순위 큐를 사용하기 위해서는 객체의 경우 정렬기준을 정해줘야 한다.
                 * Comparable을 구현해주던지, 우선순위 큐 생성자에 Argument로 comparator를 넣어주던지 해야한다.
                 */
                if(cost + node.weight < shortestTable[node.vertex]) {
                    shortestTable[node.vertex] = cost + node.weight;
                    queue.add(new Node1916(node.vertex, shortestTable[node.vertex]));
                }
            }
        }

        return shortestTable[destinationVertex];
    }

}
