package backjoon.dijkstra;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class Dijkstra1753 {

    static class Node1753 implements Comparable<Node1753> {

        int vertex;
        int weight;

        Node1753(int vertex, int weight) {
            this.vertex = vertex;
            this.weight = weight;
        }

        @Override
        public int compareTo(Node1753 o) {
            return this.weight - o.weight;
        }

    }

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 정점의 개수 V와 간선의 개수 E가 주어진다. (1 ≤ V ≤ 20,000, 1 ≤ E ≤ 300,000)
         * 모든 정점에는 1부터 V까지 번호가 매겨져 있다고 가정한다.
         * 둘째 줄에는 시작 정점의 번호 K(1 ≤ K ≤ V)가 주어진다.
         * 셋째 줄부터 E개의 줄에 걸쳐 각 간선을 나타내는 세 개의 정수 (u, v, w)가 순서대로 주어진다.
         * 이는 u에서 v로 가는 가중치 w인 간선이 존재한다는 뜻이다. u와 v는 서로 다르며 w는 10 이하의 자연수이다.
         * 서로 다른 두 정점 사이에 여러 개의 간선이 존재할 수도 있음에 유의한다.
         *
         * 2. output 조건
         * 첫째 줄부터 V개의 줄에 걸쳐, i번째 줄에 i번 정점으로의 최단 경로의 경로값을 출력한다.
         * 시작점 자신은 0으로 출력하고, 경로가 존재하지 않는 경우에는 INF를 출력하면 된다.
         *
         * 3. 해석
         * Dijkstra 알고리즘의 대표적인 문제이다.
         * 우선순위큐를 이용하는 방법이 있으니, 이를 잘 활용해보자.
         *
         * 4. 배워야할 점
         * 재귀가 항상 성능이 나쁜건 아니다.
         * 물론 재귀를 쓰지 않고서 DFS나 백트래킹을 구현할 수는 있겠지만, 많은 변수가 필요하고 고려해야할 사항이 너무 많다.
         * 태생적인 한계를 넘기 위해서 다른 알고리즘 방법을 생각하는게 낫다고 본다.
         * 기존의 것을 재구성하는 것만으로는 한계가 있었다.
         *
         * Example
         * 1번 예제
         * 5 6
         * 1
         * 5 1 1
         * 1 2 2
         * 1 3 3
         * 2 3 4
         * 2 4 5
         * 3 4 6
         * -------
         * answer
         * 0
         * 2
         * 3
         * 7
         * INF
         *
         */
        int[][] inputData = inputData();
        List<Node1753>[] graphData = getGraphData(inputData);
        int[] shortestTable = dijkstra(inputData[0][2], graphData);

        for(int i = 1; i <= shortestTable.length-1; i++) {
            if(shortestTable[i] == Integer.MAX_VALUE) {
                System.out.println("INF");
            } else {
                System.out.println(shortestTable[i]);
            }
        }

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int vertexCount = Integer.parseInt(st.nextToken());
            int edgeCount = Integer.parseInt(st.nextToken());
            int startVertex = Integer.parseInt(br.readLine());

            int[][] inputData = new int[edgeCount + 1][3];
            inputData[0][0] = vertexCount;
            inputData[0][1] = edgeCount;
            inputData[0][2] = startVertex;

            for(int i = 1; i <= edgeCount; i++) {
                st = new StringTokenizer(br.readLine());
                inputData[i][0] = Integer.parseInt(st.nextToken());
                inputData[i][1] = Integer.parseInt(st.nextToken());
                inputData[i][2] = Integer.parseInt(st.nextToken());
            }

            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Node1753>[] getGraphData(int[][] inputData) {
        int vertexCount = inputData[0][0];
        int edgeCount = inputData[0][1];

        List<Node1753>[] graphData = (List<Node1753>[]) new List[vertexCount + 1];
        for(int i = 1; i <= vertexCount; i++) {
            graphData[i] = new LinkedList();
        }

        int sourceVertex, destinationVertex, weight;
        for(int i = 1; i <= edgeCount; i++) {
            sourceVertex = inputData[i][0];
            destinationVertex = inputData[i][1];
            weight = inputData[i][2];

            graphData[sourceVertex].add(new Node1753(destinationVertex, weight));
        }

        return graphData;
    }

    public static int[] dijkstra(int startVertex, List<Node1753>[] graphData) {
        int vertexCount = graphData.length - 1;
        int[] shortestTable = new int[graphData.length];

        // 최단거리 테이블 초기화
        for(int i = 1; i <= vertexCount; i++) {
            shortestTable[i] = Integer.MAX_VALUE;
        }

        // 로직 시작 전 초기화 세팅
        PriorityQueue<Node1753> queue = new PriorityQueue<Node1753>();
        // 시작 지점은 거리를 0으로 처리
        shortestTable[startVertex] = 0;
        queue.add(new Node1753(startVertex, 0));

        while(!queue.isEmpty()) {
            Node1753 queueNode = queue.poll();
            int currentVertex = queueNode.vertex;
            int weight = queueNode.weight;

            for(Node1753 node : graphData[currentVertex]) {
                if(weight + node.weight < shortestTable[node.vertex]) {
                    shortestTable[node.vertex] = weight + node.weight;
                    queue.add(new Node1753(node.vertex, shortestTable[node.vertex]));
                }
            }
        }

        return shortestTable;
    }

}
