package backjoon.math;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.StringTokenizer;

public class Math1016 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 어떤 정수 X가 1보다 큰 제곱수로 나누어 떨어지지 않을 때, 그 수를 제곱ㄴㄴ수라고 한다.
         * 제곱수는 정수의 제곱이다.
         * 첫째 줄에 두 정수 min과 max가 주어진다.
         * (1 ≤ min ≤ 1,000,000,000,000), (min ≤ max ≤ min + 1,000,000)
         *
         * 2. output 조건
         * min과 max가 주어지면, min보다 크거나 같고, max보다 작거나 같은 제곱ㄴㄴ수가 몇 개 있는지 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * prime number를 알아야 효과적으로 문제를 풀 수 있다.
         * 제곱 ㄴㄴ수는 소인수분해 결과, 임의의 prime number가 1개의 곱으로 이루어져 있다.
         * 하지만 prime number를 구하고 난 후에 경우의 수에 따라서 조합을 하는 것이 굉장히 오래 걸린다.
         * 공학적으로 문제를 풀려면 prime number의 제곱으로 나누어서 남은 수를 세는 것이 빠르다.
         *
         * Example
         * 1.
         * 1 10
         * ----------------------
         * answer
         * 7
         *
         * 2.
         * 15 15
         * ----------------------
         * answer
         * 1
         *
         * 3.
         * 1 1000
         * ----------------------
         * answer
         * 608
         *
         */

        long[] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static long[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());

            long[] inputData = new long[2];
            inputData[0] = Long.parseLong(st.nextToken());
            inputData[1] = Long.parseLong(st.nextToken());

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(long[] inputData) {
        long min = inputData[0];
        long max = inputData[1];
        int diff = (int) (max - min);

        boolean[] numArr = new boolean[diff + 1];

        for(long i = 2; i * i <= max; i++) {
            long pow = i * i;
            // min보다 작은 수부터 시작하면 Array IOB Exp가 발샐항 수 있다. 따라서 스타트 지점을 미리 체크한다.
            long j = min % pow == 0 ? min / pow : (min / pow) + 1;

            for(; j * pow <= max; j++) {
                numArr[(int) (j * pow - min)] = true;
            }
        }

        int count = 0;
        for(boolean result : numArr) {
            if(!result) {
                count++;
            }
        }

        return count;
    }

}
