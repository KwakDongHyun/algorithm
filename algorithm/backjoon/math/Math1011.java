package backjoon.math;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Math1011 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력의 첫 줄에는 테스트케이스의 개수 T가 주어진다.
         * 각각의 테스트 케이스에 대해 현재 위치 x와 목표 위치 y가 정수로 주어지며, x는 항상 y보다 작은 값을 갖는다. (0 ≤ x < y < 2^31)
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해 x지점으로부터 y지점까지 정확히 도달하는데 필요한 최소한의 공간이동 장치 작동 횟수를 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 1부터 최소 이동을 위한 이동방식을 직접 계산하다보면 수학적인 공식이 나타난다.
         * 현재위치와 목표위치 간의 거리에 따라서 이동 장지 작동 횟수의 최소값 공식이 있다.
         * 현재위치와 목표위치 간의 거리에 따른 최소 이동 횟수 공식은 아래와 같다.
         *
         *  1. 가정
         *  현재위치와 목표위치 간의 거리를 k, 거리가 k일 때의 최소 이동 횟수는 Ak라고 정의한다. (1 ≤ k < 2^31 - 1)
         *
         *  2. 공식
         *  k가 1, 2, 3일 때는 각각 1, 2, 3으로 고정된 값이다.
         *  k가 i^2와 같은 제곱 수인 경우 최소 이동 횟수는 2k-1이다. (1 ≤ i < 2^15, 대략적인 범위이다.)
         *
         *  k는 항상 제곱 수들의 범위 안에 속하며 a = i^2, b = (i+1)^2 이라고 가정해보자.
         *  a의 최소 이동 횟수는 (2i - 1)이고 b의 최소 이동 횟수는 2(i+1) - 1 = (2i + 1)이다.
         *  a < k < b 라고 정의하면, a와 b 중에서 k가 더 가까운 쪽에 위치한 곳에 따라 최소 이동 횟수가 정해진다.
         *  k - a < b - k 라고 한다면 최소 이동 횟수는 (2i - 1) + 1 = 2i이다.
         *  k - a > b - k 라고 한다면 최소 이동 횟수는 2i + 1이다.
         *
         * Example
         * 1.
         * 3
         * 0 3
         * 1 5
         * 45 50
         * -------
         * answer
         * 3
         * 3
         * 4
         *
         * 2.

         * --------
         * answer
         *
         * 3.
         *
         * --------
         * answer
         *
         *
         */

        int[][] inputData = inputData();
        List<Long> results = solveProblem(inputData);
        for(long result : results) {
            System.out.println(result);
        }

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            int[][] inputData = new int[testCase][2];

            StringTokenizer st;
            for(int i = 0; i < testCase; i++) {
                st = new StringTokenizer(br.readLine());
                inputData[i][0] = Integer.parseInt(st.nextToken());
                inputData[i][1] = Integer.parseInt(st.nextToken());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<Long> solveProblem(int[][] inputData) {
        int testCase = inputData.length;
        List<Long> results = new ArrayList<Long>(testCase);

        for(int idx = 0; idx < testCase; idx++) {
            long distance = inputData[idx][1] - inputData[idx][0];

            if(distance < 4) {
                results.add(distance);
            } else {
                long limit = (int) Math.pow(2, 16);
                long lLimit = 0, rLimit = 0, i = 0;

                for(long k = 2; k < limit; k++) {
                    if(distance <= k*k) {
                        lLimit = (k-1)*(k-1);
                        rLimit = k*k;
                        i = k-1;
                        break;
                    }
                }

                if(distance - lLimit < rLimit - distance) {
                    results.add(2*i);
                } else {
                    results.add(2*i + 1);
                }
            }

        } // for-end

        return results;
    }

}
