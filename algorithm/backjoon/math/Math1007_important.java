package backjoon.math;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Math1007_important {

    private static double sumOfX, sumOfY;

    private static int vectorCount;
    private static double minVectorLen;
    private static boolean[] visit;

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 테스트 케이스의 개수 T가 주어진다. 각 테스트 케이스는 다음과 같이 구성되어있다.
         *
         * 테스트 케이스의 첫째 줄에 점의 개수 N이 주어진다. N은 짝수이다.
         * 둘째 줄부터 N개의 줄에 점의 좌표가 주어진다. N은 20이하의 자연수이며, 좌표는 절댓값이 100,000보다 작거나 같은 정수다.
         * (1 ≤ N ≤ 20), (|x|, |y| ≤ 100,000)
         * 모든 점은 서로 다르다.
         *
         * 2. output 조건
         * 각 테스트 케이스마다 정답을 출력한다. 절대/상대 오차는 10-6까지 허용한다.
         * 벡터 매칭에 있는 벡터의 합의 길이의 최솟값을 출력하는 프로그램을 작성하라.
         *
         * 평면 상에 N개의 점이 찍혀있고, 그 점을 집합 P라고 하자.
         * 집합 P의 벡터 매칭은 벡터의 집합인데, 모든 벡터는 집합 P의 한 점에서 시작해서, 또 다른 점에서 끝나는 벡터의 집합이다.
         * 또, P에 속하는 모든 점은 한 번씩 쓰여야 한다.
         * 벡터 매칭에 있는 벡터의 개수는 P에 있는 점의 절반이다.
         *
         * 3. 주의사항
         * 재귀적인 방법으로 경우의 수를 탐색해야한다.
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 2
         * 4
         * 5 5
         * 5 -5
         * -5 5
         * -5 -5
         * 2
         * -100000 -100000
         * 100000 100000
         * -------
         * answer
         * 0.000000000000
         * 282842.712474619038
         *
         * 2.
         * 1
         * 10
         * 26 -76
         * 65 -83
         * 78 38
         * 92 22
         * -60 -42
         * -27 85
         * 42 46
         * -86 98
         * 92 -47
         * -41 38
         * --------
         * answer
         * 13.341664064126334
         *
         */

        List<int[][]> inputData = inputData();
        List<Double> results = solveProblem(inputData);
        for(double result : results) {
            System.out.println(result);
        }

    }

    public static List<int[][]> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            List<int[][]> data = new ArrayList<int[][]>(testCase);

            for(int i = 0; i < testCase; i++) {
                int N = Integer.parseInt(br.readLine());
                int[][] points = new int[2][N];
                StringTokenizer st;

                for(int j = 0; j < N; j++) {
                    st = new StringTokenizer(br.readLine());
                    points[0][j] = Integer.parseInt(st.nextToken());
                    points[1][j] = Integer.parseInt(st.nextToken());
                }

                data.add(points);
            }

            return data;

        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<Double> solveProblem(List<int[][]> inputData) {
        List<Double> results = new ArrayList<Double>(inputData.size());

        for(int[][] data : inputData) {
            int length = data[0].length;
            vectorCount = length / 2;
            sumOfX = 0;
            sumOfY = 0;

            for(int i = 0; i < length; i++) {
                sumOfX += data[0][i];
                sumOfY += data[1][i];
            }

            minVectorLen = Double.MAX_VALUE;
            visit = new boolean[length];
            solve(data, 0, 0);
            results.add(minVectorLen);
        }

        return results;
    }

    private static void solve(int[][] data, int index, int count) {
        if(count == vectorCount) {
            double partialSumX = 0, partialSumY = 0;

            for(int i = 0; i < data[0].length; i++) {
                if(visit[i]) {
                    partialSumX += data[0][i];
                    partialSumY += data[1][i];
                }
            }

            double x = sumOfX - 2 * partialSumX;
            double y = sumOfY - 2 * partialSumY;
            double vectorLen = Math.sqrt(x*x + y*y);
            minVectorLen = Math.min(minVectorLen, vectorLen);
            return;
        }

        for(int i = index; i < data[0].length; i++) {
            if(!visit[i]) {
                visit[i] = true;
                solve(data, i + 1, count + 1);
                visit[i] = false;
            }
        }
    }

}
