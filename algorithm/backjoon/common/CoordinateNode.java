package backjoon.common;

public class CoordinateNode {

    private int x;
    private int y;

    public CoordinateNode (int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getCoordinateX() {
        return this.x;
    }

    public int getCoordinateY() {
        return this.y;
    }

}
