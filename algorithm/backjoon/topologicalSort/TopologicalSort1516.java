package backjoon.topologicalSort;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class TopologicalSort1516 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 건물의 종류 수 N(1 ≤ N ≤ 500)이 주어진다.
         * 다음 N개의 줄에는 각 건물을 짓는데 걸리는 시간과 그 건물을 짓기 위해 먼저 지어져야 하는 건물들의 번호가 주어진다.
         * 건물의 번호는 1부터 N까지로 하고, 각 줄은 -1로 끝난다고 하자. 각 건물을 짓는데 걸리는 시간은 100,000보다 작거나 같은 자연수이다.
         * 모든 건물을 짓는 것이 가능한 입력만 주어진다.
         *
         * 2. output 조건
         * N개의 각 건물이 완성되기까지 걸리는 최소 시간을 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 예전에 비슷하게 가장 마지막 건물의 건설시간을 구하는 문제가 있었다.
         * topological Sort를 이용하는 1005번 문제와 매우 유사하다.
         * 위상정렬을 사용하면 문제야 풀리겠지만 그래도 이번 문제에서는 DP를 이용해서도 풀어보고 싶다.
         *
         * Example
         * 1.
         * 5
         * 10 -1
         * 10 1 -1
         * 4 1 -1
         * 4 3 1 -1
         * 3 3 -1
         * ----------------------
         * answer
         * 10
         * 20
         * 14
         * 18
         * 17
         *
         * 2.
         *
         * ----------------------
         * answer
         *
         *
         */

        Map<String, Object> inputData = inputData();
        int[] results = solveProblem(inputData);
        for(int i = 1; i < results.length; i++) {
            System.out.println(results[i]);
        }

    }

    public static Map<String, Object> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int number = Integer.parseInt(br.readLine());
            int[] indegrees = new int[number + 1];
            int[] time = new int[number + 1];

            List<List<Integer>> graphInfo = new ArrayList<List<Integer>>(number);
            for(int i = 0; i <= number; i++) {
                List<Integer> linkedBuilding = new LinkedList<Integer>();
                graphInfo.add(linkedBuilding);
            }

            int buildingNumber;
            for(int i = 1; i <= number; i++) {
                String str = br.readLine();
                String[] data = str.split(" ");
                time[i] = Integer.parseInt(data[0]);
                List<Integer> linkedBuilding = graphInfo.get(i);

                for(int j = 1; j < data.length; j++) {
                    buildingNumber = Integer.parseInt(data[j]);
                    if(buildingNumber != -1) {
                        indegrees[i]++;
                        graphInfo.get(buildingNumber).add(i);
                    }
                }

                graphInfo.add(linkedBuilding);
            }

            Map<String, Object> inputData = new HashMap<String, Object>();
            inputData.put("indegrees", indegrees);
            inputData.put("time", time);
            inputData.put("graphInfo", graphInfo);
            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int[] solveProblem(Map<String, Object> inputData) {
        int[] indegrees = (int[]) inputData.get("indegrees");
        int[] time = (int[]) inputData.get("time");
        List<List<Integer>> graphInfo = (List<List<Integer>>) inputData.get("graphInfo");

        /*
         * indegree가 0인 index부터 건설 시작
         * results : 해당 index 건물을 짓는데 걸리는 시간
         */
        Queue<Integer> queue = new LinkedList<Integer>();
        int[] results = new int[time.length];
        for(int i = 1; i < indegrees.length; i++) {
            if(indegrees[i] == 0) {
                queue.add(i);
                results[i] = time[i];
            }
        }

        int vertex, constructionTime, curConstructionTime;
        List<Integer> linkedVertices;
        while(!queue.isEmpty()) {
            vertex = queue.poll();
            linkedVertices = graphInfo.get(vertex);
            curConstructionTime = results[vertex];
//            System.out.println("curConstructionTime : " + curConstructionTime);

            for(int linkedVertex : linkedVertices) {
                indegrees[linkedVertex]--;
                constructionTime = time[linkedVertex];
//                System.out.println("results[linkedVertex] : " + results[linkedVertex]);
//                System.out.println("curConstructionTime + constructionTime : " + curConstructionTime + constructionTime);

                if(results[linkedVertex] < curConstructionTime + constructionTime) {
                    results[linkedVertex] = curConstructionTime + constructionTime;
                }

                if(indegrees[linkedVertex] == 0) {
                    queue.offer(linkedVertex);
                }
            }

        }

        return results;
    }

}
