package backjoon.topologicalSort;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

public class TopologicalSort1005 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에는 테스트케이스의 개수 T가 주어진다. 각 테스트 케이스는 다음과 같이 주어진다.
         * 첫째 줄에 건물의 개수 N과 건물간의 건설순서 규칙의 총 개수 K이 주어진다. (건물의 번호는 1번부터 N번까지 존재한다)
         * 둘째 줄에는 각 건물당 건설에 걸리는 시간 D1, D2, ..., DN이 공백을 사이로 주어진다.
         * 셋째 줄부터 K+2줄까지 건설순서 X Y가 주어진다. (이는 건물 X를 지은 다음에 건물 Y를 짓는 것이 가능하다는 의미이다)
         * 마지막 줄에는 백준이가 승리하기 위해 건설해야 할 건물의 번호 W가 주어진다.
         * (2 ≤ N ≤ 1000)
         * (1 ≤ K ≤ 100,000)
         * (1 ≤ X, Y, W ≤ N)
         * (0 ≤ Di ≤ 100,000, Di는 정수)
         *
         *
         * 2. output 조건
         * 건물 W를 건설완료 하는데 드는 최소 시간을 출력한다.
         * 편의상 건물을 짓는 명령을 내리는 데는 시간이 소요되지 않는다고 가정한다.
         * 건설순서는 모든 건물이 건설 가능하도록 주어진다.
         *
         * 3. 주의사항
         * 그래프 완전 탐색인 DFS, BFS로는 풀기가 어려울 것 같다.
         * 특히 Dijkstra는 사이클이 형성될 경우 적용이 불가능하기 때문에 다른 방법으로 접근해야 한다.
         *
         * 4. 해석
         * 위상 정렬을 보지 않고서 시도했었다. 결국에는 순서대로 조회하면서 처리해나가는 방법이 유일하다.
         * 좀 더 넓은 시야에서 이 문제점을 해결하기 위해서 어떤 방법을 사용하면 좋을지에 대해서
         * 생각을 더 많이 해보는 훈련을 하는게 좋을 것 같다.
         *
         * Example
         * 1.
         * 2
         * 4 4
         * 10 1 100 10
         * 1 2
         * 1 3
         * 2 4
         * 3 4
         * 4
         * 8 8
         * 10 20 1 5 8 7 1 43
         * 1 2
         * 1 3
         * 2 4
         * 2 5
         * 3 6
         * 5 7
         * 6 7
         * 7 8
         * 7
         * -------
         * answer
         * 120
         * 39
         *
         */
        List<Map<String, Object>> dataList = inputData();
        int result;
        for(Map<String, Object> data : dataList) {
            result = solveProblem(data);
            System.out.println(result);
        }

    }

    public static List<Map<String, Object>> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            StringTokenizer st;
            int vertex, edge, i, t;
            int[] values;
            int[][] connections;
            Map<String, Object> inputData;
            List<Map<String, Object>> dataList = new LinkedList<Map<String, Object>>();

            for(t = 0; t < testCase; t++) {

                st = new StringTokenizer(br.readLine());
                vertex = Integer.parseInt(st.nextToken());
                edge = Integer.parseInt(st.nextToken());

                values = new int[vertex + 1];
                st = new StringTokenizer(br.readLine());
                for(i = 1; i <= vertex; i++) {
                    values[i] = Integer.parseInt(st.nextToken());
                }

                connections = new int[edge + 1][2];
                for(i = 0; i < edge; i++) {
                    st = new StringTokenizer(br.readLine());
                    connections[i][0] = Integer.parseInt(st.nextToken());
                    connections[i][1] = Integer.parseInt(st.nextToken());
                }
                connections[edge][0] = Integer.parseInt(br.readLine()); // 건설해야하는 건물

                inputData = new HashMap<String, Object>();
                inputData.put("values", values);
                inputData.put("connections", connections);
                dataList.add(inputData);

            }

            return dataList;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static int solveProblem(Map<String, Object> data) {
        int[] values = (int[]) data.get("values");
        int[][] connections = (int[][]) data.get("connections");
        int target = connections[connections.length - 1][0];
        int[] dp = new int[values.length];

        // data 초기화
        Map<String, Object> processedData = preProcess(values.length, connections);
        int[] indegrees = (int[]) processedData.get("indegrees");
        List<List<Integer>> graph = (List<List<Integer>>) processedData.get("graph");

        /*
         * 진입차수가 0인 노드를 넣으면서 위상 정렬을 수행한다.
         */
        Queue<Integer> queue = new LinkedList<Integer>();
        int i;
        for(i = 1; i < indegrees.length; i++) {
            if(indegrees[i] == 0) {
                queue.offer(i);
                dp[i] = values[i]; // 시작 지점은 dp에 바로 값을 넣는다.
            }
        }

        int vertex, value;
        List<Integer> linkedVertexs;
        while(!queue.isEmpty()) {
            vertex = queue.poll();
            value = dp[vertex];

            if(vertex == target) {
                break;
            }

            linkedVertexs = graph.get(vertex);
            for(Integer linkedVertex : linkedVertexs) {
                indegrees[linkedVertex]--;

                if(value > dp[linkedVertex]) {
                    dp[linkedVertex] = value;
                }

                if(indegrees[linkedVertex] == 0) {
                    queue.add(linkedVertex);
                    dp[linkedVertex] += values[linkedVertex];
                }
            }
        }


        return dp[target];
    }

    /**
     * 알고리즘 로직을 시작하기 전에 데이터 전처리
     * topologicalSort를 사용하기 위해서 indegrees array와 graph를 만든다.
     *
     * @param vertexCount
     * @param connections
     * @return
     */
    @SuppressWarnings({"unchecked"})
    private static Map<String, Object> preProcess(int vertexCount, int[][] connections) {
        Map<String, Object> processedData = new HashMap<String, Object>();

        int[] indegrees = new int[vertexCount];
        List<List<Integer>> graph = new ArrayList<List<Integer>>(vertexCount);

        int i;
        for(i = 0; i < vertexCount; i++) {
            graph.add(new LinkedList<Integer>());
        }

        int src, dest;
        for(int[] edge : connections) {
            src = edge[0];
            dest = edge[1];
            graph.get(src).add(dest);

            indegrees[dest]++;
        }

        processedData.put("indegrees", indegrees);
        processedData.put("graph", graph);
        return processedData;
    }

}
