package backjoon.topologicalSort;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class TopologicalSort2252 {

    private static int vertices;
    private static int edge;
    private static List<List<Integer>> graphInfo = new ArrayList<>();
    private static int[] indegrees;
    private static List<Integer> result = new LinkedList<>();

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        for(int i : result) {
            System.out.print(i + " ");
        }

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            vertices = Integer.parseInt(st.nextToken());
            edge = Integer.parseInt(st.nextToken());

            for(int i = 0; i <= vertices; i++) {
                graphInfo.add(new LinkedList<>());
            }

            indegrees = new int[vertices + 1];

            int source, destination;
            for(int i = 0; i < edge; i++) {
                st = new StringTokenizer(br.readLine());
                source = Integer.parseInt(st.nextToken());
                destination = Integer.parseInt(st.nextToken());

                graphInfo.get(source).add(destination);
                indegrees[destination]++;
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        Queue<Integer> queue = new LinkedList<>();
        for(int i = 1; i <= vertices; i++) {
            if(indegrees[i] == 0) {
                queue.add(i);
                result.add(i);
            }
        }

        int vertex;
        List<Integer> adjacentVertices;
        while(!queue.isEmpty()) {
            vertex = queue.poll();
//            result.add(vertex);
            adjacentVertices = graphInfo.get(vertex);

            for(int adjacentVertex : adjacentVertices) {
                indegrees[adjacentVertex]--;

                if(indegrees[adjacentVertex] == 0) {
                    queue.offer(adjacentVertex);
                    result.add(adjacentVertex);
                }
            }
        }
    }

}
