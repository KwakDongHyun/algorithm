package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph2206_hard {

    // 2178 문제 참고.
    private static int rowCount;
    private static int columnCount;
    private static int[][] inputData;
    private static int[][] costData;

    private static boolean visited[][][];

    // 우, 하, 좌, 상 (시계방향)
    private static int[] movementX = {1, 0, -1, 0};
    private static int[] movementY = {0, 1, 0, -1};

    static class CoordinateNode2206 {

        private int x;
        private int y;
        private boolean breakable;

        CoordinateNode2206(int x, int y, boolean breakable) {
            this.x = x;
            this.y = y;
            this.breakable = breakable;
        }

        public int getCoordinateX() {
            return this.x;
        }

        public int getCoordinateY() {
            return this.y;
        }

        public boolean isBreakable() {
            return this.breakable;
        }
    }

    public static void main(String[] args) {
        preprocessData();
        getDistanceCost();

        if(costData[rowCount - 1][columnCount - 1] == 0) {
            System.out.println(-1);
        } else {
            System.out.println(costData[rowCount - 1][columnCount - 1]);
        }
    }

    public static int[][] preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            rowCount = Integer.parseInt(st.nextToken());
            columnCount = Integer.parseInt(st.nextToken());

            inputData = new int[rowCount][columnCount];
            costData = new int[rowCount][columnCount];
            visited = new boolean[rowCount][columnCount][2];
            costData[0][0] = 1;

            String data;
            for(int i = 0; i < rowCount; i++) {
                data = br.readLine();
                for(int j = 0; j < columnCount; j++) {
                    inputData[i][j] = data.charAt(j) - '0';
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void getDistanceCost() {
        Queue<CoordinateNode2206> queue = new LinkedList<CoordinateNode2206>();
        queue.offer(new CoordinateNode2206(0, 0, true));

        while(!queue.isEmpty()) {
            CoordinateNode2206 node = queue.poll();
            int x = node.getCoordinateX();
            int y = node.getCoordinateY();
            boolean breakable = node.isBreakable();
            int cost = costData[y][x];

//            System.out.println("x : " + x + ", y : " + y + ", breakable : " + breakable + ", cost : " + cost);

            for(int i = 0; i < 4; i++) {
                int nextX = x + movementX[i];
                int nextY = y + movementY[i];

                if(nextX < 0 || nextX >= columnCount || nextY < 0 || nextY >= rowCount) {
                    continue;
                }

                /*
                 * 벽을 부술 수 있는지 유무에 따라서 처리를 달리해준다.
                 */
                if(inputData[nextY][nextX] == 1) {
                    // 벽이 있을 때는 현재 벽을 부술 수 있는 상태여야지만 이동이 가능하다.
                    if(breakable && !visited[nextY][nextX][0]) {
                        visited[nextY][nextX][0] = true;
                        costData[nextY][nextX] = cost + 1;
                        queue.offer(new CoordinateNode2206(nextX, nextY, false));
                    }
                } else {
                    /*
                     * 도착지점까지 가는 길목에 벽이 1개 있는 상황에서
                     * 벽을 부수고 이동하는 단축거리와 돌아서 가는 거리가 일부 겹치는 상황이 있을 수 있다.
                     * 이 경우에 벽을 굳이 부수지 않고서 단축한 경로를 통해서 이미 지나갔던 곳을 재차 방문해야한다.
                     * 이렇게 될 경우, 벽을 부술 수 있는지 여부에 대한 상태값을 기준으로 cost를 조회하면 된다.
                     *
                     * 굳이 복잡하게 같은 위치의 두 위상의 데이터끼리 비교해서 더 작은 값을 가져오는 방법을
                     * 쓰지 않아도 된다는 말이다.
                     */
                    int z = breakable ? 1 : 0;
                    if(!visited[nextY][nextX][z]) {
                        visited[nextY][nextX][z] = true;
                        costData[nextY][nextX] = cost + 1;
                        queue.offer(new CoordinateNode2206(nextX, nextY, breakable));
                    }
                }

                if(nextX == columnCount - 1 && nextY == rowCount - 1) {
                    return;
                }
            }
        }

    }

}
