package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph2583 {

    private static int m;
    private static int n;
    private static int k;

    // 해당 좌표에 방문했는지 여부를 나타내는 2차원 배열
    private static boolean[][] visit;
    private static List<Integer> countInfo;
    private static int[] movementX = {1, 0, -1, 0};
    private static int[] movementY = {0, -1, 0, 1};
    private static int number = 1;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        Collections.sort(countInfo);
        System.out.println(number - 1);
        for(int count : countInfo) {
            System.out.print(count + " ");
        }

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            m = Integer.parseInt(st.nextToken());
            n = Integer.parseInt(st.nextToken());
            k = Integer.parseInt(st.nextToken());

            visit = new boolean[m][n];
            countInfo = new LinkedList<Integer>();

            int x1, y1, x2, y2;
            for(int i = 0; i < k; i++) {
                st = new StringTokenizer(br.readLine());
                x1 = Integer.parseInt(st.nextToken());
                y1 = Integer.parseInt(st.nextToken());
                x2 = Integer.parseInt(st.nextToken());
                y2 = Integer.parseInt(st.nextToken());

                for(int x = x1; x < x2; x++) {
                    for(int y = y1; y < y2; y++) {
                        visit[y][x] = true;
                    }
                }
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(!visit[i][j]) {
                    visit[i][j] = true;
                    bfs(i, j);
                    number++;
                }
            }
        }
    }

    private static void bfs(int sourceY, int sourceX) {
        int[] source = {sourceX, sourceY};
        Queue<int[]> queue = new LinkedList<int[]>();
        queue.add(source);
        int count = 1;

        while(!queue.isEmpty()) {
            int[] coordinate = queue.poll();
            int x = coordinate[0];
            int y = coordinate[1];

            /*if(!visit[y][x]) {
                visit[y][x] = true;
                count++;
            }*/

            for(int i = 0; i < 4; i++) {
                int nextX = x + movementX[i];
                int nextY = y + movementY[i];

                if(nextX < 0 || nextX >= n || nextY < 0 || nextY >= m) {
                    continue;
                }

                if(visit[nextY][nextX]) {
                    continue;
                }

                visit[nextY][nextX] = true;
                count++;
                int[] nextCoordinate = {nextX, nextY};
                queue.add(nextCoordinate);
            }
        }

        /*for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                System.out.print(coordinateInfo[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();*/

        countInfo.add(count);
    }

}
