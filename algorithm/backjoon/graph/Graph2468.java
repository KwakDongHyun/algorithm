package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph2468 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에는 어떤 지역을 나타내는 2차원 배열의 행과 열의 개수를 나타내는 수 N이 입력된다. (2 ≤ N ≤ 100)
         * 둘째 줄부터 N개의 각 줄에는 2차원 배열의 첫 번째 행부터 N번째 행까지 순서대로 한 행씩 높이 정보가 입력된다.
         * 각 줄에는 각 행의 첫 번째 열부터 N번째 열까지 N개의 높이 정보를 나타내는 자연수가 빈 칸을 사이에 두고 입력된다. (1 ≤ H ≤ 100)
         *
         * 2. output 조건
         * 첫째 줄에 장마철에 물에 잠기지 않는 안전한 영역의 최대 개수를 출력한다.
         *
         * 3. 해석
         * 브루트 포스로 강수량에 따라서 안전 영역을 매 번 계산해줄 수 밖에 없다.
         *
         * Example
         * 1.
         * 5
         * 6 8 2 6 2
         * 3 2 3 4 6
         * 6 7 3 3 2
         * 7 2 5 3 6
         * 8 9 5 2 7
         * -------
         * answer
         * 5
         *
         */
        int[][] inputData = inputData();
        System.out.println(solveProblem(inputData));

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int mapSize = Integer.parseInt(br.readLine());
            int[][] inputData = new int[mapSize][mapSize];
            StringTokenizer st;

            for(int i = 0; i < mapSize; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < mapSize; j++) {
                    inputData[i][j] = Integer.parseInt(st.nextToken());
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(int[][] inputData) {
        int mapSize = inputData.length;
        boolean[][] visit;
        Queue<int[]> queue;
        int safetyZone, maxSafetyZone = 0;

        // i는 강우에 따른 물의 높이
        for(int i = 0; i <= 100; i++) {
            visit = new boolean[mapSize][mapSize];
            queue = new LinkedList<int[]>();
            safetyZone = 0;

            for(int j = 0; j < mapSize; j++) {
                for(int k = 0; k < mapSize; k++) {
                    if(inputData[j][k] > i && !visit[j][k]) {
                        int[] coordinate = {j, k};
                        queue.add(coordinate);
                        visit[j][k] = true;
                        exploreByBreadthFirstSearch(inputData, visit, queue, i);
                        safetyZone++;
                    }
                }
            }

            if(maxSafetyZone < safetyZone) {
                maxSafetyZone = safetyZone;
            }
        }

        return maxSafetyZone;
    }

    private static void exploreByBreadthFirstSearch(int[][] inputData, boolean[][] visit, Queue<int[]> queue, int rainfall) {
        int mapSize = inputData.length;
        int[] moveX = {-1, 0, 1, 0};
        int[] moveY = {0, -1, 0, 1};
        int x, y, nextX, nextY;
        int[] coordinate;

        while(!queue.isEmpty()) {
            coordinate = queue.poll();
            y = coordinate[0];
            x = coordinate[1];

            for(int i = 0; i < 4; i++) {
                nextX = x + moveX[i];
                nextY = y + moveY[i];

                if(nextX < 0 || nextX >= mapSize || nextY < 0 || nextY >= mapSize) {
                    continue;
                }
                if(inputData[nextY][nextX] <= rainfall || visit[nextY][nextX]) {
                    continue;
                }

                int[] nextCoordinate = {nextY, nextX};
                queue.add(nextCoordinate);
                visit[nextY][nextX] = true;
            }
        }
    }

}
