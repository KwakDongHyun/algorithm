package backjoon.graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph2606 {

    public static void main(String[] args) throws IOException {

        /*
         * 1. input 조건
         * 첫째 줄에는 컴퓨터의 수가 주어진다. (1 ≤ N ≤ 100)
         * 각 컴퓨터에는 1번 부터 차례대로 번호가 매겨진다.
         * 둘째 줄에는 네트워크 상에서 직접 연결되어 있는 컴퓨터 쌍의 수가 주어진다.
         * 이어서 그 수만큼 한 줄에 한 쌍씩 네트워크 상에서 직접 연결되어 있는 컴퓨터의 번호 쌍이 주어진다.
         *
         * 2. output 조건
         * 1번 컴퓨터가 웜 바이러스에 걸렸을 때, 1번 컴퓨터를 통해 웜 바이러스에 걸리게 되는 컴퓨터의 수를 첫째 줄에 출력한다.
         *
         * 3. 해석
         * 1260번 문제와 똑같다. 최대한 Java Library를 활용해서 빠르게 푸는 연습부터 한다.
         *
         * Example
         * 1번 예제
         * 7
         * 6
         * 1 2
         * 2 3
         * 1 5
         * 5 2
         * 5 6
         * 4 7
         * -------
         * answer
         * 4
         *
         */
        List<List<Node>> graphData = getGraphData();
        List<Node> resultOfBreadthFirstSearch = getBreadthFirstSearch(graphData);
        System.out.println(resultOfBreadthFirstSearch.size());

    }

    public static List<List<Node>> getGraphData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int vertexCount = Integer.parseInt(br.readLine());
            int edgeCount = Integer.parseInt(br.readLine());

            List<List<Node>> graphData = new ArrayList<List<Node>>();
            for(int i = 0; i <= vertexCount; i++) {
                graphData.add(new LinkedList<Node>());
            }

            Node node;
            StringTokenizer st;
            int sourceVertex, destinationVertex;
            for(int i = 0; i < edgeCount; i++) {
                st = new StringTokenizer(br.readLine());
                sourceVertex = Integer.parseInt(st.nextToken());
                destinationVertex = Integer.parseInt(st.nextToken());

                node = new Node(destinationVertex);
                graphData.get(sourceVertex).add(node);

                node = new Node(sourceVertex);
                graphData.get(destinationVertex).add(node);
            }

            return graphData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Node> getBreadthFirstSearch(List<List<Node>> graphData) {
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(new Node(1));
        int[] visited = new int[graphData.size()];
        List<Node> visitedList = new LinkedList<Node>();

        Node queueNode;
        int vertexIndex;
        while(!queue.isEmpty()) {
            queueNode = queue.poll();
            vertexIndex = queueNode.getVertex();
            if(visited[vertexIndex] == 0) {
                visited[vertexIndex] = 1;
                visitedList.add(queueNode);
            }

//            System.out.println("stackNode : " + vertexIndex);

            List<Node> nodeList = graphData.get(vertexIndex);
            for(Node node : nodeList) {
                if(visited[node.getVertex()] == 0) {
                    queue.add(node);
                }
            }
        }

        visitedList.remove(0);
        return visitedList;
    }

}
