package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Graph10026 {

    public static void main(String[] args) {
        /*
         * 1. input 조건
         * 첫째 줄에 N이 주어진다. (1 ≤ N ≤ 100)
         * 둘째 줄부터 N개 줄에는 그림이 주어진다.
         *
         * 2. output 조건
         * 적록색약이 아닌 사람이 봤을 때의 구역의 개수와 적록색약인 사람이 봤을 때의 구역의 수를 공백으로 구분해 출력한다.
         *
         * 3. 해석
         * 2667 단지번호 붙이기와 같은 문제이다.
         * 그러나 조건이 1개 추가되었을 때도 탐색을 해야하는 경우가 생겼다.
         *
         * 4. 풀이
         * Grouping을 하려면 반드시 그룹을 짓는 '기준'이 있다.
         * 원본 데이터를 바꿔서도 안되고, 그렇다고 복사해서 데이터를 다시 짜맞춰서 로직을 돌리면 얼마나 비효율적인가?
         * 그렇게 하기 보다는 BFS 탐색에서 색맹여부에 따라 데이터를 탐색하는 방법을 구분짓는게 현재로서는 제일 쉽고 경제적이다.
         * 그렇기 때문에 이런 종류의 문제가 나온다면 숫자로 데이터를 바꿔야 진행하기가 매우 쉽다.
         * 꼭 이 문제도 다시 한 번 보기를 권장한다.
         * -------------------------------------------
         * 그리고 함수형 프로그래밍으로 구현해보려고 여러 생각을 도입했지만.. 이렇게 지도처럼 지역을 카테고리별로 다르게 그룹핑 하는 문제의 경우에는
         * 고려할 사항이 너무 많고, 기준이 모호해서 도저히 할 수가 없었다.
         * 아무대나 함수형 프로그래밍이 적용되는건 역시 아니다.. 연산 기준이 복잡하면 절대 그렇게 될 수가 없다.
         * 물론 overload를 사용하면 되긴 하지만은.. 그렇게 안하려고 했는데 어쩔 수 없나보다
         *
         * 또한, 매번 기준이 추기될 때마다 새롭게 BFS를 돌리거나 코드를 수정하는게 너무 비효율적이라서
         * 색이 종류가 늘어나더라도, 그룹핑 짓는 경우의 수가 달라지더라도 동일한 기능을 제공할 수 있도록 업그레이드 해보려고 시도했지만
         * 이 역시나 함수형 프로그래밍과 비슷한 이유로 안되더라..
         * 이점이 정말 아쉽다.
         *
         * Example
         * 1번 예제
         * 5
         * RRRBB
         * GGBBB
         * BBBRR
         * BBRRR
         * RRRRR
         * -------
         * answer
         * 4 3
         *
         */
        int[][] inputData = inputData();
        int resultOfNormal = getComplexCount(inputData, false);
        int resultOfBlindness = getComplexCount(inputData, true);
        System.out.println(resultOfNormal + " " + resultOfBlindness);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int length = Integer.parseInt(br.readLine());
            int[][] inputData = new int[length][length];
            char[] chars; // A:65, Z:90 -> 0 ~ 25

            for(int i = 0; i < length; i++) {
                chars = br.readLine().toCharArray();
                for(int j = 0; j < chars.length; j++) {
                    inputData[i][j] = chars[j] - 'A';
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getComplexCount(int[][] inputData, boolean colorBlindness) {
        int length = inputData.length;
        boolean[][] visited = new boolean[length][length];
        Queue<int[]> queue = new LinkedList<int[]>();

        int complexSize = 0;
        for(int i = 0; i < length; i++) {
            for(int j = 0; j < length; j++) {
                if(visited[i][j]) {
                    continue;
                } else {
                    int[] coordinate = {i, j};
                    queue.add(coordinate);
                    visited[i][j] = true;
                    exploreByBreadthFirstSearch(inputData, colorBlindness, queue, visited);
                    complexSize++;
                }
            }
        }

        return complexSize;
    }

    /**
     * BFS 메서드
     * @param inputData
     * @param colorBlindness
     * @return
     */
    private static void exploreByBreadthFirstSearch(int[][] inputData, boolean colorBlindness, Queue<int[]> queue, boolean[][] visited) {
        int length = inputData.length;
        // 좌, 상, 우, 하
        int[] movementRow = {0, -1, 0, 1};
        int[] movementCol = {-1, 0, 1, 0};

        int row, col, nextRow, nextCol;
        int[] coordinate;
        int color; // A:65, Z:90 -> 0 ~ 25

        while(!queue.isEmpty()) {
            coordinate = queue.poll();
            row = coordinate[0];
            col = coordinate[1];
            color = inputData[row][col];

            for(int i = 0; i < 4; i++) {
                nextRow = row + movementRow[i];
                nextCol = col + movementCol[i];

                if(nextRow < 0 || nextRow >= length || nextCol < 0 || nextCol >= length) {
                    continue;
                } else if(visited[nextRow][nextCol]) {
                    continue;
                }

                /*
                 * 여기가 색맹과 정상에 따라서 로직이 갈리는 분기이다.
                 * Grouping을 하기 위해서는 Boundary를 결정해줘야 한다.
                 * B:1, G:6, R:18 이지만 일반적인 수학으로 생각해보자
                 * B < G < R 순으로 크기를 비교하고, 이를 각각 x < y < z라고 하자.
                 *
                 * 1. B의 경우
                 * 수식은 항상 곱셉과 덧셈만이 항등성을 가진다. 곱셉을 해보자
                 * x^2 < yx < zx 이고 B는 어느 것 하고도 일치해서는 안된다.
                 * 그러면 B는 x^2과 같지 않은 경우 무조건 색이 다른 것이다.
                 *
                 * 2. G의 경우
                 * xy < y^2 < zy 이고, xy만 아니면 같은 단지로 인식하면 된다.
                 *
                 * 3. R의 경우
                 * xz < yz < z^2 이고, xz만 아니면 같은 단지로 인식하면 된다.
                 *
                 * G와 R은 그럼 결국 xy나 xz만 아니면 나머지 모든 경우는 같은 단지라는 말이다.
                 * x는 1이라는 성질을 이용하면 나머지는 쉽게 생각할 수 있다.
                 *
                 */
                if(!colorBlindness) {
                    if(color != inputData[nextRow][nextCol]) {
                        continue;
                    }
                } else {
                    if(color == 1) {
                        if(color * inputData[nextRow][nextCol] > 1) {
                            continue;
                        }
                    } else {
                        if(color * inputData[nextRow][nextCol] <= color) {
                            continue;
                        }
                    }
                }

                int[] nextCoordinate = {nextRow, nextCol};
                queue.add(nextCoordinate);
                visited[nextRow][nextCol] = true;
            }
        } // while end

    }

}
