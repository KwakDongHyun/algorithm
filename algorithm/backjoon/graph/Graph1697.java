package backjoon.graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph1697 {

    public static void main(String[] args) throws IOException {

        /*
         * 1. input 조건
         * 첫 번째 줄에 수빈이가 있는 위치 N과 동생이 있는 위치 K가 주어진다. N과 K는 정수이다.
         * 수빈이는 현재 점 N(0 ≤ N ≤ 100,000)에 있고, 동생은 점 K(0 ≤ K ≤ 100,000)에 있다.
         *
         * 2. output 조건
         * 수빈이가 동생을 찾는 가장 빠른 시간을 출력한다.
         *
         * 3. 해석
         * 수빈이는 걷거나 순간이동을 할 수 있다. 만약, 수빈이의 위치가 X일 때 걷는다면 1초 후에 X-1 또는 X+1로 이동하게 된다.
         * 순간이동을 하는 경우에는 1초 후에 2*X의 위치로 이동하게 된다.
         * 1초가 지날 때마다 갈 수 있는 모든 장소를 확인해보면, 최초로 접근하는 시간이 존재할 것이다.
         * 그렇기 때문에 BFS 방법이 제일 적합하다.
         * 다르게 말하면 이 해결 방법은 탐색 가능한 모든 경우의 수를 확인한다는 것과 같다.
         * ----------------------------------------
         * 1차원 데이터이긴 하지만 7576과 문제 구성은 똑같기 때문에 거의 비슷하게 Logic을 작성하면 된다.
         *
         * Example
         * 1번 예제
         * 5 17
         * -------
         * answer
         * 4
         * 5-10-9-18-17 순으로 가면 4초가 걸린다.
         *
         */
        int[] inputData = inputData();
        int shortestContactTime = getShortestContactTime(inputData);
        System.out.println(shortestContactTime);
    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int[] inputData = new int[2];
            inputData[0] = Integer.parseInt(st.nextToken());
            inputData[1] = Integer.parseInt(st.nextToken());
            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getShortestContactTime(int[] inputData ) {
        int source = inputData[0];
        int destination = inputData[1];
        int[] visit = new int[100_001]; // 기록 저장용 배열

        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(source);
        visit[source] = 1;
        int position, nextPosition;

        while(!queue.isEmpty()) {
            position = queue.poll();
            if(position == destination) {
                break;
            }

            for(int i = 0; i < 3; i++) {
                switch (i) {
                    case 0 :
                        nextPosition = position - 1;
                        break;
                    case 1 :
                        nextPosition = position + 1;
                        break;
                    default :
                        nextPosition = position * 2;
                        break;
                }

                if(nextPosition < 0 || nextPosition > 100_000 || visit[nextPosition] != 0) {
                    continue;
                }
                queue.add(nextPosition);
                visit[nextPosition] = visit[position] + 1;
            }
        }

        return visit[destination] - 1;
    }

}
