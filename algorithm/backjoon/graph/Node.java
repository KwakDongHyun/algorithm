package backjoon.graph;

import java.util.Comparator;

/**
 * 문제 풀 때는 매번 구현하기 귀찮으니까 Abstract Class 만들어두고서 상속 받아서 사용해라.
 * 백준에 제출할 때는 내부 코드에 녹여야 한다..
 */
public class Node implements Comparator<Node> {

    private int vertex;

    public Node(int vertex) {
        this.vertex = vertex;
    }

    public int getVertex() {
        return this.vertex;
    }

    @Override
    public int compare(Node o1, Node o2) {
        if(o1.getVertex() < o2.getVertex()) {
            return -1;
        } else if (o1.getVertex() == o2.getVertex()) {
            return 0;
        } else {
            return -1;
        }
    }

}
