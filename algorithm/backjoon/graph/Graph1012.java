package backjoon.graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph1012 {

    public static void main(String[] args) throws IOException {

        /*
         * 1. input 조건
         * 입력의 첫 줄에는 테스트 케이스의 개수 T가 주어진다.
         * 그 다음 줄부터 각각의 테스트 케이스에 대해 첫째 줄에는 배추를 심은 배추밭의 가로길이 M(1 ≤ M ≤ 50)과 세로길이 N(1 ≤ N ≤ 50)
         * 그리고 배추가 심어져 있는 위치의 개수 K(1 ≤ K ≤ 2500)이 주어진다.
         * 그 다음 K줄에는 배추의 위치 X(0 ≤ X ≤ M-1), Y(0 ≤ Y ≤ N-1)가 주어진다. 두 배추의 위치가 같은 경우는 없다.
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해 필요한 최소의 배추흰지렁이 마리 수를 출력한다.
         *
         * 3. 해석
         * 2667번 문제와 똑같다. BFS로 빠르게 탐색하는게 좋아보인다.
         *
         * Example
         * 1번 예제
         * 2
         * 10 8 17
         * 0 0
         * 1 0
         * 1 1
         * 4 2
         * 4 3
         * 4 5
         * 2 4
         * 3 4
         * 7 4
         * 8 4
         * 9 4
         * 7 5
         * 8 5
         * 9 5
         * 7 6
         * 8 6
         * 9 6
         * 10 10 1
         * 5 5
         * -------
         * answer
         * 5
         * 1
         *
         */
        List<int[][]> input2DimensionData = input2DimensionData();
        List<Integer> resultList = new LinkedList<Integer>();
        int complexCount;
        for(int[][] inputData : input2DimensionData) {
            complexCount = getComplexCount(inputData).size();
            resultList.add(complexCount);
        }

        for(int result : resultList) {
            System.out.println(result);
        }

    }

    public static List<int[][]> input2DimensionData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            List<int[][]> input2DimensionData = new LinkedList<int[][]>();
            StringTokenizer st;
            int[][] mapData;
            int locationCount;

            for(int i = 0; i < testCase; i++) {
                st = new StringTokenizer(br.readLine());
                mapData = new int[Integer.parseInt(st.nextToken())][Integer.parseInt(st.nextToken())];
                locationCount = Integer.parseInt(st.nextToken());
                for(int j = 0; j < locationCount; j++) {
                    st = new StringTokenizer(br.readLine());
                    mapData[Integer.parseInt(st.nextToken())][Integer.parseInt(st.nextToken())] = 1;
                }

                input2DimensionData.add(mapData);
            }
            return input2DimensionData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Integer> getComplexCount(int[][] inputData) {
        int rowSize = inputData.length;
        int columnSize = inputData[0].length;
        int[][] visit = new int[rowSize][columnSize];

        Queue<int[]> queue = new LinkedList<int[]>();
        List<Integer> resultList = new LinkedList<Integer>();
        for(int i = 0; i < rowSize; i++) {
            for(int j = 0; j < columnSize; j++) {
                switch (inputData[i][j]) {
                    case 0:
                        if(visit[i][j] == 0) {
                            visit[i][j] = -1;
                        }
                        break;

                    case 1:
                        if(visit[i][j] == 0) {
                            int[] coordinate = {i, j};
                            queue.add(coordinate);
                            visit[i][j] = 1;
                            resultList.add(exploreByBreadthFirstSearch(inputData, queue, visit));
                        }
                        break;
                }
            }
        }

        return resultList;
    }

    public static int exploreByBreadthFirstSearch(int[][] inputData, Queue<int[]> queue, int[][] visit) {
        int rowSize = inputData.length;
        int columnSize = inputData[0].length;
        // 좌, 상, 우, 하
        int[] movementX = {0, -1, 0, 1};
        int[] movementY = {-1, 0, 1, 0};

        int x, y, nextX, nextY;
        int complexCount = 1;
        while(!queue.isEmpty()) {
            int[] coordinate = queue.poll();
            x = coordinate[0];
            y = coordinate[1];

            for(int i = 0; i < 4; i++) {
                nextX = x + movementX[i];
                nextY = y + movementY[i];

                if(nextX < 0 || nextX >= rowSize || nextY < 0 || nextY >= columnSize) {
                    continue;
                }
                if(inputData[nextX][nextY] == 0 || visit[nextX][nextY] != 0) {
                    continue;
                }

                int[] nextCoordinate = {nextX, nextY};
                queue.add(nextCoordinate);
                visit[nextX][nextY] = 1;
                complexCount += 1;
            }
        }

        return complexCount;
    }

}
