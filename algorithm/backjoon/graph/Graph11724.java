package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph11724 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 정점의 개수 N과 간선의 개수 M이 주어진다. (1 ≤ N ≤ 1,000, 0 ≤ M ≤ N×(N-1)/2)
         * 둘째 줄부터 M개의 줄에 간선의 양 끝점 u와 v가 주어진다. (1 ≤ u, v ≤ N, u ≠ v)
         * 같은 간선은 한 번만 주어진다.
         *
         * 2. output 조건
         * 첫째 줄에 연결 요소의 개수를 출력한다.
         * 즉, 모든 vertex를 순회하면서 얼마나 많은 group이 존재하는지를 출력하면 된다. (분리되어 있는 group을 말함)
         *
         * 3. 해석
         *
         *
         * Example
         * 1번 예제
         * 6 5
         * 1 2
         * 2 5
         * 5 1
         * 3 4
         * 4 6
         * -------
         * answer
         * 2
         *
         */
        List<List<Integer>> graphData = getGraphData();
        int groupCount = getConnectedComponent(graphData);
        System.out.println(groupCount);

    }

    public static List<List<Integer>> getGraphData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int vertexCount = Integer.parseInt(st.nextToken());
            int edgeCount = Integer.parseInt(st.nextToken());

            List<List<Integer>> graphData = new ArrayList<List<Integer>>();
            for(int i = 0; i <= vertexCount; i++) {
                graphData.add(new LinkedList<Integer>());
            }

            int source, destination;
            for(int i = 0; i < edgeCount; i++) {
                st = new StringTokenizer(br.readLine());
                source = Integer.parseInt(st.nextToken());
                destination = Integer.parseInt(st.nextToken());

                graphData.get(source).add(destination);
                graphData.get(destination).add(source);
            }

            return graphData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getConnectedComponent(List<List<Integer>> graphData) {
        int vertexCount = graphData.size() - 1;
        int[] visit = new int[vertexCount + 1];
        Queue<Integer> queue = new LinkedList<Integer>();
//        List<Integer> connectedComponents = new LinkedList<Integer>();
        int connectedComponent = 0;

        // logic 초기값 세팅
        for(int i = 1; i <= vertexCount; i++) {
            if(visit[i] == 0) {
                queue.add(i);
                visit[i] = 1;
                exploreByBreadthFirstSearch(graphData, queue, visit);
                connectedComponent += 1;
            }
        }

        return connectedComponent;
    }

    public static void exploreByBreadthFirstSearch(List<List<Integer>> graphData, Queue<Integer> queue, int[] visit) {
        int vertex;
        while(!queue.isEmpty()) {
            vertex = queue.poll();
            List<Integer> nodeList = graphData.get(vertex);

            for(int node : nodeList) {
                if(visit[node] != 0) {
                    continue;
                }
                queue.add(node);
                visit[node] = 1;
            }
        }
    }

}
