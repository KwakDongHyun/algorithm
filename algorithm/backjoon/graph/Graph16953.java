package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph16953 {

    private static int source;
    private static int destination;
    private static boolean[] visit;
    private static int cost;

    static class Node16953 {

        private int vertex;
        private int cost;
        Node16953(int vertex, int cost) {
            this.vertex = vertex;
            this.cost = cost;
        }
    }

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        if(!visit[source]) {
            System.out.println(-1);
        } else {
            System.out.println(cost);
        }

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            source = Integer.parseInt(st.nextToken());
            destination = Integer.parseInt(st.nextToken());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        Queue<Node16953> queue = new LinkedList<>();
        queue.offer(new Node16953(destination, 1));
        visit = new boolean[destination + 1];

        while(!queue.isEmpty()) {
            Node16953 node = queue.poll();
            if(node.vertex == source) {
                cost = node.cost;
                break;
            }

            int remainder = node.vertex % 10;
            if(remainder % 2 == 1 && remainder != 1) {
                continue;
            }

            int nextVertex = 0;
            for(int i = 0; i < 2; i++) {
                switch (i) {
                    case 0:
                        if(node.vertex % 10 == 1) {
                            nextVertex = node.vertex / 10;
                        }
                        break;
                    default:
                        nextVertex = node.vertex / 2;
                        break;
                }

                if(nextVertex < source || visit[nextVertex]) {
                    continue;
                }

                int nextCost = node.cost + 1;
                visit[nextVertex] = true;
                queue.offer(new Node16953(nextVertex, nextCost));
            }
        }
    }

}
