package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

public class Graph11725 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 노드의 개수 N (2 ≤ N ≤ 100,000)이 주어진다. 둘째 줄부터 N-1개의 줄에 트리 상에서 연결된 두 정점이 주어진다.
         *
         * 2. output 조건
         * 첫째 줄부터 N-1개의 줄에 각 노드의 부모 노드 번호를 2번 노드부터 순서대로 출력한다.
         *
         * 3. 해석
         * 그래프 형태로 데이터를 구조화하고 그래프 탐색을 통해서 부모노드 정보를 기록하면 된다.
         * 루트 노드를 곧 그래프 탐색의 시작 노드라고 생각하면 된다.
         * visit을 같이 운영하면 현재 탐색 중인 노드와 연결된 자식 노드들을 검색하기 쉽다.
         *
         * Example
         * 1.
         * 7
         * 1 6
         * 6 3
         * 3 5
         * 4 1
         * 2 4
         * 4 7
         * -------
         * answer
         * 4
         * 6
         * 1
         * 3
         * 1
         * 4
         *
         */
        List<List<Integer>> graph = inputData();
        int[] parentNodes = solveProblem(graph);
        for(int i = 2; i < parentNodes.length; i++) {
            System.out.println(parentNodes[i]);
        }

    }

    public static List<List<Integer>> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int nodeCnt = Integer.parseInt(br.readLine());
            List<List<Integer>> graph = new ArrayList<List<Integer>>(nodeCnt + 1);

            for(int i = 0; i <= nodeCnt; i++) {
                graph.add(new LinkedList<Integer>());
            }

            StringTokenizer st;
            int src, dest;
            for(int i = 1; i < nodeCnt; i++) {
                st = new StringTokenizer(br.readLine());
                src = Integer.parseInt(st.nextToken());
                dest = Integer.parseInt(st.nextToken());

                graph.get(src).add(dest);
                graph.get(dest).add(src);
            }

            return graph;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int[] solveProblem(List<List<Integer>> graph) {
        boolean[] visit = new boolean[graph.size()];
        int[] parentNodes = new int[graph.size()];
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(1);
        visit[1] = true;

        int vertex;
        List<Integer> linkedList;
        while(!stack.isEmpty()) {
            vertex = stack.pop();
            linkedList = graph.get(vertex);

            for(Integer node : linkedList) {
                if(!visit[node]) {
                    stack.push(node);
                    visit[node] = true;
                    parentNodes[node] = vertex;
                }
            }
        }

        return parentNodes;
    }

}
