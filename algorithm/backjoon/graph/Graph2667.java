package backjoon.graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Graph2667 {

    public static void main(String[] args) throws IOException {

        /*
         * 1. input 조건
         * 첫 번째 줄에는 지도의 크기 N(정사각형이므로 가로와 세로의 크기는 같으며 5≤N≤25)이 입력되고
         * 그 다음 N줄에는 각각 N개의 자료(0 혹은 1)가 입력된다.
         *
         * 2. output 조건
         * 첫 번째 줄에는 총 단지수를 출력하시오. 그리고 각 단지내 집의 수를 오름차순으로 정렬하여 한 줄에 하나씩 출력하시오.
         *
         * 3. 해석
         * BFS로 빠르게 탐색하는게 좋아보인다.
         *
         * Example
         * 1번 예제
         * 7
         * 0110100
         * 0110101
         * 1110101
         * 0000111
         * 0100000
         * 0111110
         * 0111000
         * -------
         * answer
         * 3
         * 7
         * 8
         * 9
         *
         */
        int[][] input2DimensionData = input2DimensionData();
        List<Integer> complexList = getComplexList(input2DimensionData);
        complexList.sort((o1, o2) -> {
            if(o1 < o2) {
                return -1;
            } else if(o1 == o2) {
                return 0;
            } else {
                return 1;
            }
        });

        StringBuilder sb = new StringBuilder();
        sb.append(complexList.size());
        sb.append("\n");
        for(int result : complexList) {
            sb.append(result).append("\n");
        }
        System.out.println(sb);
    }

    public static int[][] input2DimensionData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int mapSize = Integer.parseInt(br.readLine());
            int[][] input2DimensionData = new int[mapSize][mapSize];
            String input;

            for(int i = 0; i < mapSize; i++) {
                input = br.readLine();
                for(int j = 0; j < input.length(); j++) {
                    input2DimensionData[i][j] = input.charAt(j) - '0';
                }
            }

            return input2DimensionData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 이번 문제부터 코테에 맞춰서 풀이 방법에 변화를 준다.
     * 좌표를 1차 배열로 만들어서 list의 element로 사용한다.
     * @param input2DimensionData
     * @return
     */
    public static List<Integer> getComplexList(int[][] input2DimensionData) {
        int mapSize = input2DimensionData.length;
        int[][] visit = new int[mapSize][mapSize];

        Queue<int[]> queue = new LinkedList<int[]>();
        List<Integer> resultList = new LinkedList<Integer>();
        for(int i = 0; i < mapSize; i++) {
            for(int j = 0; j < mapSize; j++) {
                switch (input2DimensionData[i][j]) {
                    case 0 :
                        if(visit[i][j] == 0) {
                            visit[i][j] = -1;
                        }
                        break;

                    case 1:
                        if(visit[i][j] == 0) {
                            int[] coordinate = {i, j};
                            queue.add(coordinate);
                            visit[i][j] = 1;
                            resultList.add(exploreByBreadthFirstSearch(input2DimensionData, queue, visit));
                        }
                        break;
                }
            }
        }

        return resultList;
    }

    /**
     * BFS 함수로 단지 탐색
     * @param queue
     * @param visit
     */
    public static int exploreByBreadthFirstSearch(int[][] input2DimensionData, Queue<int[]> queue, int[][] visit) {
        // 좌, 상, 우, 하
        int mapSize = visit.length;
        int[] movementX = {0, 1, 0, -1};
        int[] movementY = {-1, 0, 1, 0};

        int x, y, nextX, nextY;
        int complexCount = 1;
        while(!queue.isEmpty()) {
            int[] coordinate = queue.poll();
            x = coordinate[0];
            y = coordinate[1];

            for(int i = 0; i < 4; i++) {
                nextX = x + movementX[i];
                nextY = y + movementY[i];

                // map의 경계선을 넘거나 방문할 필요가 없으면 다음 방위를 탐색.
                if(nextX < 0 || nextX >= mapSize || nextY < 0 || nextY >= mapSize) {
                    continue;
                }
                if(input2DimensionData[nextX][nextY] == 0 || visit[nextX][nextY] != 0) {
                    continue;
                }

                int[] nextCoordinate = {nextX, nextY};
                queue.add(nextCoordinate);
                visit[nextX][nextY] = 1;
                complexCount += 1;
            }
        }

        return complexCount;
    }


}
