package backjoon.graph;

import backjoon.common.CoordinateNode;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph2178 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 두 정수 N, M(2 ≤ N, M ≤ 100)이 주어진다.
         * 다음 N개의 줄에는 M개의 정수로 미로가 주어진다.
         * 각각의 수들은 붙어서 입력으로 주어진다.
         *
         * 2. output 조건
         * 미로에서 1은 이동할 수 있는 칸을 나타내고, 0은 이동할 수 없는 칸을 나타낸다.
         * 이러한 미로가 주어졌을 때, (1, 1)에서 출발하여 (N, M)의 위치로 이동할 때 지나야 하는 최소의 칸 수를 구하는 프로그램을 작성하시오.
         * 첫째 줄에 지나야 하는 최소의 칸 수를 출력한다.
         * 항상 도착위치로 이동할 수 있는 경우만 입력으로 주어진다.
         *
         * 3. 해석
         * 최소 값을 구해야한다고 해서 모든 경로를 계산해서 하려고 했었다.
         * 그런데 다르게 생각해보자. 굳이 하나씩 경로 탐색을 할 필요가 있을까?
         * 동시에 여러 경로를 탐색하면서 값을 넣다 보면 빠른 경로 값이 먼저 저장될 것이다.
         * 그러면 자연스레 상대적으로 긴 경로는 값을 넣을 필요가 없어진다.
         * -----------------------------------------------------
         * 즉, BFS만으로도 이런 부류의 문제는 해결이 된다는 것이다.
         * 교차로를 만나더라도 BFS 탐색이라면 빠른 경로를 통해 먼저 탐색한 경우의 값이 들어갈 것이고, 먼 길을 돌아서 왔던 값은 저장할 필요도 없어질 거다.
         *
         * 4. 미해결 원인 분석
         * worst case를 생각했다면, dfs나 기존의 모든 경로 탐색이 잘못된 방법임을 알 수 있었다.
         * 모두 1로 채워져 있는 경우, 정말로 모든 경우의 수를 다 따져서 경로 탐색을 하게 될 것이므로 당연히 시간초과에 낭비다.
         * 그런데 BFS라면, 최단 경로만을 넣게 되기 때문에 (동시 탐색이라서) 항상 일정한 성능을 보장할 수 있다.
         *
         * Example
         * 1번 예제
         * 4 6
         * 101111
         * 101010
         * 101011
         * 111011
         * -------
         * answer
         * 15
         *
         * 2번 예제
         * 2 25
         * 1011101110111011101110111
         * 1110111011101110111011101
         * -------
         * answer
         * 38
         *
         * 3번 예제
         * 7 7
         * 1011111
         * 1110001
         * 1000001
         * 1000001
         * 1000001
         * 1000001
         * 1111111
         * -------
         * answer
         * 13
         *
         */
        int[][] input2DimensionData = input2DimensionData();
        int minTrackCount = getMinTrackCount(input2DimensionData);
        System.out.println(minTrackCount);

    }

    public static int[][] input2DimensionData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int row = Integer.parseInt(st.nextToken());
            int column = Integer.parseInt(st.nextToken());
            int[][] input2DimensionData = new int[row][column];

            for(int i = 0; i < row; i++) {
                char[] mazeInfo = br.readLine().toCharArray();
                for(int j = 0; j < mazeInfo.length; j++) {
                    input2DimensionData[i][j] = Integer.parseInt(Character.toString(mazeInfo[j]));
                }
            }

            return input2DimensionData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getMinTrackCount(int[][] input2DimensionData) {
        int row = input2DimensionData.length;
        int column = input2DimensionData[0].length;
        int[][][] mazeInfo = new int[row][column][2]; // 3차원으로 저장. 1번째는 visited flag, 2번째는 shortest cost.
        int[] movementX = {0, -1, 0, 1};
        int[] movementY = {-1, 0, 1, 0};

        Queue<CoordinateNode> queue = new LinkedList<CoordinateNode>();
        queue.add(new CoordinateNode(0, 0));
        mazeInfo[0][0][0] = 1;
        mazeInfo[0][0][1] = input2DimensionData[0][0];

        int x, y, nextX, nextY;
        while(!queue.isEmpty()) {
            CoordinateNode node = queue.poll();
            x = node.getCoordinateX();
            y = node.getCoordinateY();

            for(int i = 0; i < 4; i++) {
                nextX = x + movementX[i];
                nextY = y + movementY[i];

                if(nextX < 0 || nextX >= row || nextY < 0 || nextY >= column) {
                    continue;
                }
                if(input2DimensionData[nextX][nextY] != 1 || mazeInfo[nextX][nextY][0] == 1) {
                    continue;
                }

                queue.add(new CoordinateNode(nextX, nextY));
                mazeInfo[nextX][nextY][0] = 1;
                mazeInfo[nextX][nextY][1] = mazeInfo[x][y][1] + input2DimensionData[nextX][nextY];
            }
        }

        return mazeInfo[row-1][column-1][1];
    }

}
