package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph7576 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫 줄에는 상자의 크기를 나타내는 두 정수 M,N이 주어진다.
         * M은 상자의 가로 칸의 수, N은 상자의 세로 칸의 수를 나타낸다. 단, 2 ≤ M,N ≤ 1,000 이다.
         * 둘째 줄부터는 하나의 상자에 저장된 토마토들의 정보가 주어진다.
         * 즉, 둘째 줄부터 N개의 줄에는 상자에 담긴 토마토의 정보가 주어진다.
         * 하나의 줄에는 상자 가로줄에 들어있는 토마토의 상태가 M개의 정수로 주어진다.
         * 정수 1은 익은 토마토, 정수 0은 익지 않은 토마토, 정수 -1은 토마토가 들어있지 않은 칸을 나타낸다.
         * 토마토가 하나 이상 있는 경우만 입력으로 주어진다.
         *
         * 2. output 조건
         * 여러분은 토마토가 모두 익을 때까지의 최소 날짜를 출력해야 한다.
         * 만약, 저장될 때부터 모든 토마토가 익어있는 상태이면 0을 출력해야 하고, 토마토가 모두 익지는 못하는 상황이면 -1을 출력해야 한다.
         *
         * 3. 해석
         * BFS로 빠르게 탐색하는게 좋아보인다.
         *
         * Example
         * 1번 예제
         * 6 4
         * 0 0 0 0 0 0
         * 0 0 0 0 0 0
         * 0 0 0 0 0 0
         * 0 0 0 0 0 1
         * -------
         * answer
         * 8
         *
         */
        int[][] input2DimensionData = input2DimensionData();
        int periodOfCompleteRipening = getPeriodOfCompleteRipening(input2DimensionData);
        System.out.println(periodOfCompleteRipening);

    }

    public static int[][] input2DimensionData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int columnSize = Integer.parseInt(st.nextToken());
            int rowSize = Integer.parseInt(st.nextToken());
            int[][] input2DimensionData = new int[rowSize][columnSize];

            int length;
            for(int i = 0; i < rowSize; i++) {
                st = new StringTokenizer(br.readLine());
                length = st.countTokens();
                for(int j = 0; j < length; j++) {
                    input2DimensionData[i][j] = Integer.parseInt(st.nextToken());
                }
            }

            return input2DimensionData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getPeriodOfCompleteRipening(int[][] input2DimensionData) {
        int rowSize = input2DimensionData.length;
        int columnSize = input2DimensionData[0].length;
        int[][] visit = new int[rowSize][columnSize];
        // 좌, 상, 우, 하
        int[] movementX = {0, -1, 0, 1};
        int[] movementY = {-1, 0, 1, 0};

        // logic을 위한 초기화. 여기서부터 while까지가 BFS Core Logic.
        Queue<int[]> queue = new LinkedList<int[]>();
        for(int row = 0; row < rowSize; row++) {
            for(int col = 0; col < columnSize; col++) {
                if(input2DimensionData[row][col] == 1) {
                    int[] coordinate = {row, col};
                    queue.add(coordinate);
                    visit[row][col] = 1;
                }
            }
        }

        /*
         * 고민을 좀 많이 했던 부분.
         * next 좌표의 visit을 +1씩 늘려가는 방식이 편하다고 생각.
         * 몇 일차인지 정보를 나타내는 것이고, 기간을 표현하고 싶으면 -1만큼 해주면 된다.
         */
        int x, y, nextX, nextY;
        int periodOfRipening = 1;
        int[] coordinate;
        while(!queue.isEmpty()) {
            coordinate = queue.poll();
            x = coordinate[0];
            y = coordinate[1];

            for(int i = 0; i < 4; i++) {
                nextX = x + movementX[i];
                nextY = y + movementY[i];

                if(nextX < 0 || nextX >= rowSize || nextY < 0 || nextY >= columnSize) {
                    continue;
                }
                if(input2DimensionData[nextX][nextY] != 0 || visit[nextX][nextY] != 0) {
                    continue;
                }

                int[] nextCoordinate = {nextX, nextY};
                queue.add(nextCoordinate);
                visit[nextX][nextY] = visit[x][y] + 1;

                if(periodOfRipening < visit[nextX][nextY]) {
                    periodOfRipening = visit[nextX][nextY];
                }
            }
        }

        // 답 출력 부분. 방문할 수 없는 위치가 하나라도 있으면 -1, 모두 방문했으면 숙성일 -1 return.
        for(int row = 0; row < rowSize; row++) {
            for(int col = 0; col < columnSize; col++) {
                if(input2DimensionData[row][col] != -1 && visit[row][col] == 0) {
                    return -1;
                }
            }
        }

        return periodOfRipening-1;
    }

}
