package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph4963 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력은 여러 개의 테스트 케이스로 이루어져 있다.
         * 각 테스트 케이스의 첫째 줄에는 지도의 너비 w와 높이 h가 주어진다. w와 h는 50보다 작거나 같은 양의 정수이다.
         * 둘째 줄부터 h개 줄에는 지도가 주어진다. 1은 땅, 0은 바다이다.
         * 입력의 마지막 줄에는 0이 두 개 주어진다.
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해서, 섬의 개수를 출력한다.
         *
         * 3. 해석
         * 2667번 문제의 연장 선상에 있는 문제다.
         * 대각선을 고려하는 것이 추가되었다.
         *
         * 1 1
         * 0
         * 2 2
         * 0 1
         * 1 0
         * 3 2
         * 1 1 1
         * 1 1 1
         * 5 4
         * 1 0 1 0 0
         * 1 0 0 0 0
         * 1 0 1 0 1
         * 1 0 0 1 0
         * 5 4
         * 1 1 1 0 1
         * 1 0 1 0 1
         * 1 0 1 0 1
         * 1 0 1 1 1
         * 5 5
         * 1 0 1 0 1
         * 0 0 0 0 0
         * 1 0 1 0 1
         * 0 0 0 0 0
         * 1 0 1 0 1
         * 0 0
         */
        List<int[][]> inputData = inputData();
        int[] results = countNumberOfIsland(inputData);
        for(int result : results) {
            System.out.println(result);
        }

    }

    public static List<int[][]> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            boolean continuation = true;
            StringTokenizer st;
            int width, height;
            List<int[][]> inputData = new LinkedList<int[][]>();

            while(continuation) {
                st = new StringTokenizer(br.readLine());
                width = Integer.parseInt(st.nextToken());
                height = Integer.parseInt(st.nextToken());

                if(width == 0 && height == 0) {
                    continuation = false;
                    continue;
                }

                int[][] mapData = new int[height][width];
                for(int i = 0; i < height; i++) {
                    st = new StringTokenizer(br.readLine());
                    for(int j = 0; j < width; j++) {
                        mapData[i][j] = Integer.parseInt(st.nextToken());
                    }
                }

                inputData.add(mapData);
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int[] countNumberOfIsland(List<int[][]> inputData) {
        int[] results = new int[inputData.size()];
        int index = 0;
        int height, width;
        int[][] visit;
        Queue<int[]> queue = new LinkedList<int[]>();
        int numberOfIsland;

        for(int[][] data : inputData) {
            height = data.length;
            width = data[0].length;
            visit = new int[height][width];
            numberOfIsland = 0;

            for(int i = 0; i < height; i++) {
                for(int j = 0; j < width; j++) {
                    if(data[i][j] == 1 && visit[i][j] == 0) {
                        int[] coordinate = {i, j};
                        queue.add(coordinate);
                        visit[i][j] = 1;
                        exploreByBreadthFirstSearch(data, visit, queue);
                        numberOfIsland++;
                    }
                }
            }

            results[index++] = numberOfIsland;
        }

        return results;
    }

    private static void exploreByBreadthFirstSearch(int[][] inputData, int[][] visit, Queue<int[]> queue) {
        int height = inputData.length;
        int width = inputData[0].length;
        int row, col, nextRow, nextCol;
        int[] coordinate;
        // 좌, 상, 우, 하, 좌상, 우상, 우하, 좌하
        int[] moveRow = {0, -1, 0, 1, -1, -1, 1, 1};
        int[] moveCol = {-1, 0, 1, 0, -1, 1, 1, -1};

        while(!queue.isEmpty()) {
            coordinate = queue.poll();
            row = coordinate[0];
            col = coordinate[1];

            for(int i = 0; i < moveRow.length; i++) {
                nextRow = row + moveRow[i];
                nextCol = col + moveCol[i];

                if(nextRow < 0 || nextRow >= height || nextCol < 0 || nextCol >= width) {
                    continue;
                }
                if(inputData[nextRow][nextCol] == 0 || visit[nextRow][nextCol] == 1) {
                    continue;
                }

                int[] nextCoordinate = {nextRow, nextCol};
                queue.add(nextCoordinate);
                visit[nextRow][nextCol] = 1;
            }
        }

    }

}
