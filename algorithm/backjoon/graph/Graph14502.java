package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph14502 {

    public static int[][] input2DimensionData;
    public static int maxSafeZone = 0;

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 지도의 세로 크기 N과 가로 크기 M이 주어진다. (3 ≤ N, M ≤ 8)
         * 둘째 줄부터 N개의 줄에 지도의 모양이 주어진다. 0은 빈 칸, 1은 벽, 2는 바이러스가 있는 위치이다.
         * 2의 개수는 2보다 크거나 같고, 10보다 작거나 같은 자연수이다. (2 ≤ 초기 바이러스 수(2) ≤ 10)
         * 빈 칸의 개수는 3개 이상이다.
         *
         * 2. output 조건
         * 얻을 수 있는 안전 영역의 최대 크기를 출력한다.
         * 새로 세울 수 있는 벽의 개수는 3개이며, 벽 3개를 세워서 만들 수 있는 가장 큰 안전 영역을 만들면 된다.
         *
         * 3. 해석
         * 어려운 문제다. 기존의 문제들처럼 단순 방문 및 탐색이 아니다.
         * 격자 무늬에서 최대 공간을 확보하기 가장 좋은 방법은 대각선(diagonal)을 이용하는 것이다.
         * ----------------------------------------------------------
         * 아무래도 이런 문제는 어떤 규칙성을 찾기가 매우 어렵다.
         * 풀이 방법을 아무리 생각해봐도 찾을 수가 없었다.
         * 그러므로 이런 문제는 브루트 포스로 풀 수 밖에 없다.
         *
         * 4. 배워야할 점
         * 재귀가 항상 성능이 나쁜건 아니다.
         * 물론 재귀를 쓰지 않고서 DFS나 백트래킹을 구현할 수는 있겠지만, 많은 변수가 필요하고 고려해야할 사항이 너무 많다.
         * 태생적인 한계를 넘기 위해서 다른 알고리즘 방법을 생각하는게 낫다고 본다.
         * 기존의 것을 재구성하는 것만으로는 한계가 있었다.
         *
         *
         * Example
         * 1번 예제
         * 7 7
         * 2 0 0 0 1 1 0
         * 0 0 1 0 1 2 0
         * 0 1 1 0 1 0 0
         * 0 1 0 0 0 0 0
         * 0 0 0 0 0 1 1
         * 0 1 0 0 0 0 0
         * 0 1 0 0 0 0 0
         * -------
         * answer
         * 27
         *
         */
        input2DimensionData = input2DimensionData();
        /*
         * DFS를 통해서 벽을 세울 수 있는 모든 경우의 수를 추출함.
         * 벽 3개를 세우면 BFS를 진행.
         */
        exploreByDepthFirstSearch(0);
        System.out.println(maxSafeZone);

    }

    public static int[][] input2DimensionData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int rowSize = Integer.parseInt(st.nextToken());
            int colSize = Integer.parseInt(st.nextToken());
            int[][] input2DimensionData = new int[rowSize][colSize];

            for(int i = 0; i < rowSize; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < colSize; j++) {
                    input2DimensionData[i][j] = Integer.parseInt(st.nextToken());
                }
            }
            return input2DimensionData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 벽을 3개 세우는 역할. 재귀 아닌 방식으로 짜면서 일반화로 고도화시켜보려 했지만.. 지금으로선 방법을 모르겠다.
     * 애시당초 DFS와 백트래킹을 재귀가 아닌 방식으로 짜는거 자체가 넌센스가 아닐까 싶다.
     * 원리가 다른 알고리즘을 사용해야지, 태생적 한계인 애를 다른 방식으로 구현하기는 어려울 것 같다.
     * @param wall
     */
    public static void exploreByDepthFirstSearch(int wall) {
        int rowSize = input2DimensionData.length;
        int colSize = input2DimensionData[0].length;

        if(wall == 3) {
            exploreByBreadthFirstSearch();
            return;
        }

        for(int i = 0; i < rowSize; i++) {
            for(int j = 0; j < colSize; j++) {
                if(input2DimensionData[i][j] == 0) {
                    input2DimensionData[i][j] = 1;
                    exploreByDepthFirstSearch(wall + 1);
                    input2DimensionData[i][j] = 0;
                }
            }
        }
    }

    public static void exploreByBreadthFirstSearch() {
        int rowSize = input2DimensionData.length;
        int colSize = input2DimensionData[0].length;

        // 원본 배열을 clone. 데이터 보존을 위해서 copy해서 사용함.
        int[][] visit = Arrays.stream(input2DimensionData)
                .map(int[]::clone)
                .toArray(int[][]::new);
        Queue<int[]> queue = new LinkedList<int[]>();
        // 좌, 상, 우, 하
        int[] movementX = {0, -1, 0, 1};
        int[] movementY = {-1, 0, 1, 0};

        // queue 초기 시작점 insert
        for(int i = 0; i < rowSize; i++) {
            for(int j = 0; j < colSize; j++) {
                if(visit[i][j] == 2) {
                    int[] coordinate = {i, j};
                    queue.add(coordinate);
                }
            }
        }

        // BFS core logic
        int x, y, nextX, nextY;
        int[] coordinate;
        while(!queue.isEmpty()) {
            coordinate = queue.poll();
            x = coordinate[0];
            y = coordinate[1];

            for(int i = 0; i < 4; i++) {
                nextX = x + movementX[i];
                nextY = y + movementY[i];

                if(nextX < 0 || nextX >= rowSize || nextY < 0 || nextY >= colSize) {
                    continue;
                }
                if(visit[nextX][nextY] != 0) {
                    continue;
                }

                int[] nextCoordinate = {nextX, nextY};
                queue.add(nextCoordinate);
                visit[nextX][nextY] = 2;
            }
        }

        // 안전 공간 숫자 세기
        int safeZone = 0;
        for(int i = 0; i < rowSize; i++) {
            for(int j = 0; j < colSize; j++) {
                if(visit[i][j] == 0) {
                    safeZone += 1;
                }
            }
        }

        if(safeZone > maxSafeZone) {
            maxSafeZone = safeZone;
        }
    }

}
