package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph2644 {

    private static int vertices;
    private static int[] problem = new int[2];
    private static int edge;
    private static List<List<Integer>> graphInfo = new ArrayList<>();
    private static int[] costArr;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        System.out.println(costArr[problem[1]] == 0 ? -1 : costArr[problem[1]]);

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            vertices = Integer.parseInt(br.readLine());
            for(int i = 0; i <= vertices; i++) {
                graphInfo.add(new LinkedList<>());
            }

            StringTokenizer st = new StringTokenizer(br.readLine());
            problem[0] = Integer.parseInt(st.nextToken());
            problem[1] = Integer.parseInt(st.nextToken());
            edge = Integer.parseInt(br.readLine());

            for(int i = 0; i < edge; i++) {
                st = new StringTokenizer(br.readLine());
                int source = Integer.parseInt(st.nextToken());
                int destination = Integer.parseInt(st.nextToken());
                graphInfo.get(source).add(destination);
                graphInfo.get(destination).add(source);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        int source = problem[0];
        costArr = new int[vertices + 1];
        boolean[] visit = new boolean[vertices + 1];
        visit[source] = true;

        Queue<Integer> queue = new LinkedList<>();
        queue.add(source);

        while(!queue.isEmpty()) {
            int vertex = queue.poll();
            List<Integer> adjacentVertices = graphInfo.get(vertex);

            for(int adjacentVertex : adjacentVertices) {
                if(!visit[adjacentVertex]) {
                    costArr[adjacentVertex] = costArr[vertex] + 1;
                    queue.add(adjacentVertex);
                    visit[adjacentVertex] = true;
                }
            }
        }
    }

}
