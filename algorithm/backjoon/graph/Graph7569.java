package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph7569 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫 줄에는 상자의 크기를 나타내는 두 정수 M,N과 쌓아올려지는 상자의 수를 나타내는 H가 주어진다.
         * M은 상자의 가로 칸의 수, N은 상자의 세로 칸의 수를 나타낸다. 단, 2 ≤ M ≤ 100, 2 ≤ N ≤ 100, 1 ≤ H ≤ 100 이다.
         * 둘째 줄부터는 가장 밑의 상자부터 가장 위의 상자까지에 저장된 토마토들의 정보가 주어진다.
         * 즉, 둘째 줄부터 N개의 줄에는 하나의 상자에 담긴 토마토의 정보가 주어진다.
         * 각 줄에는 상자 가로줄에 들어있는 토마토들의 상태가 M개의 정수로 주어진다.
         * 정수 1은 익은 토마토, 정수 0 은 익지 않은 토마토, 정수 -1은 토마토가 들어있지 않은 칸을 나타낸다.
         * 이러한 N개의 줄이 H번 반복하여 주어진다. 토마토가 하나 이상 있는 경우만 입력으로 주어진다.
         *
         * 2. output 조건
         * 여러분은 토마토가 모두 익을 때까지 최소 며칠이 걸리는지를 계산해서 출력해야 한다.
         * 만약, 저장될 때부터 모든 토마토가 익어있는 상태이면 0을 출력해야 하고, 토마토가 모두 익지는 못하는 상황이면 -1을 출력해야 한다.
         *
         * 3. 해석
         * 7576번 문제에서 확장된 버전이다.
         * 차원이 높아진 경우에 대해서 동일한 해답을 구하는 것이니 잘 생각하길 바란다.
         *
         * Example
         * 1번 예제
         * 5 3 1
         * 0 -1 0 0 0
         * -1 -1 0 1 1
         * 0 0 0 1 1
         * -------
         * answer
         * -1
         *
         * 2번 예제
         * 5 3 2
         * 0 0 0 0 0
         * 0 0 0 0 0
         * 0 0 0 0 0
         * 0 0 0 0 0
         * 0 0 1 0 0
         * 0 0 0 0 0
         * -------
         * answer
         * 4
         *
         */
        int[][][] input3DData = input3DData();
        int periodOfCompleteRipening = getPeriodOfCompleteRipening(input3DData);
        System.out.println(periodOfCompleteRipening);

    }

    public static int[][][] input3DData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int col = Integer.parseInt(st.nextToken()); // i
            int row = Integer.parseInt(st.nextToken()); // j
            int hgt = Integer.parseInt(st.nextToken()); // k

            int[][][] input3DData = new int[hgt][row][col];

            // hgt -> row -> col 순으로
            for(int k = 0; k < hgt; k++) {
                for(int j = 0; j < row; j++) {
                    st = new StringTokenizer(br.readLine());
                    for(int i = 0; i < col; i++) {
                        input3DData[k][j][i] = Integer.parseInt(st.nextToken());
                    }
                }
            }

            return input3DData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getPeriodOfCompleteRipening(int[][][] input3DData) {
        int col = input3DData[0][0].length; // i, x
        int row = input3DData[0].length; // j, y
        int hgt = input3DData.length; // k, z
        Queue<int[]> queue = new LinkedList<int[]>();
        int[][][] visited = input3DData.clone();

        /*
         * 초기에 익은 토마토 좌표값 queue에 add
         * 초기에 완성되어 있는지 여부는 이때 검사.
         */
        int completeFlag = 1;
        int sum = 0;
        for(int k = 0; k < hgt; k++) {
            for(int j = 0; j < row; j++) {
                for(int i = 0; i < col; i++) {
                    if(input3DData[k][j][i] == 1) {
                        int[] coordinate = {k, j, i};
                        queue.add(coordinate);
                    }

                    completeFlag *= input3DData[k][j][i];
                    sum += input3DData[k][j][i];
                }
            }
        }

        // 모두 익어있는 경우 or 모두 -1로 채워져있는 경우
        if(completeFlag != 0) {
            if(sum == col * row * hgt * -1) {
                return -1;
            }
            return 0;
        }

        /*
         * main logic
         */
        int z, y, x, nextZ, nextY, nextX, day = 0;
        int[] coordinate;

        // 위, 아래, 좌, 상, 우, 하
        int[] moveCol = {0, 0, -1, 0, 1, 0};
        int[] moveRow = {0, 0, 0, -1, 0, 1};
        int[] moveHgt = {1, -1, 0, 0, 0, 0};

        while(!queue.isEmpty()) {
            coordinate = queue.poll();
            z = coordinate[0];
            y = coordinate[1];
            x = coordinate[2];

            for(int i = 0; i < 6; i++) {
                nextZ = z + moveHgt[i];
                nextY = y + moveRow[i];
                nextX = x + moveCol[i];

                if(nextZ < 0 || nextZ >= hgt || nextY < 0 || nextY >= row || nextX < 0 || nextX >= col) {
                    continue;
                }
                if(input3DData[nextZ][nextY][nextX] != 0 || visited[nextZ][nextY][nextX] > 0) {
                    continue;
                }

                int[] nextCoordinate = {nextZ, nextY, nextX};
                queue.add(nextCoordinate);
                visited[nextZ][nextY][nextX] = visited[z][y][x] + 1;

                if(day < visited[nextZ][nextY][nextX]) {
                    day = visited[nextZ][nextY][nextX];
                }
            }

        }

        /*
         * BFS 끝난 후에도 0이 있으면 전부 익지 못하는 case이다.
         */
        for(int k = 0; k < hgt; k++) {
            for(int j = 0; j < row; j++) {
                for(int i = 0; i < col; i++) {
                    if(visited[k][j][i] == 0) {
                        return -1;
                    }
                }
            }
        }

        return day-1;
    }

}
