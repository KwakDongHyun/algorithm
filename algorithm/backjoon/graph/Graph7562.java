package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph7562 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력의 첫째 줄에는 테스트 케이스의 개수가 주어진다.
         * 각 테스트 케이스는 세 줄로 이루어져 있다. 첫째 줄에는 체스판의 한 변의 길이 l(4 ≤ l ≤ 300)이 주어진다.
         * 체스판의 크기는 l × l이다. 체스판의 각 칸은 두 수의 쌍 {0, ..., l-1} × {0, ..., l-1}로 나타낼 수 있다.
         * 둘째 줄과 셋째 줄에는 나이트가 현재 있는 칸, 나이트가 이동하려고 하는 칸이 주어진다.
         *
         * 2. output 조건
         * 각 테스트 케이스마다 나이트가 최소 몇 번만에 이동할 수 있는지 출력한다.
         *
         * 3. 해석
         * BFS로 완전 탐색을 해서 최초로 도착했을 때 이동 횟수를 출력하면 된다.
         *
         * Example
         * 1.
         * 3
         * 8
         * 0 0
         * 7 0
         * 100
         * 0 0
         * 30 50
         * 10
         * 1 1
         * 1 1
         * -------
         * answer
         * 5
         * 28
         * 0
         *
         */
        List<int[]> inputData = inputData();
        int[] results = solveProblem(inputData);
        for(int result : results) {
            System.out.println(result);
        }

    }

    public static List<int[]> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            List<int[]> inputData = new ArrayList<int[]>();
            StringTokenizer st;

            for(int i = 0; i < testCase; i++) {
                int mapSize = Integer.parseInt(br.readLine());
                int[] data = new int[5];
                data[0] = mapSize;

                st = new StringTokenizer(br.readLine());
                data[1] = Integer.parseInt(st.nextToken());
                data[2] = Integer.parseInt(st.nextToken());

                st = new StringTokenizer(br.readLine());
                data[3] = Integer.parseInt(st.nextToken());
                data[4] = Integer.parseInt(st.nextToken());

                inputData.add(data);
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int[] solveProblem(List<int[]> inputData) {
        int[] results = new int[inputData.size()];
        int index = 0;
        for(int[] data : inputData) {
            results[index++] = exploreByBreadthFirstSearch(data);
        }

        return results;
    }

    private static int exploreByBreadthFirstSearch(int[] data) {
        int[] moveX = {-2, -2, -1, 1, 2, 2, -1, 1};
        int[] moveY = {-1, 1, -2, -2, -1, 1, 2, 2};

        int mapSize = data[0];
        int[][] visit = new int[mapSize][mapSize];
        Queue<int[]> queue = new LinkedList<int[]>();

        int destY = data[3], destX = data[4];
        int[] coordinate = {data[1], data[2]};
        queue.add(coordinate);
        visit[data[1]][data[2]] = 1;

        int x, y, nextX, nextY;
        int turnCnt = 0;

        while(!queue.isEmpty()) {
            coordinate = queue.poll();
            y = coordinate[0];
            x = coordinate[1];

            if(destY == y && destX == x) {
                turnCnt = visit[y][x] - 1;
                break;
            }

            for(int i = 0; i < 8; i++) {
                nextX = x + moveX[i];
                nextY = y + moveY[i];

                if(nextX < 0 || nextX >= mapSize || nextY < 0 || nextY >= mapSize) {
                    continue;
                }
                if(visit[nextY][nextX] > 0) {
                    continue;
                }

                int[] nextCoordinate = {nextY, nextX};
                queue.add(nextCoordinate);
                visit[nextY][nextX] = visit[y][x] + 1;
            }

        }

        return turnCnt;
    }

}
