package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

public class Graph16236 {

    private static int n;
    private static int[][] mapInfo;
    private static int[][] distanceArr;
    private static Coordinate16236 initialCoordinate;
    private static int[] movementX = {1, 0, -1, 0};
    private static int[] movementY = {0, 1, 0, -1};
    private static int sharkSize = 2;
    private static int count = 0;
    private static int time = 0;

    static class Coordinate16236 {
        private int y;
        private int x;

        Coordinate16236(int y, int x) {
            this.y = y;
            this.x = x;
        }

        int getCoordinateX() {
            return this.x;
        }

        int getCoordinateY() {
            return this.y;
        }

    }

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        System.out.println(time);

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            n = Integer.parseInt(br.readLine());
            mapInfo = new int[n][n];

            StringTokenizer st;
            for(int i = 0; i < n; i++) {
                st = new StringTokenizer(br.readLine());
                for(int j = 0; j < n; j++) {
                    mapInfo[i][j] = Integer.parseInt(st.nextToken());
                    if(mapInfo[i][j] == 9) {
                        initialCoordinate = new Coordinate16236(i, j);
                    }
                }
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        Coordinate16236 coordinate = initialCoordinate;
        // 상어가 위치한 곳을 0으로 초기화
        mapInfo[coordinate.y][coordinate.x] = 0;

        while(true) {
            Coordinate16236 targetFish = searchFish(coordinate);
            int x = targetFish.x;
            int y = targetFish.y;

            // 로직 종료
            if(y == -1 && x == -1) {
                break;
            }

            count++;
            if(sharkSize == count) {
                sharkSize++;
                count = 0;
            }
            time += distanceArr[y][x];
            mapInfo[y][x] = 0;
            coordinate = targetFish;
        }

    }

    private static Coordinate16236 searchFish(Coordinate16236 startCoordinate) {
        distanceArr = new int[n][n];
        Queue<Coordinate16236> queue = new LinkedList<>();
        queue.add(startCoordinate);

        boolean isFound = false;
        int minimumDistance = 0;
        Coordinate16236 result = new Coordinate16236(-1, -1);

        int x, y, nextX, nextY;
        while(!queue.isEmpty()) {
            Coordinate16236 coordinate = queue.poll();
            x = coordinate.getCoordinateX();
            y = coordinate.getCoordinateY();

            for(int i = 0; i < 4; i++) {
                nextX = x + movementX[i];
                nextY = y + movementY[i];

                if(nextX < 0 || nextX >= n || nextY < 0 || nextY >= n) {
                    continue;
                }

                if(sharkSize < mapInfo[nextY][nextX] || distanceArr[nextY][nextX] == -1 || distanceArr[nextY][nextX] > 0) {
                    continue;
                }

                distanceArr[nextY][nextX] = distanceArr[y][x] + 1;

                if(mapInfo[nextY][nextX] > 0 && sharkSize > mapInfo[nextY][nextX]) {
                    // 최초로 발견시에만 동작
                    if(!isFound) {
                        isFound = true;
                        minimumDistance = distanceArr[nextY][nextX];
                        result.y = nextY;
                        result.x = nextX;
                        continue;
                    }

                    if(distanceArr[nextY][nextX] == minimumDistance) {
                        if(nextY < result.getCoordinateY()) {
                            result.y = nextY;
                            result.x = nextX;
                        } else if(nextY == result.getCoordinateY()) {
                            if(nextX < result.getCoordinateX()) {
                                result.x = nextX;
                            }
                        }
                    }
                }

                queue.offer(new Coordinate16236(nextY, nextX));
            }
        }

        return result;
    }

}
