package backjoon.graph;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.StringTokenizer;


public class Graph1260 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 정점의 개수 N(1 ≤ N ≤ 1,000), 간선의 개수 M(1 ≤ M ≤ 10,000), 탐색을 시작할 정점의 번호 V가 주어진다.
         * 다음 M개의 줄에는 간선이 연결하는 두 정점의 번호가 주어진다. 어떤 두 정점 사이에 여러 개의 간선이 있을 수 있다.
         * 입력으로 주어지는 간선은 양방향이다.
         *
         * 2. output 조건
         * 첫째 줄에 DFS를 수행한 결과를, 그 다음 줄에는 BFS를 수행한 결과를 출력한다.
         * V부터 방문된 점을 순서대로 출력하면 된다.
         *
         * 3. 해석
         * 그래프 탐색의 기본 알고리즘인 BFS와 DFS를 구현하는 거다.
         * 각각 Stack과 Queue를 사용한다는 건 알고 있지만, library의 도움 없이 만드는게 좋지 않을까 싶다.
         *
         * Example
         * 1번 예제
         * 4 5 1
         * 1 2
         * 1 3
         * 1 4
         * 2 4
         * 3 4
         * -------
         * answer
         * 1 2 4 3
         * 1 2 3 4
         *
         * 2번 예제
         * 5 5 3
         * 5 4
         * 5 2
         * 1 2
         * 3 4
         * 3 1
         * -------
         * answer
         * 3 1 2 5 4
         * 3 1 4 2 5
         *
         * 3번 예제
         * 1000 1 1000
         * 999 1000
         * -------
         * answer
         * 1000 999
         * 1000 999
         *
         */
        int[][] inputData = inputData();
        List<List<Node>> graphData = getGraphData(inputData);
        int startVertex = inputData[0][1];

        List<Node> resultOfDepthFirstSearch = getDepthFirstSearch(graphData, startVertex);
        List<Node> resultOfBreadthFirstSearch = getBreadthFirstSearch(graphData, startVertex);

        for(Node node : resultOfDepthFirstSearch) {
            System.out.print(node.getVertex() + " ");
        }
        System.out.println();
        for(Node node : resultOfBreadthFirstSearch) {
            System.out.print(node.getVertex() + " ");
        }

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int vertexCount = Integer.parseInt(st.nextToken());
            int edgeCount = Integer.parseInt(st.nextToken());
            int startVertex = Integer.parseInt(st.nextToken());

            // edge 개수는 length-1 이면 되므로 굳이 안 넣는다.
            int[][] inputData = new int[edgeCount + 1][2];
            inputData[0][0] = vertexCount;
            inputData[0][1] = startVertex;

            for(int i = 1; i <= edgeCount; i++) {
                st = new StringTokenizer(br.readLine());
                inputData[i][0] = Integer.parseInt(st.nextToken());
                inputData[i][1] = Integer.parseInt(st.nextToken());
            }

            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<List<Node>> getGraphData(int[][] inputData) {
        int vertexCount = inputData[0][0];
        int edgeCount = inputData.length - 1;

        // 생성 및 초기화.
        List<List<Node>> graphData =  new ArrayList<List<Node>>(vertexCount + 1);
        for(int i = 0; i <= vertexCount; i++) {
            graphData.add(new LinkedList<Node>());
        }

        Node node;
        int sourceVertex, destinationVertex;
        for(int i = 1; i <= edgeCount; i++) {
            sourceVertex = inputData[i][0];
            destinationVertex = inputData[i][1];

            node = new Node(destinationVertex);
            graphData.get(sourceVertex).add(node);

            node = new Node(sourceVertex);
            graphData.get(destinationVertex).add(node);
        }

        return graphData;
    }

    public static List<Node> getDepthFirstSearch(List<List<Node>> graphData, int startVertex) {
        Stack<Node> stack = new Stack<Node>();
        stack.push(new Node(startVertex));
        int[] visited = new int[graphData.size()];
        List<Node> visitedList = new LinkedList<Node>();

        // 내림차순 정렬
        for(List<Node> nodeList : graphData) {
            if(!nodeList.isEmpty()) {
                nodeList.sort((o1, o2) -> {
                    if(o1.getVertex() < o2.getVertex()) {
                        return 1;
                    } else if (o1.getVertex() == o2.getVertex()) {
                        return 0;
                    } else {
                        return -1;
                    }
                });
            }
        }

        Node stackNode;
        int vertexIndex;
        while(!stack.isEmpty()) {
            stackNode = stack.pop();
            vertexIndex = stackNode.getVertex();
            if(visited[vertexIndex] == 0) {
                visited[vertexIndex] = 1;
                visitedList.add(stackNode);
            }

            List<Node> nodeList = graphData.get(vertexIndex);
            /*
            for(int i = nodeList.size()-1; i > -1; i--) {
                if(visited[nodeList.get(i).getVertex()] == 0) {
                    stack.push(nodeList.get(i));
                }
            }
            */

            for(Node node : nodeList) {
                if(visited[node.getVertex()] == 0) {
                    stack.push(node);
                }
            }
        }

        return visitedList;
    }

    public static List<Node> getBreadthFirstSearch(List<List<Node>> graphData, int startVertex) {
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(new Node(startVertex));
        int[] visited = new int[graphData.size()];
        List<Node> visitedList = new LinkedList<Node>();

        // 처음 생성할 때 정렬해주는게 낫다.
        for(List<Node> nodeList : graphData) {
            if(!nodeList.isEmpty()) {
                nodeList.sort((o1, o2) -> {
                    if(o1.getVertex() < o2.getVertex()) {
                        return -1;
                    } else if (o1.getVertex() == o2.getVertex()) {
                        return 0;
                    } else {
                        return 1;
                    }
                });
            }
        }

        Node queueNode;
        int vertexIndex;
        while(!queue.isEmpty()) {
            queueNode = queue.poll();
            vertexIndex = queueNode.getVertex();
            if(visited[vertexIndex] == 0) {
                visited[vertexIndex] = 1;
                visitedList.add(queueNode);
            }

            List<Node> nodeList = graphData.get(vertexIndex);
            for(Node node : nodeList) {
                if(visited[node.getVertex()] == 0) {
                    queue.add(node);
                }
            }
        }

        return visitedList;
    }

}
