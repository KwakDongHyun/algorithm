package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Deque;
import java.util.LinkedList;

public class String12904 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 S가 둘째 줄에 T가 주어진다. (1 ≤ S의 길이 ≤ 999, 2 ≤ T의 길이 ≤ 1000, S의 길이 < T의 길이)
         *
         * 2. output 조건
         * S를 T로 바꿀 수 있으면 1을 없으면 0을 출력한다.
         * 문자열을 바꿀 때는 다음과 같은 두 가지 연산만 가능하다.
         *  1. 문자열의 뒤에 A를 추가한다.
         *  2. 문자열을 뒤집고 뒤에 B를 추가한다.
         *
         * 3. 주의사항
         * 앞뒤로 문자를 자유롭게 삭제해야 하므로 deque를 사용한다.
         *
         * 4. 해석
         * 풀이법에 대해서 갈피를 못잡겠네.. 그냥 경우의 수로 막 만들어야하나?
         * 만약에 수행조건대로 연산을 그냥 하게되면 고려해야할 것이 너무 많다.
         * 계속해서 문자가 추가가 되면서 경우의 수가 급격히 늘어나게 된다.
         * 수 많은 경우 중에 답에 맞는 것 1개를 찾는 작업인데 당연히 브루트포스 밖에 풀이가 없다.
         * 특히 2번 연산을 수행할 경우, 앞의 문자까지 변동되기 때문에 곧이곧대로 풀이법을 진행할 경우 매번 문자열을 비교해야하는데
         * 이 작업만으로도 O(n^3)이 걸린다.
         *
         * 연산은 문자열 길이가 같을 때까지 진행한다는 것에 초점을 맞추면 반대로 역산은 어떤가?
         * 역산을 실행할 경우 아래와 같다.
         *  1. 문자열 뒤에 A를 삭제한다.
         *  2. 문자열 뒤에 B를 삭제하고, 문자열을 뒤집는다.
         *
         * 맨 뒤에 있는 문자에 따라서 역산을 진행하면서 문자열 길이가 S와 같을 때까지 진행하면 되기 때문에 훨씬 쉽다.
         * 연산마다 생성되는 다른 변수는 존재하지 않고, 오로지 삭제 자체에만 신경쓰면 되고 그마저도 맨 뒤의 문자만 고려하면 되기 때문이다.
         * 경우의 수라는 변수만 하더라도 문자열 길이가 늘어날수록 기억해야할 데이터가 급격히 많아지지만, 삭제는 뒤로 갈수록 문자열 길이가 줄기 때문에 경우의 수가
         * 급격히 적어져서 고려해야할 것이 상대적으로 적어진다. 따라서 역산으로 문제를 풀어가자.
         * (직감만으로 S에서 T로 가는것보다는 거꾸로 가는게 훨씬 쉽다고 생각했는데.. 빠르게 직감에서 한 발 더 나아가서 용기를 내면 좋았겠다.)
         *
         *
         * Example
         * 1.
         * B
         * ABBA
         * -------
         * answer
         * 1
         *
         * 2.
         * AB
         * ABB
         * --------
         * answer
         * 0
         *
         */

        String[] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] inputData = new String[2];
            inputData[0] = br.readLine();
            inputData[1] = br.readLine();

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String[] inputData) {
        String src = inputData[0];
        String dest = inputData[1];
        int srcLen = src.length();

        char[] chars = dest.toCharArray();
        Deque<Character> deque = new LinkedList<Character>();
        for(char c : chars) {
            deque.offer(c);
        }

        boolean reverse = false;
        char c;
        while(deque.size() > srcLen) {
            if(!reverse) {
                c = deque.peekLast();
            } else {
                c = deque.peekFirst();
            }

            switch (c) {
                case 'A' :
                    if(!reverse) {
                        deque.pollLast();
                    } else {
                        deque.pollFirst();
                    }
                    break;

                case 'B' :
                    if(!reverse) {
                        deque.pollLast();
                        reverse = true;
                    } else {
                        deque.pollFirst();
                        reverse = false;
                    }
            }
        }

        StringBuilder sb = new StringBuilder();
        while(!deque.isEmpty()) {
            if(!reverse) {
                sb.append(deque.pollFirst());
            } else {
                sb.append(deque.pollLast());
            }
        }

        return src.equals(sb.toString()) ? 1 : 0;
    }

}
