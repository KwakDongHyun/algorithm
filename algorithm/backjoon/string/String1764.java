package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class String1764 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 듣도 못한 사람의 수 N, 보도 못한 사람의 수 M이 주어진다.
         * 이어서 둘째 줄부터 N개의 줄에 걸쳐 듣도 못한 사람의 이름과, N+2째 줄부터 보도 못한 사람의 이름이 순서대로 주어진다.
         * 이름은 띄어쓰기 없이 알파벳 소문자로만 이루어지며, 그 길이는 20 이하이다. N, M은 500,000 이하의 자연수이다.
         * 듣도 못한 사람의 명단에는 중복되는 이름이 없으며, 보도 못한 사람의 명단도 마찬가지이다.
         *
         * 2. output 조건
         * 듣보잡의 수와 그 명단을 사전순으로 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 3 4
         * ohhenrie
         * charlie
         * baesangwook
         * obama
         * baesangwook
         * ohhenrie
         * clinton
         * -------
         * answer
         * 2
         * baesangwook
         * ohhenrie
         *
         */

        Map<String, String[]> inputDataMap = inputData();
        List<String> result = solveProblem(inputDataMap);
        System.out.println(result.size());
        for(String s : result) {
            System.out.println(s);
        }

    }

    public static Map<String, String[]> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int n = Integer.parseInt(st.nextToken());
            int m = Integer.parseInt(st.nextToken());

            String[] listen = new String[n];
            String[] seen = new String[m];

            for(int i = 0; i < n; i++) {
                listen[i] = br.readLine();
            }

            for(int i = 0; i < m; i++) {
                seen[i] = br.readLine();
            }

            Map<String, String[]> inputDataMap = new HashMap<String, String[]>();
            inputDataMap.put("listen", listen);
            inputDataMap.put("seen", seen);

            return inputDataMap;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<String> solveProblem(Map<String, String[]> inputDataMap) {
        String[] listen = inputDataMap.get("listen");
        String[] seen = inputDataMap.get("seen");
        // Map을 사용하면 value를 사용하지도 않는데 더미데이터를 넣어야만 하기 때문에 비효율적이다.
        // 따라서 순차적으로 검색하는 list가 아닌 hash 함수를 통해 빠르게 검색할 수 있는 Set을 사용한다.
        Set<String> set = new HashSet<String>();

        for(String s : listen) {
            set.add(s);
        }

        List<String> results = new LinkedList<String>();
        for(String s : seen) {
            if(set.contains(s)) {
                results.add(s);
            }
        }

        Collections.sort(results);
        return results;
    }

}
