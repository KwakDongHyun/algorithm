package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class String5430_important {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 테스트 케이스의 개수 T가 주어진다. T는 최대 100이다.
         * 각 테스트 케이스의 첫째 줄에는 수행할 함수 p가 주어진다. p의 길이는 1보다 크거나 같고, 100,000보다 작거나 같다.
         * 다음 줄에는 배열에 들어있는 수의 개수 n이 주어진다. (0 ≤ n ≤ 100,000)
         * 다음 줄에는 [x1,...,xn]과 같은 형태로 배열에 들어있는 정수가 주어진다. (1 ≤ xi ≤ 100)
         * 전체 테스트 케이스에 주어지는 p의 길이의 합과 n의 합은 70만을 넘지 않는다.
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해서, 입력으로 주어진 정수 배열에 함수를 수행한 결과를 출력한다. 만약, 에러가 발생한 경우에는 error를 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         * 어떻게 보면 자료구조를 만드는 것과 같은 느낌이라고 생각한다. (뭐, 배열을 다루니까...)
         * ------------------------------------------------------------------------
         * 카테고리 분류에서 Deque을 보고 나서 여러 생각이 들었다.
         * 어차피 해당 데이터를 유지해서 다른 곳에 쓸 것도 아니고, 알고리즘 문제 풀이 시간을 줄인다는 생각을 하면 굳이 모든 것을
         * 다 구현할 필요가 있었을까? 라는 생각이 들었다.
         * 설사 이런 이유가 아니더라도 자료구조를 최대한 활용하면 편리한 상황이 더 많지 않을까?
         * 각 데이터마다 order flag 값만 가지고 있으면 deque에서 호출하는 method만 다르게 사용하면 될 뿐이라 훨씬 간략하고 편하다.
         *
         * 순수하게 수학적 + 공학적으로 문제를 "푼다"라는 것만 생각해봤지, 이런식으로 생각하려는 시도는 해보지를 않았다...
         * 앞으로는 공학적으로 구현할 때도 한 번 쯤은 '어떤 자료구조를 사용하면 더 효과적일까?' 라는 생각을 하는 습관을 들이자..
         * 정말 중요한 문제라고 생각한다.
         *
         * Example
         * 1.
         * 4
         * RDD
         * 4
         * [1,2,3,4]
         * DD
         * 1
         * [42]
         * RRD
         * 6
         * [1,1,2,3,5,8]
         * D
         * 0
         * []
         * -------
         * answer
         * [2,1]
         * error
         * [1,2,3,5,8]
         * error
         *
         */

        Map<String, Object> inputDataMap = inputData();
        List<String> results = solveProblem(inputDataMap);
        for(String result : results) {
            System.out.println(result);
        }

    }

    public static Map<String, Object> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            String[] functions = new String[testCase];
            List<Deque<Integer>> arrData = new ArrayList<Deque<Integer>>(testCase);

            String regexp = "\\d+"; // 숫자 1개 이상
            Pattern pattern = Pattern.compile(regexp);
            Matcher matcher;

            for(int i = 0; i < testCase; i++) {
                String command = br.readLine();
                int arrLength = Integer.parseInt(br.readLine());
                String arrStr = br.readLine();

                Deque<Integer> deque = new LinkedList<Integer>();
                matcher = pattern.matcher(arrStr);
                while(matcher.find()) {
                    deque.offerLast(Integer.parseInt(matcher.group()));
                }

                functions[i] = command;
                arrData.add(deque);
            }

            Map<String, Object> inputDataMap = new HashMap<String, Object>();
            inputDataMap.put("testCase", testCase);
            inputDataMap.put("functions", functions);
            inputDataMap.put("arrData", arrData);

            return inputDataMap;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<String> solveProblem(Map<String, Object> inputDataMap) {
        List<String> results = new LinkedList<String>();

        int testCase = (Integer) inputDataMap.get("testCase");
        String[] functions = (String[]) inputDataMap.get("functions");
        List<Deque<Integer>> arrData = (List<Deque<Integer>>) inputDataMap.get("arrData");

        String function;
        Deque<Integer> deque;
        char[] commands;
        boolean reverseFlag; // deque function을 수행할 때 기준이 되는 값
        boolean stop, doNext;

        for(int i = 0; i < testCase; i++) {
            function = functions[i];
            deque = arrData.get(i);
            commands = function.toCharArray();
            reverseFlag = false;
            stop = false;
            doNext = false;

            for(char command : commands) {
                switch (command) {
                    case 'R' :
                        if(reverseFlag) {
                            reverseFlag = false;
                        } else {
                            reverseFlag = true;
                        }
                        break;

                    case 'D' :
                        if(deque.isEmpty()) {
                            stop = true;
                            doNext = true;
                        } else {
                            if(!reverseFlag) {
                                deque.removeFirst();
                            } else {
                                deque.removeLast();
                            }
                        }
                        break;
                }

                if(stop) {
                    results.add("error");
                    break;
                }
            }

            if(!doNext) {
                StringBuilder sb = new StringBuilder("[");

                if(reverseFlag) {
                    while(!deque.isEmpty()) {
                        sb.append(deque.pollLast());
                        sb.append(",");
                    }

                    if(sb.length() > 1) {
                        sb.deleteCharAt(sb.length() - 1);
                    }

                    sb.append("]");
                } else {
                    while(!deque.isEmpty()) {
                        sb.append(deque.pollFirst());
                        sb.append(",");
                    }

                    if(sb.length() > 1) {
                        sb.deleteCharAt(sb.length() - 1);
                    }

                    sb.append("]");
                }

                results.add(sb.toString());
            }
        }

        return results;
    }

}
