package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String11720 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 숫자의 개수 N (1 ≤ N ≤ 100)이 주어진다. 둘째 줄에 숫자 N개가 공백없이 주어진다.
         *
         * 2. output 조건
         * 입력으로 주어진 숫자 N개의 합을 출력한다.
         *
         */
        String[] inputData = inputData();
        int sum = sumOfData(inputData);
        System.out.println(sum);

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] inputData = new String[2];
            inputData[0] = br.readLine();
            inputData[1] = br.readLine();
            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int sumOfData(String[] inputData) {
        char[] data = inputData[1].toCharArray();
        int sum = 0;
        for(char c : data) {
            sum += c;
        }
        return sum - ('0' * data.length);
    }

}
