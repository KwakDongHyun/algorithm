package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String17609 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력의 첫 줄에는 주어지는 문자열의 개수를 나타내는 정수 T(1 ≤ T ≤ 30)가 주어진다.
         * 다음 줄부터 T개의 줄에 걸쳐 한 줄에 하나의 문자열이 입력으로 주어지며 영문 알파벳 소문자로만 이루어져 있다. (3 ≤ 길이 ≤ 100,000)
         *
         * 2. output 조건
         * 각 문자열이 회문인지, 유사 회문인지, 둘 모두 해당되지 않는지를 판단하여
         * 회문이면 0, 유사 회문이면 1, 둘 모두 아니면 2를 순서대로 한 줄에 하나씩 출력한다.
         * 그 자체는 회문이 아니지만 한 문자를 삭제하여 회문으로 만들 수 있는 문자열이라면 “유사회문”(pseudo palindrome)이라고 부른다.
         *
         * 3. 주의사항
         * 전에 풀었던 문제와는 다르다. (DP 10942)
         * 거기서는 주어진 문자열 내에서 palindrome이 몇 개씩 있을 수 있는 문제이고, 이를 풀기 위한 전용 알고리즘까지 존재한다.
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 7
         * abba
         * summuus
         * xabba
         * xabbay
         * comcom
         * comwwmoc
         * comwwtmoc
         * -------
         * answer
         * 0
         * 1
         * 1
         * 2
         * 2
         * 0
         * 1
         *
         * 2.
         * 1
         * xyyyyxy
         * -------
         * answer
         * 1
         */

        String[] inputData = inputData();
        int[] results = solveProblem(inputData);

        StringBuilder sb = new StringBuilder();
        for(int result : results) {
            sb.append(result);
            sb.append("\n");
        }
        System.out.println(sb.toString());

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            String[] inputData = new String[testCase];

            for(int i = 0; i < testCase; i++) {
                inputData[i] = br.readLine();
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int[] solveProblem(String[] inputData) {
        int testCase = inputData.length;
        int[] results = new int[testCase];

        String data;
        int leftIdx, rightIdx;
        int removeCount;
        char[] chars;

        // 두 가지 가능성 탐색을 위해서 회귀 지점을 저장함.
        int tempLeftIdx, tempRightIdx;
        int result;

        for(int i = 0; i < testCase; i++) {
            data = inputData[i];
            chars = data.toCharArray();
            leftIdx = 0;
            rightIdx = data.length() - 1;
            removeCount = 0;

            tempLeftIdx = -1;
            tempRightIdx = -1;

            while(leftIdx <= rightIdx) {

                if(chars[leftIdx] != chars[rightIdx]) {
                    removeCount++;

                    if(removeCount >= 2) {
                        /*
                         * (leftIdx + 1, rightIdx)을 선택한 결과 회문이 아니었다.
                         * 다른 경우를 선택한 결과에 대해서 탐색을 해보고 최종 결론을 내린다.
                         */
                        if(tempLeftIdx != -1 && tempRightIdx != -1) {
//                            System.out.println("another possiblity check");
//                            System.out.println("tempLeftIdx : " + tempLeftIdx + ", tempRightIdx : " + tempRightIdx);
                            leftIdx = tempLeftIdx;
                            rightIdx = tempRightIdx;
                            tempLeftIdx = -1;
                            tempRightIdx = -1;
                            removeCount--;
                            continue;
                        }

                        break;
                    }

                    /*
                     * (leftIdx + 1, rightIdx), (leftIdx, rightIdx - 1)을 모두 만족하지만
                     * (leftIdx + 1, rightIdx)이 아니라 (leftIdx, rightIdx - 1) 선택했을 시 유사회문인 경우가 있다.
                     * 이 경우에는 두 가지 가능성을 모두 염두해두고서 탐색해야지만 알 수 있다.
                     */
                    if(chars[leftIdx + 1] == chars[rightIdx]) {
                        tempLeftIdx = leftIdx;
                        tempRightIdx = rightIdx - 1;
                        leftIdx = leftIdx + 1;
                    } else if(chars[leftIdx] == chars[rightIdx - 1]) {
                        rightIdx = rightIdx - 1;
                    } else {
                        removeCount++;
                        break;
                    }
                }

                leftIdx++;
                rightIdx--;
            }

            switch (removeCount) {
                case 0 :
                    results[i] = 0;
                    break;

                case 1:
                    results[i] = 1;
                    break;

                default:
                    results[i] = 2;
                    break;
            }
        }

        return results;
    }

}
