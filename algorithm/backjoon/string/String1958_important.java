package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String1958_important {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫 줄에는 첫 번째 문자열이, 둘째 줄에는 두 번째 문자열이, 셋째 줄에는 세 번째 문자열이 주어진다.
         * 각 문자열은 알파벳 소문자로 이루어져 있고, 길이는 100보다 작거나 같다.
         *
         * 2. output 조건
         * 첫 줄에 첫 번째 문자열과 두 번째 문자열과 세 번째 문자열의 LCS의 길이를 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * abcdefghijklmn
         * bdefg
         * efg
         * -------
         * answer
         * 3
         *
         */

        String[] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] inputData = new String[3];

            for(int i = 0; i < 3; i++) {
                inputData[i] = br.readLine();
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String[] inputData) {
        String str1st = inputData[0];
        String str2nd = inputData[1];
        String str3rd = inputData[2];
        int str1stLen = str1st.length();
        int str2ndLen = str2nd.length();
        int str3rdLen = str3rd.length();
        char[] str1stChars = str1st.toCharArray();
        char[] str2ndChars = str2nd.toCharArray();
        char[] str3rdChars = str3rd.toCharArray();

        int[][][] dp = new int[str1stLen + 1][str2ndLen + 1][str3rdLen + 1];

        for(int i = 1; i <= str1stLen; i++) {
            for(int j = 1; j <= str2ndLen; j++) {
                for(int k = 1; k <= str3rdLen; k++) {
                    if(str1stChars[i-1] == str2ndChars[j-1] && str2ndChars[j-1] == str3rdChars[k-1]) {
                        dp[i][j][k] = dp[i-1][j-1][k-1] + 1;
                    } else {
                        dp[i][j][k] = Math.max(Math.max(dp[i-1][j][k], dp[i][j-1][k]), dp[i][j][k-1]);
                    }
                }
            }
        }

        return dp[str1stLen][str2ndLen][str3rdLen];
    }

}
