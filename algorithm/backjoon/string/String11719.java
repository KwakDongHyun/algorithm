package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String11719 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력이 주어진다. 입력은 최대 100줄로 이루어져 있고, 알파벳 소문자, 대문자, 공백, 숫자로만 이루어져 있다.
         * 각 줄은 100글자를 넘지 않으며, 빈 줄이 주어질 수도 있고, 각 줄의 앞 뒤에 공백이 있을 수도 있다.
         *
         * 2. output 조건
         * 입력받은 그대로 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         *     Hello
         *
         * Baekjoon
         *    Online Judge
         * -------
         * answer
         *     Hello
         *
         * Baekjoon
         *    Online Judge
         *
         */

        String inputData = inputData();
        System.out.println(inputData);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringBuilder sb = new StringBuilder();
            String inputData = "";
            boolean isEnd = false;

            while(!isEnd) {
                inputData = br.readLine();
                if(inputData == null) {
                    isEnd = true;
                    continue;
                }

                sb.append(inputData);
                sb.append("\n");
            }

            return sb.toString();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
