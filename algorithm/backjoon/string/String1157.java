package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class String1157 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 알파벳 대소문자로 이루어진 단어가 주어진다. 주어지는 단어의 길이는 1,000,000을 넘지 않는다.
         *
         * 2. output 조건
         * 첫째 줄에 이 단어에서 가장 많이 사용된 알파벳을 대문자로 출력한다. 단, 가장 많이 사용된 알파벳이 여러 개 존재하는 경우에는 ?를 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * Mississipi
         * -------
         * answer
         * ?
         *
         * 2.
         * zZa
         * --------
         * answer
         * Z
         *
         */

        String inputData = inputData();
        String result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String solveProblem(String inputData) {
        char[] chars = inputData.toCharArray();
        int[] alphaArr = new int[26];

        /*
         * A65 ~ Z90 : 26개 알파벳 대문자
         * a97 ~ z122 : 26개 알파벳 소문자
         */
        for(char c : chars) {
            if(65 <= c && c <= 90) {
                alphaArr[c - 65] += 1;
            }

            if(97 <= c && c <= 122) {
                alphaArr[c - 97] += 1;
            }
        }

        int maxCount = Arrays.stream(alphaArr).max().getAsInt();
        List<Character> maxAlpha = new LinkedList<Character>();
        for(int i = 0; i < 26; i++) {
            if(maxCount == alphaArr[i]) {
                maxAlpha.add((char) (i + 65));
            }
        }

        return maxAlpha.size() == 1 ? maxAlpha.get(0).toString() : "?";
    }

}
