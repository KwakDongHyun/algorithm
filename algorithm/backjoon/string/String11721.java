package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

public class String11721 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 단어가 주어진다. 단어는 알파벳 소문자와 대문자로만 이루어져 있으며, 길이는 100을 넘지 않는다.
         * 길이가 0인 단어는 주어지지 않는다.
         *
         * 2. output 조건
         * 입력으로 주어진 단어를 열 개씩 끊어서 한 줄에 하나씩 출력한다.
         * 단어의 길이가 10의 배수가 아닌 경우에는 마지막 줄에는 10개 미만의 글자만 출력할 수도 있다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * BaekjoonOnlineJudge
         * -------
         * answer
         * BaekjoonOn
         * lineJudge
         *
         * 2.
         * OneTwoThreeFourFiveSixSevenEightNineTen
         * --------
         * answer
         * OneTwoThre
         * eFourFiveS
         * ixSevenEig
         * htNineTen
         *
         */

        String inputData = inputData();
        List<String> results = solveProblem(inputData);
        for(String result : results) {
            System.out.println(result);
        }

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<String> solveProblem(String inputData) {
        List<String> results = new LinkedList<String>();

        // 나눗셈의 몫과 나머지
        int length = inputData.length();
        int quotient = length / 10;
        int remainder = length % 10;
        int startIdx, endIdx;

        for(int i = 1; i <= quotient; i++) {
            startIdx = (i-1) * 10;
            endIdx = i * 10;
            results.add(inputData.substring(startIdx, endIdx));
        }

        if(remainder != 0) {
            results.add(inputData.substring(quotient * 10));
        }

        return results;
    }

}
