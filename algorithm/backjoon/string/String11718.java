package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String11718 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 입력이 주어진다. 입력은 최대 100줄로 이루어져 있고, 알파벳 소문자, 대문자, 공백, 숫자로만 이루어져 있다.
         * 각 줄은 100글자를 넘지 않으며, 빈 줄은 주어지지 않는다. 또, 각 줄은 공백으로 시작하지 않고, 공백으로 끝나지 않는다.
         *
         * 2. output 조건
         * 입력받은 그대로 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * Hello
         * Baekjoon
         * Online Judge
         * -------
         * answer
         * Hello
         * Baekjoon
         * Online Judge
         *
         */

        inputData();


    }

    public static void inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {

            /*
             * 이거 문제가 좀 이상하네.. 시작부터 NPE를 반환하는건 뭔 경우지?
             * ----------------------------------------------------
             * 공백이나 null 입력했을 때, trim에서 에러가 나서 그런 것 같다.
             * null인데 trim을 할 수는 없으니까
             */
            StringBuilder sb = new StringBuilder();
            String inputData = "";
            boolean isBlank = false;

            while(!isBlank) {
                inputData = br.readLine();
                if(inputData == null || inputData.isEmpty()) {
                    isBlank = true;
                    continue;
                }

                sb.append(inputData);
                sb.append("\n");
            }

            System.out.println(sb);


//            String inputData;
//            while((inputData = br.readLine()) != null) {
//                System.out.println(inputData);
//            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
