package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class String1427 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 정렬하려고 하는 수 N이 주어진다. N은 1,000,000,000보다 작거나 같은 자연수이다.
         *
         * 2. output 조건
         * 첫째 줄에 자리수를 내림차순으로 정렬한 수를 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 2143
         * -------
         * answer
         * 4321
         *
         * 2.
         * 61423
         * --------
         * answer
         * 64321
         *
         */

        String inputData = inputData();
        String result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String solveProblem(String inputData) {
        char[] chars = inputData.toCharArray();
        Arrays.sort(chars);

        StringBuilder sb = new StringBuilder();
        for(int i = chars.length - 1; i > -1; i--) {
            sb.append(chars[i]);
        }

        return sb.toString();
    }

}
