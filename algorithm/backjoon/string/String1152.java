package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String1152 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫 줄에 영어 대소문자와 공백으로 이루어진 문자열이 주어진다. 이 문자열의 길이는 1,000,000을 넘지 않는다.
         * 단어는 공백 한 개로 구분되며, 공백이 연속해서 나오는 경우는 없다. 또한 문자열은 공백으로 시작하거나 끝날 수 있다.
         *
         * 2. output 조건
         * 첫째 줄에 단어의 개수를 출력한다.
         *
         * 3. 해석
         * 특별히 여기선 없다.
         *
         * Example
         * 1번 예제
         *
         * -------
         * answer
         *
         *
         */
        String inputData = inputData();
        int numberOfWord = countWord(inputData);
        System.out.println(numberOfWord);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int countWord(String inputData) {
        inputData = inputData.trim().replaceAll("\\s+", " ");
        /*
         * 공백만 입력할 시에는 replaceAll 메소드로는 처리가 안된다.
         * 공백을 제거하더라도 비어있는 문자열로 인식하기 때문이다. 즉, 아무것도 입력하지 않은 경우를 처리할 수가 없다.
         */
        if(inputData.equals("")) {
            return 0;
        } else {
            return inputData.split(" ").length;
        }
    }

}
