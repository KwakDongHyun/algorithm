package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class String1316 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 단어의 개수 N이 들어온다. N은 100보다 작거나 같은 자연수이다.
         * 둘째 줄부터 N개의 줄에 단어가 들어온다. 단어는 알파벳 소문자로만 되어있고 중복되지 않으며, 길이는 최대 100이다.
         *
         * 2. output 조건
         * 그룹 단어의 개수를 출력한다.
         * 그룹 단어란 단어에 존재하는 모든 문자에 대해서, 각 문자가 연속해서 나타나는 경우만을 말한다.
         * 예를 들면, ccazzzzbb는 c, a, z, b가 모두 연속해서 나타나고, kin도 k, i, n이 연속해서 나타나기 때문에 그룹 단어이지만,
         * aabbbccb는 b가 떨어져서 나타나기 때문에 그룹 단어가 아니다.
         *
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 4
         * aba
         * abab
         * abcabc
         * a
         * -------
         * answer
         * 1
         *
         * 2.
         * 5
         * ab
         * aa
         * aca
         * ba
         * bb
         * --------
         * answer
         * 4
         *
         */

        String[] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            String[] inputData = new String[testCase];

            for(int i = 0; i < testCase; i++) {
                inputData[i] = br.readLine();
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String[] inputData) {
        int testCase = inputData.length;
        int result = 0;

        /*
         * 영문자들에 대한 ASCII 코드값
         * A65 ~ Z90 : 26개 알파벳 대문자
         * a97 ~ z122 : 26개 알파벳 소문자
         */
        String data;
        char[] chars;
        boolean[] alphaArr = new boolean[26];
        int prevCharVal, curCharVal;
        boolean isGroupWord;

        for(int i = 0; i < testCase; i++) {
            data = inputData[i];
            chars = data.toCharArray();
            Arrays.fill(alphaArr, false);
            isGroupWord = true;

            for(int j = 0; j < chars.length; j++) {
                curCharVal = chars[j] - 97;

                // 첫 문자는 예외처리로 데이터 저장만 진행
                if(j == 0) {
                    alphaArr[curCharVal] = true;
                    continue;
                }

                prevCharVal = chars[j - 1] - 97;
                // 이전 문자와 다르면서 이미 기존에 등장했던 문자라면 그룹 단어가 아니다
                if(curCharVal != prevCharVal && alphaArr[curCharVal] == true) {
                    isGroupWord = false;
                    break;
                }

                alphaArr[curCharVal] = true;
            }

            if(isGroupWord) {
                result++;
            }
        }

        return result;
    }

}
