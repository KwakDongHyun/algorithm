package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class String10798 {

    private static List<String> data = new LinkedList<>();
    private static int[] lengthArr = new int[5];
    private static int maxLength;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String s;
            for(int i = 0; i < 5; i++) {
                s = br.readLine();
                data.add(s);
                lengthArr[i] = s.length();
            }

            maxLength = Arrays.stream(lengthArr).max().getAsInt();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < maxLength; i++) {
            for(int j = 0; j < 5; j++) {
                if(i < lengthArr[j])
                    sb.append(data.get(j).charAt(i));
            }
        }

        System.out.println(sb);
    }

}
