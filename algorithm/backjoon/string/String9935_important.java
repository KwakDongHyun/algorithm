package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Stack;

public class String9935_important {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 문자열이 주어진다. 문자열의 길이는 1보다 크거나 같고, 1,000,000보다 작거나 같다.
         * 둘째 줄에 폭발 문자열이 주어진다. 길이는 1보다 크거나 같고, 36보다 작거나 같다.
         * 두 문자열은 모두 알파벳 소문자와 대문자, 숫자 0, 1, ..., 9로만 이루어져 있다.
         *
         * 2. output 조건
         * 모든 폭발이 끝난 후 남은 문자열을 출력한다.
         *
         * 폭발 문자열이 폭발하면 그 문자는 문자열에서 사라지며 남은 문자열은 합쳐지게 된다.
         * 폭발은 다음과 같은 과정으로 진행된다.
         *  1. 문자열이 폭발 문자열을 포함하고 있는 경우에, 모든 폭발 문자열이 폭발하게 된다. 남은 문자열을 순서대로 이어 붙여 새로운 문자열을 만든다.
         *  2. 새로 생긴 문자열에 폭발 문자열이 포함되어 있을 수도 있다.
         *  3. 폭발은 폭발 문자열이 문자열에 없을 때까지 계속된다.
         *
         * 남아있는 문자가 없는 경우, 이때는 "FRULA"를 출력한다.
         * 폭발 문자열은 같은 문자를 두 개 이상 포함하지 않는다.
         *
         * 3. 주의사항
         * 규칙만 듣고보면 고전게임인 뿌요뿌요, 테트리스와 비슷한 규칙이라는 것을 알 수 있다.
         * 아니.. 같은 계통의 알고리즘인가? 특정 조건을 만족하면 해당 데이터는 사라지고, 연쇄적인 반응이 일어날 수 있다는 것까지 동일하다.
         *
         * 이런 문제는 단순하게 아무 생각없이 풀면 타임오버가 나올 수 밖에 없을거라 생각한다.
         * 데이터가 많아질수록 매번 단순하게 일일히 언제 다 비교를 하고 있을 것인가?
         * 어떻게든 효과적으로 데이터를 처리할 수 있는 풀이 방법을 생각해야 한다.
         *
         * 4. 해석
         * 풀이법을 분석해보니 이전에 풀었던 괄호 문제의 연장선상에 있는 문제였다. (4949번 문제를 참고해라.)
         * 매칭하는게 더는 없을 때까지 사람이 생각하는 일반적인 풀이법은 재귀방식이라서 수행이 매우 오래걸린다.
         * Bottom-up 방식으로 알고리즘을 작성하려면 선형으로 탐색하며 문제를 해결해야한다.
         *
         * 풀이법은 아래와 같다.
         *  1. 입력받은 문자열을 순차적으로 탐색하면서 폭발 문자열의 마지막 문자를 찾을 때까지는 stack에 넣는다.
         *  2. 마지막 문자를 발견했을 경우, 폭발 문자열의 길이만큼 stack에서 문자들을 꺼내서 문자열로 조합한다.
         *  3. 폭발 문자열일 경우 그대로 다음 문자부터 탐색을 재시작하고, 폭발 문자열이 아닐 경우 stack에 다시 넣어준다.
         *     일치하지 않은 문자들은 출력시켜야 하기 때문이다.
         *
         * 괄호 문제와 메커니즘은 동일하다. 순차적으로 문자를 탐색하다가 특정 문자를 발견하면 검사하는 로직을 실행시키고,
         * 결과에 따라서 나머지 작업을 진행한다.
         * 마치 대수학과 비슷한 개념같지 않은가? 선형대수학에서 행렬의 원소에는 상수부터 함수 혹은 특정 식(expression)까지 무엇이든
         * 들어갈 수 있다는 것.
         * 임의의 폭발 문자열 X는 특정 부분 문자열 a와 b의 조합으로 이루어진다고 가정하고,
         * 문자열 a x a a b b y z b 라고 있을시 부분 문자열 a와 b를 찾을 때까지 순차적으로 탐색하고, b를 발견할시 이전 문자열과
         * 조합하여 x와 같은지 검사하는 것.
         *
         * a와 b는 괄호같은 상수가 아닌 추상적인 구문(expression)이지만, 원리는 같다.
         * 그리고 구문에 대한 상세 풀이는, 결국 모든 부분 문자열을 조합해야하므로 끝 문자를 찾을때까지 탐색하면 된다는 결론에 도달한다.
         * 이 문제를 통해서 stack이 굉장히 강력한 자료구조라는걸 새삼 느꼈다.
         *
         * Example
         * 1.
         * mirkovC4nizCC44
         * C4
         * -------
         * answer
         * mirkovniz
         *
         * 2.
         * 12ab112ab2ab
         * 12ab
         * --------
         * answer
         * FRULA
         *
         */

        String[] inputData = inputData();
        String result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] inputData = new String[2];
            inputData[0] = br.readLine();
            inputData[1] = br.readLine();

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String solveProblem(String[] inputData) {
        Stack<Character> stack = new Stack<Character>();
        String str = inputData[0];
        String matchStr = inputData[1];

        /*
         * 매칭해야할 문자열의 마지막 문자 인덱스
         */
        char[] chars = str.toCharArray();
        char[] matchStrChars = matchStr.toCharArray();
        int lastIdx = matchStr.length() - 1;
        int temp;
        char lastMatchStr = matchStrChars[lastIdx];

        for(char c : chars) {
            if(c != lastMatchStr) {
                stack.push(c);
            } else {
                temp = lastIdx - 1;
                StringBuilder sb = new StringBuilder();
                sb.append(c);

                while(!stack.isEmpty()) {
                    if(temp < 0) {
                        break;
                    }

                    sb.append(stack.pop());
                    temp--;
                }

                String combinedStr = sb.reverse().toString();
//                System.out.println("combinedStr : " + combinedStr);

                if(matchStr.equals(combinedStr)) {
                    continue;
                } else {
                    char[] combindedStrChars = combinedStr.toCharArray();
                    for(char returnChar : combindedStrChars) {
                        stack.push(returnChar);
                    }
                }

            }
        }

        if(stack.isEmpty()) {
            return "FRULA";
        } else {
            StringBuilder sb = new StringBuilder();
            while(!stack.isEmpty()) {
                sb.append(stack.pop());
            }

            return sb.reverse().toString();
        }

    }

}
