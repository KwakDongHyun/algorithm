package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String27866 {

    private static String input;
    private static int index;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            input = br.readLine();
            index = Integer.parseInt(br.readLine());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        System.out.println(input.charAt(index - 1));
    }

}
