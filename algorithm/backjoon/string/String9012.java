package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Stack;

public class String9012 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 괄호의 모양이 바르게 구성된 문자열을 올바른 괄호 문자열(Valid PS, VPS)이라고 부른다.
         * “(x)”, “(())()”, “((()))” 는 VPS이지만 “(()(”, “(())()))” , 그리고 “(()” 는 모두 VPS가 아닌 문자열이다.
         * 입력은 T개의 테스트 데이터로 주어진다. 입력의 첫 번째 줄에는 입력 데이터의 수를 나타내는 정수 T가 주어진다.
         * 각 테스트 데이터의 첫째 줄에는 괄호 문자열이 한 줄에 주어진다. 하나의 괄호 문자열의 길이는 2 이상 50 이하이다.
         *
         * 2. output 조건
         * 만일 입력 괄호 문자열이 올바른 괄호 문자열(VPS)이면 “YES”, 아니면 “NO”를 한 줄에 하나씩 차례대로 출력해야 한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 6
         * (())())
         * (((()())()
         * (()())((()))
         * ((()()(()))(((())))()
         * ()()()()(()()())()
         * (()((())()(
         * -------
         * answer
         * NO
         * NO
         * YES
         * NO
         * YES
         * NO
         *
         * 2.
         * 3
         * ((
         * ))
         * ())(()
         * --------
         * answer
         * NO
         * NO
         * NO
         *
         */

        String[] inputData = inputData();
        String[] results = solveProblem(inputData);
        for(String result : results) {
            System.out.println(result);
        }

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            String[] inputData = new String[testCase];

            for(int i = 0; i < testCase; i++) {
                inputData[i] = br.readLine();
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String[] solveProblem(String[] inputData) {
        int testCase = inputData.length;
        String[] results = new String[testCase];

        String data;
        Stack<Character> stack = new Stack<Character>();
        char[] chars;
        int parenthesisPairs;
        boolean doingNext;

        for(int i = 0; i < testCase; i++) {
            data = inputData[i];
            chars = data.toCharArray();
            stack.clear();

            for(char c : chars) {
                stack.push(c);
            }

            parenthesisPairs = 0;
            doingNext = false;

            while(!stack.isEmpty()) {
                char c = stack.pop();
                switch(c) {
                    case ')' :
                        parenthesisPairs++;
                        break;

                    case '(' :
                        parenthesisPairs--;
                        break;

                    default :
                        // 괄호 이외의 문자는 모두 NO이다.
                        doingNext = true;
                        break;
                }

                if(doingNext || parenthesisPairs < 0) {
                    results[i] = "NO";
                    break;
                }
            }

            if(parenthesisPairs == 0) {
                results[i] = "YES";
            } else {
                results[i] = "NO";
            }
        }

        return results;
    }

}
