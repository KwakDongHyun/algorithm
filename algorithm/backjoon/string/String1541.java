package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class String1541 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 식이 주어진다. 식은 ‘0’~‘9’, ‘+’, 그리고 ‘-’만으로 이루어져 있고, 가장 처음과 마지막 문자는 숫자이다.
         * 그리고 연속해서 두 개 이상의 연산자가 나타나지 않고, 5자리보다 많이 연속되는 숫자는 없다.
         * 수는 0으로 시작할 수 있다. 입력으로 주어지는 식의 길이는 50보다 작거나 같다.
         *
         * 2. output 조건
         * 괄호를 적절히 쳐서 이 식의 값을 최소로 만드는 프로그램을 작성하시오.
         *
         * 3. 주의사항
         * 식에 대한 문자열을 분해하는 방법은 여러 개가 있지만, 정확하면서 효율적인 방법을 찾아야한다.
         *
         * 4. 해석
         * 난이도가 실버2 라고는 하지만, 바로 생각해날만큼 쉽지 않았고.. 처음 풀 때는 꽤 어려웠다.
         * 처리해야할 작업이 2개인데, 시간도 오래 걸리지 않으면서 어떻게 하면 효과적으로 처리할 수 있을까 고민을 많이 했다.
         *  1. 데이터를 구분해서 저장 (숫자,연산자)
         *  2. 최소 값을 만들기 위해서 해야하는 방법은 무엇이 있을지 생각해야한다.
         *  3. 컴퓨터에 최적화된 구현을 어떻게 할까?
         *     특히, 3개 이상 덧셈이 있는 경우.
         *
         * 종이에 2가지 예제를 직접 풀어보면서 풀이 방법에 대해서 정의할 수 있었다.
         *  1. 모든 +연산을 먼저 수행한다.
         *  2. 나머지 -연산을 수행한다.
         *
         * 이렇게 하면 항상 Expression에 대한 최소 값을 얻을 수 있다.
         *
         * Example
         * 1.
         * 55-50+40
         * -------
         * answer
         * -35
         *
         * 2.
         * 00009-00009
         * --------
         * answer
         * 0
         *
         */

        String inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String inputData) {
        String[] splitArr = inputData.split("-");
        int sum, result = 0;

        for(int i = 0; i < splitArr.length; i++) {
            // 덧셈 먼저 수행
            String additionExpression = splitArr[i];
            String[] addSplit = additionExpression.split("\\+");
            sum = Arrays.stream(addSplit).mapToInt(data -> Integer.parseInt(data)).sum();

            // 뺄셈 이후 수행
            if(i == 0) {
                result += sum;
            } else {
                result -= sum;
            }
        }

        return result;
    }

}
