package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String7490_important {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫 번째 줄에 테스트 케이스의 개수가 주어진다(<10).
         * 각 테스트 케이스엔 자연수 N이 주어진다(3 <= N <= 9).
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해 ASCII 순서에 따라 결과가 0이 되는 모든 수식을 출력한다.
         * 각 테스트 케이스의 결과는 한 줄을 띄워 구분한다.
         *
         * 3. 주의사항
         * 1부터 N까지의 수를 오름차순으로 쓴 수열 1 2 3 ... N에서 '+'나 '-', 또는 ' '(공백)을 숫자 사이에 삽입하여
         * 0이 되는 수식을 찾는 문제다. (+는 더하기, -는 빼기, 공백은 숫자를 이어 붙이는 것을 뜻한다.)
         * ASCII 순서상 ' ', '+', '-' 순서로 표기한다.
         *
         * 4. 해석
         * BackTracking을 사용하여 문제를 풀어야 한다. 전체 경우의 수를 모두 따져봐야 하는데다가 하나씩 바꾸면서 확인해야 하기 때문이다.
         * 이런 단조로운 재귀 문제를 푸는데 어려움을 많이 느끼는 것 같다.
         * 구성을 어떻게 짜야하는지, 어떻게 하면 모듈 단위로 잘 분리해서 설계하고 구현할 수 있는지, 불필요한 function call을 줄이고
         * 필요한 연산만 호출하여 연산 수행량을 낮출 수 있는지.. 등등
         * 화려한 기교나 널리 알려진 사용법보다는 이런 로우 레벨로 코드를 설계하는 연습이 많이 필요하다고 생각한다.
         *
         * Example
         * 1.
         * 2
         * 3
         * 7
         * -------
         * answer
         * 1+2-3
         *
         * 1+2-3+4-5-6+7
         * 1+2-3-4+5+6-7
         * 1-2 3+4+5+6+7
         * 1-2 3-4 5+6 7
         * 1-2+3+4-5+6-7
         * 1-2-3-4-5+6+7
         *
         */

        int[] inputData = inputData();
        solveProblem(inputData);

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            int[] inputData = new int[testCase];

            for(int i = 0; i < testCase; i++) {
                inputData[i] = Integer.parseInt(br.readLine());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void solveProblem(int[] inputData) {
         int testCase = inputData.length;
         for(int i = 0; i < testCase; i++) {
             int seriesLen = inputData[i];
             char[] operations = new char[seriesLen + 1];
             operations[1] = '+'; // 일관성을 위해서 1에 대응되는 연산을 +로 초기화함. 1은 무조건 +로 더해야만 하니까
             addOperation(seriesLen, 2, operations);
             System.out.println();
         }
    }

    private static void addOperation(int seriesLen, int num, char[] operations) {
        if(num > seriesLen) {

            if(calculate(seriesLen, operations) == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(1);
                for(int i = 2; i <= seriesLen; i++) {
                    sb.append(operations[i]);
                    sb.append(i);
                }
                System.out.println(sb.toString());
                return;
            }

        } else {

            for(int i = 0; i < 3; i++) {
                switch (i) {
                    case 0 :
                        operations[num] = ' ';
                        addOperation(seriesLen, num + 1, operations);
                        break;

                    case 1:
                        operations[num] = '+';
                        addOperation(seriesLen, num + 1, operations);
                        break;

                    case 2:
                        operations[num] = '-';
                        addOperation(seriesLen, num + 1, operations);
                        break;
                }
            }

        }

    }

    private static int calculate(int seriesLen, char[] operations) {
        int sum = 0;
        int exponent = 1; // 지수. 십진수라는 것을 유념
        int temp = 0;

        for(int i = seriesLen; i > 0; i--) {
            char operation = operations[i];
            switch(operation) {
                case ' ' :
                    temp += i * exponent;
                    exponent *= 10;
                    break;

                case '+' :
                    if(temp != 0) {
                        sum += i * exponent + temp;
                        temp = 0;
                        exponent = 1;
                    } else {
                        sum += i;
                    }
                    break;

                case '-' :
                    if(temp != 0) {
                        sum -= (i * exponent + temp);
                        temp = 0;
                        exponent = 1;
                    } else {
                        sum -= i;
                    }
                    break;
            }
        }

        return sum;
    }
}
