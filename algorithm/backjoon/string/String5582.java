package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class String5582 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄과 둘째 줄에 문자열이 주어진다. 문자열은 대문자로 구성되어 있으며, 길이는 1 이상 4000 이하이다.
         *
         * 2. output 조건
         * 첫째 줄에 두 문자열에 모두 포함 된 부분 문자열 중 가장 긴 것의 길이를 출력한다.
         *
         * 3. 주의사항
         * 예전에 풀었던 LCS(Longest Common Subsequence)과 문제가 약간 다르다. (9251번과 9252번 문제를 참고하라.)
         * 그때는 '수열'이었기 때문에 문자열이 아닌 문자를 기준으로 문제를 풀었다. 그렇기 때문에 연속적인 데이터도 아니었다.
         * 그러나 이 문제는 '문자열'을 다루기 때문에 수열과는 좀 다르다.
         *
         * 4. 해석
         * 좀 더 나은 해결법을 떠올리려고 애를 썼는데.. 결국엔 초기에 생각했던 DP방식을 사용하는 2차원 배열 풀이법이 답이었다.
         * 2개 문자열 비교가 아닐 경우에도 풀 수 있는 방법을 떠올리려고 했는데.. 투머치였던걸까??
         *
         * Example
         * 1.
         * ABRACADABRA
         * ECADADABRBCRDARA
         * -------
         * answer
         * 5
         *
         * 2.
         * UPWJCIRUCAXIIRGL
         * SBQNYBSBZDFNEV
         * --------
         * answer
         * 0
         *
         */

        String[] inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] inputData = new String[2];
            inputData[0] = br.readLine();
            inputData[1] = br.readLine();
            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String[] inputData) {
        String search = inputData[0];
        String target = inputData[1];
        int searchLen = search.length();
        int targetLen = target.length();

        int[][] dp = new int[searchLen + 1][targetLen + 1];

        for(int i = 1; i <= searchLen; i++) {
            for(int j = 1; j <= targetLen; j++) {
                if(search.charAt(i-1) == target.charAt(j-1)) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                } else {
                    dp[i][j] = 0;
                }
            }
        }

        return Arrays.stream(dp)
                .flatMapToInt(data -> Arrays.stream(data))
                .max()
                .getAsInt();
    }

}
