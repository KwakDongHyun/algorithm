package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String8958 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 테스트 케이스의 개수가 주어진다.
         * 각 테스트 케이스는 한 줄로 이루어져 있고, 길이가 0보다 크고 80보다 작은 문자열이 주어진다.
         * 문자열은 O와 X만으로 이루어져 있다.
         *
         * 2. output 조건
         * 각 테스트 케이스마다 점수를 출력한다.
         * "OOXXOXXOOO"와 같은 OX퀴즈의 결과가 있다. O는 문제를 맞은 것이고, X는 문제를 틀린 것이다.
         * 문제를 맞은 경우 그 문제의 점수는 그 문제까지 연속된 O의 개수가 된다. 예를 들어, 10번 문제의 점수는 3이 된다.
         * "OOXXOXXOOO"의 점수는 1+2+0+0+1+0+0+1+2+3 = 10점이다.
         *
         * 3. 해석
         * 특별히 여기선 없다. string과 char를 잘 다루면 된다.
         *
         * Example
         * 1번 예제
         * 5
         * OOXXOXXOOO
         * OOXXOOXXOO
         * OXOXOXOXOXOXOX
         * OOOOOOOOOO
         * OOOOXOOOOXOOOOX
         * -------
         * answer
         * 10
         * 9
         * 7
         * 55
         * 30
         *
         */
        String[] inputData = inputData();
        int[] scoreArr = getScore(inputData);
        for(int score : scoreArr) {
            System.out.println(score);
        }

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            String[] inputData = new String[testCase];
            for(int i = 0; i < testCase; i++) {
                inputData[i] = br.readLine();
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int[] getScore(String[] inputData) {
        int[] scoreArr = new int[inputData.length];
        int index = 0, sum, score;
        char[] charArr;

        for(String data : inputData) {
            score = 0;
            sum = 0;
            charArr = data.toCharArray();

            for(char c : charArr) {
                switch (c) {
                    case 'O':
                        score++;
                        sum += score;
                        break;

                    case 'X':
                        score = 0;
                        break;
                }
            }

            scoreArr[index++] = sum;
        }

        return scoreArr;
    }

}
