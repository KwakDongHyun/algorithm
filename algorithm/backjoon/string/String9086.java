package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

public class String9086 {

    private static List<String> data = new LinkedList<>();

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            for(int i = 0; i < testCase; i++)
                data.add(br.readLine());

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        StringBuilder sb;
        for(String s : data) {
            sb = new StringBuilder();
            sb.append(s.charAt(0));
            sb.append(s.charAt(s.length() - 1));
            System.out.println(sb);
        }
    }

}
