package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class String2675 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 테스트 케이스의 개수 T(1 ≤ T ≤ 1,000)가 주어진다.
         * 각 테스트 케이스는 반복 횟수 R(1 ≤ R ≤ 8), 문자열 S가 공백으로 구분되어 주어진다.
         * S의 길이는 적어도 1이며, 20글자를 넘지 않는다.
         *
         * 2. output 조건
         * 각 테스트 케이스에 대해 P를 출력한다.
         * 문자열 S를 입력받은 후에, 각 문자를 R번 반복해 새 문자열 P를 만든 후 출력
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 2
         * 3 ABC
         * 5 /HTP
         * -------
         * answer
         * AAABBBCCC
         * /////HHHHHTTTTTPPPPP
         *
         */

        String[][] inputData = inputData();
        String[] results = solveProblem(inputData);
        for(String result : results) {
            System.out.println(result);
        }

    }

    public static String[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int testCase = Integer.parseInt(br.readLine());
            String[][] inputData = new String[testCase][2];

            StringTokenizer st;
            for(int i = 0; i < testCase; i++) {
                st = new StringTokenizer(br.readLine());
                inputData[i][0] = st.nextToken();
                inputData[i][1] = st.nextToken();
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String[] solveProblem(String[][] inputData) {
        int testCase = inputData.length;
        String[] results = new String[testCase];

        for(int i = 0; i < testCase; i++) {
            int repetition = Integer.parseInt(inputData[i][0]);
            String str = inputData[i][1];

            StringBuilder sb = new StringBuilder();
            char[] chars = str.toCharArray();
            for(char c : chars) {
                for(int j = 0; j < repetition; j++) {
                    sb.append(c);
                }
            }

            results[i] = sb.toString();
        }

        return results;
    }

}
