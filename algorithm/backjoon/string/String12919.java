package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String12919 {

    private static int result = 0;

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 S가 둘째 줄에 T가 주어진다. (1 ≤ S의 길이 ≤ 49, 2 ≤ T의 길이 ≤ 50, S의 길이 < T의 길이)
         *
         * 2. output 조건
         * S를 T로 바꿀 수 있으면 1을 없으면 0을 출력한다.
         * 문자열을 바꿀 때는 다음과 같은 두 가지 연산만 가능하다.
         *  1. 문자열의 뒤에 A를 추가한다.
         *  2. 문자열의 뒤에 B를 추가하고 문자열을 뒤집는다.
         *
         * 3. 주의사항
         * 12904번 문제와 동일하다. 두 번째 규칙이 다르지만 동일하게 풀면 된다.
         * S에서 T로 생성하는 방법을 따지는 것보다는 역산으로 T에서 S를 만드는게 연산도 빠르고 고려해야할 것이 적다.
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * A
         * BABA
         * -------
         * answer
         * 1
         *
         * 2.
         * BAAAAABAA
         * BAABAAAAAB
         * --------
         * answer
         * 1
         *
         */

        String[] inputData = inputData();
        solveProblem(inputData);
        System.out.println(result);

    }

    public static String[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String[] inputData = new String[2];
            inputData[0] = br.readLine();
            inputData[1] = br.readLine();

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void solveProblem(String[] inputData) {
        String src = inputData[0];
        String target = inputData[1];
        search(src, target, 0, target.length() - 1, false);
    }

    private static void search(String src, String target, int front, int bear, boolean reverse) {
//        System.out.println(target.substring(front, bear + 1) + ", reverse : " + reverse);

        if(result == 1) {
            return;
        }

        if(bear - front + 1 == src.length()) {
            String str;
            if(!reverse) {
                str = target.substring(front, bear + 1);
            } else {
                StringBuilder sb = new StringBuilder();
                for(int i = bear; i >= front; i--) {
                    sb.append(target.charAt(i));
                }
                str = sb.toString();
            }

            result = src.equals(str) ? 1 : 0;
            return;
        }

        /*
         * 역산 과정에서 1,2번에 대한 역산을 모두 실행할 수 있는 경우가 존재할 수 있다.
         * 이럴 경우에는 모든 역산 과정을 고려하여 전체 탐색을 하면서 답을 찾아야만 한다.
         * 즉, Brute Force로 문제를 해결해야 하기 때문에 if-else 형태가 아닌 if만으로 구현해야 한다.
         */
        if(!reverse) {
            if(target.charAt(bear) == 'A')
                search(src, target, front, bear-1, false);
            if(target.charAt(front) == 'B')
                search(src, target, front+1, bear, true);
        } else {
            if(target.charAt(front) == 'A')
                search(src, target, front+1, bear, true);
            if(target.charAt(bear) == 'B')
                search(src, target, front, bear-1, false);
        }
    }

}
