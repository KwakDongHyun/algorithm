package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String2743 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 영어 소문자와 대문자로만 이루어진 단어가 주어진다. 단어의 길이는 최대 100이다.
         *
         * 2. output 조건
         * 첫째 줄에 입력으로 주어진 단어의 길이를 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * pulljima
         * -------
         * answer
         * 8
         *
         */

        String inputData = inputData();
        System.out.println(inputData.length());

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
