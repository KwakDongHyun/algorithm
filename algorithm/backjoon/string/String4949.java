package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class String4949 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 각 문자열은 마지막 글자를 제외하고 영문 알파벳, 공백, 소괄호("( )"), 대괄호("[ ]")로 이루어져 있으며,
         * 온점(".")으로 끝나고, 길이는 100글자보다 작거나 같다.
         * 입력의 종료조건으로 맨 마지막에 온점 하나(".")가 들어온다.
         *
         * 2. output 조건
         * 각 줄마다 해당 문자열이 균형을 이루고 있으면 "yes"를, 아니면 "no"를 출력한다.
         *
         * 3. 주의사항
         *
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * So when I die (the [first] I will see in (heaven) is a score list).
         * [ first in ] ( first out ).
         * Half Moon tonight (At least it is better than no Moon at all].
         * A rope may form )( a trail in a maze.
         * Help( I[m being held prisoner in a fortune cookie factory)].
         * ([ (([( [ ] ) ( ) (( ))] )) ]).
         *  .
         * .
         * -------
         * answer
         * yes
         * yes
         * no
         * no
         * no
         * yes
         * yes
         *
         */

        List<String> inputData = inputData();
        List<String> results = solveProblem(inputData);
        for(String result : results) {
            System.out.println(result);
        }

    }

    public static List<String> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            List<String> inputData = new LinkedList<String>();
            String str;
            while(!(str = br.readLine()).equals(".")) {
                inputData.add(str);
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<String> solveProblem(List<String> inputData) {
        List<String> results = new LinkedList<String>();
        Stack<Character> stack = new Stack<Character>();

        char c, stackChar;
        boolean isBalance;

        for(String data : inputData) {
            char[] chars = data.toCharArray();
            stack.clear();
            isBalance = true;

            for(int i = 0; i < chars.length; i++) {
                // 오른쪽 괄호가 먼저 나와서는 안된다.
                c = chars[i];
                if(c == ']' || c == ')') {
                    if(stack.isEmpty()) {
                        /*
                         * 1. 첫 등장한 괄호가 오른쪽 괄호인 경우
                         * 2. 최상위 계층에서 한쌍을 이루고 나서 남은 오른쪽 괄호인 경우
                         *    ex) () )
                         * 뭐가 됐던 간에 stack이 비어있으면 쌍을 이루지 못한 오른쪽 괄호라는 의미이기 때문에 불균형이다.
                         */
                        isBalance = false;
                        break;
                    } else {
                        /*
                         * stack에서 일치하지 않은 괄호가 나오면 불균형이다.
                         */
                        stackChar = stack.pop();
                        if((c == ']' && stackChar != '[') || (c == ')' && stackChar != '(')) {
                            isBalance = false;
                            break;
                        }
                    }
                }

                if(c == '(' || c == '[') {
                    stack.push(c);
                }
            }

            /*
             * 결과값 저장.
             */
            if(!isBalance || !stack.isEmpty()) {
                results.add("no");
            } else {
                results.add("yes");
            }

        }

        return results;
    }

}
