package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class String10988 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 단어가 주어진다. 단어의 길이는 1보다 크거나 같고, 100보다 작거나 같으며, 알파벳 소문자로만 이루어져 있다.
         *
         * 2. output 조건
         * 첫째 줄에 팰린드롬이면 1, 아니면 0을 출력한다.
         * 팰린드롬(palindrome)은 거꾸로 읽어도 제대로 읽는 것과 같은 문장이나 낱말, 숫자, 문자열(sequence of characters) 등이다.
         * 국어로는 회문(回文)이라고 한다.
         *
         * 3. 주의사항
         * Dp10942와 동일한 문제이다.
         * 단어라고 해서 풀이가 달라지거나 더 좋은 풀이가 있는 것 같지는 않다.
         * Dp방식이 가장 깔끔하고 정확하다고 생각한다.
         * 연습겸 보지 않고 동일하게 구현해봐라.
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * level
         * -------
         * answer
         * 1
         *
         * 2.
         * baekjoon
         * --------
         * answer
         * 0
         *
         */

        String inputData = inputData();
        int result = solveProblem(inputData);
        System.out.println(result);

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int solveProblem(String inputData) {
        char[] chars = inputData.toCharArray();

        /*
         * 수열처럼 홀수와 짝수 길이의 단어일 때 각각 풀이가 달라진다.
         * 홀수는 자기자신과 비교하는 dp[i][i]로부터 2개씩 길이가 늘어나면서 palindrome이 형성된다.
         * 짝수는 dp[i][i+1]로부터 2개씩 늘어난다.
         *
         * 주어진 단어가 홀수이던지 짝수이던지 상관없이 풀기 위해서는 홀짝의 경우에 대해서 모두 검토해야 한다.
         * 1,2,3,4,5... 식으로 단어를 토막내면서 비교하고 메모이제이션을 통해서 답을 빠르게 구한다.
         */
        int length = chars.length;
        boolean[][] dp = new boolean[length][length];

        // 홀수의 시작점인 문자 자기 자신에 대한 비교
        for(int i = 0; i < length; i++) {
            dp[i][i] = true;
        }

        // 짝수의 시작점인 이웃한 2개의 문자 비교
        for(int i = 0; i < length - 1; i++) {
            if(chars[i] == chars[i + 1]) {
                dp[i][i + 1] = true;
            }
        }

        for(int j = 2; j < length; j++) {
            for(int i = 0; i + j < length; i++) {
                if(dp[i + 1][i + j - 1] && chars[i] == chars[i + j]) {
                    dp[i][i + j] = true;
                }
            }
        }

        return dp[0][length - 1] ? 1 : 0;
    }

}
