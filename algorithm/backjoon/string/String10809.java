package backjoon.string;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class String10809 {

    public static void main(String[] args) throws IOException {

        String inputData = inputData();
        int[] result = solveProblem(inputData);
        for(int value : result) {
            System.out.print(value + " ");
        }

    }

    public static String inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int[] solveProblem(String inputData) {
        // a:97 ~ z:122. -97을 해서 인덱스를 구성한다.
        int[] alphaArr = new int[26];
        int length = alphaArr.length;
        for(int i = 0; i < length; i++) {
            alphaArr[i] = -1;
        }

        char[] chars = inputData.toCharArray();
        length = chars.length;
        for(int i = 0; i < length; i++) {
            if(alphaArr[(chars[i] - 97)] == -1) {
                alphaArr[(chars[i] - 97)] = i;
            }
        }

        return alphaArr;
    }

}
