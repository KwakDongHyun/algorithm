package backjoon.string;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class String1181 {

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 단어의 개수 N이 주어진다. (1 ≤ N ≤ 20,000)
         * 둘째 줄부터 N개의 줄에 걸쳐 알파벳 소문자로 이루어진 단어가 한 줄에 하나씩 주어진다.
         * 주어지는 문자열의 길이는 50을 넘지 않는다.
         *
         * 2. output 조건
         * 아래와 같은 조건에 따라 정렬하는 프로그램을 작성하시오.
         * 1. 길이가 짧은 것부터
         * 2. 길이가 같으면 사전 순으로
         * 단, 중복된 단어는 하나만 남기고 제거해야 한다.
         *
         * 3. 주의사항
         * 사전순 정렬을 직접 호출하고 싶으면 이미 구현되어 있는 compareTo method를 사용하는 것일 기억하자.
         * Wrapper class, 핵심 class(String 등)는 이미 구현되어 있다.
         *
         * 4. 해석
         *
         *
         * Example
         * 1.
         * 13
         * but
         * i
         * wont
         * hesitate
         * no
         * more
         * no
         * more
         * it
         * cannot
         * wait
         * im
         * yours
         * -------
         * answer
         * i
         * im
         * it
         * no
         * but
         * more
         * wait
         * wont
         * yours
         * cannot
         * hesitate
         *
         */

        List<String> inputData = inputData();
        solveProblem(inputData);

        StringBuilder sb = new StringBuilder();
        sb.append(inputData.get(0)).append("\n");

        for(int i = 1; i < inputData.size(); i++) {
            if(inputData.get(i-1).equals(inputData.get(i))) {
                continue;
            }

            sb.append(inputData.get(i)).append("\n");
        }

        System.out.println(sb);
    }

    public static List<String> inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int wordCnt = Integer.parseInt(br.readLine());
            List<String> inputData = new LinkedList<String>();

            for(int i = 0; i < wordCnt; i++) {
                inputData.add(br.readLine());
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void solveProblem(List<String> inputData) {
        inputData.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if(o1.length() == o2.length()) {
                    return o1.compareTo(o2);
                } else {
                    return o1.length() - o2.length();
                }
            }
        });
    }

}
