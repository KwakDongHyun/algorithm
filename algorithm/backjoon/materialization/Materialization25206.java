package backjoon.materialization;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Materialization25206 {

    private static double numerator = 0;
    private static double denominator = 0;
    private static Map<Character, Double> gradeMap = new HashMap<>();

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            gradeMap.put('A', 4.0);
            gradeMap.put('B', 3.0);
            gradeMap.put('C', 2.0);
            gradeMap.put('D', 1.0);
            gradeMap.put('+', 0.5);
            gradeMap.put('0', 0.0);
            gradeMap.put('F', 0.0);
            gradeMap.put('P', 0.0);

            StringTokenizer st;
            double score, grade;
            String gradeStr;
            for(int i = 0; i < 20; i++) {
                st = new StringTokenizer(br.readLine());
                st.nextToken();
                score = Double.parseDouble(st.nextToken());
                gradeStr = st.nextToken();

                grade = gradeMap.get(gradeStr.charAt(0));
                if(gradeStr.length() > 1)
                    grade += gradeMap.get(gradeStr.charAt(1));

                numerator += score * grade;
                if(gradeStr.charAt(0) == 'P')
                    continue;
                denominator += score;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
//        System.out.println(numerator);
//        System.out.println(denominator);
        System.out.println(numerator / denominator);
    }

}
