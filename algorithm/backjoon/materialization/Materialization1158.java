package backjoon.materialization;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class Materialization1158 {

    private static int number;
    private static int order;
    private static List<Integer> results = new LinkedList<>();

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

        StringBuilder sb = new StringBuilder("<");
        for(int i = 0; i < number; i++) {
            sb.append(results.get(i));
            if((i + 1) != number)
                sb.append(", ");
        }
        sb.append(">");
        System.out.println(sb);
    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            number = Integer.parseInt(st.nextToken());
            order = Integer.parseInt(st.nextToken());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        boolean[] deleted = new boolean[number];

        int index = -1, count = 0;
        while(results.size() != number) {
            if(index == number - 1) {
                index = 0;
            } else {
                index++;
            }

            if(!deleted[index])
                count++;

            if(count == order) {
                results.add(index + 1);
                deleted[index] = true;
                count = 0;
            }

        }
    }

}
