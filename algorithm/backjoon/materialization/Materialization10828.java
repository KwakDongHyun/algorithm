package backjoon.materialization;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Materialization10828 {

    private static final int DEFAULT_CAPACITY = 10;
    private static int[] array = new int[DEFAULT_CAPACITY];
    private static int size = 0;
    private static String[] commands;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int commandCount = Integer.parseInt(br.readLine());
            commands = new String[commandCount];

            for(int i = 0; i < commandCount; i++) {
                String s = br.readLine();
                commands[i] = s;
            }

            return;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        int commandCount = commands.length;
        for(int i = 0; i < commandCount; i++) {
            String command = commands[i];
            String[] arr = command.split(" ");
//            System.out.println("command : " + command);
//            System.out.println("command arr size : " + arr.length);

            switch (arr[0]) {
                case "push" :
                    push(Integer.parseInt(arr[1]));
                    break;

                case "pop" :
                    System.out.println(pop());
                    break;

                case "size" :
                    System.out.println(size());
                    break;

                case "empty" :
                    System.out.println(empty());
                    break;

                case "top" :
                    System.out.println(top());
                    break;
            }
        }
    }

    private static void push(int x) {
        if(size == array.length - 1) {
            resize();
        }

        array[size] = x;
        size++;
    }

    private static void resize() {
        int length = array.length;
        int[] newArray = new int[length + DEFAULT_CAPACITY];

        for(int i = 0; i < length; i++) {
            newArray[i] = array[i];
        }

        array = newArray;
    }

    private static int pop() {
        if(size == 0) {
            return -1;
        }

        int result = array[size - 1];
        size--;
        return result;
    }

    private static int size() {
        return size;
    }

    private static int empty() {
        return size == 0 ? 1 : 0;
    }

    private static int top() {
        if(size == 0) {
            return -1;
        }

        return array[size - 1];
    }

}
