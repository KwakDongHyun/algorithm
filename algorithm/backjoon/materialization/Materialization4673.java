package backjoon.materialization;

public class Materialization4673 {

    private static int[] generator = new int[10001];

    public static void main(String[] args) {

//        preprocessData();
        solveProblem();

    }

//    public static void preprocessData() {
//        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
//
//        } catch(Exception e) {
//            e.printStackTrace();
//        }
//    }

    private static void solveProblem() {
        for(int i = 1; i <= 10000; i++) {
            int n = d(i);
            while(n < 10001) {
                generator[n]++;
                n = d(n);
            }
        }

        for(int i = 1; i < 10001; i++) {
            if(generator[i] == 0) {
                System.out.println(i);
            }
        }
    }

    private static int d(int n) {
        int result = n;
        while(n/10 > 0) {
            result += n%10;
            n = n/10;
        }

        return result + n;
    }

}
