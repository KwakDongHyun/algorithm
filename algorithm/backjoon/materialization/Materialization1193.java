package backjoon.materialization;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Materialization1193 {

    private static int input;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            input = Integer.parseInt(br.readLine());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        /*
         * 한 사이클 수열의 시작점과 끝점을 먼저 찾는다.
         * 어느 사이클에 속한지만 알면 값은 구하기가 쉽다.
         */
        int start = 1;
        int interval = 2;
        int end = start + interval;
        int numeratorOfStart = 1;

        while(input > end) {
            start = end + 1;
            interval += 4;
            end = start + interval;
            numeratorOfStart += 2;
        }

        // 값을 찾는다.
        StringBuilder sb = new StringBuilder();

        int index = start;
        int numerator = numeratorOfStart;
        int denominator = 1;
        while(numerator > 0) {
            if(index == input) {
                sb.append(numerator).append("/").append(denominator);
                System.out.println(sb);
                return;
            }

            numerator--;
            denominator++;
            index++;
        }

        index = end;
        numerator = numeratorOfStart + 1;
        denominator = 1;
        while(numerator > 0) {
            if(index == input) {
                sb.append(numerator).append("/").append(denominator);
                System.out.println(sb);
                return;
            }

            numerator--;
            denominator++;
            index--;
        }
    }

}
