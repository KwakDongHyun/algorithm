package backjoon.materialization;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Stack;

public class Materialization10773 {

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int k = Integer.parseInt(br.readLine());

            Stack<Integer> stack = new Stack<>();
            int n;
            for(int i = 0; i < k; i++) {
                n = Integer.parseInt(br.readLine());
                if(n > 0) {
                    stack.push(n);
                } else {
                    stack.pop();
                }
            }

            int result = 0;
            for(int data : stack) {
                result += data;
            }
            System.out.println(result);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {

    }

}
