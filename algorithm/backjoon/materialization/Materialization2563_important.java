package backjoon.materialization;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Materialization2563_important {

    private static int count;
    private static int[][] coordinates = new int[100][100];
    private static int overlapSection = 0;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            count = Integer.parseInt(br.readLine());
            StringTokenizer st;
            int startX, startY;
            for(int i = 0; i < count; i++) {
                st = new StringTokenizer(br.readLine());
                startX = Integer.parseInt(st.nextToken());
                startY = Integer.parseInt(st.nextToken());

                for(int x = startX; x < startX + 10; x++) {
                    for(int y = startY; y < startY + 10; y++) {
                        coordinates[y][x]++;
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * 집합론 관점으로 생각해보면 배열에 저장된 값은 해당 구역을 공집합으로서
     * 몇 개의 집합(사각형)에 저장되어 있는지를 말한다.
     * 즉, 몇 개의 집합에서 해당 구역을 공집합 원소로서 가지고 있냐는 말이다.
     * 3이면 3개의 집합에서 해당 구역을 공집합으로 가지고 있다는 말이다.
     *
     * 제일 간단한 것은 그냥 0 이상인 값을 카운트하면 되지만, 수학적 사고력 증가를 위해서 이 풀이법을 남긴다.
     */
    private static void solveProblem() {
        int width = 100 * count;
        for(int x = 0; x < 100; x++) {
            for(int y = 0; y < 100; y++) {
                if(coordinates[y][x] > 1) {
                    width = width - (coordinates[y][x] - 1);
                }
            }
        }

        System.out.println(width);
    }

}
