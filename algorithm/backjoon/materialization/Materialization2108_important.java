package backjoon.materialization;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Materialization2108_important {

    private static int num;
    private static int[] data;

    static class Node2108 {
        private int number;
        private int frequency;

        Node2108(int number, int frequency) {
            this.number = number;
            this.frequency = frequency;
        }
    }

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            num = Integer.parseInt(br.readLine());
            data = new int[num];

            for(int i = 0; i < num; i++) {
                data[i] = Integer.parseInt(br.readLine());
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        Arrays.sort(data);
        System.out.println(Math.round((double) Arrays.stream(data).sum() / num));
        System.out.println(data[(num - 1)/2]);

        Map<Integer, Integer> statistics = new HashMap<>();
        for(int number : data) {
            if(!statistics.containsKey(number)) {
                statistics.put(number, 1);
                continue;
            }
            statistics.put(number, statistics.get(number) + 1);
        }

        List<Node2108> distributionChart = new ArrayList<>();
        for(Map.Entry<Integer, Integer> entry : statistics.entrySet()) {
            distributionChart.add(new Node2108(entry.getKey(), entry.getValue()));
        }
        Collections.sort(distributionChart, (o1, o2) -> {
                    if(o1.frequency == o2.frequency) {
                        return o1.number - o2.number;
                    }
                    return o2.frequency - o1.frequency;
                });

        int maxFrequency = distributionChart.get(0).frequency;
        int count = 0;
        boolean existence = false;
        for(Node2108 node : distributionChart) {
            if(node.frequency == maxFrequency)
                count++;

            if(count > 1)
                existence = true;
        }

        System.out.println(existence ? distributionChart.get(1).number : distributionChart.get(0).number);
        System.out.println(data[num - 1] - data[0]);

//        System.out.println(Math.round((double) -9/5));
    }

}
