package backjoon.materialization;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Materializatoin7568 {

    private static int number;
    private static int[] results;
    private static int[][] peopleInfo;

    public static void main(String[] args) {

        preprocessData();
        solveProblem();
        for(int result : results) {
            System.out.print((result + 1) + " ");
        }

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            number = Integer.parseInt(br.readLine());
            results = new int[number];
            peopleInfo = new int[number][2];

            StringTokenizer st;
            for(int i = 0; i < number; i++) {
                st = new StringTokenizer(br.readLine());
                peopleInfo[i][0] = Integer.parseInt(st.nextToken());
                peopleInfo[i][1] = Integer.parseInt(st.nextToken());
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {
        for(int i = 0; i < number; i++) {
            for(int j = 0; j < number; j++) {
                if(i == j)
                    continue;

                int leftWeight = peopleInfo[i][0];
                int leftHeight = peopleInfo[i][1];
                int rightWeight = peopleInfo[j][0];
                int rightHeight = peopleInfo[j][1];

                if(leftWeight < rightWeight && leftHeight < rightHeight) {
                    results[i]++;
                }
            }
        }
    }

}
