package backjoon.materialization;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Materialization1966 {

    private static int testCase;
    private static String[][] data;

    static class Node1966 {
        private int index;
        private int importance;

        Node1966(int index, int importance) {
            this.index = index;
            this.importance = importance;
        }
    }

    public static void main(String[] args) {

        preprocessData();
        solveProblem();

    }

    public static void preprocessData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            testCase = Integer.parseInt(br.readLine());
            data = new String[testCase][2];
            for(int i = 0; i < testCase; i++) {
                data[i][0] = br.readLine();
                data[i][1] = br.readLine();
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void solveProblem() {

        StringTokenizer st;
        int sheetsOfPaper, target, targetIndex;
        Queue<Node1966> queue;
        for(int i = 0; i < testCase; i++) {
            /*
             * 전처리
             */
            st = new StringTokenizer(data[i][0]);
            sheetsOfPaper = Integer.parseInt(st.nextToken());
            target = Integer.parseInt(st.nextToken());

            queue = new LinkedList<>();
            st = new StringTokenizer(data[i][1]);
            for(int j = 0; j < sheetsOfPaper; j++) {
                queue.offer(new Node1966(j, Integer.parseInt(st.nextToken())));
            }

            /*
             * 메인 로직
             */
            int count = 1;
            boolean changed = false;
            while(!queue.isEmpty()) {
                Node1966 node = queue.peek();

                for(Node1966 otherNode : queue) {
                    if(node.importance < otherNode.importance) {
                        changed = true;
                        break;
                    }
                }

                if(changed) {
                    queue.poll();
                    queue.offer(node);
                } else {
                    if(node.index == target) {
                        System.out.println(count);
                        break;
                    }
                    queue.poll();
                    count++;
                }
                changed = false;
            }
        }

    }

}
