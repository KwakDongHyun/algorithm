package backjoon.greedy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Greedy13305 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int cityNum = Integer.parseInt(br.readLine());
//        int[] road = new int[cityNum-1];
        int[] road = new int[cityNum];
        int[] gasPrice = new int[cityNum];

        String[] roadInfo = br.readLine().split(" ");
        for(int i = 0; i < cityNum-1; i++) {
            road[i] = Integer.parseInt(roadInfo[i]);
        }
        road[cityNum-1] = 0; // IOBE 에러 처리를 위해 마지막은 0으로 넣음

        String[] gasPriceInfo = br.readLine().split(" ");
        for(int i = 0; i < cityNum; i++) {
            gasPrice[i] = Integer.parseInt(gasPriceInfo[i]);
        }

//        System.out.println("============= check Data =============");
//        for(int i = 0; i < cityNum-1; i++) {
//            System.out.println("road[" + i + "] : " + road[i]);
//            System.out.println("gasPrice[" + i + "] : " + gasPrice[i]);
//        }

        int minGasPrice = 1000000001;
        int roadLength = 0;
        int totalPrice = 0;
//        List<Integer> cityIndexList = new ArrayList<Integer>();
        for(int i = 0; i < cityNum; i++) {
            if(minGasPrice >= gasPrice[i]) {
//                cityIndexList.add(i);
                // 갱신 전에 가격 정산부터 함
                totalPrice = totalPrice + (minGasPrice * roadLength);
//                System.out.println("============= Start " + i + " =============");
//                System.out.println("minGasPrice : " + minGasPrice);
//                System.out.println("roadLength : " + roadLength);
//                System.out.println("totalPrice : " + totalPrice);

                minGasPrice = gasPrice[i];
//                System.out.println("nextMinGasPrice : " + gasPrice[i]);
                roadLength = road[i];
//                System.out.println("nextRoadLength : " + roadLength);
//                System.out.println("============= End =============");
            } else {
                roadLength += road[i];
            }

        }

        System.out.println(totalPrice);

    }

}
