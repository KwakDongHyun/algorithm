package backjoon.greedy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Greedy10162 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int cookTime = Integer.parseInt(br.readLine());
        // 5분, 1분, 10초
        // 300초, 60초, 10초
        int[] button = {300, 60, 10};
        int[] buttonClick = new int[3];

        for(int i = 0; i < button.length; i++) {
            buttonClick[i] = cookTime / button[i];
            cookTime = cookTime % button[i];
        }

        if(cookTime != 0) {
            System.out.println(-1);
        } else {
            System.out.println(buttonClick[0] + " " + buttonClick[1] + " " + buttonClick[2]);
        }


    }

}
