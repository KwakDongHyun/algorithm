package backjoon.greedy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Greedy1541 {

    public static void main(String[] args)throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String input = br.readLine();
//        reg = reg.replaceAll("[+_]{2,}", "");

        // 큰수를 빼주면 최소값이 된다. 즉 -50+30 이렇게 있으면 -할 수를 크게 만들기 위해 +연산을 먼저 한다는거다.
        int sum = 0;
        String[] subtraction = input.split("-");
        for(int i = 0; i < subtraction.length; i++) {
            int temp = 0;

            // 덧셈 연산을 하는 과정
            String[] addition = subtraction[i].split("\\+");
            for(int j = 0; j < addition.length; j++) {
                temp += Integer.parseInt(addition[j]);
            }

            if(i == 0) {
                sum = temp;
            } else {
                sum -= temp;
            }
        }

        System.out.println(sum);

    }

}
