package backjoon.greedy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Greedy2217 {

    static int[] ropeArr;

    public static void main(String[] args)throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int ropeCount = Integer.parseInt(br.readLine());
        ropeArr = new int[ropeCount];
        for(int i = 0; i < ropeCount; i++) {
            ropeArr[i] = Integer.parseInt(br.readLine());
        }

        //오름차순으로 정렬. 큰 무게부터 순서대로 k개를 선택해 가면서 가장 큰 무게를 갱신해서 구해가면 된다.
        Arrays.sort(ropeArr);
        int maxWeight = ropeArr[ropeCount-1];
        for(int i = ropeCount-1; i >= 0; i--) {
            int curWeight = (ropeCount - i) * ropeArr[i];
            System.out.println("ropeArr[" + i + "] : " + ropeArr[i]);
            System.out.println("ropeCount : " + (ropeCount - i));
            System.out.println("curWeight : " + curWeight);
            if(maxWeight < curWeight) {
                maxWeight = curWeight;
            }
        }
        System.out.println(maxWeight);

    }

}
