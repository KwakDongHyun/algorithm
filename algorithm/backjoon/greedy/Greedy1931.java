package backjoon.greedy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;

// 그리디 알고리즘에 대해서 개념을 적립하게 도와준 좋은 문제였던거 같다.
public class Greedy1931 {

//    static List<MeetingInfo> meetingArr = new ArrayList<MeetingInfo>();
//    static int[][] meetingArr;

//    public static class MeetingInfo {
//        int startTime;
//        int endTime;
//
//        public MeetingInfo(int startTime, int endTime) {
//            this.startTime = startTime;
//            this.endTime = endTime;
//        }
//    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int meetingArrSize = Integer.parseInt(br.readLine());
        int[][] meetingArr = new int[meetingArrSize][2];

        for(int i = 0; i < meetingArrSize; i++) {
            String[] input = br.readLine().split(" ");
//            MeetingInfo meetingInfo = new MeetingInfo(Integer.parseInt(input[0]), Integer.parseInt(input[1]));
//            meetingArr.add(meetingInfo);
            meetingArr[i][0] = Integer.parseInt(input[0]);
            meetingArr[i][1] = Integer.parseInt(input[1]);
        }

        // 회의 종료시각을 기준으로 정렬, 같은 종료시각일 경우 회의시간이 짧은 것을 우선으로.
        // Arrays 클래스의 sort 메소드를 사용하는 방법을 익히는게 좋을듯 싶다. 다른사람의 생각과 방법도 아는게 중요
        Arrays.sort(meetingArr, new Comparator<int[]>() {

            @Override
            public int compare(int[] o1, int[] o2) {
                // 종료시간을 먼저 비교하고, 만약 같으면 시작시간이 빠른순으로 정렬한다.
                if(o1[1] == o2[1]) {
                    return o1[0] - o2[0];
                } else {
                    return o1[1] - o2[1];
                }
            }
        });

        int count = 0;
        int prevEndTime = 0;    // 이전에 선택한 회의시간의 종료시간
        for(int i = 0; i < meetingArrSize; i++) {
            if(prevEndTime <= meetingArr[i][0]) {
                prevEndTime = meetingArr[i][1];
                System.out.println("start time : " + meetingArr[i][0]);
                System.out.println("end time : " + meetingArr[i][1]);
                System.out.println("==========================");
                count++;
            }
        }

        System.out.println(count);

    }


}
