package backjoon.backtracking;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BackTracking9663 {

    public static int numberOfCase = 0;

    public static int[] visit;

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 N이 주어진다. (1 ≤ N < 15)
         *
         * 2. output 조건
         * N-Queen 문제는 크기가 N × N인 체스판 위에 퀸 N개를 서로 공격할 수 없게 놓는 문제이다.
         * N이 주어졌을 때, 퀸을 놓는 방법의 수를 구하는 프로그램을 작성하시오.
         *
         * 3. 해석
         * Back-Tracking의 유명한 N-Queen 문제이다.
         * 이 문제를 통해서 백트래킹에 대해서 확실히 개념과 풀이 방법을 익힌다.
         * 재귀라고 다 안 좋은건 아니라는걸 이번 문제를 통해서 알았으면 좋겠다.
         * 좀 더 나은 좋은 방법이 있겠지만, 아직은 구상이 떠오르지를 않는다.
         *
         * 4. 풀이 방법
         * 이차원 배열을 쓰면, 배열 탐색이 너무 오래걸리고 소스가 복잡해진다.
         * 왜냐하면 행과 열 인덱스 각각을 돌면서 확인해야하기 때문이다. 특히 대각선 이동 검증이 매우 까다로워진다.
         * 그래서 일차원 배열을 쓰면서 탐색을 하는 방법을 고민해봤는데, 가능한 방법을 찾았다.
         * -----------------------------------------------------------------
         * row가 1일 때는 무조건 방문하는 것이기 때문에 1이 아닌 경우와 구분지엇다. (if-else로 가독성이 떨어져서 switch로 만들었다.)
         * depth 2부터는 자리에 퀸을 놓을 때, 이전에 놓았던 퀸들의 경로에 있는지 확인을 매번 해야한다.
         * (현재 depth-1)에 있는 퀸의 경우 (-1, 0, +1) 만큼의 경로만 고려하면 되고,
         * (현재 depth-2)에 있는 퀸의 경우 (-2, 0, 2)만큼의 경로만 고려하면 된다.
         * 각각의 숫자는 (↙, ↓, ↘)가 얼만큼 적용되냐는 것을 말한다.
         *
         * 그럼 여기서 필요한 것은 현재 depth와 얼만큼 차이가 나는지는 어떻게 알 수 있냐가 된다.
         * 이는 visit 배열에 boolean 타입이 아니라 방문했을 당시의 depth 값을 넣으면 해결이 된다.
         * 그렇게 되면 (depth - visit[index])만큼 퀸의 이동 경로를 고려하면 된다.
         * 이동 횟수는 당연히 방문한 index에다가 차이만큼 (-, 0, +)를 각각하면 된다.
         *
         * Example
         * 1번 예제
         * 8
         * -------
         * answer
         * 92
         *
         */
        int chessboardSize = inputData();
        visit = new int[chessboardSize + 1];
        backTrackByDepthFirstSearch(chessboardSize, 1);
        System.out.println(numberOfCase);

    }

    public static int inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return Integer.parseInt(br.readLine());
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void backTrackByDepthFirstSearch(int chessboardSize, int depth) {
        if(depth > chessboardSize) {
            numberOfCase += 1;
            return;
        }

        switch (depth) {
            case 1 :
                for(int i = 1; i <= chessboardSize; i++) {
                    visit[i] = depth;
//                    System.out.println("visit[" + i + "] : " + depth);
                    backTrackByDepthFirstSearch(chessboardSize, depth + 1);
                    visit[i] = 0;
                }
                break;
            default :
                for(int i = 1; i <= chessboardSize; i++) {
                    // 0이 아니면 제외해야함. 같은 column은 선택 불가이기 때문에.
                    if(visit[i] != 0) {
                        continue;
                    }

                    boolean hasToPass = false;
                    for(int j = 1; j <= chessboardSize; j++) {
                        if(visit[j] != 0) {
                            // 퀸의 이동거리에 존재하면 다른 i를 탐색해야함.
                            if(i == j || i == j - (depth - visit[j]) || i == j + (depth - visit[j])) {
                                hasToPass = true;
                                break;
                            }
                        }
                    }
                    if(hasToPass) {
                        continue;
                    }

                    visit[i] = depth;
//                    System.out.println("visit[" + i + "] : " + depth);
                    backTrackByDepthFirstSearch(chessboardSize, depth + 1);
                    visit[i] = 0;
                }
                break;
        }

    }

}
