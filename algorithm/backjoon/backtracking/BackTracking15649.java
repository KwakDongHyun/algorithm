package backjoon.backtracking;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BackTracking15649 {

    public static int[] series;
    public static boolean[] visit;
    public static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) {
        /*
         * 1. input 조건
         * 첫째 줄에 자연수 N과 M이 주어진다. (1 ≤ M ≤ N ≤ 8)
         *
         * 2. output 조건
         * 1부터 N까지 자연수 중에서 중복 없이 M개를 고른 수열을 출력해라.
         * 중복되는 수열을 여러 번 출력하면 안되며, 각 수열은 공백으로 구분해서 출력해야 한다.
         * 수열은 사전 순으로 증가하는 순서로 출력해야 한다.
         *
         * 3. 해석
         * 1260번 문제와 똑같다. 최대한 Java Library를 활용해서 빠르게 푸는 연습부터 한다.
         * 백트래킹 종류의 첫 번째 문제이니만큼 꼭 한 번은 다시 보는게 좋을 것 같다.
         *
         *
         * Example
         * 1번 예제
         * 4 2
         * -------
         * answer
         * 1 2
         * 1 3
         * 1 4
         * 2 1
         * 2 3
         * 2 4
         * 3 1
         * 3 2
         * 3 4
         * 4 1
         * 4 2
         * 4 3
         *
         */
        int[] inputData = inputData();
        int maxNumber = inputData[0];
        int length = inputData[1];
//        System.out.println("maxNumber : " + maxNumber + " length : " + length);
        series = new int[length + 1];
        visit = new boolean[maxNumber + 1];

        backTrackByDepthFirstSearch(maxNumber, length, 1);
        System.out.println(sb.toString());

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int[] inputData = {Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken())};
            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * Back-Tracking은 아무리 생각하고 시도해봐도 재귀가 답인 것 같다.
     * 애초에 이번 문제가 DP가 안된다는 사실을 일찍이 깨달은 순간, bottom-up 접근과 다른 방식의 풀이가 안된다는 것을 알고 있었다...
     * 그래도 이리저리 시도해보고 생각해본 경험은 중요했다고 생각한다.
     */
    public static void backTrackByDepthFirstSearch(int maxNumber, int length, int depth) {
        if(depth > length) {
            for(int i = 1; i <= length; i++) {
                sb.append(series[i]).append(" ");
            }
            sb.append("\n");
//            System.out.println("series ");
//            System.out.println(sb.toString());
            return;
        }

        for(int i = 1; i <= maxNumber; i++) {
            if(!visit[i]) {
                visit[i] = true;
                series[depth] = i;
                /*
                 * 재귀호출할 때 매우 주의해야할 점이 있다.
                 * depth++ 와 (depth + 1)은 작동 원리가 다르다.
                 * ++는 depth라는 변수 값 자체를 1 증가시킨다.
                 * 그렇기 때문에 모든 재귀가 끝나고 돌아올 때도 depth가 증감된 채로 유지되어 있다.
                 * 반면 (depth + 1)은 수식(Expression)이기 때문에 depth의 원래 값이 손상되지 않는다.
                 */
                backTrackByDepthFirstSearch(maxNumber, length, depth + 1);
                visit[i] = false;
            }
        }
    }

}
