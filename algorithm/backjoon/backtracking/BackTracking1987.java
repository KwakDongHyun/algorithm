package backjoon.backtracking;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BackTracking1987 {

    private static int[][] inputData;
    private static int row, col;
    private static boolean[] visit = new boolean[26];

    private static int[] moveX = {-1, 0, 1, 0};
    private static int[] moveY = {0, -1, 0, 1};
    private static int maxCount = 0;

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 R과 C가 빈칸을 사이에 두고 주어진다. (1 ≤ R,C ≤ 20)
         * 둘째 줄부터 R개의 줄에 걸쳐서 보드에 적혀 있는 C개의 대문자 알파벳들이 빈칸 없이 주어진다.
         *
         * 2. output 조건
         * 첫째 줄에 말이 지날 수 있는 최대의 칸 수를 출력한다.
         *
         * 3. 해석
         * 재귀로 DFS 완전 탐색 말고는 생각나는게 없다.
         * 일단 사람이 생각해도 완전 탐색은 필수다..
         *
         * Example
         * 1.
         * 2 4
         * CAAB
         * ADCB
         * -------
         * answer
         * 3
         *
         * 2.
         * 3 6
         * HFDFFB
         * AJHGDH
         * DGAGEH
         * -------
         * answer
         * 6
         *
         */
        int[][] inputData = inputData();
        visit[inputData[0][0]] = true;
        solveProblem(0, 0, 1);
        System.out.println(maxCount);

    }

    public static int[][] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            row = Integer.parseInt(st.nextToken());
            col = Integer.parseInt(st.nextToken());
            inputData = new int[row][col];

            char[] chars; // A65 ~ Z90 : 26개 알파벳
            for(int i = 0; i < row; i++) {
                chars = br.readLine().toCharArray();
                for(int j = 0; j < col; j++) {
                    inputData[i][j] = chars[j] - 'A';
                }
            }

            return inputData;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // DFS로 탐색하면서 백트래킹까지 써야함.
    private static void solveProblem(int y, int x, int count) {
        if(maxCount < count) {
            maxCount = count;
        }

        int nextX, nextY;
        for(int i = 0; i < 4; i++) {
            nextX = x + moveX[i];
            nextY = y + moveY[i];

            if(nextY < 0 || nextY >= row || nextX < 0 || nextX >= col) {
                continue;
            }
            if(visit[inputData[nextY][nextX]]) {
                continue;
            }

            visit[inputData[nextY][nextX]] = true;
            solveProblem(nextY, nextX, count + 1);
            visit[inputData[nextY][nextX]] = false;
        }
    }

}
