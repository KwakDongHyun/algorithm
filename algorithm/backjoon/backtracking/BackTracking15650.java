package backjoon.backtracking;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BackTracking15650 {

    public static int[] series;
    public static boolean[] visit;
    public static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) {

        /*
         * 1. input 조건
         * 첫째 줄에 자연수 N과 M이 주어진다. (1 ≤ M ≤ N ≤ 8)
         *
         * 2. output 조건
         * 1부터 N까지 자연수 중에서 중복 없이 M개를 고른 수열을 출력해라.
         * 고른 수열은 오름차순이어야 한다.
         * 중복되는 수열을 여러 번 출력하면 안되며, 각 수열은 공백으로 구분해서 출력해야 한다.
         * 수열은 사전 순으로 증가하는 순서로 출력해야 한다.
         *
         * 3. 해석
         * 15649 문제에서 오름차순 수열이라는 조건 1개가 추가되었다.
         *
         *
         * Example
         * 1번 예제
         * 4 2
         * -------
         * answer
         * 1 2
         * 1 3
         * 1 4
         * 2 3
         * 2 4
         * 3 4
         *
         */
        int[] inputData = inputData();
        int maxNumber = inputData[0];
        int length = inputData[1];
        series = new int[length + 1];
        visit = new boolean[maxNumber + 1];

        backTrackByDepthFirstSearch(maxNumber, length, 1);
        System.out.println(sb.toString());

    }

    public static int[] inputData() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int[] inputData = {Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken())};
            return inputData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void backTrackByDepthFirstSearch(int maxNumber, int length, int depth) {
        if(depth > length) {
            for(int i = 1; i <= length; i++) {
                sb.append(series[i]).append(" ");
            }
            sb.append("\n");
            return;
        }

        for(int i = 1; i <= maxNumber; i++) {
            if(!visit[i] && series[depth - 1] < i) {
                visit[i] = true;
                series[depth] = i;
                backTrackByDepthFirstSearch(maxNumber, length, depth + 1);
                visit[i] = false;
            }
        }
    }

}
