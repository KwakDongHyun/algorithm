package softeer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

class GraphNode {
    private int vertex;
    private int time;
    private int subTreeSize;

    public GraphNode(int vertex, int time) {
        this.vertex = vertex;
        this.time = time;
    }

    public int getVertex() {
        return vertex;
    }

    public int getTime() {
        return time;
    }

    public int getSubTreeSize() {
        return subTreeSize;
    }

    public void setSubTreeSize(int subTreeSize) {
        this.subTreeSize = subTreeSize;
    }

}

/*
 * subtreeSize이 솔루션 핵심이기 때문에 BFS보다는 DFS로 했다면 처음 한번 로직을 돌 때 이미 다 계산을 했을거다.
 * BFS로 쭉 밀고 나아가보려고 했으나 결국 subtreeSize는 DFS로 구할 수 밖에 없게 된다.
 */
public class DistanceSum {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st;

        int vertexCount = Integer.parseInt(br.readLine());
        int start = 0, end = 0, time = 0;
        ArrayList<ArrayList<GraphNode>> graph = new ArrayList<ArrayList<GraphNode>>();      // 연결리스트로 구현한 그래프
        ArrayList<Integer> subTreeSizeList = new ArrayList<>();
        int[] subtreeSizeArr = new int[vertexCount+1];

        for (int i = 0; i < vertexCount + 1; i++) {
            graph.add(i, new ArrayList<GraphNode>());
            subTreeSizeList.add(i, 0);
        }


        for (int i = 1; i < vertexCount; i++) {
            st = new StringTokenizer(br.readLine());
            start = Integer.parseInt(st.nextToken());
            end = Integer.parseInt(st.nextToken());
            time = Integer.parseInt(st.nextToken());

            // 최초 저장시에는 리스트 초기화
            graph.get(start).add(new GraphNode(end, time));
            // 양방향이므로 start end 바꿔서 노드를 넣는다.
            graph.get(end).add(new GraphNode(start, time));
        }

        // 리스트 중간 점검
//        for(int i = 1; i <= vertexCount; i++) {
//            System.out.println(i + " vertex");
//            ArrayList<GraphNode> linkedList = graph.get(i);
//            for(GraphNode graphNode : linkedList) {
//                System.out.println("vertex : " + graphNode.getVertex() + " time : " + graphNode.getTime());
//            }
//            System.out.println("");
//        }

        // 핵심 로직
        /*
         * 먼저 BFS나 DFS로 한 노드를 기준으로 순회를 한다.
         * 그 후에 인접하게 연결된 노드로 뻗어나가며 값을 구해나간다.
         * 문제의 핵심은 트리 형태가 아니라, DP식으로 잘게 쪼개서 생각을 하는 것이라서
         * 트리에 너무 목숨걸면 문제의 핵심을 못 보게 된다.
         * 그래프의 특징에 집중을 했었으면 의외로 쉽게 알고리즘을 찾았을 수도 있었다.
         *
         * 해당 노드 기준 서브트리의 노드 개수를 알고 싶으면 bfs기준.
         *
         */
        Queue<GraphNode> queue = new LinkedList<GraphNode>();

        boolean[] visited = new boolean[vertexCount + 1];
        int[] timeSum = new int[vertexCount + 1];
        int vertex, distance, subtreeCount = 0;

        queue.add(new GraphNode(1, 0)); // 자기 자신까지의 거리는 0이다.

        while(!queue.isEmpty()) {
            GraphNode graphNode = queue.poll();
            vertex = graphNode.getVertex();
            distance = graphNode.getTime();


            if(!visited[vertex]) {
                visited[vertex] = true;
                timeSum[vertex] += distance;
                subtreeCount = 1;

                for(GraphNode node : graph.get(vertex)) {
                    if(!visited[node.getVertex()]) {
                        timeSum[node.getVertex()] = timeSum[vertex];  // 현재 탐색하고 있는 vertex까지 거리값을 연결된 노드들한테 더해줘야함.
                        subtreeCount++;
                    }
                    queue.add(node);
                }

                subTreeSizeList.add(vertex, subtreeCount);
            }
        }

        //subtreeList를 바탕으로 size 제대로 구하기.
        //dfs 방식으로 size 크기를 구해야할 듯 싶다.
        visited = new boolean[vertexCount + 1]; // 새로 초기화해서 다시 사용
        for (int i = 1; i < vertexCount; i++) {
            int size = subTreeSizeList.get(i);
            for(GraphNode graphNode : graph.get(i)) {
                size += subTreeSizeList.get(graphNode.getVertex()) - 1;
            }
        }

        // 중간데이터 확인
        /*
        ==============================
        1 vertex
        timeSum : 0 subtreeSize : 4
        ==============================
        2 vertex
        timeSum : 5 subtreeSize : 1
        ==============================
        3 vertex
        timeSum : 2 subtreeSize : 3
        ==============================
        4 vertex
        timeSum : 8 subtreeSize : 2
        ==============================
        5 vertex
        timeSum : 6 subtreeSize : 1
        ==============================
        6 vertex
        timeSum : 3 subtreeSize : 1
        ==============================
        7 vertex
        timeSum : 14 subtreeSize : 1
         */
        for (int i = 1; i < vertexCount + 1; i++) {
            System.out.println("==============================");
            System.out.println(i + " vertex");
            System.out.println("timeSum : " + timeSum[i] + " subtreeSize : " + subTreeSizeList.get(i));
        }


    }

}
