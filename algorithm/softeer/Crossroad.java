package softeer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

class Node implements Comparable<Node> {
    private int time;
    private String way;
    private int wayToInt;
    private int order;
    private int passTime;

    public Node() {

    }

    public Node(int time, String way, int order) {
        this.time = time;
        this.way = way;
        this.order = order;

        switch (way) {
            case "A":
                wayToInt = 0;
                break;
            case "B":
                wayToInt = 1;
                break;
            case "C":
                wayToInt = 2;
                break;
            case "D":
                wayToInt = 3;
        }
    }

    public int getTime() {
        return time;
    }

    public String getWay() {
        return way;
    }

    public int getWayToInt() {
        return wayToInt;
    }

    public int getOrder() {
        return order;
    }

    public int getPassTime() {
        return passTime;
    }

    public void setPassTime(int passTime) {
        this.passTime = passTime;
    }

    @Override
    public int compareTo(Node o) {
        return this.order - o.getOrder();
    }
}

public class Crossroad {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st;

        int N = Integer.parseInt(br.readLine());
        Map<Integer, List<Node>> carData = new HashMap<Integer, List<Node>>();
        int time;
        int startTime = 1000_000_000;

        for(int i = 1; i <= N; i++) {
            st = new StringTokenizer(br.readLine());
            time = Integer.parseInt(st.nextToken());
            Node car = new Node(time, st.nextToken(), i);

            if(carData.containsKey(time)) {
                carData.get(time).add(car);
            } else {
                List<Node> nodelist = new ArrayList<Node>();
                nodelist.add(car);
                carData.put(time, nodelist);
            }

            if(startTime > time) {
                startTime = time;
            }
        }

        // 데이터 제대로 담긴지 테스트
//        for(int key : carData.keySet()) {
//            System.out.println("key : " + key);
//            List<Node> nodeList = carData.get(key);
//            for(Node node : nodeList) {
//                System.out.println("car info");
//                System.out.println("time : " + node.getTime() + ", way : " + node.getWay());
//            }
//            System.out.println("================================================");
//        }

        List<Node>[] roadInfo = (List<Node>[]) new List[4];
        roadInfo[0] = new ArrayList<Node>();
        roadInfo[1] = new ArrayList<Node>();
        roadInfo[2] = new ArrayList<Node>();
        roadInfo[3] = new ArrayList<Node>();

        // 핵심로직
        List<Node> resultList = new ArrayList<Node>();
        boolean aPassFlag;
        boolean bPassFlag;
        boolean cPassFlag;
        boolean dPassFlag;

        while(resultList.size() != N) {
            aPassFlag = false;
            bPassFlag = false;
            cPassFlag = false;
            dPassFlag = false;

            // 매 초마다 차량 진입
            if(carData.containsKey(startTime)) {
                List<Node> nodelist = carData.get(startTime);
                for(Node node : nodelist) {
                    roadInfo[node.getWayToInt()].add(node);
                }
            }

            /*
             * 오른쪽 위치에 차량 확인 후 통과. 통과
             * 통과시 통과시간 기입, 나머지 차량을 다음 시간에 기입.
             */
            List<Node> Aroad = roadInfo[0];
            List<Node> Broad = roadInfo[1];
            List<Node> Croad = roadInfo[2];
            List<Node> Droad = roadInfo[3];

            if(Droad.size() == 0 && Aroad.size() != 0) {
                aPassFlag = true;
                Node node = Aroad.get(0);
                node.setPassTime(startTime);
                resultList.add(node);
            }

            if(Aroad.size() == 0 && Broad.size() != 0) {
                bPassFlag = true;
                Node node = Broad.get(0);
                node.setPassTime(startTime);
                resultList.add(node);
            }

            if(Broad.size() == 0 && Croad.size() != 0) {
                cPassFlag = true;
                Node node = Croad.get(0);
                node.setPassTime(startTime);
                resultList.add(node);
            }

            if(Croad.size() == 0 && Droad.size() != 0) {
                dPassFlag = true;
                Node node = Droad.get(0);
                node.setPassTime(startTime);
                resultList.add(node);
            }

            // Dead Lock
            if(Aroad.size() != 0 && Broad.size() != 0 && Croad.size() != 0 && Droad.size() != 0) {
                for(Node node : Aroad) {
                    node.setPassTime(-1);
                    resultList.add(node);
                }

                for(Node node : Broad) {
                    node.setPassTime(-1);
                    resultList.add(node);
                }

                for(Node node : Croad) {
                    node.setPassTime(-1);
                    resultList.add(node);
                }

                for(Node node : Droad) {
                    node.setPassTime(-1);
                    resultList.add(node);
                }

                break;
            }

            if(aPassFlag) {
                Aroad.remove(0);
            }

            if(bPassFlag) {
                Broad.remove(0);
            }

            if(cPassFlag) {
                Croad.remove(0);
            }

            if(dPassFlag) {
                Droad.remove(0);
            }

            // 다음 시간에 교차로 정보를 기입
            if(startTime != 1000_000_000) {
                roadInfo[0] = Aroad;
                roadInfo[1] = Broad;
                roadInfo[2] = Croad;
                roadInfo[3] = Droad;
                startTime++;
            }

        }

        // 입력순서대로 정렬
        Node[] resultArr = new Node[N + 1];
        for(int i = 0; i < resultArr.length; i++) {
            resultArr[i] = new Node();
        }

        for(Node node : resultList) {
            resultArr[node.getOrder()] = node;
//            System.out.println("order : " + node.getOrder());
//            System.out.println("passTime : " + node.getPassTime());
        }

        // 출력
        for(int i = 1; i < resultArr.length; i++) {
            Node node = resultArr[i];
            System.out.println(node.getPassTime());
        }

    }
}
