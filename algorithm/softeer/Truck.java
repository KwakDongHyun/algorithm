package softeer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

class CustomerSuggestion implements Comparable<CustomerSuggestion> {

    private int needWeight;
    private int price;
    private int customerId;

    public CustomerSuggestion() {

    }

    public CustomerSuggestion(int needWeight, int price, int customerId) {
        this.needWeight = needWeight;
        this.price = price;
        this.customerId = customerId;
    }

    public int getNeedWeight() {
        return needWeight;
    }

    public int getPrice() {
        return price;
    }

    public int getCustomerId() {
        return customerId;
    }

    @Override
    public int compareTo(CustomerSuggestion o) {
        return this.needWeight - o.getNeedWeight();
    }
}

class Scenario implements Comparable<Scenario> {

    private int revenue;
    private int scenarioId;
    private int minNeedSize;

    public Scenario(int revenue, int scenarioId) {
        this.revenue = revenue;
        this.scenarioId = scenarioId;
        this.minNeedSize = 1000_000_001;
    }

    public int getRevenue() {
        return revenue;
    }

    public int getScenarioId() {
        return scenarioId;
    }

    public int getMinNeedSize() {
        return minNeedSize;
    }

    public void setMinNeedSize(int minNeedSize) {
        this.minNeedSize = minNeedSize;
    }

    @Override
    public int compareTo(Scenario o) {
        return this.revenue - o.getRevenue();
    }
}

public class Truck {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st;

        int customer = Integer.parseInt(br.readLine());
//        List<CustomerSuggestion>[] customerInfo = (List<CustomerSuggestion>[]) new List[customer];
        List<CustomerSuggestion> customerInfo = new ArrayList<>();
//        List<Integer> weightInfo = new ArrayList<Integer>();    // 신차 무게만을 모은 리스트

        /*
         * 이중 리스트 형태의 자료를 1차원 형태의 자료로 변형을 시켜야 for문을 안해도 될 것이다.
         * 그러면 해당 제안이 어느 고객의 제안인지 따로 등록을 해줘야 하므로 customer ID를 추가해준다.
         * 아이디는 0부터 해도 상관 없다.
         */
        for(int i = 0; i < customer; i++) {
//            customerInfo[i] = new ArrayList<CustomerSuggestion>();
            st = new StringTokenizer(br.readLine());
            int suggestion = Integer.parseInt(st.nextToken());

            // 고객 한 명당 제안들
            for(int j = 0; j < suggestion; j++) {
//                int needWeight = Integer.parseInt(st.nextToken());
                CustomerSuggestion customerSuggestion = new CustomerSuggestion(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), i);
                customerInfo.add(customerSuggestion);

//                if(!weightInfo.contains(needWeight)) {
//                    weightInfo.add(needWeight);
//                }
            }

//            Collections.sort(customerInfo[i]);  // 고객 제안들을 오름차순으로 정렬
        }

        Collections.sort(customerInfo); // 고객 제안들을 오름차순으로 정렬
//        Collections.sort(weightInfo);

        // 테스트 시나리오도 목표치 금액을 오름차순으로 정렬.
        int testCount = Integer.parseInt(br.readLine());
        List<Scenario> scenarioList = new ArrayList<Scenario>(testCount);
        st = new StringTokenizer(br.readLine());

        for(int i = 0; i < testCount; i++) {
            scenarioList.add(i, new Scenario(Integer.parseInt(st.nextToken()), i));
        }

        Collections.sort(scenarioList);  // 중복탐색을 줄이기 위해서 정렬을 하고서 탐색 범위를 좁혀나가기 위함.

        /*
         * 제안하면서 나오는 모든 무게들만을 Set개념으로 모아서 이걸 기준으로 돌리는게 가장 맞는것 같다.
         * 결국 답은 제안된 무게 안에서 나오는 것이기 때문에 (최소 무게라면 중간에 어중간한 무게가 나올 이유 X)
         * 무게별로 총 이익 배열에 담아서 시나리오별로 해당 이익에 가장 근접하는 최소 무게만을 출력하면 되기 때문.
         *
         * ========================================
         * 오답은 없는데 문제는 시간초과이다.. 시간초과를 어떻게 막는게 좋지?
         * for문을 1개씩 줄여나갈 수 있으면 줄여나가보자.
         *
         * 먼저 customerInfo를 1차원 리스트 형태로 바꾸었다.
         * 그렇게 되면 전체 로직 구성이 바뀌게 된다. 기존 출력예제처럼 무게별로 총 이익을 구하는 방식을 할 수가 없다.
         * 무게 기준이 아닌 가격을 기준으로 루프를 돌면서,
         * 가격의 합산이 테스트 시나리오 가격을 넘어서면 그때 최소무게를 등록하는 방식으로 해야한다.
         * 한 사람이 여러 제안을 했을 경우, 돌리다보면 가장 큰 이익을 선택해야할 필요가 있다.
         * 이는 가격 중심일 경우에 반드시 고려해야할 사항으로
         * 선택한 제안을 도중에 변경 시에 총 이익에도 값을 변경해줄 필요가 있다.
         * 그러기 위해선 이전에 선택했던 suggestion의 가격 정보를 알고 있어야 대소비교를 통해 보다 나은 선택을
         * 할 수가 있게 된다.
         *
         * =======================
         * 결국 4개의 반복문 사용(1개는 3중 반복문)에서 2개의 반복문만을 이중 for문으로 사용하는 형태로 변경할
         * 수 있게 된 것이다.
         */
        int revenue = 0;
        int[] choicePrice = new int[customer];
        int index = 0; // 시나리오 탐색시 중복탐색을 줄이기 위해서 인덱스 변수 하나 생성.

        for (int i = 0; i < customerInfo.size(); i++) {
            CustomerSuggestion customerSuggestion = customerInfo.get(i);
            int customerId = customerSuggestion.getCustomerId();
            int needWeight = customerSuggestion.getNeedWeight();
            int price = customerSuggestion.getPrice();

            // 이전에 선택한 제안보다 가격이 높을 경우 이를 선택한다. 그리고 총 이익에도 계산식을 반영해줘야함.
            // 선택한 제안의 가격을 새롭게 등록하여 기억을 시켜둔다.
            if(price > choicePrice[customerId]) {
                revenue = revenue - choicePrice[customerId] + price;
                choicePrice[customerId] = price;
            }

            /*
             * 위 로직을 통해 선택한 올바른 가격이 누적될 것이다.
             * 그리고 오로지 무게만을 기준으로 모든 제안들을 나열한 상태에서 루프를 돌린다.
             * 그렇기 다음 제안을 선택했을 때는 해당 이전까지의 제안들보다 무게가 클 것이고,
             * 총 이익은 누적된 이익에다가 루프를 돌면서 새롭게 제안별 수익이 쌓여갈 것이다.
             * 또한 이 말은 이전에 내가 짯던 소스와 같은 동작원리가 된다.
             * 게다가 어차피 if를 통해서 새롭게 이익이 큰 제안을 선택하면 기존 이익값만큼 빼고 새로 더하므로
             * 결과적으로 한 사람당 한 개의 제안만 수렴하게 된다.
             *
             * 요컨대 가격을 누적시켜가면서 현 시점의 제안을 선택했을 때, 이 무게만큼 오기까지 최선의 이익만을
             * 선택하여 누적한 총 이익을 바탕으로 테스트 케이스의 문제를 해결해 나간다는 것이다.
             *
             * =================================
             * 테스트 케이스가 많을 경우, 이미 답이 기재되어 있는 테스트 케이스도 다시 처음부터 확인하게 되는 로직이라서
             * 타임아웃이 걸리는 것 같다. 이미 기재된 답은 두 번 다시 선택하지 않도록 탐색과정에서 제외시켜줄 필요가 있다.
             *
             */
//            for(int j = 0; j < scenarioList.length; j++) {
//                Scenario scenario = scenarioList[j];
//                if(scenario.getRevenue() <= revenue && needWeight < scenario.getMinNeedSize()) {
//                    scenario.setMinNeedSize(needWeight);
//                }
//            }
            for(int j = index; j < scenarioList.size(); j++) {
                Scenario scenario = scenarioList.get(j);
//                if(scenario.getRevenue() <= revenue && needWeight < scenario.getMinNeedSize()) {
//                    scenario.setMinNeedSize(needWeight);
//                    index++;
//                }
                if(scenario.getRevenue() <= revenue) {
                    scenario.setMinNeedSize(needWeight);
                    index++;
                }
            }

        }

        /*
        // 구형 소스.
        // 무게별로 총 이익을 찾아서 답을 도출하는 방식.
        for(Integer weight : weightInfo) {
            int revenue = 0;

            for (int i = 0; i < customerInfo.length; i++) {
                List<CustomerSuggestion> suggestionList = customerInfo[i];
                int bestPrice = 0;
                int choiceWeight = 0;

                for (CustomerSuggestion customerSuggestion : suggestionList) {
                    int customerId = customerSuggestion.getCustomerId();
                    int needWeight = customerSuggestion.getNeedWeight();
                    int price = customerSuggestion.getPrice();

                    if (needWeight <= weight && bestPrice < price) {
                        bestPrice = price;
                        choiceWeight = needWeight;
                    }
                }

//                System.out.print("customerId : " + i);
//                System.out.print(" choiceWeight : " + choiceWeight);
//                System.out.println(" bestPrice : " + bestPrice);
                revenue += bestPrice;
            }
//            System.out.print("weight : " + weight);
//            System.out.println(" revenue : " + revenue);

//            System.out.println("");

            // 해당 무게의 총 이익을 토대로 시나리오를 돌면서 만족하는 것이 있으면 그것에다가 값을 대입.
            // 최소 무게부터 탐색을 하므로 대입된 순간 그게 사실 최소 무게일 것이다.
            for(Scenario scenario : scenarioList) {
                if(scenario.getMinNeedSize() == 0 && scenario.getRevenue() <= revenue) {
                    scenario.setMinNeedSize(weight);
//                    System.out.print("scenario revenue : " + scenario.getRevenue());
//                    System.out.println(" needWeight : " + scenario.getMinNeedSize());

//                    System.out.print(weight + " ");
                }
            }
        }*/

        Collections.sort(scenarioList, new Comparator<Scenario>() {
            @Override
            public int compare(Scenario o1, Scenario o2) {
                return o1.getScenarioId() - o2.getScenarioId();
            }
        });

        for(Scenario scenario : scenarioList) {
            if(scenario.getMinNeedSize() != 1000_000_001) {
                System.out.print(scenario.getMinNeedSize() + " ");
            } else {
                System.out.print(-1 + " ");
            }
        }


    }

}
