package definition.tree;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

public class Heap<E> implements Cloneable {
    private static final int DEFAULT_CAPACITY = 10; // (기본) 배열 크기
    private int size; // 요소 개수
    private Object[] array; // 힙을 구현할 배열

    private final Comparator<? super E> comparator; // 객체 정렬시 혹은 임의의 순서로 정렬할 경우 파라미터로 받을 수 있음.

    // 생성자 Type 1 (초기 크기 입력 X)
    public Heap() {
        this(null);
    }

    // 생성자 Type 2 (초기 크기 입력 o)
    public Heap(int capacity) {
        this(capacity, null);
    }

    // 초기 크기 입력 X, comparator만 입력 받을 경우
    public Heap(Comparator<? super E> comparator) {
        this.array = new Object[DEFAULT_CAPACITY];
        this.size = 0;
        this.comparator = comparator;
    }

    // 초기 크기 입력 O, comparator만 입력 받을 경우
    public Heap(int capacity, Comparator<? super E> comparator) {
        this.array = new Object[capacity];
        this.size = 0;
        this.comparator = comparator;
    }

    // 받은 인덱스의 부모 노드 인덱스를 반환
    private int getParent(int index) {
        return index / 2;
    }

    // 받은 인덱스의 왼쪽 자식 노드 인덱스를 반환
    private int getLeftChild(int index) {
        return index * 2;
    }

    // 받은 인덱스의 오른쪽 자식 노드 인덱스를 반환
    private int getRightChild(int index) {
        return index * 2 + 1;
    }

    /**
     * @param newCapacity 새로운 배열 크기
     */
    private void resize(int newCapacity) {
        Object[] newArray = new Object[newCapacity];

        for(int i = 1; i <= size; i++) {
            newArray[i] = array[i];
        }

        // null을 넣으면 GC처리를 하나보다
        this.array = null;
        this.array = newArray;
    }

    public void add(E value) {
        // 배열이 꽉 차있을 경우 크기를 2배로 늘려준다.
        if(size + 1 == array.length) {
            resize(array.length * 2);
        }

        shiftUp(size + 1, value); // 저장할 인덱스 위치와 넣을 값(타겟)을 넘겨줌
        size++;
    }

    // 상향 선별

    /**
     * @param idx     추가할 노드의 인덱스 위치
     * @param target  재배치 할 노드
     */
    private void shiftUp(int idx, E target) {
        // comparator가 존재할 경우 comparator을 인자로 넘겨준다.
        if(comparator != null) {
            shiftUpComparator(idx, target, comparator);
        } else {
            shiftUpComparable(idx, target);
        }
    }

    // Comparator을 이용한 shift-up
    @SuppressWarnings("unchecked")
    private void shiftUpComparator(int idx, E target, Comparator<? super E> comp) {
        // root노드보다 클 때까지만 탐색한다.
        while(idx > 1) {
            int parent = getParent(idx);        // 삽입노드의 부모노드 인덱스 구하기
            Object parentVal = array[parent];   // 부모노드의 값

            // 타겟 노드 값이 부모노드보다 크면 반복문 종료
            if(comp.compare(target, (E) parentVal) >= 0) {
                break;
            }

            /*
             * 타겟노드가 부모노드보다 작으므로
             * 현재 삽입될 위치에 부모노드 값을 넣고
             * 부모노드의 위치에 타겟노드 값을 넣는다.
             * 타겟은 최종 위치에 한번만 넣으면 되므로, 여기서는 부모값만 복사해서 idx에 넣는다.
             */
            array[idx] = parentVal;
            idx = parent;
        }

        // 최종적으로 삽입될 위치에 타겟 노드 값을 저장해준다.
        array[idx] = target;
    }

    // 삽입할 객체의 Comparable을 이용한 shift-up
    @SuppressWarnings("unchecked")
    private void shiftUpComparable(int idx, E target) {
        // 타겟노드가 비교될 수 있도록 Comparable 변수를 만든다.
        Comparable<? super E> comp = (Comparable<? super E>) target;

        while(idx > 1) {
            int parent = getParent(idx);
            Object parentVal = array[parent];

            if(comp.compareTo((E) parentVal) >= 0) {
                break;
            }
            array[idx] = parentVal;
            idx = parent;
        }
        array[idx] = comp;

    }

    @SuppressWarnings("unchecked")
    public E remove() {
        if(array[1] == null) {  // 만약 root가 비어있을경우 예외를 던진다.
            throw new NoSuchElementException();
        }

        E result = (E) array[1];    // 삭제된 요소를 반환하기 위한 임시 변수
        E target = (E) array[size]; // 타겟이 될 요소
        array[size] = null;         // 타겟 노드를 비운다.

        // 삭제할 노드의 인덱스와 이후 재배치 할 타겟 노드를 넘겨준다.
        shiftDown(1, target);       // 루트 노드가 삭제되므로 1을 넘겨준다.

        return result;
    }

    /**
     *
     * @param idx       삭제할 노드의 인덱스
     * @param target    재배치 할 노드
     */
    private void shiftDown(int idx, E target) {
        // comparator가 존재할 경우 comparator 을 인자로 넘겨준다.
        if(comparator != null) {
            shiftDownComparator(idx, target, comparator);
        }
        else {
            shiftDownComparable(idx, target);
        }
    }

    // Comparator을 이용한 shift-down
    @SuppressWarnings("unchecked")
    private void shiftDownComparator(int idx, E target, Comparator<? super E> comp) {

        array[idx] = null;  // 삭제할 인덱스의 노드를 삭제
        size--;

        int parent = idx;   // 삭제할 노드를 가리키는 변수. 탐색 포인터.
        int child;          // 교환될 자식을 가리키는 변수

        // 왼쪽 자식 노드의 인덱스가 배열 원소의 개수(size)보다 작을때까지 반복
        // 좌측부터 노드를 채우기 때문에 좌측에는 무조건 노드가 존재하기 때문.
        while((child = getLeftChild(parent)) <= size) {

            int right = getRightChild(parent);  // 오른쪽 자식 인덱스
            Object childVal = array[child];     // 왼쪽 자식노드의 값 (교환될 값)

            /*
             * 오른쪽 자식 인덱스가 size를 넘지 않으면서
             * 왼쪽 자식이 오른쪽 자식보다 큰 경우
             * 재배치할 노드는 작은 자식과 비교해야하므로 child와 childVal을
             * 오른쪽 자식으로 바꿔준다.
             */
            if(right <= size && comp.compare((E) childVal, (E) array[right]) > 0) {
                child = right;
                childVal = array[child];
            }

            // 재배치할 노드가 자식노드보다 작을경우 반복문을 종료한다.
            if(comp.compare(target, (E) childVal) <= 0) {
                break;
            }

            /*
             * 현재 부모 인덱스에 자식노드 값을 넣어주고(swap)
             * 부모 인덱스를 자식 인덱스로 교체
             */
            array[parent] = childVal;
            parent = child;

        }

        // 최종적으로 재배치 되는 위치에 타겟이 된 값을 넣어준다.
        array[parent] = target;

        /*
         * 배열의 크기가 최소 배열 크기(Default = 기본크기)보다는 크면서
         * 현재 원소의 개수가 전체 배열 크기의 1/4일 경우
         * 배열을 절반으로 줄임(단, 최소 크기보다는 커야함)
         */
        if(array.length > DEFAULT_CAPACITY && size < array.length / 4) {
            resize(Math.max(DEFAULT_CAPACITY, array.length / 2));
        }
    }

    // Comparable을 이용한 shift-down
    @SuppressWarnings("unchecked")
    private void shiftDownComparable(int idx, E target) {

        Comparable<? super E> comp = (Comparable<? super E>) target;

        array[idx] = null;
        size--;

        int parent = idx;
        int child;

        while((child = getLeftChild(parent)) <= size) {

            int right = getRightChild(parent);

            Object childVal = array[child];

            if(right <= size && ((Comparable<? super E>) childVal).compareTo((E) array[right]) > 0) {
                child = right;
                childVal = array[child];
            }

            if(comp.compareTo((E) childVal) <= 0) {
                break;
            }
            array[parent] = childVal;
            parent = child;
        }

        array[parent] = target;

        if(array.length > DEFAULT_CAPACITY && size < array.length / 4) {
            resize(Math.max(DEFAULT_CAPACITY, array.length / 2));
        }

    }

    public int size() {
        return this.size;
    }

    @SuppressWarnings("unchecked")
    public E peek() {
        if(array[1] == null) {
            throw new NoSuchElementException();
        }
        return (E) array[1];
    }

    public boolean isEmpty() {
        return size == 0;
    }

//    public Object[] toArray() {
//        return Arrays.copyOf(array, size + 1);
//    }

    public Object[] toArray() {
        return toArray(new Object[size]);
    }

    public <T> T[] toArray(T[] a) {
        if(a.length <= size) {
            return (T[]) Arrays.copyOfRange(array, 1, size + 1, a.getClass());
        }
        System.arraycopy(array, 1, a, 0, size);
        return a;
    }

    @Override
    public Object clone() {
        // super.clone()은 CloneNotSupportedException 처리를 해줘야함.
        // Checked Exception이라 안해주면 컴파일 오류 발생.
        try {
            Heap<?> cloneHeap = (Heap<?>) super.clone();

            cloneHeap.array = new Object[size + 1];

            // 단순히 clone()만 하면 Heap<?> 객체만 새로 deep copy가 되고, 내부의 참조값들은 복사가 안된다.
            // 그러므로 내부가 참조형태일 경우에는 원소들은 별개로 deep copy를 해줘야함.
            System.arraycopy(array, 0, cloneHeap.array, 0, size + 1);
            return cloneHeap;

        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    public boolean contains(Object value) {
        for(int i = 1; i <= size; i++) {
            if(array[i].equals(value)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        for(int i = 0; i < array.length; i++) {
            array[i] = null;
        }
        size = 0;
    }

}
