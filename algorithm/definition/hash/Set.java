package definition.hash;

/**
 * 자바 Set Interface이다.
 * Set은 HashSet, LinkedHashSet, TreeSet에 의해 구현된다.
 */
public interface Set<E> {
    /**
     * 지정된 요소가 Set에 없는 경우 요소를 추가한다.
     * @param e
     * @return
     */
    boolean add(E e);

    /**
     * 지정된 요소가 Set에 있는 경우 해당 요소를 삭제한다.
     * @param o
     * @return
     */
    boolean remove(Object o);

    /**
     * 현재 집합에 특정 요소가 포함되어 있는지 여부를 반환한다.
     * @param o
     * @return
     */
    boolean contains(Object o);

    /**
     * 지정된 객체가 현재 집합과 같은지 여부를 반환
     * @param o
     * @return
     */
    boolean equals(Object o);
}
