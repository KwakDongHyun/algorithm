package definition.hash;

import java.util.Arrays;

public class HashSet<E> implements Set<E> {

    // 최소 기본 배열 크기. 2^n 형태가 좋다고 한다.
    private final static int DEFAULT_CAPACITY = 1 << 4;

    // 3/4 이상 채워질 경우 resize하기 위한 계수
    /*
     * 테이블의 크기에 비해 데이터 양이 일정 이상 많아지면
     * 성능이 저하되기 때문에 75%가 넘으면 배열의 크기를 새로 조정해서
     * 생성하기 위한 기준값. (해시 충돌을 방지하는 체인이 너무 길어질 시)
     */
    private final static float LOAD_FACTOR = 0.75f;

    Node<E>[] table;    // 정보를 담고있는 Node를 저장할 Node타입 배열
    private int size;   // 원소의 개수수

    public HashSet() {
        table = (Node<E>[]) new Node[DEFAULT_CAPACITY];
        size = 0;
    }

    // 보조 해시 함수 (상속 방지를 위해 private static final 선언)
    /*
     *  원래는 최대한 해시충돌을 피하기 위해 보조해시함수를 써서 고른 분포를 할 수 있도록
     *  하기 위한 함수가 바로 위와같은 보조 해시 함수다.
     *  요즘에는 hash 함수도 고른 분포를 갖을  수 있게 설계되어있기 때문에 큰 역할을 하지는 못한다.
     */
    private static final int hash(Object key) {
        int hash;
        if (key == null) {
            return 0;
        } else {
            // hashCode()의 경우 음수가 나올 수 있으므로 절대값으로 변환해준다.
            return Math.abs(hash = key.hashCode()) ^ (hash >>> 16);
        }
    }

    /*
     * 지정된 요소가 Set에 없는 경우 요소를 추가한다.
     * @param e
     * @return
     */
    public boolean add(E e) {
        // key(e)에 대해 만들어두었던 보조해시함수의 값과 key(데이터 e)를 보낸다.
        return add(hash(e), e) == null;
    }

    /*
    int idx = hash % table.length;
    int idx = hash & (table.legnth - 1);

    2의 n승 꼴은 비트로 표현하면 다음과 같다.

    0000..001000...00(2)  <-- n번째 자리에 1이 됨을 의미

    위 식에서 1을 빼면 n-1번째 및 하위의 비트는 모두 1이 됨
    0000..000111...11(2)

    hash & 0000..000111...11(2)
    이는 즉, 2^n승으로 떨어지는 값 및 그 상위 비트 부분은 제외된, hash와 AND연산이 되므로
    이는 곧 hash에 2^n을 나눈 나머지 값이 됨.

    단, 용적(table.length)이 2^n 꼴일 때 가능한 것이므로
    만약 용적을 사용자의 입력에 따라 설정해주고자 하는경우에는 위와 같이 할 수 없고
    hash % table.length로 해야한다는 점 유의하길 바란다.
    */
    public E add(int hash, E key) {
        int idx = hash % table.length;

        // table[idx]가 비어있을 경우 새 노드 생성
        if(table[idx] == null) {
            table[idx] = new Node<E>(hash, key, null);
        }

        /*
         * table[idx]에 요소가 이미 존재할 경우(== 해시충돌)
         *
         * 두 가지 경우의 수가 존재
         * 1. 객체가 같은 경우
         * 2. 객체는 같지 않으나 얻어진 index가 같은 경우
        */
        else {

            Node<E> temp = table[idx];  // 현재 위치 노드
            Node<E> prev = null;        // temp의 이전 노드

            // 첫 노드(table[idx])부터 탐색한다.
            while(temp != null) {

                /*
                 * 만약 현재 노드의 객체가 같은경우(hash값이 같으면서 key가 같을 경우)는
                 * HashSet은 중복을 혀용하지 않으므로 key를 반환
                 * key가 같은 경우는 주소가 같거나 객체가 같은 경우가 존재
                 */
                if((temp.hash == hash) && (temp.key == key || temp.key.equals(key))) {
                    return key;
                }
                prev = temp;
                temp = temp.next;
            }

            // 마지막 노드에 새 노드를 연결해준다.
            prev.next = new Node<E>(hash, key, null);
        }

        size++;

        // 데이터의 개수가 현재 table 용적의 75%를 넘어가는 경우 용적을 늘려준다.
        if(size >= LOAD_FACTOR * table.length) {
            resize();
        }

        return null;    // 정상적으로 추가되었을 경우 null 반환환

    }

    @SuppressWarnings("unchecked")
    private void resize() {

        int newCapacity = table.length * 2;

        // 기존 테이블의 두배 용적으로 생성
        final Node<E>[] newTable = (Node<E>[]) new Node[newCapacity];

        // 0번째 index부터 차례대로 순회
        for(int i = 0; i < table.length; i++) {

            // 각 인덱스의 첫번째 노드(head)
            Node<E> value = table[i];

            // 해당 값이 없을 경우 다음으로 넘어간다.
            if(value == null) {
                continue;
            }

            table[i] = null;    // gc

            Node<E> nextNode;   // 현재 노드의 다음 노드를 가리키는 변수

            // 현재 인덱스에 연결된 노드들을 순회한다.
            while(value != null) {

                int idx = value.hash % newCapacity; // 새로운 인덱스를 구한다.

                /*
                 * 새로 담을 index에 노드가 존재할 경우
                 * == 새로 담을 newTable에 index값이 겹칠 경우 (해시 충돌)
                 */
                if(newTable[idx] != null) {
                    Node<E> tail = newTable[idx];

                    // 가장 마지막 노드로 간다.
                    while(tail.next != null) {
                        tail = tail.next;
                    }

                    /*
                     * 반드시 value가 참조하고 있는 다음 노드와의 연결을 끊어주어야 한다.
                     * 안하면 각 인덱스의 마지막 노드(tail)도 다른 노드를 참조하게 되어버려
                     * 잘못된 참조가 발생할 수 있다.
                     */
                    nextNode = value.next;
                    value.next = null;
                    tail.next = value;

                }
                // 충돌되지 않는다면(=빈 공간이라면 해당 노드를 추가)
                else {

                    /*
                     * 반드시 value가 참조하고 있는 다음 노드와의 연결을 끊어주어야 한다.
                     * 안하면 각 인덱스의 마지막 노드(tail)도 다른 노드를 참조하게 되어버려
                     * 잘못된 참조가 발생할 수 있다.
                     */
                    nextNode = value.next;
                    value.next = null;
                    newTable[idx] = value;
                }

                value = nextNode;   // 다음 노드로 이동
            }
        }

        table = null;       // gc
        table = newTable;   // 새로 생성한 table을 table 변수에 연결

    }

    /**
     * 지정된 요소가 Set에 있는 경우 해당 요소를 삭제한다.
     * @param o
     * @return
     */
    public boolean remove(Object o) {
        // null이 아니라는 것은 노드가 삭제되었다는 의미다.
        // 즉, 삭제할 노드가 없을 경우 null을 반환함으로써 false를 반환하고,
        // 삭제할 노드가 있을 경우 null이 아니기 때문에 true를 반환하는 원리다.
        return remove(hash(o), o) != null;
    }

    private Object remove(int hash, Object key) {

        int idx = hash % table.length;

        Node<E> node = table[idx];
        Node<E> removedNode = null;
        Node<E> prev = null;

        if(node == null) {
            return null;
        }

        while(node != null) {
            // 같은 노드를 찾았다면
            if(node.hash == hash && (node.key == key || node.key.equals(key))) {
                removedNode = node; // 삭제되는 노드를 반환하기 위해 담아둔다.

                // 해당노드의 이전 노드가 존재하지 않는 경우 (= head노드인 경우)
                if(prev == null) {
                    table[idx] = node.next;
                    node = null;
                }
                // 그 외엔 이전 노드의 next를 삭제할 노드의 다음노드와 연결해준다.
                else {
                    prev.next = node.next;
                    node = null;
                }

                size--;
                break;  // 삭제되었기 때문에 탐색 종료
            }

            prev = node;
            node = node.next;
        }

        return removedNode;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * 현재 집합에 특정 요소가 포함되어 있는지 여부를 반환한다.
     * @param o
     * @return
     */
    public boolean contains(Object o) {
        int idx = hash(o) % table.length;
        Node<E> temp = table[idx];

        /*
         * 같은 객체 내용이어도 hash값은 다를 수 있다.
         * 하지만, 내용이 같은지를 알아보고 싶을 때 쓰는 메소드이기에
         * hash값은 따로 비교 안해주어도 큰 지장 없다.
         * 단, o가 null인지는 확인해야한다.
         */
        while(temp != null) {
            // 같은 객체를 찾았을 경우 true 리턴
            if(o == temp.key || (o != null && (o.equals(temp.key)))) {
                return true;
            }
            temp = temp.next;
        }

        return false;
    }

    public void clear() {
        if (table != null && size > 0) {
            for (int i = 0; i < table.length; i++) {
                table[i] = null;	// 모든 노드를 삭제한다.
            }
            size = 0;
        }
    }

    /**
     * 지정된 객체가 현재 집합과 같은지 여부를 반환
     * @param o
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object o) {

        // 만약 파라미터 객체가 현재 객체와 동일한 객체라면 true
        if(o == this) {
            return true;
        }
        // 만약 o 객체가 HashSet이 아닌경우 false
        if(!(o instanceof HashSet)) {
            return false;
        }

        HashSet<E> oSet;

        /*
         *  Object로부터 HashSet<E>로 캐스팅 되어야 비교가 가능하기 때문에
         *  만약 캐스팅이 불가능할 경우 ClassCastException 이 발생한다.
         *  이 경우 false를 return 하도록 try-catch문을 사용해준다.
         */
        try {

            // HashSet 타입으로 캐스팅
            oSet = (HashSet<E>) o;
            // 사이즈(요소 개수)가 다르다는 것은 명백히 다른 객체다.
            if(oSet.size() != size) {
                return false;
            }

            for(int i = 0; i < oSet.table.length; i++) {
                Node<E> oTable = oSet.table[i];

                while(oTable != null) {
                    /*
                     * 서로 Capacity가 다를 수 있기 때문에 index에 연결 된 원소를을
                     * 비교하는 것이 아닌 contains로 원소의 존재 여부를 확인해야한다.
                     */
                    if(!contains(oTable)) {
                        return false;
                    }
                    oTable = oTable.next;
                }
            }

        } catch(ClassCastException e) {
            return false;
        }
        // 위 검사가 모두 완료되면 같은 객체임이 증명됨
        return true;

    }

    public Object[] toArray() {

        if (table == null) {
            return null;
        }
        Object[] ret = new Object[size];
        int index = 0;	// 인덱스 변수를 따로 둔다.

        for (int i = 0; i < table.length; i++) {

            Node<E> node = table[i];

            // 해당 인덱스에 연결 된 모든 노드를 하나씩 담는다.
            while (node != null) {
                ret[index] = node.key;
                index++;
                node = node.next;
            }
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public <T> T[] toArray(T[] a) {

        Object[] copy = toArray();	// toArray()통해 먼저 배열을 얻는다.

        // 들어온 배열이 copy된 요소 개수보다 작을경우 size에 맞게 늘려주면서 복사한다.
        if (a.length < size)
            return (T[]) Arrays.copyOf(copy, size, a.getClass());

        // 그 외에는 copy 배열을 a에 0번째부터 채운다.
        System.arraycopy(copy, 0, a, 0, size);

        return a;
    }

}
