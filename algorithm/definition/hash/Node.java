package definition.hash;

public class Node<E> {

    /*
     * hash와 key값은 불변이므로 final
     */
    final int hash;
    final E key;
    Node<E> next;

    public Node(int hash, E key, Node<E> next) {
        this.hash = hash;
        this.key = key;
        this.next = next;
    }

}
