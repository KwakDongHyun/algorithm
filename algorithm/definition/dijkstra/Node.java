package definition.dijkstra;

public class Node {

    private int vertex;
    private int weight;

    public Node(int vertex, int weight) {
        this.vertex = vertex;
        this.weight = weight;
    }

}
