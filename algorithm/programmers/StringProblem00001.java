package programmers;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class StringProblem00001 {

    public static void main(String[] args) {

        String case1 = "one4seveneight";
        String case2 = "23four5six7";
        String case3 = "1zerotwozero3";

        Map<String, String> numberMap = new LinkedHashMap<String, String>();
        numberMap.put("zero", "0");
        numberMap.put("one", "1");
        numberMap.put("two", "2");
        numberMap.put("three", "3");
        numberMap.put("four", "4");
        numberMap.put("five", "5");
        numberMap.put("six", "6");
        numberMap.put("seven", "7");
        numberMap.put("eight", "8");
        numberMap.put("nine", "9");

        String temp = case1;

        System.out.println("case1");
        for(String key : numberMap.keySet()) {
            temp = temp.replaceAll(key, numberMap.get(key));
            System.out.println("=======================");
            System.out.println(temp);
        }



    }

}
