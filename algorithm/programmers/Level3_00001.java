package programmers;

public class Level3_00001 {


    public static void main(String[] args) {
        int[][] triangle = {{7}, {3, 8}, {8, 1, 0}, {2, 7, 4, 4}, {4, 5, 2, 6, 5}};
        int answer = 0;

        int level = triangle.length;
        int rowLength;
        int[][] dp = triangle.clone();

        for(int i = 0; i < level; i++) {
            for(int j = 0; j <= i; j++) {
                if(i < 1) {
                    dp[i][j] = triangle[i][j];
                } else { // 2레벨 부터
                    if(j == 0) {
                        dp[i][0] = dp[i-1][0] + triangle[i][0];
                    } else if(i == j) {
                        dp[i][j] = dp[i-1][j-1] + triangle[i][j];
                    } else {
                        dp[i][j] = Math.max(dp[i-1][j], dp[i-1][j-1]) + triangle[i][j];
                    }
                }
            }
        }

        answer = dp[level-1][0];
        for(int i = 1; i < level; i++) {
            answer = Math.max(answer, dp[level-1][i]);
        }

        System.out.println(answer);

    }
}
